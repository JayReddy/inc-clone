<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Incognito Exchange: The first privacy-protecting exchange</title>
    <link>https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66</link>
    <description>The first generation of exchanges includes centralized exchanges like Binance and Coinbase. The second generation of exchanges comprises of decentralized exchanges (DEX) like Bancor, Kyber, and Uniswap. pDEX is an upgraded DEX. Like decentralized exchanges, it is trustless. It also implements additional features such as privacy via zero-knowledge proofs, high throughput via sharding, and inter-blockchain trading via interoperable bridges.

||**DEX**|**pDEX**|
| --- | --- | --- |
|Trustless|YES|YES|
|Privacy|NO|YES|
|High throughput|NO|YES|
|Low latency|NO|YES|
|Inter-blockchain|NO|YES|

## **Trustless**

pDEX is code ([beaconpdeproducer.go](https://github.com/incognitochain/incognito-chain/blob/production/blockchain/beaconpdeproducer.go), [beaconpdeprocess.go](https://github.com/incognitochain/incognito-chain/blob/production/blockchain/beaconpdeprocess.go)) that is deployed to thousands of Nodes powering the Incognito network. It runs entirely on-chain and is completely decentralized.

The Automated Market Making mechanism of pDEX is somewhat different from typical exchanges. This post offers a high-level overview of the mechanism and argues for its major advantages. pDEX borrows heavily from Nick Johnson’s [reddit post](https://www.reddit.com/r/ethereum/comments/54l32y/euler_the_simplest_exchange_and_currency/) in 2016, Vitalik Buterin’s [reddit post](https://www.reddit.com/r/ethereum/comments/55m04x/lets_run_onchain_decentralized_exchanges_the_way/) in 2016, Hayden Adam’s [Uniswap implementation](https://github.com/Uniswap/contracts-vyper/blob/master/contracts/uniswap_exchange.vy) in 2018.

pDEX does not use an order book. Instead, it implements an Automated Market Making algorithm that enables instant matches, no matter how large the order or how small the liquidity pool is.

### **Trading on pDEX**

The traditional order book is replaced by a bonding curve mechanism known as constant product. On a typical exchange such as Coinbase or Binance, market makers supply liquidity at various price points. pDEX takes everyone’s bids and asks, and pools them into two giant buckets. Market makers no longer specify prices at which they are willing to buy or sell; pDEX automatically creates markets based on an [Automated Market Making algorithm](https://github.com/runtimeverification/verified-smart-contracts/blob/uniswap/uniswap/x-y-k.pdf).

Here is a simple example of how trading plays out on pDEX. Let’s say there is 100,000 DAI and 10 BTC currently in the pDAI/pBTC pool. The goal is to keep this product equal to 1,000,000 at any trading volume.

&gt; DAI pool   : 100,000
&gt;
&gt;
&gt;
&gt; BTC pool   : 10
&gt;
&gt;
&gt;
&gt; Constant    : 10 * 100,000 = 1,000,000

A buyer sends 10,000 DAI to buy BTC. A fee (in this example, 0.1% or 10 DAI) is taken out for liquidity providers, and the remaining 9,990 is added to the DAI pool. The constant product is divided by the new amount of DAI in the liquidity pool to determine the new BTC pool. The buyer receives the remaining BTC.

&gt; Buyer sends : 10,000 DAI
&gt;
&gt;
&gt;
&gt;
&gt;
&gt;
&gt; DAI pool   : 100,000 + 10,000 = 110,000
&gt;
&gt;
&gt;
&gt; BTC pool   : 1,000,000 / 110,000 = 9.090909
&gt;
&gt;
&gt;
&gt; Buyer gets  : 10 – 9.090909 = 0.90909 BTC

The price has now shifted. A buyer who makes a trade in the same direction will get a slightly worse BTC/DAI rate. A buyer who makes a trade in the opposite direction will get a slightly better DAI/BTC rate.

### **Becoming a liquidity provider**

Liquidity providers play an essential role. As the name suggests, they provide liquidity to various pools on pDEX and earn rewards. [More info here.](https://we.incognito.org/t/provide/4247/85)

### **Inter-Blockchain Liquidity**

pDEX leverages Incognito’s bridges to other blockchains like Ethereum and Bitcoin to enable inter-blockchain trading. So you can trade BTC and ERC20 tokens on pDEX – with complete privacy.

When the pDEX launched at the end of 2019, the [Incognito &lt;&gt; Ethereum Bridge](https://github.com/incognitochain/bridge-eth) was the only trustless bridge on the Mainnet. With the recent launch of [Portal](https://we.incognito.org/t/the-portal/3800), other bridges are now migrating to a trustless implementation.

|**Bridges**|**Current Implementation**|
| --- | --- |
|Ethereum &lt;&gt; Incognito|[Trustless](https://we.incognito.org/t/incognito-mode-for-ethereum/53)|
|Bitcoin &lt;&gt; Incognito|[Semi-trustless](https://we.incognito.org/t/the-portal/3800) 
|Binance &lt;&gt; Incognito|[Semi-trustless](https://we.incognito.org/t/the-portal/3800) 
|Polkadot &lt;&gt; Incognito|([TBD](https://we.incognito.org/t/trustless-bridges-to-polkadot-and-kusama/6052))|

## **Privacy**

All transfers are private by default on the Incognito network. The user can freely trade without disclosing their identity. [Read how Incognito achieves privacy.](https://we.incognito.org/t/sending-cryptocurrencies-confidentially-ring-signature-homomorphic-commitment-and-zero-knowledge-range-proofs/170)

## **High Throughput**

pDEX leverages Incognito’s sharded architecture to deliver high throughput. The workload is shared among shards and trades are processed in parallel. Incognito’s throughput scales linearly by adding more shards, so it can grow to support large transaction volumes. [Read about privacy at scale.](https://we.incognito.org/t/scaling-blockchain-privacy-with-dynamic-sharding/169)

## **Low Latency**

One of the benefits of Automated Market Making is **instant matching** . If you enter an order, it will match right away. The current block time of Incognito is 40s.

Once the block is produced on a shard, it is sent to the beacon chain, which takes another 40s. A trade will complete within 80s. Incognito plans to reduce the block time to 10s, once transaction size is further optimized.

## **Conclusion**

Along with trustlessness, pDEX combines several advanced technologies like zero-knowledge proofs for privacy, sharding for high throughput, and interoperable bridges for inter-blockchain trading. We hope that pDEX will be a helpful product for the crypto community.</description>
    
    <lastBuildDate>Sun, 30 Aug 2020 10:31:47 +0000</lastBuildDate>
    <category>Product</category>
    <atom:link href="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Incognito Exchange: The first privacy-protecting exchange</title>
        <dc:creator><![CDATA[@andrey A ]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/andrey">@andrey</a> wrote:</p>
          <blockquote>
              <p>Hi <a class="mention" href="/u/culgin">@culgin</a> there are several difference on how  <strong>Provide</strong> &amp; <strong>Add</strong> works. Here are high-level diagrams. Hopefully, it helps you to see the bigger picture.</p>
<h2>Provide</h2>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/4/4375e444f414a7d32ff311c69f81de405b33dc36.png" alt="Incognito-Povide" data-base62-sha1="9CMztONbr357u7FS8rChv8glNZk" width="574" height="481"></p>
<h2>Add</h2>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/c/cb9999cf6944e4c87391ab781a02ec679df41e0f.png" alt="Incognito-Page-6" data-base62-sha1="t380UFzNtOL5TJi8mf1IrdivmyP" width="145" height="337"></p>
<p>You can see that “Provide” has additional functionality while “Add” interacts directly with the network.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/6</link>
        <pubDate>Sun, 30 Aug 2020 09:15:06 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-66-6</guid>
        <source url="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66.rss">Incognito Exchange: The first privacy-protecting exchange</source>
      </item>
      <item>
        <title>Incognito Exchange: The first privacy-protecting exchange</title>
        <dc:creator><![CDATA[@Chucky]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/chucky">@Chucky</a> wrote:</p>
          <blockquote>
              <p>Unfortunately, i believe there isnt documentation <em>yet</em> But you can always try the seach bar on the forum and type “Pool” for some more info.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/5</link>
        <pubDate>Mon, 10 Aug 2020 15:11:18 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-66-5</guid>
        <source url="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66.rss">Incognito Exchange: The first privacy-protecting exchange</source>
      </item>
      <item>
        <title>Incognito Exchange: The first privacy-protecting exchange</title>
        <dc:creator><![CDATA[@culgin Culgin]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/culgin">@culgin</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="Chucky" data-post="3" data-topic="66">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/chucky/40/6198_2.png" class="avatar"> Chucky:</div>
<blockquote>
<p>an still add liquidity to a specific po</p>
</blockquote>
</aside>
<p>Thanks! Now I understand how the “Provide” function and “Add Liquidity” differ. Apart from the link you shared, is there any other article which can help me understand how the “Pool” really works under the hood?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/4</link>
        <pubDate>Mon, 10 Aug 2020 15:08:55 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-66-4</guid>
        <source url="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66.rss">Incognito Exchange: The first privacy-protecting exchange</source>
      </item>
      <item>
        <title>Incognito Exchange: The first privacy-protecting exchange</title>
        <dc:creator><![CDATA[@Chucky]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/chucky">@Chucky</a> wrote:</p>
          <blockquote>
              <p>Hey, this was for our first instance of liquidity providing, which at times caused impermanent loss.</p>
<p>You can still add liquidity to a specific pool by providing both sides of the trade as liquidity. This is now called <code>Add</code> on the incognito app.</p>
<p>Provide is our newest mechanism, which was built to make it simpler for users to provide liquidity as well as to  remove that impermanent loss factor. For provide, this is the best place to start: <a href="https://we.incognito.org/t/pool/4247" class="inline-onebox">Pool</a></p>
<p>-Hope that helps!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/3</link>
        <pubDate>Mon, 10 Aug 2020 14:54:53 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-66-3</guid>
        <source url="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66.rss">Incognito Exchange: The first privacy-protecting exchange</source>
      </item>
      <item>
        <title>Incognito Exchange: The first privacy-protecting exchange</title>
        <dc:creator><![CDATA[@culgin Culgin]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/culgin">@culgin</a> wrote:</p>
          <blockquote>
              <p>When I provide PRV liquidity to pDex, it didn’t ask me which pool I am adding liquidity to. Does it mean that the PRV liquidity is shared across various pairs?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/2</link>
        <pubDate>Mon, 10 Aug 2020 14:40:08 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-66-2</guid>
        <source url="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66.rss">Incognito Exchange: The first privacy-protecting exchange</source>
      </item>
      <item>
        <title>Incognito Exchange: The first privacy-protecting exchange</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <p>The first generation of exchanges includes centralized exchanges like Binance and Coinbase. The second generation of exchanges comprises of decentralized exchanges (DEX) like Bancor, Kyber, and Uniswap. pDEX is an upgraded DEX. Like decentralized exchanges, it is trustless. It also implements additional features such as privacy via zero-knowledge proofs, high throughput via sharding, and inter-blockchain trading via interoperable bridges.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th></th>
<th><strong>DEX</strong></th>
<th><strong>pDEX</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Trustless</td>
<td>YES</td>
<td>YES</td>
</tr>
<tr>
<td>Privacy</td>
<td>NO</td>
<td>YES</td>
</tr>
<tr>
<td>High throughput</td>
<td>NO</td>
<td>YES</td>
</tr>
<tr>
<td>Low latency</td>
<td>NO</td>
<td>YES</td>
</tr>
<tr>
<td>Inter-blockchain</td>
<td>NO</td>
<td>YES</td>
</tr>
</tbody>
</table>
</div><h2><strong>Trustless</strong></h2>
<p>pDEX is code (<a href="https://github.com/incognitochain/incognito-chain/blob/production/blockchain/beaconpdeproducer.go">beaconpdeproducer.go</a>, <a href="https://github.com/incognitochain/incognito-chain/blob/production/blockchain/beaconpdeprocess.go">beaconpdeprocess.go</a>) that is deployed to thousands of Nodes powering the Incognito network. It runs entirely on-chain and is completely decentralized.</p>
<p>The Automated Market Making mechanism of pDEX is somewhat different from typical exchanges. This post offers a high-level overview of the mechanism and argues for its major advantages. pDEX borrows heavily from Nick Johnson’s <a href="https://www.reddit.com/r/ethereum/comments/54l32y/euler_the_simplest_exchange_and_currency/">reddit post</a> in 2016, Vitalik Buterin’s <a href="https://www.reddit.com/r/ethereum/comments/55m04x/lets_run_onchain_decentralized_exchanges_the_way/">reddit post</a> in 2016, Hayden Adam’s <a href="https://github.com/Uniswap/contracts-vyper/blob/master/contracts/uniswap_exchange.vy">Uniswap implementation</a> in 2018.</p>
<p>pDEX does not use an order book. Instead, it implements an Automated Market Making algorithm that enables instant matches, no matter how large the order or how small the liquidity pool is.</p>
<h3><strong>Trading on pDEX</strong></h3>
<p>The traditional order book is replaced by a bonding curve mechanism known as constant product. On a typical exchange such as Coinbase or Binance, market makers supply liquidity at various price points. pDEX takes everyone’s bids and asks, and pools them into two giant buckets. Market makers no longer specify prices at which they are willing to buy or sell; pDEX automatically creates markets based on an <a href="https://github.com/runtimeverification/verified-smart-contracts/blob/uniswap/uniswap/x-y-k.pdf">Automated Market Making algorithm</a>.</p>
<p>Here is a simple example of how trading plays out on pDEX. Let’s say there is 100,000 DAI and 10 BTC currently in the pDAI/pBTC pool. The goal is to keep this product equal to 1,000,000 at any trading volume.</p>
<blockquote>
<p>DAI pool   : 100,000</p>
<p>BTC pool   : 10</p>
<p>Constant    : 10 * 100,000 = 1,000,000</p>
</blockquote>
<p>A buyer sends 10,000 DAI to buy BTC. A fee (in this example, 0.1% or 10 DAI) is taken out for liquidity providers, and the remaining 9,990 is added to the DAI pool. The constant product is divided by the new amount of DAI in the liquidity pool to determine the new BTC pool. The buyer receives the remaining BTC.</p>
<blockquote>
<p>Buyer sends : 10,000 DAI</p>
<p>DAI pool   : 100,000 + 10,000 = 110,000</p>
<p>BTC pool   : 1,000,000 / 110,000 = 9.090909</p>
<p>Buyer gets  : 10 – 9.090909 = 0.90909 BTC</p>
</blockquote>
<p>The price has now shifted. A buyer who makes a trade in the same direction will get a slightly worse BTC/DAI rate. A buyer who makes a trade in the opposite direction will get a slightly better DAI/BTC rate.</p>
<h3><strong>Becoming a liquidity provider</strong></h3>
<p>Liquidity providers play an essential role. As the name suggests, they provide liquidity to various pools on pDEX and earn rewards. <a href="https://we.incognito.org/t/provide/4247/85">More info here.</a></p>
<h3><strong>Inter-Blockchain Liquidity</strong></h3>
<p>pDEX leverages Incognito’s bridges to other blockchains like Ethereum and Bitcoin to enable inter-blockchain trading. So you can trade BTC and ERC20 tokens on pDEX – with complete privacy.</p>
<p>When the pDEX launched at the end of 2019, the <a href="https://github.com/incognitochain/bridge-eth">Incognito &lt;&gt; Ethereum Bridge</a> was the only trustless bridge on the Mainnet. With the recent launch of <a href="https://we.incognito.org/t/the-portal/3800">Portal</a>, other bridges are now migrating to a trustless implementation.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th><strong>Bridges</strong></th>
<th><strong>Current Implementation</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Ethereum &lt;&gt; Incognito</td>
<td><a href="https://we.incognito.org/t/incognito-mode-for-ethereum/53">Trustless</a></td>
</tr>
<tr>
<td>Bitcoin &lt;&gt; Incognito</td>
<td><a href="https://we.incognito.org/t/the-portal/3800">Semi-trustless</a></td>
</tr>
<tr>
<td>Binance &lt;&gt; Incognito</td>
<td><a href="https://we.incognito.org/t/the-portal/3800">Semi-trustless</a></td>
</tr>
<tr>
<td>Polkadot &lt;&gt; Incognito</td>
<td>(<a href="https://we.incognito.org/t/trustless-bridges-to-polkadot-and-kusama/6052">TBD</a>)</td>
</tr>
</tbody>
</table>
</div><h2><strong>Privacy</strong></h2>
<p>All transfers are private by default on the Incognito network. The user can freely trade without disclosing their identity. <a href="https://we.incognito.org/t/sending-cryptocurrencies-confidentially-ring-signature-homomorphic-commitment-and-zero-knowledge-range-proofs/170">Read how Incognito achieves privacy.</a></p>
<h2><strong>High Throughput</strong></h2>
<p>pDEX leverages Incognito’s sharded architecture to deliver high throughput. The workload is shared among shards and trades are processed in parallel. Incognito’s throughput scales linearly by adding more shards, so it can grow to support large transaction volumes. <a href="https://we.incognito.org/t/scaling-blockchain-privacy-with-dynamic-sharding/169">Read about privacy at scale.</a></p>
<h2><strong>Low Latency</strong></h2>
<p>One of the benefits of Automated Market Making is <strong>instant matching</strong> . If you enter an order, it will match right away. The current block time of Incognito is 40s.</p>
<p>Once the block is produced on a shard, it is sent to the beacon chain, which takes another 40s. A trade will complete within 80s. Incognito plans to reduce the block time to 10s, once transaction size is further optimized.</p>
<h2><strong>Conclusion</strong></h2>
<p>Along with trustlessness, pDEX combines several advanced technologies like zero-knowledge proofs for privacy, sharding for high throughput, and interoperable bridges for inter-blockchain trading. We hope that pDEX will be a helpful product for the crypto community.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66/1</link>
        <pubDate>Wed, 05 Feb 2020 10:43:53 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-66-1</guid>
        <source url="https://we.incognito.org/t/incognito-exchange-the-first-privacy-protecting-exchange/66.rss">Incognito Exchange: The first privacy-protecting exchange</source>
      </item>
  </channel>
</rss>
