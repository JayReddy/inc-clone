<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Proteger las criptomonedas: convertir cualquier criptomoneda en una moneda de privacidad</title>
    <link>https://we.incognito.org/t/proteger-las-criptomonedas-convertir-cualquier-criptomoneda-en-una-moneda-de-privacidad/19702</link>
    <description>### [Introducción: una plataforma de monedas de privacidad descentralizadas ▸](https://we.incognito.org/t/incognito-whitepaper-incognito-mode-for-cryptonetworks/168)

# Convertir y proteger cualquier criptomoneda, en una moneda de privacidad ▾

El blindaje es el proceso de convertir criptomonedas en otras criptoredes (o &quot;monedas públicas&quot;) en monedas de privacidad en Incognito.

## Monedas de privacidad

A través de Incognito, una moneda pública puede protegerse para obtener su contraparte de moneda privada del mismo valor. Por ejemplo, BTC se puede proteger para obtener la moneda de privacidad pBTC. pBTC tiene el mismo valor que BTC, por lo que 1 pBTC siempre se puede canjear por 1 BTC y viceversa. 1

Una vez protegidas, las transacciones de monedas de privacidad son confidenciales e imposibles de rastrear. Una moneda de privacidad disfruta de lo mejor de ambos mundos. Conserva el valor de su contraparte original y se puede realizar transacciones de forma confidencial en la red de incognito.

|MONEDAS DE PRIVACIDAD|CONTRAPARTIDA|NÚMERO DE TRANSACCIONES|
| --- | --- | --- |
|pBTC|BTC|40,720|
|pUSDT|USDT|13,809|
|pETH|ETH|9,109|

*Tabla 1. Las monedas de privacidad más populares en la red Incognito desde noviembre de 2019 hasta enero de 2020.*

## Blindaje

Hemos basado el mecanismo de blindaje en la experiencia de construir nuestro puente sin confianza de primera generación, entre Incognito y Ethereum [[Incognito, 2018] ](https://ethresear.ch/t/incognito-mode-for-ethereum/6232). En particular, lo generalizamos para permitir que una gama más amplia de redes criptográficas sean interoperables con Incognito.

Las soluciones actuales de interoperabilidad de blockchain implican principalmente la construcción de puentes ad-hoc. BTC Relay [[BTC Relay, 2019] ](http://btcrelay.org/), WBTC [[WBTC, 2019] ](https://www.wbtc.network/assets/wrapped-tokens-whitepaper.pdf)y TBTC [[TBTC, 2019] ](http://docs.keep.network/tbtc/index.pdf)construyen puentes ad hoc entre Bitcoin y Ethereum, mientras que Kyber Network construye Waterloo [[Baneth, 2019] ](https://blog.kyber.network/waterloo-a-decentralized-practical-bridge-between-eos-and-ethereum-1c230ac65524), un puente ad hoc entre Ethereum y EOS. Para Incognito, hacerlo ad hoc (un puente para cada cripto-red) no es una opción escalable.

Incognito adopta un enfoque diferente: construye una vez, trabaja con cualquier cripto-red. El mecanismo de protección opera a través de un diseño de puente general que conecta Incognito a cualquier cantidad de cripto-redes, lo que permite transferencias bidireccionales seguras de criptomonedas siempre que se necesite privacidad. Esto significa que cualquier moneda ahora puede ser una moneda de privacidad. Este enfoque es especialmente útil para crear interoperabilidad con redes criptográficas que no admiten contratos inteligentes, como Bitcoin y Binance Chain.

Para obtener monedas de privacidad, el usuario primero envía una solicitud de protección al contrato inteligente de Bond con información sobre qué monedas públicas quiere proteger y la cantidad. El contrato inteligente de Bond selecciona custodios sin confianza [[Incognito, 2019] ](https://incognito.org/t/trustless-custodians-a-decentralized-approach-to-custodianship/84)para las monedas públicas y proporciona al usuario las direcciones de depósito de los custodios. Una vez que se confirma el depósito en la red criptográfica de las monedas públicas, el usuario inicia una transacción de protección en Incógnito junto con la prueba del depósito. Una prueba de depósito en una red criptográfica suele ser una sucursal de Merkle que vincula la transacción de depósito con el bloque en el que está marcada la hora, lo que demuestra que la transacción de depósito ha sido aceptada por esa red criptográfica.

![](upload://nTkIbqUboTfTNvGCcLtPqrGfYak)

*Figura 1. SPV en Bitcoin [Nakamoto, 2008]. Otras criptoredes emplean métodos SPV similares. Tenga en cuenta que, si bien hemos diseñado un puente bidireccional general entre Incognito y otras redes criptográficas, aún debemos implementar la lógica SPV específica para cada red criptográfica que agregamos al puente, incluida la transmisión de encabezados de bloque de esas redes criptográficas a Incognito y la realización de SPV en depósito. pruebas*

Los validadores de incognito verifican la transacción de blindaje y la prueba de depósito que contiene, en particular mediante la Verificación de pago simplificada [[Nakamoto, 2008] ](https://bitcoin.org/bitcoin.pdf). La mayoría de las cripto-redes admiten la verificación de pago simplificada con algunas pequeñas diferencias en las estructuras de datos subyacentes. Por ejemplo, Bitcoin y Binance implementan Merkle Tree [[Merkle, 1980] ](http://www.merkle.com/papers/Protocols.pdf)mientras que Ethereum implementa un Merkle Patricia Tree modificado [[Wood, 2014] ](https://ethereum.github.io/yellowpaper/paper.pdf).

Una vez que se verifica la prueba de depósito, se acuñan nuevas monedas de privacidad en una proporción de 1:1.

![](upload://xJhhoQMyFXMeyCIGASVo6AtSqLa)

*Figura 2. Protección de BTC y acuñación de pBTC. Otras monedas públicas siguen el mismo proceso de blindaje. Tenga en cuenta que simplificamos el paso 5 para que a los lectores les resulte sencillo seguir la lógica principal: el custodio no genera la prueba de depósito, sino los mineros de la criptored subyacente.*

## Desproteger

La desprotección es el proceso inverso al de protección: convertir las monedas privadas en monedas públicas.

El usuario inicia una transacción de desprotección en Incognito con información sobre las monedas de privacidad que desea desproteger y la cantidad.

Los validadores de incognito verifican la transacción sin blindaje, queman las monedas de privacidad y emiten una prueba de quemado. Una prueba de quemado en Incognito es una prueba criptográfica. Cuando lo firman más de ⅔ de los validadores de Incognito, prueba que las monedas de privacidad se han quemado en la red de Incognito.

Luego, el usuario envía la &quot;released proof&quot; al contrato inteligente de Bond, que verifica la &quot;released proof&quot; e instruye a un custodio para que libere las monedas públicas que respaldan esas monedas de privacidad en una proporción de 1:1.

Una vez que se confirma la liberación en su red criptográfica respectiva, el custodio envía la prueba liberada al contrato inteligente de Bond. De manera similar a la prueba de depósito, una prueba de liberación es una sucursal de Merkle que vincula la transacción de liberación con el bloque en el que está marcada la hora, lo que demuestra que la transacción de liberación ha sido aceptada por esa red criptográfica.

Después de verificar la prueba liberada, el contrato inteligente de bonos libera la garantía del custodio; los custodios pueden retirar su garantía o comenzar a aceptar depósitos de nuevos usuarios.

![](upload://bBUhqysMwXhK8nvYQPMLwLrHEOG)

*Figura 3. Desprotección de pBTC y liberación de BTC. Otras monedas públicas siguen el mismo proceso de desprotección.*

Hemos propuesto un mecanismo para convertir criptomonedas en otras redes criptográficas (o &quot;monedas públicas&quot;) en monedas de privacidad, basado en un conjunto de custodios sin confianza [[Incognito, 2019]](https://we.incognito.org/t/trustless-custodians-a-decentralized-approach-to-custodianship/84) . Una vez protegidas, las transacciones de monedas de privacidad son confidenciales e imposibles de rastrear. Una moneda de privacidad disfruta de lo mejor de ambos mundos. Conserva el valor de su contraparte original y se puede realizar transacciones de forma confidencial en la red de incógnito.

1 Se aborda una excepción en la sección Auto-Liquidation en el
documento Trustless custodios [[Incognito, 2019]](https://we.incognito.org/t/trustless-custodians-a-decentralized-approach-to-custodianship/84) .</description>
    
    <lastBuildDate>Tue, 10 Jan 2023 13:03:45 +0000</lastBuildDate>
    <category>Español</category>
    <atom:link href="https://we.incognito.org/t/proteger-las-criptomonedas-convertir-cualquier-criptomoneda-en-una-moneda-de-privacidad/19702.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Proteger las criptomonedas: convertir cualquier criptomoneda en una moneda de privacidad</title>
        <dc:creator><![CDATA[@NoaLatam Noa]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/noalatam">@NoaLatam</a> wrote:</p>
          <blockquote>
              <h3><a href="https://we.incognito.org/t/incognito-whitepaper-incognito-mode-for-cryptonetworks/168">Introducción: una plataforma de monedas de privacidad descentralizadas ▸</a></h3>
<h1>Convertir y proteger cualquier criptomoneda, en una moneda de privacidad ▾</h1>
<p>El blindaje es el proceso de convertir criptomonedas en otras criptoredes (o “monedas públicas”) en monedas de privacidad en Incognito.</p>
<h2>Monedas de privacidad</h2>
<p>A través de Incognito, una moneda pública puede protegerse para obtener su contraparte de moneda privada del mismo valor. Por ejemplo, BTC se puede proteger para obtener la moneda de privacidad pBTC. pBTC tiene el mismo valor que BTC, por lo que 1 pBTC siempre se puede canjear por 1 BTC y viceversa. 1</p>
<p>Una vez protegidas, las transacciones de monedas de privacidad son confidenciales e imposibles de rastrear. Una moneda de privacidad disfruta de lo mejor de ambos mundos. Conserva el valor de su contraparte original y se puede realizar transacciones de forma confidencial en la red de incognito.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th>MONEDAS DE PRIVACIDAD</th>
<th>CONTRAPARTIDA</th>
<th>NÚMERO DE TRANSACCIONES</th>
</tr>
</thead>
<tbody>
<tr>
<td>pBTC</td>
<td>BTC</td>
<td>40,720</td>
</tr>
<tr>
<td>pUSDT</td>
<td>USDT</td>
<td>13,809</td>
</tr>
<tr>
<td>pETH</td>
<td>ETH</td>
<td>9,109</td>
</tr>
</tbody>
</table>
</div><p><em>Tabla 1. Las monedas de privacidad más populares en la red Incognito desde noviembre de 2019 hasta enero de 2020.</em></p>
<h2>Blindaje</h2>
<p>Hemos basado el mecanismo de blindaje en la experiencia de construir nuestro puente sin confianza de primera generación, entre Incognito y Ethereum <a href="https://ethresear.ch/t/incognito-mode-for-ethereum/6232" rel="nofollow noopener">[Incognito, 2018] </a>. En particular, lo generalizamos para permitir que una gama más amplia de redes criptográficas sean interoperables con Incognito.</p>
<p>Las soluciones actuales de interoperabilidad de blockchain implican principalmente la construcción de puentes ad-hoc. BTC Relay <a href="http://btcrelay.org/" rel="nofollow noopener">[BTC Relay, 2019] </a>, WBTC <a href="https://www.wbtc.network/assets/wrapped-tokens-whitepaper.pdf" rel="nofollow noopener">[WBTC, 2019] </a>y TBTC <a href="http://docs.keep.network/tbtc/index.pdf" rel="nofollow noopener">[TBTC, 2019] </a>construyen puentes ad hoc entre Bitcoin y Ethereum, mientras que Kyber Network construye Waterloo <a href="https://blog.kyber.network/waterloo-a-decentralized-practical-bridge-between-eos-and-ethereum-1c230ac65524" rel="nofollow noopener">[Baneth, 2019] </a>, un puente ad hoc entre Ethereum y EOS. Para Incognito, hacerlo ad hoc (un puente para cada cripto-red) no es una opción escalable.</p>
<p>Incognito adopta un enfoque diferente: construye una vez, trabaja con cualquier cripto-red. El mecanismo de protección opera a través de un diseño de puente general que conecta Incognito a cualquier cantidad de cripto-redes, lo que permite transferencias bidireccionales seguras de criptomonedas siempre que se necesite privacidad. Esto significa que cualquier moneda ahora puede ser una moneda de privacidad. Este enfoque es especialmente útil para crear interoperabilidad con redes criptográficas que no admiten contratos inteligentes, como Bitcoin y Binance Chain.</p>
<p>Para obtener monedas de privacidad, el usuario primero envía una solicitud de protección al contrato inteligente de Bond con información sobre qué monedas públicas quiere proteger y la cantidad. El contrato inteligente de Bond selecciona custodios sin confianza <a href="https://incognito.org/t/trustless-custodians-a-decentralized-approach-to-custodianship/84" rel="nofollow noopener">[Incognito, 2019] </a>para las monedas públicas y proporciona al usuario las direcciones de depósito de los custodios. Una vez que se confirma el depósito en la red criptográfica de las monedas públicas, el usuario inicia una transacción de protección en Incógnito junto con la prueba del depósito. Una prueba de depósito en una red criptográfica suele ser una sucursal de Merkle que vincula la transacción de depósito con el bloque en el que está marcada la hora, lo que demuestra que la transacción de depósito ha sido aceptada por esa red criptográfica.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="/uploads/default/original/1X/a7734658484053686e0de5571c0de01270e23b38.png" data-download-href="/uploads/short-url/nTkIbqUboTfTNvGCcLtPqrGfYak.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/a7734658484053686e0de5571c0de01270e23b38_2_690x297.png" alt data-base62-sha1="nTkIbqUboTfTNvGCcLtPqrGfYak" width="690" height="297" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/a7734658484053686e0de5571c0de01270e23b38_2_690x297.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/a7734658484053686e0de5571c0de01270e23b38_2_1035x445.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/a7734658484053686e0de5571c0de01270e23b38_2_1380x594.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/a7734658484053686e0de5571c0de01270e23b38_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">2526×1088 166 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><em>Figura 1. SPV en Bitcoin [Nakamoto, 2008]. Otras criptoredes emplean métodos SPV similares. Tenga en cuenta que, si bien hemos diseñado un puente bidireccional general entre Incognito y otras redes criptográficas, aún debemos implementar la lógica SPV específica para cada red criptográfica que agregamos al puente, incluida la transmisión de encabezados de bloque de esas redes criptográficas a Incognito y la realización de SPV en depósito. pruebas</em></p>
<p>Los validadores de incognito verifican la transacción de blindaje y la prueba de depósito que contiene, en particular mediante la Verificación de pago simplificada <a href="https://bitcoin.org/bitcoin.pdf" rel="nofollow noopener">[Nakamoto, 2008] </a>. La mayoría de las cripto-redes admiten la verificación de pago simplificada con algunas pequeñas diferencias en las estructuras de datos subyacentes. Por ejemplo, Bitcoin y Binance implementan Merkle Tree <a href="http://www.merkle.com/papers/Protocols.pdf" rel="nofollow noopener">[Merkle, 1980] </a>mientras que Ethereum implementa un Merkle Patricia Tree modificado <a href="https://ethereum.github.io/yellowpaper/paper.pdf" rel="nofollow noopener">[Wood, 2014] </a>.</p>
<p>Una vez que se verifica la prueba de depósito, se acuñan nuevas monedas de privacidad en una proporción de 1:1.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="/uploads/default/original/1X/ec66079aad81750e9cf666fa2619dcccd181b9f4.jpeg" data-download-href="/uploads/short-url/xJhhoQMyFXMeyCIGASVo6AtSqLa.jpeg?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/ec66079aad81750e9cf666fa2619dcccd181b9f4_2_614x500.jpeg" alt data-base62-sha1="xJhhoQMyFXMeyCIGASVo6AtSqLa" width="614" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/ec66079aad81750e9cf666fa2619dcccd181b9f4_2_614x500.jpeg, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/ec66079aad81750e9cf666fa2619dcccd181b9f4_2_921x750.jpeg 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/ec66079aad81750e9cf666fa2619dcccd181b9f4_2_1228x1000.jpeg 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/ec66079aad81750e9cf666fa2619dcccd181b9f4_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1376×1120 63.9 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><em>Figura 2. Protección de BTC y acuñación de pBTC. Otras monedas públicas siguen el mismo proceso de blindaje. Tenga en cuenta que simplificamos el paso 5 para que a los lectores les resulte sencillo seguir la lógica principal: el custodio no genera la prueba de depósito, sino los mineros de la criptored subyacente.</em></p>
<h2>Desproteger</h2>
<p>La desprotección es el proceso inverso al de protección: convertir las monedas privadas en monedas públicas.</p>
<p>El usuario inicia una transacción de desprotección en Incognito con información sobre las monedas de privacidad que desea desproteger y la cantidad.</p>
<p>Los validadores de incognito verifican la transacción sin blindaje, queman las monedas de privacidad y emiten una prueba de quemado. Una prueba de quemado en Incognito es una prueba criptográfica. Cuando lo firman más de ⅔ de los validadores de Incognito, prueba que las monedas de privacidad se han quemado en la red de Incognito.</p>
<p>Luego, el usuario envía la “released proof” al contrato inteligente de Bond, que verifica la “released proof” e instruye a un custodio para que libere las monedas públicas que respaldan esas monedas de privacidad en una proporción de 1:1.</p>
<p>Una vez que se confirma la liberación en su red criptográfica respectiva, el custodio envía la prueba liberada al contrato inteligente de Bond. De manera similar a la prueba de depósito, una prueba de liberación es una sucursal de Merkle que vincula la transacción de liberación con el bloque en el que está marcada la hora, lo que demuestra que la transacción de liberación ha sido aceptada por esa red criptográfica.</p>
<p>Después de verificar la prueba liberada, el contrato inteligente de bonos libera la garantía del custodio; los custodios pueden retirar su garantía o comenzar a aceptar depósitos de nuevos usuarios.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="/uploads/default/original/1X/5160e63eba5460215bc5741f7a3a31f2d4321ede.jpeg" data-download-href="/uploads/short-url/bBUhqysMwXhK8nvYQPMLwLrHEOG.jpeg?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/5160e63eba5460215bc5741f7a3a31f2d4321ede_2_614x500.jpeg" alt data-base62-sha1="bBUhqysMwXhK8nvYQPMLwLrHEOG" width="614" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/5160e63eba5460215bc5741f7a3a31f2d4321ede_2_614x500.jpeg, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/5160e63eba5460215bc5741f7a3a31f2d4321ede_2_921x750.jpeg 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/5160e63eba5460215bc5741f7a3a31f2d4321ede_2_1228x1000.jpeg 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/5160e63eba5460215bc5741f7a3a31f2d4321ede_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1376×1120 61.1 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><em>Figura 3. Desprotección de pBTC y liberación de BTC. Otras monedas públicas siguen el mismo proceso de desprotección.</em></p>
<p>Hemos propuesto un mecanismo para convertir criptomonedas en otras redes criptográficas (o “monedas públicas”) en monedas de privacidad, basado en un conjunto de custodios sin confianza <a href="https://we.incognito.org/t/trustless-custodians-a-decentralized-approach-to-custodianship/84">[Incognito, 2019]</a> . Una vez protegidas, las transacciones de monedas de privacidad son confidenciales e imposibles de rastrear. Una moneda de privacidad disfruta de lo mejor de ambos mundos. Conserva el valor de su contraparte original y se puede realizar transacciones de forma confidencial en la red de incógnito.</p>
<p>1 Se aborda una excepción en la sección Auto-Liquidation en el<br>
documento Trustless custodios <a href="https://we.incognito.org/t/trustless-custodians-a-decentralized-approach-to-custodianship/84">[Incognito, 2019]</a> .</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/proteger-las-criptomonedas-convertir-cualquier-criptomoneda-en-una-moneda-de-privacidad/19702/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/proteger-las-criptomonedas-convertir-cualquier-criptomoneda-en-una-moneda-de-privacidad/19702/1</link>
        <pubDate>Tue, 10 Jan 2023 13:03:45 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-19702-1</guid>
        <source url="https://we.incognito.org/t/proteger-las-criptomonedas-convertir-cualquier-criptomoneda-en-una-moneda-de-privacidad/19702.rss">Proteger las criptomonedas: convertir cualquier criptomoneda en una moneda de privacidad</source>
      </item>
  </channel>
</rss>
