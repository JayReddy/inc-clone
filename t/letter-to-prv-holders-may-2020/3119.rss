<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Letter to PRV Holders (May 2020)</title>
    <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119</link>
    <description>Hey PRV holders:

Incognito has been on the mainnet for a full 7 months now, and we’re glad to report that growth shows no signs of slowing. Increasingly, Incognito is becoming the default privacy solution for many similarly minded folks.

Many of you follow the day to day here on incognito.org, but here’s a broader look at a very busy May.

[View PRV Holder Presentation May 2020 &gt;
](https://incognito.org/t/prv-holders-presentation-may-2020/3115)[Review development progress &gt;
](https://incognito.org/t/incognito-core-dev-2020-roadmap/552/)[Get PRV &gt;](https://incognito.org/t/how-to-buy-prv-in-3-steps/793)

As usual, let’s start with our 2 key metrics.

## Monthly shield volume: $1.88M

Shielding is when a public coin is deposited into Incognito to be anonymized. **$1.88M of crypto in 92 different currencies** entered the Incognito ecosystem in May. This is perhaps the most significant metric – it indicates that increasingly, people are beginning to choose incognito mode for their crypto.

**Our total shield volume to date is $5.36M.** Here’s a broader look at Incognito’s progress, starting from mainnet launch in Nov 2019.

![image|429x465,93%](upload://q4OHWBp9XuIGNCNqM14vbzPqePx.png) 

## Validator ecosystem:

This month, we aimed to add 400 Nodes to the Incognito network. We surpassed that goal by 26%. A warm welcome to each and every one of the **504 new Incognito Nodes**! As of today, there 1,737 Nodes powering the Incognito network (+30% month-on-month growth).

In addition, many Nodes devices were purchased in the middle of this month. The first batch has been shipped, and the next is scheduled for mid June. When all these Nodes come online, **the total number will be 1853.**

![image|415x497,91%](upload://9S8QFjMEpF1KRPSEPWKXmud1TXp.png) 

## Incognito DEX volume

pDEX is the first and only way to buy and sell crypto privately. The core dev team built pDEX because we wanted everyone to have a safer way to participate in this new economy. pDEX has been growing steadily, so we set a fairly ambitious goal – $600,000 monthly volume in May (+30% month-on-month growth).

It seems, however, that pDEX is growing 200% faster than we hoped. We saw **$1.2M in trading volume in May**, a fresh new interface, and a much more streamlined user experience. In June and in months to come, we’ll continue to improve it.

![image|1179x583,60%](upload://gbSk0MmiiSgMis4pcOFX8agaUKq.png) 

## May highlights

In addition to the metrics above, the team invested significant time and resources in building strong foundations for longer term growth – and so did the community. Here’s a quick look at what’s been built, and what’s in the works.

**Explorer:** @inccry’s Network Explorer is now, for many of us, the project’s home page. Requests for new features are open [here](https://incognito.org/t/network-explorer/2086/60).

**Interoperability:** Incognito mode for smart contracts will launch this week with pKyber. Hello privacy for DeFi! In addition, Portal v2 will ship on the mainnet soon. [85% of testcases ](https://incognito.org/t/quality-control-strategy-to-minimize-oversight-for-q2-2020/2034/9)have been completed.

**Consensus:** Zero bugs so far for BFT 1. Testing will now commence for [BFT 2.
](https://incognito.org/t/a-multi-view-solution-for-pbft-protocol/537)

**Network:** Full speed ahead for the [Dynamic Committee Size](https://incognito.org/t/dynamic-committee-size/1506) and [NAT traversal solution](https://incognito.org/t/nat-traversal-solution-for-incognito/1532) proposals – both of which will make Incognito more flexible, adaptable and resilient.

## June development

In June, the core dev team will focus on version 2s of all foundational products. Starting with the new pDEX that launched in the last week of May, we plan to release an improved app, a smoother Node set up, a more flexible and interoperable network, and of course – better QC and CX to both reduce errors and fix bugs more quickly.

### Generate strong utility

[Increase pDEX liquidity pools by 900K](https://incognito.org/t/increase-liquidity-pools-for-incognito-dex/2966)
[Privacy for DeFi starting with pKyber
](https://incognito.org/t/launch-pkyber-the-first-privacy-solution-for-defi/3147)

### Build a strong layer-2 core

[Produce 500 perfect Node devices
](https://incognito.org/t/produce-500-nodes-version-n-01/3096/)[Release Node Tree prototype
](https://incognito.org/t/node-tree-multi-pnode-set/523/)[Improve incognito app experience
](https://incognito.org/t/app-v-2/3112/2)

### Build a strong layer-1 core

[Improve Node performance and sync times
](https://incognito.org/t/improve-node-performance/2891)[Ship Portal v2 on mainnet
](https://incognito.org/t/building-out-incognito-s-trustless-bridge-to-bitcoin-and-all-other-blockchains/465/23)[Deploy Confidential Assets on devnet
](https://incognito.org/t/introduce-confidential-asset-to-incognito/2325/3)[Integrate Privacy v2 with Portal and pDEX
](https://incognito.org/t/privacy-version-2/2967)

### R&amp;D

[New smart contract interaction flow
](https://incognito.org/t/new-smart-contract-interaction-flow/2964)[Trustless bridge with Ethereum bond contract
](https://incognito.org/t/incognitos-trustless-bridge-v3-with-ethereum-bond-contract/2963)

## PRV Holder Monthly Call

The Incognito Core Team will host a webinar to discuss May results, June goals, and any questions you might have.

Date: June 3, 11am EST
Location: https://us02web.zoom.us/j/86551620328

Join us! Post your questions in advance [here](https://incognito.org/t/join-us-for-the-june-prv-holders-call/2742/).

Have a great June ahead. See you around at [incognito.org](https://incognito.org).

The Incognito Core Team</description>
    
    <lastBuildDate>Tue, 23 Jun 2020 00:13:52 +0000</lastBuildDate>
    <category>Updates</category>
    <atom:link href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Joe_Moffett Moff]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/joe_moffett">@Joe_Moffett</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/ben">@ben</a> sorry i didn’t see this, I can make you one if you’d like just send me a message.</p>
<p>I’ve been MIA for the last couple weeks because work has kept me away from my real passion of incognito <img src="https://incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/16">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/16</link>
        <pubDate>Tue, 23 Jun 2020 00:13:52 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-16</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Ben Patrick]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/ben">@Ben</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="ning" data-post="1" data-topic="3119">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://incognito.org/user_avatar/incognito.org/ning/40/1778_2.png" class="avatar"> ning:</div>
<blockquote>
<p>Improve incognito app experience</p>
</blockquote>
</aside>
<p>anywhere we can buy this shirts ?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/15">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/15</link>
        <pubDate>Thu, 11 Jun 2020 21:38:20 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-15</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Tempestblack Vincent Valencia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tempestblack">@Tempestblack</a> wrote:</p>
          <blockquote>
              <p>To the entire community I wish to apologize for having missed the meeting…was my birthday for starters…but the issue that kept me was rioting and looting issue here in Los Angeles…I am ok but things were hairy there and only started calming down last night…on the bright side my node earned some coin for my birthday!!!..I was so jazzed…thank you incognito…the rewards have already been restaked into the pool…<img src="https://incognito.org/images/emoji/apple/slightly_smiling_face.png?v=9" title=":slightly_smiling_face:" class="emoji" alt=":slightly_smiling_face:">…If one of the admins could reach out to me when one of them has a chance it seems that one transaction of prv I was attempting to stake is stuck in transition and it needs to be straightened out…well hopefully I will be present at next month live meeting…thank you once again to the community…<img src="https://incognito.org/images/emoji/apple/sunglasses.png?v=9" title=":sunglasses:" class="emoji" alt=":sunglasses:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/14">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/14</link>
        <pubDate>Fri, 05 Jun 2020 09:06:02 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-14</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Tempestblack Vincent Valencia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tempestblack">@Tempestblack</a> wrote:</p>
          <blockquote>
              <p>Very cool swag…<img src="https://incognito.org/images/emoji/apple/sunglasses.png?v=9" title=":sunglasses:" class="emoji" alt=":sunglasses:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/13">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/13</link>
        <pubDate>Fri, 05 Jun 2020 08:56:42 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-13</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Gigi_Gao Gigi Gao]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/gigi_gao">@Gigi_Gao</a> wrote:</p>
          <blockquote>
              <p>Good job!!!<br>
Many thanks to all developers, Chinese community members said you all deserve big chicken legs for hard work.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/e/e5541386819fa7b0ad0269710dd911eb7345ef81.png" alt="WechatIMG16" data-base62-sha1="wIJtlpKbqh3RdEgkKNF5CzDwa9r" width="433" height="192"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/12">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/12</link>
        <pubDate>Wed, 03 Jun 2020 13:34:37 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-12</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Gold]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/gold">@Gold</a> wrote:</p>
          <blockquote>
              <p>Cool swag, <a class="mention" href="/u/joe_moffett">@Joe_Moffett</a>!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/11">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/11</link>
        <pubDate>Tue, 02 Jun 2020 03:37:34 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-11</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@andrey A ]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/andrey">@andrey</a> wrote:</p>
          <blockquote>
              <p>Just take a 5 min of the call and ask interesting question about Incognito. And the person who answer first and correct get the prize <img src="https://incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/10">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/10</link>
        <pubDate>Tue, 02 Jun 2020 03:20:55 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-10</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Tryche Shane Aga]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tryche">@Tryche</a> wrote:</p>
          <blockquote>
              <p>perhaps all attendees names/handles/email address, go in a hat so to speak and drawn randomly? No idea how many shirts may be used for give away, but this a pretty simple way to pull it off.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/9">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/9</link>
        <pubDate>Tue, 02 Jun 2020 03:15:01 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-9</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Joe_Moffett Moff]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/joe_moffett">@Joe_Moffett</a> wrote:</p>
          <blockquote>
              <p>Sounds like a good idea to me! Any thoughts of how to do it? Random selection?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/8">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/8</link>
        <pubDate>Tue, 02 Jun 2020 03:02:00 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-8</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@andrey A ]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/andrey">@andrey</a> wrote:</p>
          <blockquote>
              <p>Hey <a class="mention" href="/u/joe_moffett">@Joe_Moffett</a> do you want to make a t-shirt giveaway during the <a href="https://incognito.org/t/join-us-for-the-june-prv-holders-call/2742">PRV holders call</a> ?</p>
<p>Could be the best place to promote your swags <img src="https://incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/7">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/7</link>
        <pubDate>Tue, 02 Jun 2020 02:26:16 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-7</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Joe_Moffett Moff]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/joe_moffett">@Joe_Moffett</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/cool">@cool</a>  if you want one just send me a message!</p>
<p>I have some other examples in other threads, but basically just let me know in a message what kind of design you’d like, shirt size, color, and your address. I’ll go back and forth with different styles if you want anything specific.</p>
<p>If you’re in the states I can do it including shipping for like 20 prv. If outside the states, i’ll have to see the price of shipping.</p>
<p>I can do some other stuff like whiskey glasses, decals, and hats. Just let me know!</p>
<aside class="quote no-group quote-modified" data-username="Joe_Moffett" data-post="1" data-topic="2194">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://incognito.org/user_avatar/incognito.org/joe_moffett/40/3711_2.png" class="avatar"><a href="https://incognito.org/t/making-incognito-swag-and-merch-available/2194/1">Making incognito SWAG and merch available</a>
</div>
<blockquote>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/f/fe171ccb6513fc74b79a91a88fa8ba88a6f55d7f_2_1536x2048.jpeg" alt="IMG_6284 (1)" data-base62-sha1="AfMNkRK5E3UZ1ogf6BkIu6P0h1d" width="1536" height="2048" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/f/fe171ccb6513fc74b79a91a88fa8ba88a6f55d7f_2_1536x2048.jpeg, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/f/fe171ccb6513fc74b79a91a88fa8ba88a6f55d7f_2_2304x3072.jpeg 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/f/fe171ccb6513fc74b79a91a88fa8ba88a6f55d7f.jpeg 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/f/fe171ccb6513fc74b79a91a88fa8ba88a6f55d7f_2_10x10.png"></p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/c/c95f5eb495b7c5462008bfaf0c70cb1a0fb5061b.jpeg" alt="incognito shirt" data-base62-sha1="sJqiDYbutBOmc1ANSx3CZ7OS0Gv" width="380" height="508"></p>
</blockquote>
</aside>
<aside class="quote no-group quote-modified" data-username="Joe_Moffett" data-post="1" data-topic="2194">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://incognito.org/user_avatar/incognito.org/joe_moffett/40/3711_2.png" class="avatar"><a href="https://incognito.org/t/making-incognito-swag-and-merch-available/2194/1">Making incognito SWAG and merch available</a>
</div>
<blockquote>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/e/ec04c27e0d0f47a6b7bbc1a0c6b3abf0ceac9d4c.jpeg" alt="IMG_6171" data-base62-sha1="xFUSBsAoLkefgfJlzbSnHEV40vq" width="2048" height="1800" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/e/ec04c27e0d0f47a6b7bbc1a0c6b3abf0ceac9d4c_2_10x10.png"></p>
</blockquote>
</aside>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/6</link>
        <pubDate>Mon, 01 Jun 2020 21:56:06 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-6</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@cool cool]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/cool">@cool</a> wrote:</p>
          <blockquote>
              <p>How can I buy it? <a class="mention" href="/u/joe_moffett">@Joe_Moffett</a></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/5</link>
        <pubDate>Mon, 01 Jun 2020 21:05:46 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-5</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@jame jame]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jame">@jame</a> wrote:</p>
          <blockquote>
              <p>Cool WORK!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/4</link>
        <pubDate>Mon, 01 Jun 2020 20:43:20 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-4</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@Joe_Moffett Moff]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/joe_moffett">@Joe_Moffett</a> wrote:</p>
          <blockquote>
              <p>Impressive! Great work by everyone, let’s keep it rolling.</p>
<p>Looking forward to the call!</p>
<p>Made another incognito shirt for a fellow enthusiast!</p>
<p>Used a different font than <a class="mention" href="/u/jamie">@Jamie</a> but thanks for the design! Also opposite colors, white circle instead of black, but hey, technically white is the absence of color, so a white circle is even more nothingness haha <img src="https://incognito.org/images/emoji/apple/man_shrugging.png?v=9" title=":man_shrugging:" class="emoji" alt=":man_shrugging:"></p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/a/a852232d0c77a209b32f1283a13d61812eeee24d.jpeg" data-download-href="/uploads/short-url/o12c0gehNYDWZk4gUlL5kuHK0dD.jpeg?dl=1" title="image" rel="nofollow noopener"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/a/a852232d0c77a209b32f1283a13d61812eeee24d_2_500x700.jpeg" alt="image" data-base62-sha1="o12c0gehNYDWZk4gUlL5kuHK0dD" width="500" height="700" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/a/a852232d0c77a209b32f1283a13d61812eeee24d_2_500x700.jpeg, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/a/a852232d0c77a209b32f1283a13d61812eeee24d_2_750x1050.jpeg 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/a/a852232d0c77a209b32f1283a13d61812eeee24d_2_1000x1400.jpeg 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/a/a852232d0c77a209b32f1283a13d61812eeee24d_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">3024×4032 2.58 MB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/3</link>
        <pubDate>Mon, 01 Jun 2020 12:19:46 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-3</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (May 2020)</title>
        <dc:creator><![CDATA[@ning]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/ning">@ning</a> wrote:</p>
          <blockquote>
              <p>Hey PRV holders:</p>
<p>Incognito has been on the mainnet for a full 7 months now, and we’re glad to report that growth shows no signs of slowing. Increasingly, Incognito is becoming the default privacy solution for many similarly minded folks.</p>
<p>Many of you follow the day to day here on <a href="http://incognito.org">incognito.org</a>, but here’s a broader look at a very busy May.</p>
<p><a href="https://incognito.org/t/prv-holders-presentation-may-2020/3115">View PRV Holder Presentation May 2020 &gt;<br>
</a><a href="https://incognito.org/t/incognito-core-dev-2020-roadmap/552/">Review development progress &gt;<br>
</a><a href="https://incognito.org/t/how-to-buy-prv-in-3-steps/793">Get PRV &gt;</a></p>
<p>As usual, let’s start with our 2 key metrics.</p>
<h2>Monthly shield volume: $1.88M</h2>
<p>Shielding is when a public coin is deposited into Incognito to be anonymized. <strong>$1.88M of crypto in 92 different currencies</strong> entered the Incognito ecosystem in May. This is perhaps the most significant metric – it indicates that increasingly, people are beginning to choose incognito mode for their crypto.</p>
<p><strong>Our total shield volume to date is $5.36M.</strong> Here’s a broader look at Incognito’s progress, starting from mainnet launch in Nov 2019.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/b/b6c3f1cb453c2cd79103ea4c04f6c5a64f6e43f3.png" alt="image" data-base62-sha1="q4OHWBp9XuIGNCNqM14vbzPqePx" width="398" height="432"></p>
<h2>Validator ecosystem:</h2>
<p>This month, we aimed to add 400 Nodes to the Incognito network. We surpassed that goal by 26%. A warm welcome to each and every one of the <strong>504 new Incognito Nodes</strong>! As of today, there 1,737 Nodes powering the Incognito network (+30% month-on-month growth).</p>
<p>In addition, many Nodes devices were purchased in the middle of this month. The first batch has been shipped, and the next is scheduled for mid June. When all these Nodes come online, <strong>the total number will be 1853.</strong></p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/4/45325d23a4b128d44997fd82f1feccc5f016f647.png" alt="image" data-base62-sha1="9S8QFjMEpF1KRPSEPWKXmud1TXp" width="377" height="452"></p>
<h2>Incognito DEX volume</h2>
<p>pDEX is the first and only way to buy and sell crypto privately. The core dev team built pDEX because we wanted everyone to have a safer way to participate in this new economy. pDEX has been growing steadily, so we set a fairly ambitious goal – $600,000 monthly volume in May (+30% month-on-month growth).</p>
<p>It seems, however, that pDEX is growing 200% faster than we hoped. We saw <strong>$1.2M in trading volume in May</strong>, a fresh new interface, and a much more streamlined user experience. In June and in months to come, we’ll continue to improve it.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/7/717a75ac9f8e51c1dec1436fe22105a568b31b0e.png" alt="image" data-base62-sha1="gbSk0MmiiSgMis4pcOFX8agaUKq" width="707" height="349"></p>
<h2>May highlights</h2>
<p>In addition to the metrics above, the team invested significant time and resources in building strong foundations for longer term growth – and so did the community. Here’s a quick look at what’s been built, and what’s in the works.</p>
<p><strong>Explorer:</strong> <a class="mention" href="/u/inccry">@inccry</a>’s Network Explorer is now, for many of us, the project’s home page. Requests for new features are open <a href="https://incognito.org/t/network-explorer/2086/60">here</a>.</p>
<p><strong>Interoperability:</strong> Incognito mode for smart contracts will launch this week with pKyber. Hello privacy for DeFi! In addition, Portal v2 will ship on the mainnet soon. <a href="https://incognito.org/t/quality-control-strategy-to-minimize-oversight-for-q2-2020/2034/9">85% of testcases </a>have been completed.</p>
<p><strong>Consensus:</strong> Zero bugs so far for BFT 1. Testing will now commence for <a href="https://incognito.org/t/a-multi-view-solution-for-pbft-protocol/537">BFT 2.<br>
</a></p>
<p><strong>Network:</strong> Full speed ahead for the <a href="https://incognito.org/t/dynamic-committee-size/1506">Dynamic Committee Size</a> and <a href="https://incognito.org/t/nat-traversal-solution-for-incognito/1532">NAT traversal solution</a> proposals – both of which will make Incognito more flexible, adaptable and resilient.</p>
<h2>June development</h2>
<p>In June, the core dev team will focus on version 2s of all foundational products. Starting with the new pDEX that launched in the last week of May, we plan to release an improved app, a smoother Node set up, a more flexible and interoperable network, and of course – better QC and CX to both reduce errors and fix bugs more quickly.</p>
<h3>Generate strong utility</h3>
<p><a href="https://incognito.org/t/increase-liquidity-pools-for-incognito-dex/2966">Increase pDEX liquidity pools by 900K</a><br>
<a href="https://incognito.org/t/launch-pkyber-the-first-privacy-solution-for-defi/3147">Privacy for DeFi starting with pKyber<br>
</a></p>
<h3>Build a strong layer-2 core</h3>
<p><a href="https://incognito.org/t/produce-500-nodes-version-n-01/3096/">Produce 500 perfect Node devices<br>
</a><a href="https://incognito.org/t/node-tree-multi-pnode-set/523/">Release Node Tree prototype<br>
</a><a href="https://incognito.org/t/app-v-2/3112/2">Improve incognito app experience<br>
</a></p>
<h3>Build a strong layer-1 core</h3>
<p><a href="https://incognito.org/t/improve-node-performance/2891">Improve Node performance and sync times<br>
</a><a href="https://incognito.org/t/building-out-incognito-s-trustless-bridge-to-bitcoin-and-all-other-blockchains/465/23">Ship Portal v2 on mainnet<br>
</a><a href="https://incognito.org/t/introduce-confidential-asset-to-incognito/2325/3">Deploy Confidential Assets on devnet<br>
</a><a href="https://incognito.org/t/privacy-version-2/2967">Integrate Privacy v2 with Portal and pDEX<br>
</a></p>
<h3>R&amp;D</h3>
<p><a href="https://incognito.org/t/new-smart-contract-interaction-flow/2964">New smart contract interaction flow<br>
</a><a href="https://incognito.org/t/incognitos-trustless-bridge-v3-with-ethereum-bond-contract/2963">Trustless bridge with Ethereum bond contract<br>
</a></p>
<h2>PRV Holder Monthly Call</h2>
<p>The Incognito Core Team will host a webinar to discuss May results, June goals, and any questions you might have.</p>
<p>Date: June 3, 11am EST<br>
Location: <a href="https://us02web.zoom.us/j/86551620328">https://us02web.zoom.us/j/86551620328</a></p>
<p>Join us! Post your questions in advance <a href="https://incognito.org/t/join-us-for-the-june-prv-holders-call/2742/">here</a>.</p>
<p>Have a great June ahead. See you around at <a href="https://incognito.org">incognito.org</a>.</p>
<p>The Incognito Core Team</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119/1</link>
        <pubDate>Mon, 01 Jun 2020 11:41:01 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-3119-1</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-may-2020/3119.rss">Letter to PRV Holders (May 2020)</source>
      </item>
  </channel>
</rss>
