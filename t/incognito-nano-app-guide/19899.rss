<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Incognito Nano App Guide</title>
    <link>https://we.incognito.org/t/incognito-nano-app-guide/19899</link>
    <description>Hello privacy community! 

We are happy to announce that the Incognito Nano App has been submitted to the Ledger store and is under the review stage. Due to the Ledger store review process can take a long time to complete, you could also side-load our app to your Nano device (**developers only**) to try some features such as: 
- View your hardware wallet account’s balances; 
- Confirm the transfer, swap, unshield transactions with your hardware device. 

Do note that the current version is only under the review stage, your feedback and comments will be highly appreciated. The following is the guide on how to use the Incognito Nano App with the Incognito extension. 

# STEP 1. INSTALL INCOGNITO LEDGER APP

## Install from Ledger Live Store

-   Soon Incognito Nano App will be available on the Ledger Live App Store
    

## Install by sideloading (with Ledgerblue tool)

1.  Download the release app archive ([incognito_nano_app_binaries](https://github.com/incognitochain/ledger/releases/download/v0.5.2-beta/incognito_nano_app_binaries.zip)) from [Github Release](https://github.com/incognitochain/ledger/releases/tag/v0.5.2-beta). Choose your app file (incognito_&lt;nanos/nanosp/nanox&gt;.elf).
    
2.  Follow [this guide](https://developers.ledger.com/docs/embedded-app/load-mac/) for sideloading apps to your Ledger device. Note: Nano X devices do not support sideloading
    

# STEP 2. INSTALL INCOGNITO EXTENSION (GOOGLE CHROME ONLY)

## Install from Chrome Web Store

-   Ledger integration features will hit the Chrome Web Store  [Incognito Wallet](https://chrome.google.com/webstore/detail/incognito-wallet/chngojfpcfnjfnaeddcmngfbbdpcdjaj?hl=vi) soon (est. Mar 2023). In the meantime, you have to install from the .zip file in developer mode
    

## Install from release file

1.  Click [here](https://github.com/incognitochain/incognito-extension-2.0/releases/download/0.2.1-beta/extension-latest.4.zip) to download the beta release version from [Github Release](https://github.com/incognitochain/incognito-extension-2.0/releases/tag/0.2.1-beta)
    
2.  To install the extension to Chrome, in your address bar, enter chrome://extensions/  
    ![](upload://1LSZ5fun8Rq8IAe9UrMbVdMHAUL.png)
    
3.  Turn on Developer Mode toggle at the top right corner  
    ![](upload://sss8LS8E6Zxewv8rNITJEXHaC0p.png)
    
4.  Click Load Unpacked, then choose the downloaded .zip file from before.  
    ![](upload://mPlP5FjkIAiteqau9f5g7NKbiR1.png)
    

# STEP 3. ENABLE FLAG IN CHROME BROWSER

1.  Why enable an experimental Chrome flag ? In short, to let the wallet connect to your device. With the recently launched [Chrome Extension Manifest V3](https://developer.chrome.com/docs/extensions/mv3/intro/), connecting to USB devices (via WebHID/WebUSB interfaces) from the extension has become experimental features, meaning users have to turn on some Chrome flags to use them. Hopefully that will not be the case in the future Chrome versions.
    
2.  To enable WebHID in web extension, in your address bar enter chrome://flags/#enable-web-hid-on-extension-service-worker  
    ![](upload://ncoXqTxphLTRWZQl1NGrZjgYSbL.png)
    
3.  Then, inside the “Enable WebHID on extension service workers” row, choose the option Enabled instead of Default  
    ![](upload://g5HX2xJGzfGq2VY529K7B3RcZHU.png)
    

# STEP 4. CONNECT TO LEDGER DEVICE FROM WEB EXTENSION

-   First, go through the basics of using the Incognito Wallet Extension by reading [this guide](https://docs.google.com/document/d/1wpbQzpBaVd7EUaDDvgpYb9CAxAJlp6n40c6I_njM8P0/edit?usp=sharing)
    
-   To use your hardware device with Incognito Wallet Extension, make sure you have the extension set up and unlocked. Remember to use completely different recovery phrases for your Ledger device vs your wallet extension  
    ![](upload://13oyukhXLxdVRPHdRqDhfeWJNQd.png)![](upload://sPZACLSAyUniwK2PjKszbl6Gclw.png)  
      
    
-   Click on the Account Name box on the top right corner to get to Account Selection screen  
    ![](upload://uB0gc5BiXSRfdvRse8BJ7kJ3htJ.png)![](upload://hidbaTlNlPUhaysw0eZ3UIIx3tL.png)
    
-   If your Account type says “Master Key” (like above), change it to “Masterless”
    
-   You will be able to find the “Connect Hardware Device” at the bottom of the extension popup. Click on it, then if you are prompted to choose a device, choose your Nano device from the list  
    ![](upload://l6vvLRXoCpsWCOZ06bMEv8JpcLl.png)
    
-   Your Ledger device will show “Export OTA Key?” screen; you will need to Approve this for the wallet extension to find your encrypted coins. If this is your first time connecting this device, the extension will also prompt you to scan your Incognito coins. As usual, you can skip this step if your Ledger account does not have any coins in the Incognito Network yet.
    

Congratulations! Your Ledger account has been connected and selected. You can now send/receive/swap your PRV and pTokens with this account.  
![](upload://8l05gbfvG5xcPUxosNCkDmnFzpf.png)![](upload://cdwku6fWNvmqK6Xc6Om5Vc4nB13.png)![](upload://4vLpnxuSgGFcv1DqUqfNICHR9xu.png)  
![](upload://vBszc3ZUo0ljzPl2SamRRQFsaAG.png)![](upload://haceCwgdHSNp16vTyPJeIr2YQw5.png)</description>
    
    <lastBuildDate>Fri, 25 Aug 2023 06:09:18 +0000</lastBuildDate>
    <category>App users</category>
    <atom:link href="https://we.incognito.org/t/incognito-nano-app-guide/19899.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Incognito Nano App Guide</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/linnovations">@Linnovations</a> <a class="mention" href="/u/brico84">@brico84</a>,</p>
<p>We were provided with this link to see that status of Ledger app integration.<br>
<aside class="onebox whitelistedgeneric">
  <header class="source">
      <img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/d/b/db1ed7c73b3a535b8dfa83b423ea8b3fa0e4ca04.png" class="site-icon" width="32" height="32">
      <a href="https://airtable.com/appIHCUneslDfKjXu/shrqrpH9y9VRorS4J/tblw7wsOQj5Fb2UzA" target="_blank">Airtable</a>
  </header>
  <article class="onebox-body">
    <div class="aspect-image" style="--aspect-ratio:690/361;"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93f42884477f0d65b22a8e3191276ec6376f4f84_2_690x361.jpeg" class="thumbnail" width="690" height="361" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93f42884477f0d65b22a8e3191276ec6376f4f84_2_690x361.jpeg, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93f42884477f0d65b22a8e3191276ec6376f4f84_2_1035x541.jpeg 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/9/3/93f42884477f0d65b22a8e3191276ec6376f4f84.jpeg 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93f42884477f0d65b22a8e3191276ec6376f4f84_2_10x10.png"></div>

<h3><a href="https://airtable.com/appIHCUneslDfKjXu/shrqrpH9y9VRorS4J/tblw7wsOQj5Fb2UzA" target="_blank">Airtable | Everyone's app platform</a></h3>

<p>Airtable is a low-code platform for building collaborative apps. Customize your workflow, collaborate, and achieve ambitious outcomes. Get started for free.</p>


  </article>
  <div class="onebox-metadata">
    
    
  </div>
  <div style="clear: both"></div>
</aside>
</p>
<p>Scrolling down on the <code>Submitted</code> category you’ll find Incognito.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-nano-app-guide/19899/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-nano-app-guide/19899/6</link>
        <pubDate>Fri, 25 Aug 2023 06:09:18 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-19899-6</guid>
        <source url="https://we.incognito.org/t/incognito-nano-app-guide/19899.rss">Incognito Nano App Guide</source>
      </item>
      <item>
        <title>Incognito Nano App Guide</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p>We’ve submitted the code to Ledger and when we last asked they said there is a large backlog of apps to review. We will reach out again to see if there’s an update regarding our position in the queue or if they have an ETA.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-nano-app-guide/19899/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-nano-app-guide/19899/5</link>
        <pubDate>Fri, 25 Aug 2023 00:39:48 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-19899-5</guid>
        <source url="https://we.incognito.org/t/incognito-nano-app-guide/19899.rss">Incognito Nano App Guide</source>
      </item>
      <item>
        <title>Incognito Nano App Guide</title>
        <dc:creator><![CDATA[@brico84 Brian]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/brico84">@brico84</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/hieutran">@hieutran</a> what is the status of this?</p>
<p>We were in the “review” stage in February, where are we now?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-nano-app-guide/19899/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-nano-app-guide/19899/4</link>
        <pubDate>Thu, 24 Aug 2023 15:22:24 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-19899-4</guid>
        <source url="https://we.incognito.org/t/incognito-nano-app-guide/19899.rss">Incognito Nano App Guide</source>
      </item>
      <item>
        <title>Incognito Nano App Guide</title>
        <dc:creator><![CDATA[@Linnovations Yes to Privacy!]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/linnovations">@Linnovations</a> wrote:</p>
          <blockquote>
              <p>Hi <a class="mention" href="/u/hieutran">@hieutran</a>, Sorry for the radio silence. I had some family health related issues that needed my attention for the last 2 weeks.</p>
<p>I finally got round to reading this article, But unfortunately I only have a Ledger Nano X and hence I can’t sideload the incognito binaries.</p>
<p>Do you have an updated time line when the incognito app will be released.<br>
I am very keen to test it and want to provide feedback.</p>
<p>Thanks.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-nano-app-guide/19899/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-nano-app-guide/19899/3</link>
        <pubDate>Tue, 21 Feb 2023 05:37:15 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-19899-3</guid>
        <source url="https://we.incognito.org/t/incognito-nano-app-guide/19899.rss">Incognito Nano App Guide</source>
      </item>
      <item>
        <title>Incognito Nano App Guide</title>
        <dc:creator><![CDATA[@Linnovations Yes to Privacy!]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/linnovations">@Linnovations</a> wrote:</p>
          <blockquote>
              <p>Excellent News!  Thanks for <a class="mention" href="/u/hieutran">@hieutran</a> for this update and the detailed steps.</p>
<p>I will be giving this a try.</p>
<p>If we want to submit Feedback and/or Report bugs, shall we post in this forum page?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-nano-app-guide/19899/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-nano-app-guide/19899/2</link>
        <pubDate>Mon, 06 Feb 2023 13:17:33 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-19899-2</guid>
        <source url="https://we.incognito.org/t/incognito-nano-app-guide/19899.rss">Incognito Nano App Guide</source>
      </item>
      <item>
        <title>Incognito Nano App Guide</title>
        <dc:creator><![CDATA[@hieutran Hieu Tran]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/hieutran">@hieutran</a> wrote:</p>
          <blockquote>
              <p>Hello privacy community!</p>
<p>We are happy to announce that the Incognito Nano App has been submitted to the Ledger store and is under the review stage. Due to the Ledger store review process can take a long time to complete, you could also side-load our app to your Nano device (<strong>developers only</strong>) to try some features such as:</p>
<ul>
<li>View your hardware wallet account’s balances;</li>
<li>Confirm the transfer, swap, unshield transactions with your hardware device.</li>
</ul>
<p>Do note that the current version is only under the review stage, your feedback and comments will be highly appreciated. The following is the guide on how to use the Incognito Nano App with the Incognito extension.</p>
<h1>STEP 1. INSTALL INCOGNITO LEDGER APP</h1>
<h2>Install from Ledger Live Store</h2>
<ul>
<li>Soon Incognito Nano App will be available on the Ledger Live App Store</li>
</ul>
<h2>Install by sideloading (with Ledgerblue tool)</h2>
<ol>
<li>
<p>Download the release app archive (<a href="https://github.com/incognitochain/ledger/releases/download/v0.5.2-beta/incognito_nano_app_binaries.zip">incognito_nano_app_binaries</a>) from <a href="https://github.com/incognitochain/ledger/releases/tag/v0.5.2-beta">Github Release</a>. Choose your app file (incognito_&lt;nanos/nanosp/nanox&gt;.elf).</p>
</li>
<li>
<p>Follow <a href="https://developers.ledger.com/docs/embedded-app/load-mac/">this guide</a> for sideloading apps to your Ledger device. Note: Nano X devices do not support sideloading</p>
</li>
</ol>
<h1>STEP 2. INSTALL INCOGNITO EXTENSION (GOOGLE CHROME ONLY)</h1>
<h2>Install from Chrome Web Store</h2>
<ul>
<li>Ledger integration features will hit the Chrome Web Store  <a href="https://chrome.google.com/webstore/detail/incognito-wallet/chngojfpcfnjfnaeddcmngfbbdpcdjaj?hl=vi">Incognito Wallet</a> soon (est. Mar 2023). In the meantime, you have to install from the .zip file in developer mode</li>
</ul>
<h2>Install from release file</h2>
<ol>
<li>
<p>Click <a href="https://github.com/incognitochain/incognito-extension-2.0/releases/download/0.2.1-beta/extension-latest.4.zip">here</a> to download the beta release version from <a href="https://github.com/incognitochain/incognito-extension-2.0/releases/tag/0.2.1-beta">Github Release</a></p>
</li>
<li>
<p>To install the extension to Chrome, in your address bar, enter chrome://extensions/<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/0/c/0c6bf0114135fa67768c32aca0622720aafbea17.png" data-download-href="/uploads/short-url/1LSZ5fun8Rq8IAe9UrMbVdMHAUL.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/c/0c6bf0114135fa67768c32aca0622720aafbea17_2_690x123.png" alt data-base62-sha1="1LSZ5fun8Rq8IAe9UrMbVdMHAUL" width="690" height="123" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/c/0c6bf0114135fa67768c32aca0622720aafbea17_2_690x123.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/c/0c6bf0114135fa67768c32aca0622720aafbea17_2_1035x184.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/c/0c6bf0114135fa67768c32aca0622720aafbea17_2_1380x246.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/c/0c6bf0114135fa67768c32aca0622720aafbea17_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1600×286 63.1 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
<li>
<p>Turn on Developer Mode toggle at the top right corner<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/c/7/c7744737450acd756751ba46509bc4afc68de631.png" data-download-href="/uploads/short-url/sss8LS8E6Zxewv8rNITJEXHaC0p.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/7/c7744737450acd756751ba46509bc4afc68de631_2_690x149.png" alt data-base62-sha1="sss8LS8E6Zxewv8rNITJEXHaC0p" width="690" height="149" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/7/c7744737450acd756751ba46509bc4afc68de631_2_690x149.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/7/c7744737450acd756751ba46509bc4afc68de631_2_1035x223.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/7/c7744737450acd756751ba46509bc4afc68de631_2_1380x298.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/7/c7744737450acd756751ba46509bc4afc68de631_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1600×347 77.5 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
<li>
<p>Click Load Unpacked, then choose the downloaded .zip file from before.<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/9/f/9ffdddcc623e91aa4d1ee3bb7702e6e782476147.png" data-download-href="/uploads/short-url/mPlP5FjkIAiteqau9f5g7NKbiR1.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/f/9ffdddcc623e91aa4d1ee3bb7702e6e782476147_2_690x275.png" alt data-base62-sha1="mPlP5FjkIAiteqau9f5g7NKbiR1" width="690" height="275" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/f/9ffdddcc623e91aa4d1ee3bb7702e6e782476147_2_690x275.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/f/9ffdddcc623e91aa4d1ee3bb7702e6e782476147_2_1035x412.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/f/9ffdddcc623e91aa4d1ee3bb7702e6e782476147_2_1380x550.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/f/9ffdddcc623e91aa4d1ee3bb7702e6e782476147_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1600×638 170 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
</ol>
<h1>STEP 3. ENABLE FLAG IN CHROME BROWSER</h1>
<ol>
<li>
<p>Why enable an experimental Chrome flag ? In short, to let the wallet connect to your device. With the recently launched <a href="https://developer.chrome.com/docs/extensions/mv3/intro/">Chrome Extension Manifest V3</a>, connecting to USB devices (via WebHID/WebUSB interfaces) from the extension has become experimental features, meaning users have to turn on some Chrome flags to use them. Hopefully that will not be the case in the future Chrome versions.</p>
</li>
<li>
<p>To enable WebHID in web extension, in your address bar enter chrome://flags/<span class="hashtag">#enable-web-hid-on-extension-service-worker</span><br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/a/2/a298e8fce2459b86277577a2025b68ca643ac031.png" data-download-href="/uploads/short-url/ncoXqTxphLTRWZQl1NGrZjgYSbL.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/2/a298e8fce2459b86277577a2025b68ca643ac031_2_690x172.png" alt data-base62-sha1="ncoXqTxphLTRWZQl1NGrZjgYSbL" width="690" height="172" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/2/a298e8fce2459b86277577a2025b68ca643ac031_2_690x172.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/2/a298e8fce2459b86277577a2025b68ca643ac031_2_1035x258.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/2/a298e8fce2459b86277577a2025b68ca643ac031_2_1380x344.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/2/a298e8fce2459b86277577a2025b68ca643ac031_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1429×357 30.5 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
<li>
<p>Then, inside the “Enable WebHID on extension service workers” row, choose the option Enabled instead of Default<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/7/0/70c7fd519c227237b152b2192785da93a0dedae6.png" data-download-href="/uploads/short-url/g5HX2xJGzfGq2VY529K7B3RcZHU.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/0/70c7fd519c227237b152b2192785da93a0dedae6_2_690x197.png" alt data-base62-sha1="g5HX2xJGzfGq2VY529K7B3RcZHU" width="690" height="197" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/0/70c7fd519c227237b152b2192785da93a0dedae6_2_690x197.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/0/70c7fd519c227237b152b2192785da93a0dedae6_2_1035x295.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/7/0/70c7fd519c227237b152b2192785da93a0dedae6.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/0/70c7fd519c227237b152b2192785da93a0dedae6_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1345×385 36.1 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
</ol>
<h1>STEP 4. CONNECT TO LEDGER DEVICE FROM WEB EXTENSION</h1>
<ul>
<li>
<p>First, go through the basics of using the Incognito Wallet Extension by reading <a href="https://docs.google.com/document/d/1wpbQzpBaVd7EUaDDvgpYb9CAxAJlp6n40c6I_njM8P0/edit?usp=sharing">this guide</a></p>
</li>
<li>
<p>To use your hardware device with Incognito Wallet Extension, make sure you have the extension set up and unlocked. Remember to use completely different recovery phrases for your Ledger device vs your wallet extension<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/0/7/0764734d3b35d40ec801a8686383749cf8ac6621.png" data-download-href="/uploads/short-url/13oyukhXLxdVRPHdRqDhfeWJNQd.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/0764734d3b35d40ec801a8686383749cf8ac6621_2_289x500.png" alt data-base62-sha1="13oyukhXLxdVRPHdRqDhfeWJNQd" width="289" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/0764734d3b35d40ec801a8686383749cf8ac6621_2_289x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/0764734d3b35d40ec801a8686383749cf8ac6621_2_433x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/0764734d3b35d40ec801a8686383749cf8ac6621_2_578x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/0764734d3b35d40ec801a8686383749cf8ac6621_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1266 28.7 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/c/a/ca1d789eea1427bdfc945a34b4ade4481bcb6d16.png" data-download-href="/uploads/short-url/sPZACLSAyUniwK2PjKszbl6Gclw.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/a/ca1d789eea1427bdfc945a34b4ade4481bcb6d16_2_290x500.png" alt data-base62-sha1="sPZACLSAyUniwK2PjKszbl6Gclw" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/a/ca1d789eea1427bdfc945a34b4ade4481bcb6d16_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/a/ca1d789eea1427bdfc945a34b4ade4481bcb6d16_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/a/ca1d789eea1427bdfc945a34b4ade4481bcb6d16_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/a/ca1d789eea1427bdfc945a34b4ade4481bcb6d16_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 65.6 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
<li>
<p>Click on the Account Name box on the top right corner to get to Account Selection screen<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/d/6/d6700ec5ff8fbea63243a851a42148e05d937fef.png" data-download-href="/uploads/short-url/uB0gc5BiXSRfdvRse8BJ7kJ3htJ.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/6/d6700ec5ff8fbea63243a851a42148e05d937fef_2_290x500.png" alt data-base62-sha1="uB0gc5BiXSRfdvRse8BJ7kJ3htJ" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/6/d6700ec5ff8fbea63243a851a42148e05d937fef_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/6/d6700ec5ff8fbea63243a851a42148e05d937fef_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/6/d6700ec5ff8fbea63243a851a42148e05d937fef_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/6/d6700ec5ff8fbea63243a851a42148e05d937fef_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 29.4 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/7/9/7933ff9f6adb3da85bed6c63c958075c10b040f9.png" data-download-href="/uploads/short-url/hidbaTlNlPUhaysw0eZ3UIIx3tL.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/9/7933ff9f6adb3da85bed6c63c958075c10b040f9_2_290x500.png" alt data-base62-sha1="hidbaTlNlPUhaysw0eZ3UIIx3tL" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/9/7933ff9f6adb3da85bed6c63c958075c10b040f9_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/9/7933ff9f6adb3da85bed6c63c958075c10b040f9_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/9/7933ff9f6adb3da85bed6c63c958075c10b040f9_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/9/7933ff9f6adb3da85bed6c63c958075c10b040f9_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 40.1 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
<li>
<p>If your Account type says “Master Key” (like above), change it to “Masterless”</p>
</li>
<li>
<p>You will be able to find the “Connect Hardware Device” at the bottom of the extension popup. Click on it, then if you are prompted to choose a device, choose your Nano device from the list<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/9/3/93e9fdcd749e27e3aae8852f0f792fe45ada096f.png" data-download-href="/uploads/short-url/l6vvLRXoCpsWCOZ06bMEv8JpcLl.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93e9fdcd749e27e3aae8852f0f792fe45ada096f_2_290x500.png" alt data-base62-sha1="l6vvLRXoCpsWCOZ06bMEv8JpcLl" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93e9fdcd749e27e3aae8852f0f792fe45ada096f_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93e9fdcd749e27e3aae8852f0f792fe45ada096f_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93e9fdcd749e27e3aae8852f0f792fe45ada096f_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/3/93e9fdcd749e27e3aae8852f0f792fe45ada096f_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 45.9 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
<li>
<p>Your Ledger device will show “Export OTA Key?” screen; you will need to Approve this for the wallet extension to find your encrypted coins. If this is your first time connecting this device, the extension will also prompt you to scan your Incognito coins. As usual, you can skip this step if your Ledger account does not have any coins in the Incognito Network yet.</p>
</li>
</ul>
<p>Congratulations! Your Ledger account has been connected and selected. You can now send/receive/swap your PRV and pTokens with this account.<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/3/a/3a7122ac63db64c50644f26726d05e1ad6839fd1.png" data-download-href="/uploads/short-url/8l05gbfvG5xcPUxosNCkDmnFzpf.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/a/3a7122ac63db64c50644f26726d05e1ad6839fd1_2_290x500.png" alt data-base62-sha1="8l05gbfvG5xcPUxosNCkDmnFzpf" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/a/3a7122ac63db64c50644f26726d05e1ad6839fd1_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/a/3a7122ac63db64c50644f26726d05e1ad6839fd1_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/a/3a7122ac63db64c50644f26726d05e1ad6839fd1_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/a/3a7122ac63db64c50644f26726d05e1ad6839fd1_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 69.6 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/5/5/55a160210a644edee9c25d69c7ff02d6b00a849d.png" data-download-href="/uploads/short-url/cdwku6fWNvmqK6Xc6Om5Vc4nB13.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/5/55a160210a644edee9c25d69c7ff02d6b00a849d_2_290x500.png" alt data-base62-sha1="cdwku6fWNvmqK6Xc6Om5Vc4nB13" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/5/55a160210a644edee9c25d69c7ff02d6b00a849d_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/5/55a160210a644edee9c25d69c7ff02d6b00a849d_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/5/55a160210a644edee9c25d69c7ff02d6b00a849d_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/5/55a160210a644edee9c25d69c7ff02d6b00a849d_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 37 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/1/f/1f9fe9382a0414398031bee46df69ab043e30808.png" data-download-href="/uploads/short-url/4vLpnxuSgGFcv1DqUqfNICHR9xu.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/1/f/1f9fe9382a0414398031bee46df69ab043e30808_2_290x500.png" alt data-base62-sha1="4vLpnxuSgGFcv1DqUqfNICHR9xu" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/1/f/1f9fe9382a0414398031bee46df69ab043e30808_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/1/f/1f9fe9382a0414398031bee46df69ab043e30808_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/1/f/1f9fe9382a0414398031bee46df69ab043e30808_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/1/f/1f9fe9382a0414398031bee46df69ab043e30808_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 42.9 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/d/d/dd7f7190bd2f876bff0d39b80c39858dac4562fa.png" data-download-href="/uploads/short-url/vBszc3ZUo0ljzPl2SamRRQFsaAG.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/d/dd7f7190bd2f876bff0d39b80c39858dac4562fa_2_290x500.png" alt data-base62-sha1="vBszc3ZUo0ljzPl2SamRRQFsaAG" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/d/dd7f7190bd2f876bff0d39b80c39858dac4562fa_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/d/dd7f7190bd2f876bff0d39b80c39858dac4562fa_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/d/dd7f7190bd2f876bff0d39b80c39858dac4562fa_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/d/dd7f7190bd2f876bff0d39b80c39858dac4562fa_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 70.9 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/7/8/784c0d2890510fe0ee1e4fd430229742b6567a75.png" data-download-href="/uploads/short-url/haceCwgdHSNp16vTyPJeIr2YQw5.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/8/784c0d2890510fe0ee1e4fd430229742b6567a75_2_290x500.png" alt data-base62-sha1="haceCwgdHSNp16vTyPJeIr2YQw5" width="290" height="500" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/8/784c0d2890510fe0ee1e4fd430229742b6567a75_2_290x500.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/8/784c0d2890510fe0ee1e4fd430229742b6567a75_2_435x750.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/8/784c0d2890510fe0ee1e4fd430229742b6567a75_2_580x1000.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/7/8/784c0d2890510fe0ee1e4fd430229742b6567a75_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">732×1260 34.2 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-nano-app-guide/19899/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-nano-app-guide/19899/1</link>
        <pubDate>Sun, 05 Feb 2023 01:48:29 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-19899-1</guid>
        <source url="https://we.incognito.org/t/incognito-nano-app-guide/19899.rss">Incognito Nano App Guide</source>
      </item>
  </channel>
</rss>
