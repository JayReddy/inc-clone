<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
    <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578</link>
    <description>For this discussion, we&#39;ll be looking at Litecoin.  However this issue likely applies to other coins such $BTC, $XMR and maybe even $ETH.  However the Incognito network volume of those assets is much larger than $LTC.  With $LTC being a recent addition to Incognito, it&#39;s easier to review Litecoin&#39;s full transaction history with Incognito.

CONTEXT
=

When $LTC is shielded, Incognito generates a single use address to receive the coin.

![image|1308x365, 75%](upload://23k37ZA03DtlxoeUA8ka9EyPMO4.png) 

Here we see that single-use shielding address [Lba4WKQybGoZMwv4LEJKsZjVAj2HVRh8uy](https://blockchair.com/litecoin/address/Lba4WKQybGoZMwv4LEJKsZjVAj2HVRh8uy) received 4.35 $LTC.  Address MS8g7KDz3atp82qrQidtqBXtpHimNr2Uwp is a change address in the bottom transaction.

About fifteen seconds later we see the received $LTC automagically sent to [LLD6JTeDP8smXDFQf1FiGwoQeaeWWvqSMd](https://blockchair.com/litecoin/address/LLD6JTeDP8smXDFQf1FiGwoQeaeWWvqSMd) which is Incognito&#39;s $LTC vault.  It is here in the vault this new $LTC mingles with the existing $LTC represented on the network, making new friends and generally enjoying the big party at it&#39;s new home.

It&#39;s important to remember that both **Lba4W...8uy** (as a single use receive address) and **LLD6J...SMd** (the vault address) are BOTH controlled by Incognito&#39;s $LTC bridge.  For every shield transaction, the receive address will be different but still controlled by Incognito&#39;s $LTC bridge.

THE ISSUE
=

Let&#39;s take a look at the amount of $LTC reported by the external network explorer.

![image|868x136](upload://mIhuUJ4nJL1gnQ2xRihoKFZXXn4.png) 

Blockchair (and any other $LTC explorer) reports the address currently holds 52.43 $LTC.

Now let&#39;s compare this amount with the $pLTC reported by the Incognito network explorer:

![image|366x522](upload://bjQVjnpOqYIWwYifRvhqvQBLGi8.png) 

These snapshots were taken today at the same time.  The $LTC vault holds 52.43 $LTC while there are 52.52 $pLTC tokens minted on the network.  That, ladies and gentlemen, is NOT a 1:1 peg.

Why the difference?  I&#39;ll tell you.  It&#39;s &quot;internal&quot; transaction fees.  To be certain, I pulled the transaction history for all^ 204 transactions involving **LLD6J...SMd**.  The fees representing transfers in (shielding) and out of (unshielding) the vault equals 0.0865$LTC.  52.4319 (vault balance) + 0.0865 (total tx fees) = 52.5184.  Yep, there&#39;s the missing $LTC.

The automagic transfer of received funds from the single use address to the vault address is an on-chain transfer.  On-chain transfers, of course, incur miner fees.  These fees appear to be deducted from the vault balance but are not represented in the corresponding pToken allocations.

WHAT&#39;S THE BIG DEAL?
=
Miner fees are small, right?  Eventually these small miner fees will begin to add up to a not insignificant amount of missing value in the $LTC vault.  Should a crypto bank-run occur, the last user in line is going to have a very nasty surprise.

Consider this example (fees are exaggerated for clarity&#39;s sake):

* The Incognito vault has a balance of 0 $LTC.
--
* Alice shields 10 $LTC.
* Incognito mints 10 $pLTC and deposits them to Alice&#39;s Incognito address.
* Incognito transfers the 10 $LTC to the vault.  The transfer deducts 0.5 $LTC in miner fees.
* The Incognito vault holds 9.5 $LTC.  The Incognito network has 10 $pLTC.
* Bob shields 10 $LTC.
* Incognito mints 10 $pLTC and deposits them to Bob&#39;s Incognito address.
* Incognito transfers the 10 $LTC to the vault.  The transfer deducts 0.5 $LTC in miner fees.
* The Incognito vault holds 19 $LTC.  The Incognito network has 20 $pLTC.
--
* Alice unshields her 10 $pLTC.
* Incognito withdraws 10 $pLTC from Alice&#39;s Incognito address and burns them.
* Incognito transfers 10 $LTC from the vault to Alice&#39;s external LTC address. The transfer deducts 0.5 $LTC in miner fees from the vault balance.
* The Incognito vault holds 8.5 $LTC.  The Incognito network has 10 $pLTC.
--
* Bob unshields his 10 $pLTC.
* Incognito withdraws 10 $pLTC from Bob&#39;s Incognito address and burns them.
* However there are only the 8.5 $LTC left after transaction fees -- *so Bob only receives 8 $LTC, after his own unshielding transaction fee*?

With large volume coins such as $BTC, $XMR and even $ETH, it does seem unlikely the entire balance of pTokens would ever be redeemed revealing the above issue for the last transactor.  But the *issue would still exist*.

Conversely with mid and small volume coins such as $LTC, $DASH, $ZIL, etc, one can see how it is plausible for the entire network pToken balance to be redeemed (for one reason or another) leaving the last transactor without a complete redemption of burn pTokens.

As-is, $LTC has been available on Incognito for about 6 weeks.  In that six weeks, $10 in value has already been eroded by fees.  In a year, that will be over $90 at the current shield/unshield rate.  In all likelihood, it will be much more.

SOLUTIONS?
=
I don&#39;t know what the solution should be.  Should some of the DAO funds be used to make the vault &quot;whole&quot; again?  Should the &quot;internal&quot; vault transfers be deducted from the pToken minted?  Perhaps both?  Perhaps neither?  Maybe this becomes a good first use for the oft-proposed DAO community governance?

----
^ Current as of 2030 UTC on 1/27/2021.</description>
    
    <lastBuildDate>Thu, 11 Mar 2021 07:21:13 +0000</lastBuildDate>
    <category>Help</category>
    <atom:link href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@binh Lay]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/binh">@binh</a> wrote:</p>
          <blockquote>
              <p>Hi <a class="mention" href="/u/stradimarius">@Stradimarius</a> and everybody,</p>
<p>Currently, the shielding process for trusted bridges relies on temporary addresses, which is made up of two transactions (we’ll use 1 BTC as an example):<br>
Tx 1: Shielding 1 BTC to a temporary address.<br>
Tx 2: Sending that 1 BTC to the BTC vault address.<br>
Since users only initiate the shielding transaction and pay fees for Tx 1, the fees for Tx 2 are left unpaid. Those fees are BTC network fees, not Incognito fees, so for now, these are paid by Incognito via the vault address. This is why the address would have slightly less than a 1:1 balance, as we have to top up the balance and make sure the ratio is balanced at 1:1. This results in a brief time period where the balance isn’t exactly 1:1. The deficit though is only a sum of fees, so there’s no risk of not being able to supply enough crypto to users who unshield.<br>
Even though this workaround is only for trusted bridges and is thus abolished when bridges are made trustless, it’s still not ideal. So, we’re working on a solution in the meantime.</p>
<p>and please check the solution for decentralized bridged (<strong>Portal v4</strong>) is on the plan to release in May here  :</p>
<aside class="quote quote-modified" data-post="1" data-topic="9605">
  <div class="title">
    <div class="quote-controls"></div>
    <img alt="" width="20" height="20" src="/user_avatar/we.incognito.org/0xkraken/40/9124_2.png" class="avatar">
    <a href="https://we.incognito.org/t/incognito-s-trustless-non-custodians-bridge-v4/9605">Incognito’s trustless non-custodians bridge V4</a> <a class="badge-wrapper  box" href="/c/core-dev/building"><span class="badge-category-parent-bg" style="background-color: #196F3D;"></span><span class="badge-category-bg" style="background-color: #196F3D;"></span><span style="color: #FFFFFF" data-drop-close="true" class="badge-category clear-badge" title="Projects and initiatives under active development by the Incognito core team.">Core</span></a>
  </div>
  <blockquote>
    Problem: 
As you know, we finished the design and implementation of Incognito’s trustless bridge for public tokens (BTC and BNB) that allow users to shield public tokens decentralized, aka Portal v2 (with Incognito vault) and Portal v3 (with Ethereum smart contract vault). See detail: <a href="https://we.incognito.org/t/incognitos-trustless-bridge-v3-with-ethereum-bond-contract/2963" class="inline-onebox">Incognito's trustless bridge V3 with Ethereum bond contract</a> 
However, there are some problems with our current design. 

First, the logic of the shielding and unshielding processes is quite complicated. So, it’s to…
  </blockquote>
</aside>

<p>thank you,</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/13">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/13</link>
        <pubDate>Thu, 11 Mar 2021 07:21:13 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-13</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@Stradimarius Stradimarius]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/stradimarius">@Stradimarius</a> wrote:</p>
          <blockquote>
              <p>Waiting for 1 month and a half now.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/12">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/12</link>
        <pubDate>Wed, 10 Mar 2021 19:01:11 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-12</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@Tempestblack Vincent Valencia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tempestblack">@Tempestblack</a> wrote:</p>
          <blockquote>
              <p>Waits at the edge of his seat for the answer…hears final jeopardy music playing in the background…raises eyebrow and thinks…‘wtf’… <img src="https://we.incognito.org/images/emoji/apple/sunglasses.png?v=9" title=":sunglasses:" class="emoji" alt=":sunglasses:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/11">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/11</link>
        <pubDate>Wed, 10 Mar 2021 16:48:45 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-11</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@inccry]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/inccry">@inccry</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="Revolve" data-post="9" data-topic="9578">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/revolve/40/4171_2.png" class="avatar"> Revolve:</div>
<blockquote>
<p>Was the bug related to Incscan?</p>
</blockquote>
</aside>
<p>Maybe. Before any investigation on my side, I prefer to wait for Incognito team answer about how they manage fees paid in PRV by the user but spent in targeted currency.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/10">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/10</link>
        <pubDate>Tue, 09 Mar 2021 22:13:25 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-10</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@Revolve Noah]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/revolve">@Revolve</a> wrote:</p>
          <blockquote>
              <p>Was the bug related to Incscan? I remember seeing a few bugs like this in the past with ghost coins and negative liquidity.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/9">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/9</link>
        <pubDate>Tue, 09 Mar 2021 09:50:38 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-9</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@Stradimarius Stradimarius]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/stradimarius">@Stradimarius</a> wrote:</p>
          <blockquote>
              <p>I was looking for this post and the answer. It seems it has been forgotten.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/7">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/7</link>
        <pubDate>Mon, 08 Mar 2021 20:20:02 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-7</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@2657k1AUcann]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/2657k1aucann">@2657k1AUcann</a> wrote:</p>
          <blockquote>
              <p>Has a solution been implemented?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/6</link>
        <pubDate>Mon, 08 Mar 2021 20:13:51 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-6</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@cobo]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/cobo">@cobo</a> wrote:</p>
          <blockquote>
              <p>Curious about the answer <img src="https://we.incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/5</link>
        <pubDate>Sun, 07 Feb 2021 09:20:31 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-5</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@Peter Peter]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/peter">@Peter</a> wrote:</p>
          <blockquote>
              <p>Thanks <a class="mention" href="/u/mike_wagner">@Mike_Wagner</a> for addressing the problem. We will provide proper feedback soon.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/4</link>
        <pubDate>Thu, 28 Jan 2021 12:07:11 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-4</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@inccry]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/inccry">@inccry</a> wrote:</p>
          <blockquote>
              <p>It could be an Incscan bug too <img src="https://we.incognito.org/images/emoji/apple/wink.png?v=9" title=":wink:" class="emoji" alt=":wink:"></p>
<p>I don’t process data before a token is validated so you could see some differences between on-chain and Incscan data if some shielding / unshielding operations happened before the validation of the token.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/3</link>
        <pubDate>Thu, 28 Jan 2021 07:48:09 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-3</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@Natalia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/natalia">@Natalia</a> wrote:</p>
          <blockquote>
              <p>^^^</p>
<p>Nice post friend…and needs to be addressed.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/2</link>
        <pubDate>Thu, 28 Jan 2021 00:11:02 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-2</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
      <item>
        <title>[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</title>
        <dc:creator><![CDATA[@Mike_Wagner Mike Wagner]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/mike_wagner">@Mike_Wagner</a> wrote:</p>
          <blockquote>
              <p>For this discussion, we’ll be looking at Litecoin.  However this issue likely applies to other coins such $BTC, $XMR and maybe even $ETH.  However the Incognito network volume of those assets is much larger than $LTC.  With $LTC being a recent addition to Incognito, it’s easier to review Litecoin’s full transaction history with Incognito.</p>
<h1>CONTEXT</h1>
<p>When $LTC is shielded, Incognito generates a single use address to receive the coin.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/0/0e6485738e7a22f760045073bb83bfd6a6c682a8.png" alt="image" data-base62-sha1="23k37ZA03DtlxoeUA8ka9EyPMO4" width="981" height="273"></p>
<p>Here we see that single-use shielding address <a href="https://blockchair.com/litecoin/address/Lba4WKQybGoZMwv4LEJKsZjVAj2HVRh8uy">Lba4WKQybGoZMwv4LEJKsZjVAj2HVRh8uy</a> received 4.35 $LTC.  Address MS8g7KDz3atp82qrQidtqBXtpHimNr2Uwp is a change address in the bottom transaction.</p>
<p>About fifteen seconds later we see the received $LTC automagically sent to <a href="https://blockchair.com/litecoin/address/LLD6JTeDP8smXDFQf1FiGwoQeaeWWvqSMd">LLD6JTeDP8smXDFQf1FiGwoQeaeWWvqSMd</a> which is Incognito’s $LTC vault.  It is here in the vault this new $LTC mingles with the existing $LTC represented on the network, making new friends and generally enjoying the big party at it’s new home.</p>
<p>It’s important to remember that both <strong>Lba4W…8uy</strong> (as a single use receive address) and <strong>LLD6J…SMd</strong> (the vault address) are BOTH controlled by Incognito’s $LTC bridge.  For every shield transaction, the receive address will be different but still controlled by Incognito’s $LTC bridge.</p>
<h1>THE ISSUE</h1>
<p>Let’s take a look at the amount of $LTC reported by the external network explorer.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/9/9f31478cda95b72e53af0fce057a45e9664d4dda.png" alt="image" data-base62-sha1="mIhuUJ4nJL1gnQ2xRihoKFZXXn4" width="868" height="136"></p>
<p>Blockchair (and any other $LTC explorer) reports the address currently holds 52.43 $LTC.</p>
<p>Now let’s compare this amount with the $pLTC reported by the Incognito network explorer:</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/4/4f567185b8ee18bbc8280de9feebdb8998e7e424.png" alt="image" data-base62-sha1="bjQVjnpOqYIWwYifRvhqvQBLGi8" width="366" height="522"></p>
<p>These snapshots were taken today at the same time.  The $LTC vault holds 52.43 $LTC while there are 52.52 $pLTC tokens minted on the network.  That, ladies and gentlemen, is NOT a 1:1 peg.</p>
<p>Why the difference?  I’ll tell you.  It’s “internal” transaction fees.  To be certain, I pulled the transaction history for all^ 204 transactions involving <strong>LLD6J…SMd</strong>.  The fees representing transfers in (shielding) and out of (unshielding) the vault equals 0.0865$LTC.  52.4319 (vault balance) + 0.0865 (total tx fees) = 52.5184.  Yep, there’s the missing $LTC.</p>
<p>The automagic transfer of received funds from the single use address to the vault address is an on-chain transfer.  On-chain transfers, of course, incur miner fees.  These fees appear to be deducted from the vault balance but are not represented in the corresponding pToken allocations.</p>
<h1>WHAT’S THE BIG DEAL?</h1>
<p>Miner fees are small, right?  Eventually these small miner fees will begin to add up to a not insignificant amount of missing value in the $LTC vault.  Should a crypto bank-run occur, the last user in line is going to have a very nasty surprise.</p>
<p>Consider this example (fees are exaggerated for clarity’s sake):</p>
<ul>
<li>The Incognito vault has a balance of 0 $LTC.<br>
–</li>
<li>Alice shields 10 $LTC.</li>
<li>Incognito mints 10 $pLTC and deposits them to Alice’s Incognito address.</li>
<li>Incognito transfers the 10 $LTC to the vault.  The transfer deducts 0.5 $LTC in miner fees.</li>
<li>The Incognito vault holds 9.5 $LTC.  The Incognito network has 10 $pLTC.</li>
<li>Bob shields 10 $LTC.</li>
<li>Incognito mints 10 $pLTC and deposits them to Bob’s Incognito address.</li>
<li>Incognito transfers the 10 $LTC to the vault.  The transfer deducts 0.5 $LTC in miner fees.</li>
<li>The Incognito vault holds 19 $LTC.  The Incognito network has 20 $pLTC.<br>
–</li>
<li>Alice unshields her 10 $pLTC.</li>
<li>Incognito withdraws 10 $pLTC from Alice’s Incognito address and burns them.</li>
<li>Incognito transfers 10 $LTC from the vault to Alice’s external LTC address. The transfer deducts 0.5 $LTC in miner fees from the vault balance.</li>
<li>The Incognito vault holds 8.5 $LTC.  The Incognito network has 10 $pLTC.<br>
–</li>
<li>Bob unshields his 10 $pLTC.</li>
<li>Incognito withdraws 10 $pLTC from Bob’s Incognito address and burns them.</li>
<li>However there are only the 8.5 $LTC left after transaction fees – <em>so Bob only receives 8 $LTC, after his own unshielding transaction fee</em>?</li>
</ul>
<p>With large volume coins such as $BTC, $XMR and even $ETH, it does seem unlikely the entire balance of pTokens would ever be redeemed revealing the above issue for the last transactor.  But the <em>issue would still exist</em>.</p>
<p>Conversely with mid and small volume coins such as $LTC, $DASH, $ZIL, etc, one can see how it is plausible for the entire network pToken balance to be redeemed (for one reason or another) leaving the last transactor without a complete redemption of burn pTokens.</p>
<p>As-is, $LTC has been available on Incognito for about 6 weeks.  In that six weeks, $10 in value has already been eroded by fees.  In a year, that will be over $90 at the current shield/unshield rate.  In all likelihood, it will be much more.</p>
<h1>SOLUTIONS?</h1>
<p>I don’t know what the solution should be.  Should some of the DAO funds be used to make the vault “whole” again?  Should the “internal” vault transfers be deducted from the pToken minted?  Perhaps both?  Perhaps neither?  Maybe this becomes a good first use for the oft-proposed DAO community governance?</p>
<hr>
<p>^ Current as of 2030 UTC on 1/27/2021.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578/1</link>
        <pubDate>Wed, 27 Jan 2021 23:14:51 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-9578-1</guid>
        <source url="https://we.incognito.org/t/bug-amount-of-ptoken-in-network-is-not-fully-pegged-1-1-to-vault-balance/9578.rss">[BUG] Amount of pToken in network is not fully pegged 1:1 to vault balance</source>
      </item>
  </channel>
</rss>
