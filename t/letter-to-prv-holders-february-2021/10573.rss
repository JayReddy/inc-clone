<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Letter to PRV Holders (February 2021)</title>
    <link>https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573</link>
    <description>[Gary Gensler](https://www.foxbusiness.com/markets/sec-nominee-gensler-bitcoin-cryptocurrency), US president Joe Biden’s pick for chair of the SEC, is positioning himself as somewhat of a friend to crypto. 

In his confirmation hearing, Gensler admitted his desire to regulate crypto to “protect” investors, while also suggesting his optimism about the positive impacts crypto and related technologies will have.  It’s a bit of a mixed bag, but hey, it’s the best we’ll get from an organization whose sole purpose is control (government). 


The US is certainly not the center of the world, especially the crypto world, but what the US does in the next few years will without a doubt influence the stance much of the rest of the world takes toward cryptocurrency. 

Based on the continuous support cryptoassets are getting from companies in the US, and the hesitant-but-open stance of US regulators, we’re looking at the beginning of a new era for cryptocurrency. That is, the era of mass adoption. 

## Crypto is changing

If millions of new crypto users are on their way, most of whom still think you have to buy Bitcoin in whole numbers, then we’re going to need to change the culture of crypto. 

The primary conversations currently are arguments between two parties:

- Bulls vs bears
- BTC maxis vs altcoiners
- Etherheads vs altDeFi
- Crypto vs doom &amp; gloom media

But as soon as we hear the confused but enthusiastic grunts of the hordes of newcomers, we’ll be forced to stop and figure out how to onboard them all.

![image|480x360,120%](upload://7XXWaq9eWiMQG7D44wEwqM7GyxC.jpeg) 
(Praise to the artist, whose name I could not find)

Maybe there’s more than infighting, maybe I’m just spending too much time on Twitter (it’s *bad*, y’all).

But regardless, the times are a-changing, and crypto is moving from an insular industry of nerds to the workin’ (wo)man’s party.

Which brings me to what we’re going to do about it. 

## Trust, transparency, and traction

If you’re reading this letter, it’s probably because you have monk-like patience and are dedicated to privacy. We all know how much I write (my letters are getting longer, aren’t they?). And not only that, but you’ve had to do quite a bit of digging to understand the ins and outs of Incognito.

Not everyone can do that.

In February, we decided to fix that. We brainstormed how to transform the face of Incognito, to be even more transparent and easy to understand. We need to be transparent enough for crypto veterans to know the project is legit, and simple enough for anyone to start using it. It needs to be shareable, wearable, and  community-led. 

So, we spent time gathering feedback from you, and brainstorming how to restructure all of the information we have available. In March and April, we’re going to take what we’ve learned and make the frontend of Incognito as effective as the backend. 


It may have seemed like we slowed down in February, but nothing could be further from the truth. Just because Bitcoin dipped doesn’t mean we did ;) 

As you’ll see below, February was the largest month of growth we’ve ever had, and given not only the coming project redesign but also the new features launching soon, that growth is likely only going to increase.  


# February growth

## Trading volume: +$77.6M

&gt; Previous month’s growth: +$44.5M

![image|1024x768,69%](upload://kWoe0X2RI4wpQr9raMmawRkzutT.jpeg) 
In January, anonymous trading volume nearly doubled. In February, it nearly doubled again. Average daily trading volume is between $2-3 million, with an all-time high in daily volume of more than $6 million.

Corporate adoption of crypto is a threat to personal privacy, because your data is valuable. To avoid being the product, people are trading their crypto anonymously more than ever. Let’s keep up the good work and continue sharing the pDEX with anyone who could use privacy.

## Shield volume: +$19M

&gt; Previous month’s growth: +$18M

![image|1024x768,69%](upload://dc4sFd4JvKn5Bnk1IRL1VVFsql1.jpeg) 
Almost every month, Incognito grows faster than it did the month before. The last two months of shield volume growth account for about half of the current volume. 

## Liquidity: +$14M

 &gt; Previous month’s growth: +$4.5M

![image|1024x768,69%](upload://ifeB5lH30wcIY6HzX6GbN7G9cRb.jpeg) 
With daily trading volume at all-time highs, it’s important to have enough liquidity to enable people to trade freely. Liquidity in the pDEX is up $14 million from last month, a growth of 311%  compared to the previous month. 

___
# February development, March Goals

I&#39;ve been promising some key features for months now, and it looks like my word will be made tech (John 1:14, anyone? No?) very soon. Across all aspects of technical development, most features have moved beyond initial testing and into revision phases, preparing for launch.

 Growth efforts were.. forewent? foregoed? We decided to forego active growth efforts in favor of user research and planning for the redesign, so when we do seek new users, they’ll have easy access to all the information they need. @Elena did still send requests to listing sites like CMC, and I spoke with multiple journalists about coming feature PRs, but I decided to wait on them. We also welcomed new members to the growth team in February. 

We still need your help as community leaders to create the community that will soon lead the project, but it’s not fair to ask you to do all the work, after all ;) 

A few development highlights include: 

## [App v.5](https://we.incognito.org/t/app-v-5/9584)

- v.4.1.4 is live, putting a few finishing touches on App v.4 before the launch of v.5
- The web extension was released for beta testing
- [Ledger integration](https://we.incognito.org/t/incognito-ledger-integration/8504/17) initial development has finished

**March goals:**
- Improve shielding experience, allow shielding directly from metamask
- Develop UI for Portal v4
- Build a full Node service


## [Privacy v.2](https://we.incognito.org/t/privacy-version-2/2967/16)
Increasing the scope of privacy is always a top priority. Privacy v.2 will launch by May. In February, key updates include:

- [Confidential assets](https://we.incognito.org/t/confidential-asset/5607/9) deployed in Testnet
- Staking flow v.2 finished Testnet run and merged with code
- Dynamic committee size in staking flow v.3 entered testing

**March goals:**

- Finish CA devnet testing
- Deploy multiview and staking flow v.2 to mainnet
- Local testing for staking flow v.3
- Deploy Privacy v.2 as a whole to testnet

## [Bridges](https://we.incognito.org/t/incognito-s-trustless-non-custodians-bridge-v4/9605/15)

- Gas fee reduction was prepared for testnet deployment
- Protocol design, shielding flow, and implementation of Bridge v.4 were finished
- Review initial Ethereum audit and update for final Coinspect audit

**March goals:**

- Refigure [gas reduction strategy](https://we.incognito.org/t/bridge-contract-gas-optimization-with-chi-gastoken/9788/7)
- Finish, test, and deploy bridge v.4 on Privacy v.1


## [Web v.3 and FAQ](https://we.incognito.org/t/build-faq-section-for-the-new-web/9737/12)

- FAQs were organized and answered by the core team, implementation held for the rest of web v.3
- Incognito.org was redesigned, a new site was wireframed.
- Privacy user feedback survey was issued


**March goals:**

- Parse survey responses and implement feedback
- Finalize web v.3 redesign and code new site 
- Increase [App Store reviews](https://we.incognito.org/t/building-transparency-and-trust/10549) and overall rating


___

# March PRV Holders Call

The next PRV Holders Call is this Thursday/Friday! Join us for the usual progress presentation, and to discuss up-and-coming hot topics, such as:

* Security audit progress
* Web v.3 - a new face for the project.
* The product development roadmap.

## Event details

This month, we want to experiment with a new time for the call. Be sure to add it to your calendar!

&gt; **Time**: 2AM UTC | 9AM GMT+7 | 9PM EST | 6PM PST
**Date**: Friday, March 5th, 2021 (For UTC &amp; GMT). Thursday, March 4th (For USA). 
**Where: https://us02web.zoom.us/j/87168535851**
**Livestream: Set a reminder for the [stream](https://youtu.be/mumaqzyIUGk).**

[RSVP here](https://we.incognito.org/t/join-us-for-the-march-2021-prv-holders-call/10530), and comment with topics you want to discuss on the call.

___
# Don&#39;t wait for next month - 
Get involved this month! We&#39;re launching a bunch of new features and updates, and growing quickly. The community needs people like you that are familiar with the project to help guide the community forward. It won&#39;t be long before the entire privacy movement will be 100% community-led, and we need to be prepared. 

So, share your thoughts, questions, and ideas here, and let&#39;s bring privacy to every crypto user on the planet together. 

See you around!</description>
    
    <lastBuildDate>Mon, 08 Mar 2021 00:09:00 +0000</lastBuildDate>
    <category>Updates</category>
    <atom:link href="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Letter to PRV Holders (February 2021)</title>
        <dc:creator><![CDATA[@marko]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/marko">@marko</a> wrote:</p>
          <blockquote>
              <p>Welcome here <a class="mention" href="/u/lucy">@lucy</a><br>
Yes, Incognito is an exciting project <img src="https://we.incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/6</link>
        <pubDate>Mon, 08 Mar 2021 00:09:00 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-10573-6</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573.rss">Letter to PRV Holders (February 2021)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (February 2021)</title>
        <dc:creator><![CDATA[@Josef]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/josef">@Josef</a> wrote:</p>
          <blockquote>
              <p>Great work from Incognito, as usual. Keep it up! <img src="https://we.incognito.org/images/emoji/apple/wink.png?v=9" title=":wink:" class="emoji" alt=":wink:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/4</link>
        <pubDate>Fri, 05 Mar 2021 23:20:34 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-10573-4</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573.rss">Letter to PRV Holders (February 2021)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (February 2021)</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <p>Thanks for reading them, and for the encouragement <a class="mention" href="/u/tempestblack">@Tempestblack</a>!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/3</link>
        <pubDate>Fri, 05 Mar 2021 19:15:02 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-10573-3</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573.rss">Letter to PRV Holders (February 2021)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (February 2021)</title>
        <dc:creator><![CDATA[@Tempestblack Vincent Valencia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tempestblack">@Tempestblack</a> wrote:</p>
          <blockquote>
              <p>Hey <a class="mention" href="/u/aaron">@aaron</a> …another posting in which your thoughts and relaying of information as to the status of the  Project are exemplary…thank you so much for your efforts and input… <img src="https://we.incognito.org/images/emoji/apple/sunglasses.png?v=9" title=":sunglasses:" class="emoji" alt=":sunglasses:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/2</link>
        <pubDate>Fri, 05 Mar 2021 14:39:23 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-10573-2</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573.rss">Letter to PRV Holders (February 2021)</source>
      </item>
      <item>
        <title>Letter to PRV Holders (February 2021)</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <p><a href="https://www.foxbusiness.com/markets/sec-nominee-gensler-bitcoin-cryptocurrency">Gary Gensler</a>, US president Joe Biden’s pick for chair of the SEC, is positioning himself as somewhat of a friend to crypto.</p>
<p>In his confirmation hearing, Gensler admitted his desire to regulate crypto to “protect” investors, while also suggesting his optimism about the positive impacts crypto and related technologies will have.  It’s a bit of a mixed bag, but hey, it’s the best we’ll get from an organization whose sole purpose is control (government).</p>
<p>The US is certainly not the center of the world, especially the crypto world, but what the US does in the next few years will without a doubt influence the stance much of the rest of the world takes toward cryptocurrency.</p>
<p>Based on the continuous support cryptoassets are getting from companies in the US, and the hesitant-but-open stance of US regulators, we’re looking at the beginning of a new era for cryptocurrency. That is, the era of mass adoption.</p>
<h2>Crypto is changing</h2>
<p>If millions of new crypto users are on their way, most of whom still think you have to buy Bitcoin in whole numbers, then we’re going to need to change the culture of crypto.</p>
<p>The primary conversations currently are arguments between two parties:</p>
<ul>
<li>Bulls vs bears</li>
<li>BTC maxis vs altcoiners</li>
<li>Etherheads vs altDeFi</li>
<li>Crypto vs doom &amp; gloom media</li>
</ul>
<p>But as soon as we hear the confused but enthusiastic grunts of the hordes of newcomers, we’ll be forced to stop and figure out how to onboard them all.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/3/37d68d8654a145195ae760f9a6a7d5030209c44c.jpeg" alt="image" data-base62-sha1="7XXWaq9eWiMQG7D44wEwqM7GyxC" width="576" height="432"><br>
(Praise to the artist, whose name I could not find)</p>
<p>Maybe there’s more than infighting, maybe I’m just spending too much time on Twitter (it’s <em>bad</em>, y’all).</p>
<p>But regardless, the times are a-changing, and crypto is moving from an insular industry of nerds to the workin’ (wo)man’s party.</p>
<p>Which brings me to what we’re going to do about it.</p>
<h2>Trust, transparency, and traction</h2>
<p>If you’re reading this letter, it’s probably because you have monk-like patience and are dedicated to privacy. We all know how much I write (my letters are getting longer, aren’t they?). And not only that, but you’ve had to do quite a bit of digging to understand the ins and outs of Incognito.</p>
<p>Not everyone can do that.</p>
<p>In February, we decided to fix that. We brainstormed how to transform the face of Incognito, to be even more transparent and easy to understand. We need to be transparent enough for crypto veterans to know the project is legit, and simple enough for anyone to start using it. It needs to be shareable, wearable, and  community-led.</p>
<p>So, we spent time gathering feedback from you, and brainstorming how to restructure all of the information we have available. In March and April, we’re going to take what we’ve learned and make the frontend of Incognito as effective as the backend.</p>
<p>It may have seemed like we slowed down in February, but nothing could be further from the truth. Just because Bitcoin dipped doesn’t mean we did <img src="https://we.incognito.org/images/emoji/apple/wink.png?v=9" title=":wink:" class="emoji" alt=":wink:"></p>
<p>As you’ll see below, February was the largest month of growth we’ve ever had, and given not only the coming project redesign but also the new features launching soon, that growth is likely only going to increase.</p>
<h1>February growth</h1>
<h2>Trading volume: +$77.6M</h2>
<blockquote>
<p>Previous month’s growth: +$44.5M</p>
</blockquote>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/9/92c535265255b3ac947412d50830016d9f11a93d.jpeg" alt="image" data-base62-sha1="kWoe0X2RI4wpQr9raMmawRkzutT" width="706" height="529"><br>
In January, anonymous trading volume nearly doubled. In February, it nearly doubled again. Average daily trading volume is between $2-3 million, with an all-time high in daily volume of more than $6 million.</p>
<p>Corporate adoption of crypto is a threat to personal privacy, because your data is valuable. To avoid being the product, people are trading their crypto anonymously more than ever. Let’s keep up the good work and continue sharing the pDEX with anyone who could use privacy.</p>
<h2>Shield volume: +$19M</h2>
<blockquote>
<p>Previous month’s growth: +$18M</p>
</blockquote>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/5/5c799a961402fb2384747a411e7a43ba5035a0cf.jpeg" alt="image" data-base62-sha1="dc4sFd4JvKn5Bnk1IRL1VVFsql1" width="706" height="529"><br>
Almost every month, Incognito grows faster than it did the month before. The last two months of shield volume growth account for about half of the current volume.</p>
<h2>Liquidity: +$14M</h2>
<blockquote>
<p>Previous month’s growth: +$4.5M</p>
</blockquote>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/7/7fe004f97a3acdcd80268adf72ef2538ea85c249.jpeg" alt="image" data-base62-sha1="ifeB5lH30wcIY6HzX6GbN7G9cRb" width="706" height="529"><br>
With daily trading volume at all-time highs, it’s important to have enough liquidity to enable people to trade freely. Liquidity in the pDEX is up $14 million from last month, a growth of 311%  compared to the previous month.</p>
<hr>
<h1>February development, March Goals</h1>
<p>I’ve been promising some key features for months now, and it looks like my word will be made tech (John 1:14, anyone? No?) very soon. Across all aspects of technical development, most features have moved beyond initial testing and into revision phases, preparing for launch.</p>
<p>Growth efforts were… forewent? foregoed? We decided to forego active growth efforts in favor of user research and planning for the redesign, so when we do seek new users, they’ll have easy access to all the information they need. <a class="mention" href="/u/elena">@Elena</a> did still send requests to listing sites like CMC, and I spoke with multiple journalists about coming feature PRs, but I decided to wait on them. We also welcomed new members to the growth team in February.</p>
<p>We still need your help as community leaders to create the community that will soon lead the project, but it’s not fair to ask you to do all the work, after all <img src="https://we.incognito.org/images/emoji/apple/wink.png?v=9" title=":wink:" class="emoji" alt=":wink:"></p>
<p>A few development highlights include:</p>
<h2><a href="https://we.incognito.org/t/app-v-5/9584">App v.5</a></h2>
<ul>
<li>v.4.1.4 is live, putting a few finishing touches on App v.4 before the launch of v.5</li>
<li>The web extension was released for beta testing</li>
<li>
<a href="https://we.incognito.org/t/incognito-ledger-integration/8504/17">Ledger integration</a> initial development has finished</li>
</ul>
<p><strong>March goals:</strong></p>
<ul>
<li>Improve shielding experience, allow shielding directly from metamask</li>
<li>Develop UI for Portal v4</li>
<li>Build a full Node service</li>
</ul>
<h2><a href="https://we.incognito.org/t/privacy-version-2/2967/16">Privacy v.2</a></h2>
<p>Increasing the scope of privacy is always a top priority. Privacy v.2 will launch by May. In February, key updates include:</p>
<ul>
<li>
<a href="https://we.incognito.org/t/confidential-asset/5607/9">Confidential assets</a> deployed in Testnet</li>
<li>Staking flow v.2 finished Testnet run and merged with code</li>
<li>Dynamic committee size in staking flow v.3 entered testing</li>
</ul>
<p><strong>March goals:</strong></p>
<ul>
<li>Finish CA devnet testing</li>
<li>Deploy multiview and staking flow v.2 to mainnet</li>
<li>Local testing for staking flow v.3</li>
<li>Deploy Privacy v.2 as a whole to testnet</li>
</ul>
<h2><a href="https://we.incognito.org/t/incognito-s-trustless-non-custodians-bridge-v4/9605/15">Bridges</a></h2>
<ul>
<li>Gas fee reduction was prepared for testnet deployment</li>
<li>Protocol design, shielding flow, and implementation of Bridge v.4 were finished</li>
<li>Review initial Ethereum audit and update for final Coinspect audit</li>
</ul>
<p><strong>March goals:</strong></p>
<ul>
<li>Refigure <a href="https://we.incognito.org/t/bridge-contract-gas-optimization-with-chi-gastoken/9788/7">gas reduction strategy</a>
</li>
<li>Finish, test, and deploy bridge v.4 on Privacy v.1</li>
</ul>
<h2><a href="https://we.incognito.org/t/build-faq-section-for-the-new-web/9737/12">Web v.3 and FAQ</a></h2>
<ul>
<li>FAQs were organized and answered by the core team, implementation held for the rest of web v.3</li>
<li>
<a href="http://Incognito.org">Incognito.org</a> was redesigned, a new site was wireframed.</li>
<li>Privacy user feedback survey was issued</li>
</ul>
<p><strong>March goals:</strong></p>
<ul>
<li>Parse survey responses and implement feedback</li>
<li>Finalize web v.3 redesign and code new site</li>
<li>Increase <a href="https://we.incognito.org/t/building-transparency-and-trust/10549">App Store reviews</a> and overall rating</li>
</ul>
<hr>
<h1>March PRV Holders Call</h1>
<p>The next PRV Holders Call is this Thursday/Friday! Join us for the usual progress presentation, and to discuss up-and-coming hot topics, such as:</p>
<ul>
<li>Security audit progress</li>
<li>Web v.3 - a new face for the project.</li>
<li>The product development roadmap.</li>
</ul>
<h2>Event details</h2>
<p>This month, we want to experiment with a new time for the call. Be sure to add it to your calendar!</p>
<blockquote>
<p><strong>Time</strong>: 2AM UTC | 9AM GMT+7 | 9PM EST | 6PM PST<br>
<strong>Date</strong>: Friday, March 5th, 2021 (For UTC &amp; GMT). Thursday, March 4th (For USA).<br>
<strong>Where: <a href="https://us02web.zoom.us/j/87168535851">https://us02web.zoom.us/j/87168535851</a></strong><br>
<strong>Livestream: Set a reminder for the <a href="https://youtu.be/mumaqzyIUGk">stream</a>.</strong></p>
</blockquote>
<p><a href="https://we.incognito.org/t/join-us-for-the-march-2021-prv-holders-call/10530">RSVP here</a>, and comment with topics you want to discuss on the call.</p>
<hr>
<h1>Don’t wait for next month -</h1>
<p>Get involved this month! We’re launching a bunch of new features and updates, and growing quickly. The community needs people like you that are familiar with the project to help guide the community forward. It won’t be long before the entire privacy movement will be 100% community-led, and we need to be prepared.</p>
<p>So, share your thoughts, questions, and ideas here, and let’s bring privacy to every crypto user on the planet together.</p>
<p>See you around!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573/1</link>
        <pubDate>Fri, 05 Mar 2021 01:44:16 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-10573-1</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-february-2021/10573.rss">Letter to PRV Holders (February 2021)</source>
      </item>
  </channel>
</rss>
