<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>[Product] Incognito universal swap</title>
    <link>https://we.incognito.org/t/product-incognito-universal-swap/17036</link>
    <description>#### [Unified privacy tokens and interoperable apps &gt;](https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914) 
#### [Privacy multichain: use-cases, caveats and solutions &gt;](https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873)

In the previous [topic](https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873), we’ve discussed the second major part of Incognito - the multichain (alongside privacy). In technical terms, it’s a capability of sending coins along with arbitrary messages from a blockchain to another but in the concrete real use-cases for end-users, it may be a universal swap function that will allow users to:

* Swap privacy coins (e.g. XMR, LTC, ZEC, DASH, …) with Incognito exchange’s liquidity pools. It can be seen that these privacy coins have been slowly delisted from CEXs and it’s barely to find the supported pools for those coins on other DEXs.

* Swap a coin from one blockchain to another through Incognito chain.
* Swap a coin on a source chain for another coin on a destination chain through Incognito chain. This is where sending arbitrary message capability shines since it allows the swap to utilize the external DEXs’ liquidity pools.
* Swap a coin for another with the best rate by aggregating liquidity from external DEXs across blockchains.

That sounds complicated, huh? No worries, from the end-user view, those will be merged into a dead-simple UI that depends on the swapping coins, it will fall into one of the above four cases.

Let’s check out the following UI of the web-based app to have better intuition.

## Deposit

To enter the privacy De-Fi world in the Incognito ecosystem, you need to deposit assets into Incognito chain first.

![|624x343](upload://15v2huiR1t52262MkTre1ZpIw3R.png)

On the web, we will still support two options for the deposit function like what you have experienced on the mobile app:

* Option 1: deposit from other platforms (like CEXs for example) by sending funds to a temporary address.

* Option 2: deposit directly to Incognito smart contracts from your own crypto wallets like Metamask.

In option (1), you just need to connect to [Incognito wallet extension](https://we.incognito.org/t/incognito-extension-alpha-version-is-now-released/16935) but in option (2), you will need to connect both Metamask and Incognito extensions. The former is used for sending your funds to Incognito smart contracts and the latter is used for receiving counterpart privacy coins.

## Swap

As aforementioned, on the same swap view, it depends on which token(s) and network(s) you are selecting to determine which case of swap (along with liquidity pool) the swap falls into.

![|624x343](upload://7MZmzBcg4OExlS8Ssy1QSWPCikP.png)

![|624x343](upload://tUozmHsVR0tuATPJ6xXB3nCV6wL.png)

For example:

* If you select XMR as ‘From token’, USDC as ‘To token’, Incognito chain for both ‘From and To networks’ → you’re swapping with Incognito exchange’s pools.
* If you select ETH for both ‘From and To token’, Incognito chain as ‘From network’ (after deposited from Ethereum network) and Polygon as ‘To network’ → you’re taking ETH out of Polygon’s vault to your Polygon address.

* If you select MATIC as ‘From token’, USDC as ‘To token’, Incognito chain as ‘From network’ and Polygon as ‘To network’ → your swap might be split into two smaller ones: one swap with liquidity pools of Uniswap and another swap with liquidity pools of QuickSwap on the same Polygon network. As a result, you will receive the two swapped USDC in your Polygon wallet.
* If you select USDC as ‘From token’, ETH as ‘To token’, Incognito chain for both ‘From and To network’ → your swap might be split into three smaller ones; they respectively swap with liquidity pools of Raydium on Solana, Uniswap on Polygon and PancakeSwap on BSC. Consequently, you will receive a unified privacy ETH from those smaller swaps in your Incognito wallet.

Please note that all these swaps are completely anonymous and the splits are for searching for the best rate of the initial swaps that also take fees into account.

## Closing thoughts

In this topic, we just go through the universal swap - the first product that comes from the foundational components we&#39;ve been developing for years now: privacy blockchain, bridges, Incognito AMM exchange and unified privacy token protocol. We do believe that despite of fragmentation of blockchains, their L2 and DEXs, the complicated things should be completely abstracted from user to user. What they need is an integrated place allowing them to swap tokens regardless of what happens behind the scene.

Also, with sending arbitrary messages capability, Incognito products should not be limited to swap (swap is the most popular usecase in DeFi now though) but will be able to extend to broader usercases such as universal lending or universal NFT marketplace, etc.

Thank you!</description>
    
    <lastBuildDate>Thu, 03 Nov 2022 13:12:09 +0000</lastBuildDate>
    <category>App users</category>
    <atom:link href="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <aside class="quote" data-post="10" data-topic="18659">
  <div class="title">
    <div class="quote-controls"></div>
    <img alt="" width="20" height="20" src="/user_avatar/we.incognito.org/jared/40/16605_2.png" class="avatar">
    <a href="https://we.incognito.org/t/issue-transaction-relaying-service-issues/18659/10">[Issue] Transaction Relaying Service Issues</a> 
  </div>
  <blockquote>
    <img src="https://we.incognito.org/images/emoji/apple/orange_circle.png?v=9" title="orange_circle" alt="orange_circle" class="emoji"> Attention App Users: 
The devs have fixed the issue with the Transaction Relaying Service that affected trades and shields/unshields in the app.  The backend is resyncing the data and once complete will process pending TXs automatically.
  </blockquote>
</aside>

          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/12">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/12</link>
        <pubDate>Thu, 03 Nov 2022 13:12:09 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-12</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@freedom45]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/freedom45">@freedom45</a> wrote:</p>
          <blockquote>
              <p>Anyway I can fix it in my end?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/11">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/11</link>
        <pubDate>Thu, 03 Nov 2022 13:11:13 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-11</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@freedom45]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/freedom45">@freedom45</a> wrote:</p>
          <blockquote>
              <p>I need a reply when all the bugs are fix. I’ve never seen this kind of problems with your program</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/10">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/10</link>
        <pubDate>Thu, 03 Nov 2022 13:10:33 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-10</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@abduraman Abduraman Blackram]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/abduraman">@abduraman</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="freedom45" data-post="8" data-topic="17036">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/freedom45/40/15481_2.png" class="avatar"> freedom45:</div>
<blockquote>
<p>Ive seen prv remain at .3466 since yesterday</p>
</blockquote>
</aside>
<p>It is normal since <img src="https://we.incognito.org/images/emoji/apple/point_down.png?v=9" title=":point_down:" class="emoji" alt=":point_down:"></p>
<aside class="quote" data-post="9" data-topic="18659">
  <div class="title">
    <div class="quote-controls"></div>
    <img alt="" width="20" height="20" src="/user_avatar/we.incognito.org/abduraman/40/832_2.png" class="avatar">
    <a href="https://we.incognito.org/t/issue-transaction-relaying-service-issues/18659/9">[Issue] Transaction Relaying Service Issues</a> 
  </div>
  <blockquote>
    No trade transaction =&gt; no change in pDEX state =&gt; frozen PRV price
  </blockquote>
</aside>

          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/9">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/9</link>
        <pubDate>Thu, 03 Nov 2022 12:33:08 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-9</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@freedom45]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/freedom45">@freedom45</a> wrote:</p>
          <blockquote>
              <p>I also notice there is no update change in all crypto coins. Ive seen prv remain at .3466 since yesterday</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/8">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/8</link>
        <pubDate>Thu, 03 Nov 2022 12:25:13 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-8</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@freedom45]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/freedom45">@freedom45</a> wrote:</p>
          <blockquote>
              <p>Gotcha</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/7">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/7</link>
        <pubDate>Thu, 03 Nov 2022 02:31:53 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-7</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p>Please reference this post:</p>
<aside class="quote" data-post="1" data-topic="18659">
  <div class="title">
    <div class="quote-controls"></div>
    <img alt="" width="20" height="20" src="/user_avatar/we.incognito.org/jared/40/16605_2.png" class="avatar">
    <a href="https://we.incognito.org/t/issue-transaction-relaying-service-issues/18659/">[Issue] Transaction Relaying Service Issues</a> 
  </div>
  <blockquote>
    We have detected an issue with the Transaction Relaying Service and the devs are looking into this issue. This impacts transactions preventing them from possibly going through. We will keep you updated both here and in the app once things are back to normal.
  </blockquote>
</aside>

          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/6</link>
        <pubDate>Thu, 03 Nov 2022 01:11:47 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-6</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@freedom45]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/freedom45">@freedom45</a> wrote:</p>
          <blockquote>
              <p>I have over 6 failed attempts at swap, first time is ever happened. I am trying to swap prv to xmr with no sucess.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/5</link>
        <pubDate>Thu, 03 Nov 2022 01:01:22 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-5</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <p>Yes, exactly…<br>
We understand that’s a tradeoff for UX (and even trust since new users may be concerned if they have to deposit funds to a new platform). We like the idea you proposed earlier with a seamless experience from end to end.<br>
But introducing a non-privacy flow/feature to a privacy-first platform like Incognito, is a big change, it breaks what we (and the community) have built for years <img src="https://we.incognito.org/images/emoji/apple/wink.png?v=9" title=":wink:" class="emoji" alt=":wink:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/4</link>
        <pubDate>Wed, 13 Jul 2022 11:39:25 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-4</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@abduraman Abduraman Blackram]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/abduraman">@abduraman</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="duc" data-post="1" data-topic="17036">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/duc/40/357_2.png" class="avatar"> duc:</div>
<blockquote>
<p>We do believe that despite of fragmentation of blockchains, their L2 and DEXs, the complicated things should be completely abstracted from user to user.</p>
</blockquote>
</aside>
<p>Well said <img src="https://we.incognito.org/images/emoji/apple/black_heart.png?v=9" title=":black_heart:" class="emoji" alt=":black_heart:"></p>
<p>I also have a question. At least one side (from or to) of Swap feature always be Incognito, won’t it?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/3</link>
        <pubDate>Wed, 13 Jul 2022 10:47:12 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-3</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/2</link>
        <pubDate>Wed, 13 Jul 2022 04:12:27 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-2</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
      <item>
        <title>[Product] Incognito universal swap</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <h4><a href="https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914">Unified privacy tokens and interoperable apps &gt;</a></h4>
<h4><a href="https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873">Privacy multichain: use-cases, caveats and solutions &gt;</a></h4>
<p>In the previous <a href="https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873">topic</a>, we’ve discussed the second major part of Incognito - the multichain (alongside privacy). In technical terms, it’s a capability of sending coins along with arbitrary messages from a blockchain to another but in the concrete real use-cases for end-users, it may be a universal swap function that will allow users to:</p>
<ul>
<li>
<p>Swap privacy coins (e.g. XMR, LTC, ZEC, DASH, …) with Incognito exchange’s liquidity pools. It can be seen that these privacy coins have been slowly delisted from CEXs and it’s barely to find the supported pools for those coins on other DEXs.</p>
</li>
<li>
<p>Swap a coin from one blockchain to another through Incognito chain.</p>
</li>
<li>
<p>Swap a coin on a source chain for another coin on a destination chain through Incognito chain. This is where sending arbitrary message capability shines since it allows the swap to utilize the external DEXs’ liquidity pools.</p>
</li>
<li>
<p>Swap a coin for another with the best rate by aggregating liquidity from external DEXs across blockchains.</p>
</li>
</ul>
<p>That sounds complicated, huh? No worries, from the end-user view, those will be merged into a dead-simple UI that depends on the swapping coins, it will fall into one of the above four cases.</p>
<p>Let’s check out the following UI of the web-based app to have better intuition.</p>
<h2>Deposit</h2>
<p>To enter the privacy De-Fi world in the Incognito ecosystem, you need to deposit assets into Incognito chain first.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/0/7/07a15a0bd05daf2c37f2a9dffd35f92733ac603f.png" data-download-href="/uploads/short-url/15v2huiR1t52262MkTre1ZpIw3R.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/07a15a0bd05daf2c37f2a9dffd35f92733ac603f_2_624x343.png" alt data-base62-sha1="15v2huiR1t52262MkTre1ZpIw3R" width="624" height="343" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/07a15a0bd05daf2c37f2a9dffd35f92733ac603f_2_624x343.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/07a15a0bd05daf2c37f2a9dffd35f92733ac603f_2_936x514.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/07a15a0bd05daf2c37f2a9dffd35f92733ac603f_2_1248x686.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/0/7/07a15a0bd05daf2c37f2a9dffd35f92733ac603f_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1488×818 212 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p>On the web, we will still support two options for the deposit function like what you have experienced on the mobile app:</p>
<ul>
<li>
<p>Option 1: deposit from other platforms (like CEXs for example) by sending funds to a temporary address.</p>
</li>
<li>
<p>Option 2: deposit directly to Incognito smart contracts from your own crypto wallets like Metamask.</p>
</li>
</ul>
<p>In option (1), you just need to connect to <a href="https://we.incognito.org/t/incognito-extension-alpha-version-is-now-released/16935">Incognito wallet extension</a> but in option (2), you will need to connect both Metamask and Incognito extensions. The former is used for sending your funds to Incognito smart contracts and the latter is used for receiving counterpart privacy coins.</p>
<h2>Swap</h2>
<p>As aforementioned, on the same swap view, it depends on which token(s) and network(s) you are selecting to determine which case of swap (along with liquidity pool) the swap falls into.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/3/6/3698e5ad8921a2d84cd38adb370da74690a1c093.png" data-download-href="/uploads/short-url/7MZmzBcg4OExlS8Ssy1QSWPCikP.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3698e5ad8921a2d84cd38adb370da74690a1c093_2_624x343.png" alt data-base62-sha1="7MZmzBcg4OExlS8Ssy1QSWPCikP" width="624" height="343" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3698e5ad8921a2d84cd38adb370da74690a1c093_2_624x343.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3698e5ad8921a2d84cd38adb370da74690a1c093_2_936x514.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3698e5ad8921a2d84cd38adb370da74690a1c093_2_1248x686.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3698e5ad8921a2d84cd38adb370da74690a1c093_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1492×820 211 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/d/1/d19f0eb7942ecdabb10f665dfd1eac4789730dff.png" data-download-href="/uploads/short-url/tUozmHsVR0tuATPJ6xXB3nCV6wL.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/1/d19f0eb7942ecdabb10f665dfd1eac4789730dff_2_624x343.png" alt data-base62-sha1="tUozmHsVR0tuATPJ6xXB3nCV6wL" width="624" height="343" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/1/d19f0eb7942ecdabb10f665dfd1eac4789730dff_2_624x343.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/1/d19f0eb7942ecdabb10f665dfd1eac4789730dff_2_936x514.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/1/d19f0eb7942ecdabb10f665dfd1eac4789730dff_2_1248x686.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/d/1/d19f0eb7942ecdabb10f665dfd1eac4789730dff_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1488×816 210 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p>For example:</p>
<ul>
<li>
<p>If you select XMR as ‘From token’, USDC as ‘To token’, Incognito chain for both ‘From and To networks’ → you’re swapping with Incognito exchange’s pools.</p>
</li>
<li>
<p>If you select ETH for both ‘From and To token’, Incognito chain as ‘From network’ (after deposited from Ethereum network) and Polygon as ‘To network’ → you’re taking ETH out of Polygon’s vault to your Polygon address.</p>
</li>
<li>
<p>If you select MATIC as ‘From token’, USDC as ‘To token’, Incognito chain as ‘From network’ and Polygon as ‘To network’ → your swap might be split into two smaller ones: one swap with liquidity pools of Uniswap and another swap with liquidity pools of QuickSwap on the same Polygon network. As a result, you will receive the two swapped USDC in your Polygon wallet.</p>
</li>
<li>
<p>If you select USDC as ‘From token’, ETH as ‘To token’, Incognito chain for both ‘From and To network’ → your swap might be split into three smaller ones; they respectively swap with liquidity pools of Raydium on Solana, Uniswap on Polygon and PancakeSwap on BSC. Consequently, you will receive a unified privacy ETH from those smaller swaps in your Incognito wallet.</p>
</li>
</ul>
<p>Please note that all these swaps are completely anonymous and the splits are for searching for the best rate of the initial swaps that also take fees into account.</p>
<h2>Closing thoughts</h2>
<p>In this topic, we just go through the universal swap - the first product that comes from the foundational components we’ve been developing for years now: privacy blockchain, bridges, Incognito AMM exchange and unified privacy token protocol. We do believe that despite of fragmentation of blockchains, their L2 and DEXs, the complicated things should be completely abstracted from user to user. What they need is an integrated place allowing them to swap tokens regardless of what happens behind the scene.</p>
<p>Also, with sending arbitrary messages capability, Incognito products should not be limited to swap (swap is the most popular usecase in DeFi now though) but will be able to extend to broader usercases such as universal lending or universal NFT marketplace, etc.</p>
<p>Thank you!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/product-incognito-universal-swap/17036/1</link>
        <pubDate>Wed, 13 Jul 2022 03:50:54 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-17036-1</guid>
        <source url="https://we.incognito.org/t/product-incognito-universal-swap/17036.rss">[Product] Incognito universal swap</source>
      </item>
  </channel>
</rss>
