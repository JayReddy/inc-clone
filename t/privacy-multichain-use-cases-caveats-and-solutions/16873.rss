<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Privacy multichain: use-cases, caveats and solutions</title>
    <link>https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873</link>
    <description>### [Unified privacy tokens and interoperable apps &gt;](https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914) 

Incognito’s privacy multichain protocol is solving a key problem Incognito platform is facing as described in the [previous topic](https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914): inconvenient experience in usability as swapping with Incognito exchange’s fragmented liquidity. But more than that, in this topic let’s explore an important use-case that the protocol is bringing to our privacy supporters: a possibility to send an arbitrary message from a blockchain to another with anonymity.

### Desires of sending arbitrary messages across blockchains

The past few years have seen a surge in the number of sidechains, rollups, and Layer-1 blockchains which support and scale applications handling billions of dollars worth of value. The multichain future is here, but it requires a protocol that enables interoperability and composability across chains, while unlocking new areas of design and possibility in cryptonetworks.

Incognito privacy multichain is a protocol that will enable users to send tokens or any kind of message in a single transaction across applications that live on various chains and is generalizable enough to run on any chain.

Let’s check out some popular use-cases via the illustrations:

#### :pushpin: Cross-chain DEX

![|624x243](upload://br8ph9DKLPkqjr0zrZ9UKzc86k2.png)
*Message 1: sending USDC from a Ethereum to Polygon*

![|624x232](upload://vZK5SktyKTioFRLKdeolczKOuSA.png)
*Message 2: swapping USDC from Ethereum for MATIC with Polygon’s Uniswap*

The protocol enables a cross-chain DEX that play directly with native assets, an app using Incognito multichain protocol to send messages between chains can be built such that liquidity pools exist on both chains, and users can simply deposit their native assets in one pool and withdraw native assets from another (message 1).

Furthermore, the users can deposit their native assets in one pool on a source chain and then use the native assets from another pool to swap for other assets with a DEX on a destination chain (message 2).

#### :pushpin: Multi-chain lending

Similarly, for example, a user with USDC on Ethereum wants to take advantage of an opportunity on Polygon. The protocol allows the user to lend his USDC out then directly borrow in MATIC with a lending dapp on Polygon.

![|624x247](upload://szx0PbTwL6wXS0D2g6RcPXJLMi1.png)

Message 3: lending USDC from Ethereum out to borrow MATIC with a lending dapp on Polygon

#### :pushpin: Multi-chain aggregation

Currently, aggregators typically operate within the area of single chain ecosystems. One downside of those single chain aggregation systems is that they can not take advantage of any yield opportunities (for yield aggregators like Yearn Finance) or liquidity (for liquidity aggregators like 1Inch) outside of their current ecosystem, potentially missing out on many of the best yields or rates respectively.

A multi-chain aggregator would be strictly better than a single chain aggregator, as in the worst case the strategy would degrade to taking advantages on only one chain, and in the best case it would have exponentially more opportunities to choose from.

These three examples represent typical (but not exhausted) possibilities that the protocol enables developers to write their applications without worrying about differing semantics between inter- and intra-chain transactions.

***Notes**: Thanks for reading till here, although you could feel that the previous section repeats some parts we discussed in [the topic](https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914), we believe that it deserves to state it again in a more generalized way in order to clear out the potentials of the protocol. Also, users will be able to choose what to do with their resulting assets: either unlock to a destination chain’s address or re-deposit to Incognito to re-achieve privacy. In the above figures, we only illustrated the former flow for more simple illustration.*

### Caveats

One advantage of the protocol is that it won’t issue wrapped tokens on the destination chains like existing bridge solutions but allow users to use directly native assets on the destinations for their desires. But this is introducing a foreseeable problem we need to address: lack of liquidity (the native assets) on the destination chains.

In an ideal situation, we assume different people with different desires will eventually “serve” each other but in an extreme case where a liquidity pool has much more demand than others, it likely that pool’s liquidity will be shortage very soon and without liquidity, the above use-cases are not possible.

### Solutions

Lack of liquidity on chains with higher demand is also a question raised a couple of times by the community members and we’ve discussed a few solutions to attack the problem in different lens and approaches. It looks like there is no a sort of silver bullet for it, each has its own pros and cons.

#### :pushpin: On-demand serving

This seems to be a quick and practical solution - once one wants to use liquidity of a chain but the pool doesn’t has enough liquidity, he could either: (1) choose to use another liquidity pool on different chain that has sufficient liquidity or (2) place an order into a queue along with a reward to incentivise someone who willing to provide the needed liquidity to serve the user’s request.

The idea is super simple, for example, a user A wants to send 100k USDC from Polygon to BSC, but there is only 40k USDC in BSC’s liquidity pool and needs another 60k USDC to be able to fullfil the demand. The user would place an order to request 60k USDC into a request queue and willing to pay 0.04% of the 60K USDC to anyone who help to serve the order.

User B who has 10k USDC could repeat 6 times a process of:

* **Step 1**: Deposit his 10k USDC to a CEX (eg. Binance).

* **Step 2**: Deposit the 10k USDC from that CEX to Incognito via BSC bridge.

* **Step 3**: Withdraw the 10k USDC from Fantom pool back to the CEX via Fantom bridge (assume that Fantom pool has USDC liquidity).

When the user B repeats the process 6 times that costs him:

6 * $1.5 (Incognito deposit &amp; withwrawal fees) + 6 * $1 (CEX withdrarwal fees) = $15
And he would be able to serve the order of the user A with a profit of $60k * 0.04% - $15 = $9

(Please note that the 0.04% above is a parameter that is subject to change over time to balance out the motivation of those two actor types, aka the user A &amp; B in the above example)

#### :pushpin: Motivation for liquidity provision

While the on-demand serving is designed to attack directly the lack of liquidity problem, it’s not perfect since it passively waits for liquidity demand to be present through liquidity orders. The UX doesn’t seem to be smooth where users have to wait for their orders to be fulfilled as well as suffer the fees.

For those reasons, having incentive for people to actively provide liquidity to pools would be a perfect complement to the on-demand serving. We’re going discuss the initiative in the next topic to avoid making this one mega long.

### Closing thoughts

We’ve just presented an overview for the privacy multichain protocol’s design and its applications. Apart from the motivation for liquidity provision that we owe you as stated above, you could also realize that there would always be a tradeoff between UX convenience and privacy (even decentralization sometimes), we are trying to figure out a way to balance out the tradeoff but always look forward to discussing with you about that, feel free to drop your comments below, this would absolutely be an interesting discussion we believe.

Thank you!

### [[Product] Incognito universal swap &gt;](https://we.incognito.org/t/product-incognito-universal-swap/17036)</description>
    
    <lastBuildDate>Wed, 13 Jul 2022 03:54:26 +0000</lastBuildDate>
    <category>Interoperability</category>
    <atom:link href="https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Privacy multichain: use-cases, caveats and solutions</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <h3><a href="https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914">Unified privacy tokens and interoperable apps &gt;</a></h3>
<p>Incognito’s privacy multichain protocol is solving a key problem Incognito platform is facing as described in the <a href="https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914">previous topic</a>: inconvenient experience in usability as swapping with Incognito exchange’s fragmented liquidity. But more than that, in this topic let’s explore an important use-case that the protocol is bringing to our privacy supporters: a possibility to send an arbitrary message from a blockchain to another with anonymity.</p>
<h3>Desires of sending arbitrary messages across blockchains</h3>
<p>The past few years have seen a surge in the number of sidechains, rollups, and Layer-1 blockchains which support and scale applications handling billions of dollars worth of value. The multichain future is here, but it requires a protocol that enables interoperability and composability across chains, while unlocking new areas of design and possibility in cryptonetworks.</p>
<p>Incognito privacy multichain is a protocol that will enable users to send tokens or any kind of message in a single transaction across applications that live on various chains and is generalizable enough to run on any chain.</p>
<p>Let’s check out some popular use-cases via the illustrations:</p>
<h4>
<img src="https://we.incognito.org/images/emoji/apple/pushpin.png?v=9" title=":pushpin:" class="emoji" alt=":pushpin:"> Cross-chain DEX</h4>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/5/0/50292bf75b67e6b49d0665829968f7b065d82192.png" data-download-href="/uploads/short-url/br8ph9DKLPkqjr0zrZ9UKzc86k2.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/0/50292bf75b67e6b49d0665829968f7b065d82192_2_624x243.png" alt data-base62-sha1="br8ph9DKLPkqjr0zrZ9UKzc86k2" width="624" height="243" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/0/50292bf75b67e6b49d0665829968f7b065d82192_2_624x243.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/0/50292bf75b67e6b49d0665829968f7b065d82192_2_936x364.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/0/50292bf75b67e6b49d0665829968f7b065d82192_2_1248x486.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/5/0/50292bf75b67e6b49d0665829968f7b065d82192_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1600×623 130 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><br>
<em>Message 1: sending USDC from a Ethereum to Polygon</em></p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/e/0/e03e24af1195951806827a23298bdc57d609a720.png" data-download-href="/uploads/short-url/vZK5SktyKTioFRLKdeolczKOuSA.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/e/0/e03e24af1195951806827a23298bdc57d609a720_2_624x232.png" alt data-base62-sha1="vZK5SktyKTioFRLKdeolczKOuSA" width="624" height="232" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/e/0/e03e24af1195951806827a23298bdc57d609a720_2_624x232.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/e/0/e03e24af1195951806827a23298bdc57d609a720_2_936x348.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/e/0/e03e24af1195951806827a23298bdc57d609a720_2_1248x464.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/e/0/e03e24af1195951806827a23298bdc57d609a720_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1600×594 136 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div><br>
<em>Message 2: swapping USDC from Ethereum for MATIC with Polygon’s Uniswap</em></p>
<p>The protocol enables a cross-chain DEX that play directly with native assets, an app using Incognito multichain protocol to send messages between chains can be built such that liquidity pools exist on both chains, and users can simply deposit their native assets in one pool and withdraw native assets from another (message 1).</p>
<p>Furthermore, the users can deposit their native assets in one pool on a source chain and then use the native assets from another pool to swap for other assets with a DEX on a destination chain (message 2).</p>
<h4>
<img src="https://we.incognito.org/images/emoji/apple/pushpin.png?v=9" title=":pushpin:" class="emoji" alt=":pushpin:"> Multi-chain lending</h4>
<p>Similarly, for example, a user with USDC on Ethereum wants to take advantage of an opportunity on Polygon. The protocol allows the user to lend his USDC out then directly borrow in MATIC with a lending dapp on Polygon.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/c/8/c8411ec09befbc6566dca5132f35cecbfb776fd5.png" data-download-href="/uploads/short-url/szx0PbTwL6wXS0D2g6RcPXJLMi1.png?dl=1" title=""><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/8/c8411ec09befbc6566dca5132f35cecbfb776fd5_2_624x247.png" alt data-base62-sha1="szx0PbTwL6wXS0D2g6RcPXJLMi1" width="624" height="247" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/8/c8411ec09befbc6566dca5132f35cecbfb776fd5_2_624x247.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/8/c8411ec09befbc6566dca5132f35cecbfb776fd5_2_936x370.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/8/c8411ec09befbc6566dca5132f35cecbfb776fd5_2_1248x494.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/8/c8411ec09befbc6566dca5132f35cecbfb776fd5_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename"></span><span class="informations">1600×631 145 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p>Message 3: lending USDC from Ethereum out to borrow MATIC with a lending dapp on Polygon</p>
<h4>
<img src="https://we.incognito.org/images/emoji/apple/pushpin.png?v=9" title=":pushpin:" class="emoji" alt=":pushpin:"> Multi-chain aggregation</h4>
<p>Currently, aggregators typically operate within the area of single chain ecosystems. One downside of those single chain aggregation systems is that they can not take advantage of any yield opportunities (for yield aggregators like Yearn Finance) or liquidity (for liquidity aggregators like 1Inch) outside of their current ecosystem, potentially missing out on many of the best yields or rates respectively.</p>
<p>A multi-chain aggregator would be strictly better than a single chain aggregator, as in the worst case the strategy would degrade to taking advantages on only one chain, and in the best case it would have exponentially more opportunities to choose from.</p>
<p>These three examples represent typical (but not exhausted) possibilities that the protocol enables developers to write their applications without worrying about differing semantics between inter- and intra-chain transactions.</p>
<p><em><strong>Notes</strong>: Thanks for reading till here, although you could feel that the previous section repeats some parts we discussed in <a href="https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914">the topic</a>, we believe that it deserves to state it again in a more generalized way in order to clear out the potentials of the protocol. Also, users will be able to choose what to do with their resulting assets: either unlock to a destination chain’s address or re-deposit to Incognito to re-achieve privacy. In the above figures, we only illustrated the former flow for more simple illustration.</em></p>
<h3>Caveats</h3>
<p>One advantage of the protocol is that it won’t issue wrapped tokens on the destination chains like existing bridge solutions but allow users to use directly native assets on the destinations for their desires. But this is introducing a foreseeable problem we need to address: lack of liquidity (the native assets) on the destination chains.</p>
<p>In an ideal situation, we assume different people with different desires will eventually “serve” each other but in an extreme case where a liquidity pool has much more demand than others, it likely that pool’s liquidity will be shortage very soon and without liquidity, the above use-cases are not possible.</p>
<h3>Solutions</h3>
<p>Lack of liquidity on chains with higher demand is also a question raised a couple of times by the community members and we’ve discussed a few solutions to attack the problem in different lens and approaches. It looks like there is no a sort of silver bullet for it, each has its own pros and cons.</p>
<h4>
<img src="https://we.incognito.org/images/emoji/apple/pushpin.png?v=9" title=":pushpin:" class="emoji" alt=":pushpin:"> On-demand serving</h4>
<p>This seems to be a quick and practical solution - once one wants to use liquidity of a chain but the pool doesn’t has enough liquidity, he could either: (1) choose to use another liquidity pool on different chain that has sufficient liquidity or (2) place an order into a queue along with a reward to incentivise someone who willing to provide the needed liquidity to serve the user’s request.</p>
<p>The idea is super simple, for example, a user A wants to send 100k USDC from Polygon to BSC, but there is only 40k USDC in BSC’s liquidity pool and needs another 60k USDC to be able to fullfil the demand. The user would place an order to request 60k USDC into a request queue and willing to pay 0.04% of the 60K USDC to anyone who help to serve the order.</p>
<p>User B who has 10k USDC could repeat 6 times a process of:</p>
<ul>
<li>
<p><strong>Step 1</strong>: Deposit his 10k USDC to a CEX (eg. Binance).</p>
</li>
<li>
<p><strong>Step 2</strong>: Deposit the 10k USDC from that CEX to Incognito via BSC bridge.</p>
</li>
<li>
<p><strong>Step 3</strong>: Withdraw the 10k USDC from Fantom pool back to the CEX via Fantom bridge (assume that Fantom pool has USDC liquidity).</p>
</li>
</ul>
<p>When the user B repeats the process 6 times that costs him:</p>
<p>6 * $1.5 (Incognito deposit &amp; withwrawal fees) + 6 * $1 (CEX withdrarwal fees) = $15<br>
And he would be able to serve the order of the user A with a profit of $60k * 0.04% - $15 = $9</p>
<p>(Please note that the 0.04% above is a parameter that is subject to change over time to balance out the motivation of those two actor types, aka the user A &amp; B in the above example)</p>
<h4>
<img src="https://we.incognito.org/images/emoji/apple/pushpin.png?v=9" title=":pushpin:" class="emoji" alt=":pushpin:"> Motivation for liquidity provision</h4>
<p>While the on-demand serving is designed to attack directly the lack of liquidity problem, it’s not perfect since it passively waits for liquidity demand to be present through liquidity orders. The UX doesn’t seem to be smooth where users have to wait for their orders to be fulfilled as well as suffer the fees.</p>
<p>For those reasons, having incentive for people to actively provide liquidity to pools would be a perfect complement to the on-demand serving. We’re going discuss the initiative in the next topic to avoid making this one mega long.</p>
<h3>Closing thoughts</h3>
<p>We’ve just presented an overview for the privacy multichain protocol’s design and its applications. Apart from the motivation for liquidity provision that we owe you as stated above, you could also realize that there would always be a tradeoff between UX convenience and privacy (even decentralization sometimes), we are trying to figure out a way to balance out the tradeoff but always look forward to discussing with you about that, feel free to drop your comments below, this would absolutely be an interesting discussion we believe.</p>
<p>Thank you!</p>
<h3><a href="https://we.incognito.org/t/product-incognito-universal-swap/17036">[Product] Incognito universal swap &gt;</a></h3>
          </blockquote>
          <p><a href="https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873/1</link>
        <pubDate>Tue, 07 Jun 2022 11:58:57 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16873-1</guid>
        <source url="https://we.incognito.org/t/privacy-multichain-use-cases-caveats-and-solutions/16873.rss">Privacy multichain: use-cases, caveats and solutions</source>
      </item>
  </channel>
</rss>
