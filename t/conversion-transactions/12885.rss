<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Conversion Transactions</title>
    <link>https://we.incognito.org/t/conversion-transactions/12885</link>
    <description>## [Introduction to Privacy V2 ▸](https://we.incognito.org/t/introduction-to-privacy-v2/12776)
## [Full-nodes&#39; Cache - A Faster Way to Retrieve Users&#39; Output Coins ▸](https://we.incognito.org/t/full-nodes-cache-a-faster-way-to-retrieve-users-output-coins/12810)

Hi Incognito users,

It&#39;s time for Privacy V2 again. In the previous post, we have talked about how a UTXO v2 is different from a UTXO v1 in its public key, and in the way to retrieve it at the full-node side. Today, we&#39;ll explain how to convert UTXOs v1 to  v2 to use them in transactions v2.

## What is a conversion transaction?
A conversion transaction is a special transaction that converts a list of UTXOs in version 1 into a UTXO in version 2. The UTXOs could be PRV or tokens. 

As Privacy V2 is coming in a few days, we&#39;ll see more and more transactions v2 on the network. If you miss how crazy Privacy V2 is, visit this [post](https://we.incognito.org/t/introduction-to-privacy-v2/12776).  Each transaction v2 consumes a list of UTXOs v2 as input and produces a list of UTXOs v2. Therefore, conversion is mandatory for those who still have UTXOs v1 and wish to use them in v2 to enhance privacy. Besides, the Incognito network will soon not accept transactions of v1. Therefore, UTXOs conversion is a must.

A conversion transaction is (by default) *non-private*. All information (sender, amount, tokenID) will be publicly visible. Its properties are summarized in the following table.

Property|Description|Note
--|--|---
Hide amount| NO|
Hide sender| NO|
Hide receiver| YES | One-time address
Hide asset type | No 
*Table. A summary of conversion transactions. Although the receiver is hidden by a one-time address, in most cases, the receiver of this transaction is also the sender.*

## How to convert your UTXOs
### Using the Incognito app
Firstly, navigate to the **Asset** screen.
![Screen Shot 2021-07-09 at 6.56.48 PM|944x1684, 50%](upload://zPawkhq6pWKKgiLvbe1dsVB1PkK.png) 

If you still have UTXOs v1, there will be a little message at the bottom of the screen. Hit on that message. A converting screen will appear.
![Screen Shot 2021-07-09 at 6.55.59 PM|942x1654, 50%](upload://1wxXBa9BwAXFuR1pywgPY2IXxv2.png) 

A list of tokens with UTXOs v1 will be displayed. Hit on **Go to convert screen** button.
![image|934x1694, 50%](upload://rv46fseuXCVwXNJzzub5o3kRqVe.png) 

Now, click the **Convert** button and wait until the process finishes. When the process is successful, the **Convert** screen will look like the following
![image|942x1682, 50%](upload://fzwmXjw6ihIlG80yl5P9kfBcvN3.png) 

Note that a conversion transaction consumes at most 30 UTXOs v1 at a time. In case you have a huge number of UTXOs v1, you have to spend an enormous amount of time. Another solution is to consolidate these UTXOs into a smaller set (less than 30) before converting time. The consolidating function is also available on the App as well as on the [CLI tool](https://github.com/incognitochain/incognito-cli#consolidate).

However, the consolidation only works before the `PrivacyV2BreakPoint` takes effect because v1 transactions will be rejected after that time. More detail on this will be discussed in a different post.  To avoid this annoying problem, you can use the [CLI tool](https://github.com/incognitochain/incognito-cli) to convert all of your UTXOs with a single command.

### Using the CLI tool
Converting UTXOs on the Incognito app is a costly operation if you have a lot of UTXOs  v1. Users will have to wait for an enormous amount of time before this operation finishes. Therefore, have built a very simple [command-line application](https://github.com/incognitochain/incognito-cli) for some basic functionalities, including converting UTXOs. The instruction on how to use this tool can be found [here](https://github.com/incognitochain/incognito-cli#convert). 

```shell
$ incognito-cli help convert
NAME:
   incognito-cli convert - Convert UTXOs of an account w.r.t a tokenID.

USAGE:
   convert --privateKey PRIVATE_KEY [--tokenID TOKEN_ID] [--numThreads NUM_THREADS] [--enableLog ENABLE_LOG] [--logFile LOG_FILE]

   OPTIONAL flags are denoted by a [] bracket.

CATEGORY:
   TRANSACTIONS

DESCRIPTION:
   This function helps convert UTXOs v1 of a user to UTXO v2 w.r.t a tokenID. Please note that this process is time-consuming and requires a considerable amount of CPU.

OPTIONS:
   --privateKey value, --prvKey value  a base58-encoded private key
   --tokenID value                     ID of the token (default: &quot;0000000000000000000000000000000000000000000000000000000000000004&quot;)
   --numThreads value                  number of threads used in this action (default: 4)
   --enableLog                         enable log for this action (default: false)
   --logFile value                     location of the log file (default: &quot;os.Stdout&quot;)
```
Suppose your private key is `` and you want to convert the token `` using `10` threads.  Just run the following command, and wait.
```shell
$ incognito-cli convert --privateKey YOUR_PRIVATE_KEY --tokenID --numThreads 10
```

## Conclusion
Conversion transactions are the portal to the [Privacy V2](https://we.incognito.org/t/introduction-to-privacy-v2/12776) world where you can truly go incognito. However, the way to that utopian world costs you some sort of privacy. This amount of privacy is just the same as a regular v1 trade transaction or a shielding, un-shielding transaction that you have been doing until now. Therefore, in the end, it does not cost that much. In the next post, we&#39;ll explain how [Privacy V2](https://we.incognito.org/t/introduction-to-privacy-v2/12776) affects pDEX trading transactions.

## [pDEX Trade with Privacy V2 ▸](https://we.incognito.org/t/pdex-trade-with-privacy-v2/12957)</description>
    
    <lastBuildDate>Sat, 10 Jul 2021 10:01:36 +0000</lastBuildDate>
    <category>Privacy</category>
    <atom:link href="https://we.incognito.org/t/conversion-transactions/12885.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Conversion Transactions</title>
        <dc:creator><![CDATA[@khanhj]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/khanhj">@khanhj</a> wrote:</p>
          <blockquote>
              
          </blockquote>
          <p><a href="https://we.incognito.org/t/conversion-transactions/12885/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/conversion-transactions/12885/2</link>
        <pubDate>Mon, 12 Jul 2021 02:33:59 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-12885-2</guid>
        <source url="https://we.incognito.org/t/conversion-transactions/12885.rss">Conversion Transactions</source>
      </item>
      <item>
        <title>Conversion Transactions</title>
        <dc:creator><![CDATA[@daniel]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/daniel">@daniel</a> wrote:</p>
          <blockquote>
              <h2><a href="https://we.incognito.org/t/introduction-to-privacy-v2/12776">Introduction to Privacy V2 ▸</a></h2>
<h2><a href="https://we.incognito.org/t/full-nodes-cache-a-faster-way-to-retrieve-users-output-coins/12810">Full-nodes’ Cache - A Faster Way to Retrieve Users’ Output Coins ▸</a></h2>
<p>Hi Incognito users,</p>
<p>It’s time for Privacy V2 again. In the previous post, we have talked about how a UTXO v2 is different from a UTXO v1 in its public key, and in the way to retrieve it at the full-node side. Today, we’ll explain how to convert UTXOs v1 to  v2 to use them in transactions v2.</p>
<h2>What is a conversion transaction?</h2>
<p>A conversion transaction is a special transaction that converts a list of UTXOs in version 1 into a UTXO in version 2. The UTXOs could be PRV or tokens.</p>
<p>As Privacy V2 is coming in a few days, we’ll see more and more transactions v2 on the network. If you miss how crazy Privacy V2 is, visit this <a href="https://we.incognito.org/t/introduction-to-privacy-v2/12776">post</a>.  Each transaction v2 consumes a list of UTXOs v2 as input and produces a list of UTXOs v2. Therefore, conversion is mandatory for those who still have UTXOs v1 and wish to use them in v2 to enhance privacy. Besides, the Incognito network will soon not accept transactions of v1. Therefore, UTXOs conversion is a must.</p>
<p>A conversion transaction is (by default) <em>non-private</em>. All information (sender, amount, tokenID) will be publicly visible. Its properties are summarized in the following table.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th>Property</th>
<th>Description</th>
<th>Note</th>
</tr>
</thead>
<tbody>
<tr>
<td>Hide amount</td>
<td>NO</td>
<td></td>
</tr>
<tr>
<td>Hide sender</td>
<td>NO</td>
<td></td>
</tr>
<tr>
<td>Hide receiver</td>
<td>YES</td>
<td>One-time address</td>
</tr>
<tr>
<td>Hide asset type</td>
<td>No</td>
<td></td>
</tr>
</tbody>
</table>
</div><p><em>Table. A summary of conversion transactions. Although the receiver is hidden by a one-time address, in most cases, the receiver of this transaction is also the sender.</em></p>
<h2>How to convert your UTXOs</h2>
<h3>Using the Incognito app</h3>
<p>Firstly, navigate to the <strong>Asset</strong> screen.<br>
<img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/f/fb14da3ee1a9fb66a61b38d63026e84e64de267a.png" alt="Screen Shot 2021-07-09 at 6.56.48 PM" data-base62-sha1="zPawkhq6pWKKgiLvbe1dsVB1PkK" width="472" height="842"></p>
<p>If you still have UTXOs v1, there will be a little message at the bottom of the screen. Hit on that message. A converting screen will appear.<br>
<img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/0/0ab00cf13af4f7494985a8832ef21cb166645630.png" alt="Screen Shot 2021-07-09 at 6.55.59 PM" data-base62-sha1="1wxXBa9BwAXFuR1pywgPY2IXxv2" width="471" height="827"></p>
<p>A list of tokens with UTXOs v1 will be displayed. Hit on <strong>Go to convert screen</strong> button.<br>
<img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/c/c0bdb2bbb11ee9ba5e576f6dc055e65d6d5ed4fc.png" alt="image" data-base62-sha1="rv46fseuXCVwXNJzzub5o3kRqVe" width="467" height="847"></p>
<p>Now, click the <strong>Convert</strong> button and wait until the process finishes. When the process is successful, the <strong>Convert</strong> screen will look like the following<br>
<img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/6/6d248ef2accb3866855ae5893c3b6930d7e1e2ed.png" alt="image" data-base62-sha1="fzwmXjw6ihIlG80yl5P9kfBcvN3" width="471" height="841"></p>
<p>Note that a conversion transaction consumes at most 30 UTXOs v1 at a time. In case you have a huge number of UTXOs v1, you have to spend an enormous amount of time. Another solution is to consolidate these UTXOs into a smaller set (less than 30) before converting time. The consolidating function is also available on the App as well as on the <a href="https://github.com/incognitochain/incognito-cli#consolidate">CLI tool</a>.</p>
<p>However, the consolidation only works before the <code>PrivacyV2BreakPoint</code> takes effect because v1 transactions will be rejected after that time. More detail on this will be discussed in a different post.  To avoid this annoying problem, you can use the <a href="https://github.com/incognitochain/incognito-cli">CLI tool</a> to convert all of your UTXOs with a single command.</p>
<h3>Using the CLI tool</h3>
<p>Converting UTXOs on the Incognito app is a costly operation if you have a lot of UTXOs  v1. Users will have to wait for an enormous amount of time before this operation finishes. Therefore, have built a very simple <a href="https://github.com/incognitochain/incognito-cli">command-line application</a> for some basic functionalities, including converting UTXOs. The instruction on how to use this tool can be found <a href="https://github.com/incognitochain/incognito-cli#convert">here</a>.</p>
<pre><code class="lang-auto">$ incognito-cli help convert
NAME:
   incognito-cli convert - Convert UTXOs of an account w.r.t a tokenID.

USAGE:
   convert --privateKey PRIVATE_KEY [--tokenID TOKEN_ID] [--numThreads NUM_THREADS] [--enableLog ENABLE_LOG] [--logFile LOG_FILE]

   OPTIONAL flags are denoted by a [] bracket.

CATEGORY:
   TRANSACTIONS

DESCRIPTION:
   This function helps convert UTXOs v1 of a user to UTXO v2 w.r.t a tokenID. Please note that this process is time-consuming and requires a considerable amount of CPU.

OPTIONS:
   --privateKey value, --prvKey value  a base58-encoded private key
   --tokenID value                     ID of the token (default: "0000000000000000000000000000000000000000000000000000000000000004")
   --numThreads value                  number of threads used in this action (default: 4)
   --enableLog                         enable log for this action (default: false)
   --logFile value                     location of the log file (default: "os.Stdout")
</code></pre>
<p>Suppose your private key is <code>and you want to convert the token</code> using <code>10</code> threads.  Just run the following command, and wait.</p>
<pre><code class="lang-auto">$ incognito-cli convert --privateKey YOUR_PRIVATE_KEY --tokenID --numThreads 10
</code></pre>
<h2>Conclusion</h2>
<p>Conversion transactions are the portal to the <a href="https://we.incognito.org/t/introduction-to-privacy-v2/12776">Privacy V2</a> world where you can truly go incognito. However, the way to that utopian world costs you some sort of privacy. This amount of privacy is just the same as a regular v1 trade transaction or a shielding, un-shielding transaction that you have been doing until now. Therefore, in the end, it does not cost that much. In the next post, we’ll explain how <a href="https://we.incognito.org/t/introduction-to-privacy-v2/12776">Privacy V2</a> affects pDEX trading transactions.</p>
<h2><a href="https://we.incognito.org/t/pdex-trade-with-privacy-v2/12957">pDEX Trade with Privacy V2 ▸</a></h2>
          </blockquote>
          <p><a href="https://we.incognito.org/t/conversion-transactions/12885/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/conversion-transactions/12885/1</link>
        <pubDate>Sat, 10 Jul 2021 10:01:36 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-12885-1</guid>
        <source url="https://we.incognito.org/t/conversion-transactions/12885.rss">Conversion Transactions</source>
      </item>
  </channel>
</rss>
