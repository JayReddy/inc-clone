<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Introducing new database mode</title>
    <link>https://we.incognito.org/t/introducing-new-database-mode/16506</link>
    <description>In the next release, we will support several modes aiming to reduce Incognito node’s disk usage.

**1/ FFStorage mode (--ffstorage)**

In previous versions, each block and transaction was not optimally encoded, causing a significant increase in storage capacity. We used LevelDB to store them, as well as all other blockchain data, causing massive CPU and RAM consumption every time LevelDB compresses stale data. In this version, we encode blocks and transactions with protobuf, reducing data storage capacity. We also introduced a flat-file system to replace LevelDB in storing block data, to ensure that other data types will not affect the block saving process. Unfortunately, in order to use this version, you need to delete the old data and perform the synchronization process again. The synchronization in this version is also faster than the old version.

**2/ Batch-Commit mode (--sync-mode batch commit)**

We use Ethereum stateDB to maintain our feature database (transaction, consensus). It allows Incognito to maintain states in any checkpoint which helps us debug, change view, and look up states in the past. Ethereum StateDB use Modified Merkle Patricia Trie (MPT) to organize the data objects as a tree which has some limitations:

* When the number of records reaches a certain threshold, Leveldb compression is frequently enabled. The MPT generates a large number of branch nodes, which are used to construct the tree. These nodes just hold routing data, not actual data. Creating a record for each node not only consumes database space but also slows the operation down.
* Deleting these records requires an extensive offline process.

To reduce the number of records persisted into disk, we implement a batch commit mechanism. Once a block is inserted, the node will not commit trie data to disk immediately. Instead, commit trie data to disk per batch block. By committing to disk at the last insertion time, we can build only one branch node for two or more leaf nodes with the same prefix. To put it another way, we&#39;ll wait until a large amount of nodes are installed before committing everything to disk. Prior to then, nodes were simply committed to memory. This mode is similar to Ethereum full-sync mode.

**3/ Lite mode (--sync-mode lite)**

Ethereum-based StateDB implementation allows clients to revert state at specific points in the past. However, there are types of database that node operation doesn&#39;t need this functionality, such as transaction DB. With MPT, there is overhead of internal data structure that makes disk size increase not linearly. In addition, retrieving a value in MPT will need several disk IO.

To support users running low cost nodes, we implement a hybrid data structure, called lite-statedb. As a hybrid mechanism, finalized states are stored in key-value DB, and an unfinalized state (multiview) in link list data structure.

By only persisting the final value, this method does not pose any overhead to the database, but the tradeoff is the node cannot retrieve state in the past. 

**Result**

Here is the result after we benchmark.

* Old:the current version code
* Archive: the default mode (commit every block), but with ffstorage enabled
* Batch: batch-commit mode with ffstorage enabled
* Lite: lite mode with ffstorage enabled

![|624x337,105%](upload://46RC2K4isdD05z55bC47oI0dBqy.png)

![|593x368,110%](upload://1rQZtFeYAYWTAHYn7jLZIcpc1Jl.png)

From the result, we see that beacon db size is only affected by ffstorage mode (reduce 15%), as it does not have any stateDB that can run in batch-commit or lite mode. On the other hand, shard db size is significantly reduced as its transactionDB can be applied batch or lite.

Regarding the full node database, all shard chains will be reduced 3 times if using batch-commit, and 5 times if using lite mode. And overall, a full node can reduce to 50 - 70% data size.

**Note on release**

Although these features are tested in several environments, it is not battle-tested yet. To reduce risk to our whole blockchain, we will not apply this feature in production-branch yet, but beta branch instead. There are 3 phases to release:

* Core &amp; community volunteer (2 week): core team will run several nodes with new modes, and we welcome any volunteer from community running new modes with us
* Partial foundation node (2 week): a portion of fixed node will be upgrade new modes
* Full upgrade: these features will be merged and applied in production code.

We recommend that node operators should run at least 1 node with production-branch. In case of bug or db corruption of the beta-branch, you can clone and revert to the production-branch.

The branch and docker tags for this feature will be announced later.</description>
    
    <lastBuildDate>Tue, 09 Aug 2022 14:58:13 +0000</lastBuildDate>
    <category>Scalability</category>
    <atom:link href="https://we.incognito.org/t/introducing-new-database-mode/16506.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@radonm Radoslav Milanov]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/radonm">@radonm</a> wrote:</p>
          <blockquote>
              <p>I have that but some time after the hard linking storage grows quite a bit and it has to be done again and requires downtime. It seems state pruning will be a live maintenance.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/15">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/15</link>
        <pubDate>Tue, 09 Aug 2022 14:58:13 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-15</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <p>Hey <a class="mention" href="/u/radonm">@radonm</a>, please look into the “How to prune data” of the <a href="https://we.incognito.org/t/state-pruning/17057">topic</a> to shrink disk usage via an RPC call.<br>
Note that pruning is a CPU-intensive process so if you are running multiple nodes on the same server, be careful with the CPU usage, it’s common to take about 30-40% CPU for a pruning process.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/14">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/14</link>
        <pubDate>Mon, 08 Aug 2022 23:24:41 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-14</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p>We are looking into discontinuing these database modes in favor of state pruning. Right now the devs are working on state pruning the bootstrap data. Once complete, node operators will only need to bootstrap their data to take advantage.</p>
<p>The best way to shrink the disk usage is by hard linking, there is a community made script to assist with this:</p>
<aside class="quote" data-post="21" data-topic="14239">
  <div class="title">
    <div class="quote-controls"></div>
    <img alt="" width="20" height="20" src="/user_avatar/we.incognito.org/j053/40/17926_2.png" class="avatar">
    <a href="https://we.incognito.org/t/node-operator-bootstrapping-introduction-guide/14239/21">Node Operator Bootstrapping Introduction &amp; Guide <img src="https://we.incognito.org/images/emoji/apple/cowboy_hat_face.png?v=9" title="cowboy_hat_face" alt="cowboy_hat_face" class="emoji"></a> <a class="badge-wrapper  box" href="/c/validators/validator-tutorials"><span class="badge-category-parent-bg" style="background-color: #778899;"></span><span class="badge-category-bg" style="background-color: #778899;"></span><span style="color: #FFFFFF" data-drop-close="true" class="badge-category clear-badge" title="Need help with something? There’s probably a tutorial for that here. Can’t find one? Help the community out by writing one.">Tutorials</span></a>
  </div>
  <blockquote>
    Here is a node script I made to automate the creation of hard links. You could run it periodically without any problem to save a lot of space.
  </blockquote>
</aside>

          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/13">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/13</link>
        <pubDate>Mon, 08 Aug 2022 17:58:35 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-13</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@radonm Radoslav Milanov]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/radonm">@radonm</a> wrote:</p>
          <blockquote>
              <p>So what would you say is the best mode for validators? How to shrink disk usage and how much more RAM would that need ?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/12">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/12</link>
        <pubDate>Mon, 08 Aug 2022 17:49:15 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-12</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@fredlee 🦆]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/fredlee">@fredlee</a> wrote:</p>
          <blockquote>
              <p>I’m currently testing all database modes. I’m not done with my tests yet, but I’ll publish results when I am. What I can say so far is that all modes have different requirements and strengths. I’m done testing batchmode and it requires less disk space than the old archive mode, but requires <strong>a lot</strong> more memory. We’re talking 6 times more and up. Same seems to go with all ffstorage modes, which cuts disk space, but requires about twice the memory.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/11">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/11</link>
        <pubDate>Tue, 26 Apr 2022 19:55:53 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-11</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@Linnovations Yes to Privacy!]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/linnovations">@Linnovations</a> wrote:</p>
          <blockquote>
              <p>Hi <a class="mention" href="/u/0xkumi">@0xkumi</a>, thanks for putting together this post.</p>
<p>I’m not a technical person and in fact my very 1st encounter with Linux was to install Ubuntu in order to run a Virtual Node to support this important project, I shared my experience here  -  <a href="https://we.incognito.org/t/step-by-step-how-i-built-a-virtual-node-on-worlds-smallest-mini-pc/6479" class="inline-onebox">Step-By-Step: How I built a Virtual Node on World's Smallest Mini PC</a></p>
<p>So, when you mention database modes, sorry to say, I’m totally lost. But the from the charts I can tell the changes are necessary and good.</p>
<p>Do you recommend Noobs (like me) who are not so technically savvy sit on the side lines and just wait until the Core team completes testing?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/10">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/10</link>
        <pubDate>Tue, 26 Apr 2022 19:17:42 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-10</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@0xkumi 0xkumi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/0xkumi">@0xkumi</a> wrote:</p>
          <blockquote>
              <p>The normal mode for fullnode/validator are batch<br>
Archive for backup and debugging<br>
Lite is for low-end validator-only node. Although Lite could be used as fullnode, we don’t recommend this kind of usage.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/9">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/9</link>
        <pubDate>Sun, 24 Apr 2022 21:15:16 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-9</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@Josh_Hamon Josh]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/josh_hamon">@Josh_Hamon</a> wrote:</p>
          <blockquote>
              <p>What split of modes is desired by the dev team? Are they expecting most to switch to Lite mode? With the other modes being for developers?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/8">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/8</link>
        <pubDate>Thu, 21 Apr 2022 17:10:04 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-8</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@abduraman Abduraman Blackram]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/abduraman">@abduraman</a> wrote:</p>
          <blockquote>
              <p>Here is my understanding.</p>
<p>lite is suitable only for validators or nodes that need only actual data since you cannot access the past data.</p>
<p>batch is a delayed database node. Suitable for both validators and the fullnodes that do not need the “recent” data. maybe for the owners of analytics fullnodes or validators wanting to query its past earnings etc.</p>
<p>archive is the current mode. All restrictions unlocked <img src="https://we.incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:">  The difference is that it just uses the new feature ffstorage.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/7">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/7</link>
        <pubDate>Tue, 19 Apr 2022 08:42:33 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-7</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@fredlee 🦆]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/fredlee">@fredlee</a> wrote:</p>
          <blockquote>
              <p>Can you explain the practical difference between lite, batch, and archive? Why should you not only run lite? Can you run a fullnode? Retrieve account balance? Create transactions?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/6</link>
        <pubDate>Tue, 19 Apr 2022 08:32:50 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-6</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@khanhj]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/khanhj">@khanhj</a> wrote:</p>
          <blockquote>
              <p>Hey guys,<br>
We have push out an instruction to run new DB mode<br>
<aside class="quote quote-modified" data-post="1" data-topic="16534">
  <div class="title">
    <div class="quote-controls"></div>
    <img alt="" width="20" height="20" src="/user_avatar/we.incognito.org/khanhj/40/149_2.png" class="avatar">
    <a href="https://we.incognito.org/t/instruction-of-running-new-validators-database-modes-beta-release/16534">Instruction of running new validator's database modes - beta release</a> <a class="badge-wrapper  box" href="/c/validators/validator-tutorials"><span class="badge-category-parent-bg" style="background-color: #778899;"></span><span class="badge-category-bg" style="background-color: #778899;"></span><span style="color: #FFFFFF" data-drop-close="true" class="badge-category clear-badge" title="Need help with something? There’s probably a tutorial for that here. Can’t find one? Help the community out by writing one.">Tutorials</span></a>
  </div>
  <blockquote>
    Greeting Incognito Validators, 
As you know, we are preparing to release new database modes, including Archive, Batch, and Lite. In addition, there will be another feature called FFStorage, which stores raw blocks in file-type system. For more details, please look at our <a href="https://we.incognito.org/t/introducing-new-database-mode/16506">new database mode introduction</a>. 
<img src="https://we.incognito.org/images/emoji/apple/warning.png?v=9" title="warning" alt="warning" class="emoji"> Note: only Archive/Batch and disabled FFStorage are compatible with old databases, otherwise, you have to resync the database. The bootstrap file will be provided later. 
The beta produ…
  </blockquote>
</aside>
</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/5</link>
        <pubDate>Tue, 19 Apr 2022 08:13:02 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-5</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@SPAddict25 Peter]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/spaddict25">@SPAddict25</a> wrote:</p>
          <blockquote>
              <p>Omg TLDR. Went straight to the charts. I like the green line. Green = good right? <img src="/images/emoji/apple/smiley.png?v=9" title=":smiley:" class="emoji" alt=":smiley:"></p>
<p>Happy to help out as long as instructions are ELI5 compliant <img src="/images/emoji/apple/stuck_out_tongue_closed_eyes.png?v=9" title=":stuck_out_tongue_closed_eyes:" class="emoji" alt=":stuck_out_tongue_closed_eyes:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/4</link>
        <pubDate>Thu, 14 Apr 2022 05:41:33 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-4</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@Ducky]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/ducky">@Ducky</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/spaddict25">@SPAddict25</a> as previously you asked for more info about blockchain storage reduction, please check out this post.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/3</link>
        <pubDate>Thu, 14 Apr 2022 05:11:08 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-3</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
      <item>
        <title>Introducing new database mode</title>
        <dc:creator><![CDATA[@0xkumi 0xkumi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/0xkumi">@0xkumi</a> wrote:</p>
          <blockquote>
              <p>In the next release, we will support several modes aiming to reduce Incognito node’s disk usage.</p>
<p><strong>1/ FFStorage mode (–ffstorage)</strong></p>
<p>In previous versions, each block and transaction was not optimally encoded, causing a significant increase in storage capacity. We used LevelDB to store them, as well as all other blockchain data, causing massive CPU and RAM consumption every time LevelDB compresses stale data. In this version, we encode blocks and transactions with protobuf, reducing data storage capacity. We also introduced a flat-file system to replace LevelDB in storing block data, to ensure that other data types will not affect the block saving process. Unfortunately, in order to use this version, you need to delete the old data and perform the synchronization process again. The synchronization in this version is also faster than the old version.</p>
<p><strong>2/ Batch-Commit mode (–sync-mode batch commit)</strong></p>
<p>We use Ethereum stateDB to maintain our feature database (transaction, consensus). It allows Incognito to maintain states in any checkpoint which helps us debug, change view, and look up states in the past. Ethereum StateDB use Modified Merkle Patricia Trie (MPT) to organize the data objects as a tree which has some limitations:</p>
<ul>
<li>When the number of records reaches a certain threshold, Leveldb compression is frequently enabled. The MPT generates a large number of branch nodes, which are used to construct the tree. These nodes just hold routing data, not actual data. Creating a record for each node not only consumes database space but also slows the operation down.</li>
<li>Deleting these records requires an extensive offline process.</li>
</ul>
<p>To reduce the number of records persisted into disk, we implement a batch commit mechanism. Once a block is inserted, the node will not commit trie data to disk immediately. Instead, commit trie data to disk per batch block. By committing to disk at the last insertion time, we can build only one branch node for two or more leaf nodes with the same prefix. To put it another way, we’ll wait until a large amount of nodes are installed before committing everything to disk. Prior to then, nodes were simply committed to memory. This mode is similar to Ethereum full-sync mode.</p>
<p><strong>3/ Lite mode (–sync-mode lite)</strong></p>
<p>Ethereum-based StateDB implementation allows clients to revert state at specific points in the past. However, there are types of database that node operation doesn’t need this functionality, such as transaction DB. With MPT, there is overhead of internal data structure that makes disk size increase not linearly. In addition, retrieving a value in MPT will need several disk IO.</p>
<p>To support users running low cost nodes, we implement a hybrid data structure, called lite-statedb. As a hybrid mechanism, finalized states are stored in key-value DB, and an unfinalized state (multiview) in link list data structure.</p>
<p>By only persisting the final value, this method does not pose any overhead to the database, but the tradeoff is the node cannot retrieve state in the past.</p>
<p><strong>Result</strong></p>
<p>Here is the result after we benchmark.</p>
<ul>
<li>Old:the current version code</li>
<li>Archive: the default mode (commit every block), but with ffstorage enabled</li>
<li>Batch: batch-commit mode with ffstorage enabled</li>
<li>Lite: lite mode with ffstorage enabled</li>
</ul>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/1/c/1ccf597e9f913918a0250ab03f9eed175158e19a.png" alt data-base62-sha1="46RC2K4isdD05z55bC47oI0dBqy" width="655" height="353"></p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/0/a/0a283dcee514f73f86101e88f5b80f317138cf2f.png" alt data-base62-sha1="1rQZtFeYAYWTAHYn7jLZIcpc1Jl" width="652" height="404"></p>
<p>From the result, we see that beacon db size is only affected by ffstorage mode (reduce 15%), as it does not have any stateDB that can run in batch-commit or lite mode. On the other hand, shard db size is significantly reduced as its transactionDB can be applied batch or lite.</p>
<p>Regarding the full node database, all shard chains will be reduced 3 times if using batch-commit, and 5 times if using lite mode. And overall, a full node can reduce to 50 - 70% data size.</p>
<p><strong>Note on release</strong></p>
<p>Although these features are tested in several environments, it is not battle-tested yet. To reduce risk to our whole blockchain, we will not apply this feature in production-branch yet, but beta branch instead. There are 3 phases to release:</p>
<ul>
<li>Core &amp; community volunteer (2 week): core team will run several nodes with new modes, and we welcome any volunteer from community running new modes with us</li>
<li>Partial foundation node (2 week): a portion of fixed node will be upgrade new modes</li>
<li>Full upgrade: these features will be merged and applied in production code.</li>
</ul>
<p>We recommend that node operators should run at least 1 node with production-branch. In case of bug or db corruption of the beta-branch, you can clone and revert to the production-branch.</p>
<p>The branch and docker tags for this feature will be announced later.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/introducing-new-database-mode/16506/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/introducing-new-database-mode/16506/1</link>
        <pubDate>Thu, 14 Apr 2022 05:04:31 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16506-1</guid>
        <source url="https://we.incognito.org/t/introducing-new-database-mode/16506.rss">Introducing new database mode</source>
      </item>
  </channel>
</rss>
