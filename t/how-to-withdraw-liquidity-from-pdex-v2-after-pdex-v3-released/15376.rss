<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>How to withdraw liquidity from pDEX V2 (after pDEX V3 released)</title>
    <link>https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376</link>
    <description>Hi Incognito liquidity providers,

Since pDEX v3 and a new version of Incognito-Wallet were released, it no longer interacts with the previous version pDEX v2. 

The following instructions are for withdrawing your liquidity contributed to pDEX v2, by using the new Incognito command-line tool (a.k.a incognito-cli).

We know this is pretty technical that some of you won&#39;t feel comfortable with, so don&#39;t hesitate to create a ticket for @Support, we will definitely help you with every step to make sure you can get your added liquidity back. 

## Common Token IDs
```
PRV 0000000000000000000000000000000000000000000000000000000000000004
USDT 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
BTC b832e5d3b1f01a4f0623f7fe91d6673461e1f5d37d91fe78c5c2e6183ff39696 
XMR c01e7dc1d1aba995c19b257412340b057f8ad1482ccb6a9bb0adce61afbf05d4
DAI 3f89c75324b46f13c7b036871060e641d996a24c09b3065835cb1d38b799d6c1
USDC 1ff2da446abfebea3ba30385e2ca99b0f0bbeda5c6371f4c23c939672b429a42
ETH ffd8d42dc40a8d166ea4848baf8b5f6e912ad79875f4373070b59392b1756c8f
BNB(BSC) e5032c083f0da67ca141331b6005e4a3740c50218f151a5e829e9d03227e33e2
LTC 7450ad98cb8c967afb76503944ab30b4ce3560ed8f3acc3155f687641ae34135
DASH 447b088f1c2a8e08bff622ef43a477e98af22b64ea34f99278f4b550d285fbff
ZEC a609150120c0247407e6d7725f2a9701dcbb7bab5337a70b9cef801f34bc2b5c
BUSD(BSC) be02b225bcd26eeae00d3a51e554ac0adcdcc09de77ad03202904666d427a7e4
BNB b2655152784e8639fa19521a7035f331eea1f1e911b2f3200a507ebb4554387b
```
---


## **Instruction overview**
0. Download incognito-cli tool
1. Get token balances by private key
2. Get a share of a pair in pDEX v2
3. Withdraw share
4. Withdraw earned trading fee

## Step by step to withdraw pDEX v2 liquidity

The example below shows steps to withdraw liquidity from the PRV - USDT pair by using the incognito-cli on Windows. You can apply a similar process for other pairs on other operating systems like Linux or MacOSX too.

**Note**: In the following commands, you should replace `PRIVATEKEY` with a string you find in the Incognito app (under `Keychain &gt; Private key`). Similarly, replace `ADDRESS` with a string you find under `Keychain &gt; Your incognito address`.  

**0/ Download incognito-cli tool**
Please download the pre-built executable binary of the incognito-cli  [here](https://drive.google.com/drive/folders/12gtK73ATZ2YroqF7XUEMXVlNTa6134BC?usp=sharing) or follow this [instruction](https://we.incognito.org/t/incognito-command-line/15375) to build your own. NOTE: if you want to withdraw pDEX v2 liquidity or rewards, in **Step 4** of **Part 1**, check out this [COMMIT](http://2e842843297a80a345feadfbe02f314986f3c815) instead of `development`.

Once the CLI tool is downloaded, you can run it from your command line program as follows:

![image|689x228](upload://2sS5uted6UAaDRM1br8nzSzBTYu.png) 


**1a/ Get PRV balance** (note: this may take up to 20 minutes for the first run)

```
PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 balance -p PRIVATEKEY
```
**1b/ Get USDT balance**
```
PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 balance -p PRIVATEKEY --tokenID 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
```

**2/ Get your share of a pair**

```
PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 pdeshare --address ADDRESS --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
```


**3/ Withdraw your share**

In the following command, please replace `AMOUNT` with a share amount returned from previous step. 

```
PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 pdewithdraw --privateKey PRIVATEKEY --amount AMOUNT --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
```

After **5 minutes**, call the get balance command again to see your balance changes or check history on the wallet app.

![image|436x314, 75%](upload://6ZTTjxK3wUwSAdudlmv8rD3wAgn.png) 

![image|434x320, 75%](upload://l0oaLONZXiWCB3tUon8SS3f2qlm.png) 

**4/ Withdraw earned trading fee**

- get the earned fees amount.
```
PS C:\Users\khanhj\Documents\repo\incognito-cli&gt;  .\incognito-cli.exe -c 1 -d 1 pdelpfee --address ADDRESS --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
```

- withdraw the earned fees.
```
PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 pdefeewithdraw --privateKey PRIVATEKEY --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
```
- check PRV balance again.

![image|440x460, 75%](upload://5C5meCAf9z2jRD78njURK8WwUhs.png) 


---
That&#39;s it.
Thank you for your patience and happy New Year!</description>
    
    <lastBuildDate>Sat, 15 Jan 2022 16:03:48 +0000</lastBuildDate>
    <category>Tutorials</category>
    <atom:link href="https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>How to withdraw liquidity from pDEX V2 (after pDEX V3 released)</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/kingofcrypto">@KingOfCrypto</a>,</p>
<p>We have a support ticket open with you and we are awaiting your reply to assist you in the removal of pDEX v2 liquidity.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376/3</link>
        <pubDate>Sat, 15 Jan 2022 16:03:48 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-15376-3</guid>
        <source url="https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376.rss">How to withdraw liquidity from pDEX V2 (after pDEX V3 released)</source>
      </item>
      <item>
        <title>How to withdraw liquidity from pDEX V2 (after pDEX V3 released)</title>
        <dc:creator><![CDATA[@KingOfCrypto Fishbelly Republic]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/kingofcrypto">@KingOfCrypto</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="khanhj" data-post="1" data-topic="15376">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/khanhj/40/149_2.png" class="avatar"> khanhj:</div>
<blockquote>
<p>We know this is pretty technical that some of you won’t feel comfortable with, so don’t hesitate to create a ticket for <a class="mention-group notify" href="/groups/support">@Support</a>, we will definitely help you with every step to make sure you can get your added liquidity back.</p>
</blockquote>
</aside>
<p>Sincerely, up till now I am still wondering why you are allowing us to be passing through this kind tedious stuff.</p>
<p>Something you can simply tell us to withdraw our liquidity before the move, the way you announced to us last year that we shouldn’t do some stuffs when you’re trying to move Provision to pDEX v3!</p>
<p>My question are: are we all developers, are we all have access to PC? Some on this space only have access to mobile phone not PC.</p>
<p>I can’t just vividly understand why we should be passing through stress just because we provide liquidity on this crypto community!</p>
<p>This supposed to be the work of the developers since they failed their duty of announcing to us to remove our liquidity before the final upgrade!</p>
<p>This is not the first Blockchain programme some of us will join and it will not be the last, let’s be nice to ourselves by all means please!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376/2</link>
        <pubDate>Sat, 15 Jan 2022 08:07:13 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-15376-2</guid>
        <source url="https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376.rss">How to withdraw liquidity from pDEX V2 (after pDEX V3 released)</source>
      </item>
      <item>
        <title>How to withdraw liquidity from pDEX V2 (after pDEX V3 released)</title>
        <dc:creator><![CDATA[@khanhj]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/khanhj">@khanhj</a> wrote:</p>
          <blockquote>
              <p>Hi Incognito liquidity providers,</p>
<p>Since pDEX v3 and a new version of Incognito-Wallet were released, it no longer interacts with the previous version pDEX v2.</p>
<p>The following instructions are for withdrawing your liquidity contributed to pDEX v2, by using the new Incognito command-line tool (a.k.a incognito-cli).</p>
<p>We know this is pretty technical that some of you won’t feel comfortable with, so don’t hesitate to create a ticket for <a class="mention-group notify" href="/groups/support">@Support</a>, we will definitely help you with every step to make sure you can get your added liquidity back.</p>
<h2>Common Token IDs</h2>
<pre><code class="lang-auto">PRV 0000000000000000000000000000000000000000000000000000000000000004
USDT 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
BTC b832e5d3b1f01a4f0623f7fe91d6673461e1f5d37d91fe78c5c2e6183ff39696 
XMR c01e7dc1d1aba995c19b257412340b057f8ad1482ccb6a9bb0adce61afbf05d4
DAI 3f89c75324b46f13c7b036871060e641d996a24c09b3065835cb1d38b799d6c1
USDC 1ff2da446abfebea3ba30385e2ca99b0f0bbeda5c6371f4c23c939672b429a42
ETH ffd8d42dc40a8d166ea4848baf8b5f6e912ad79875f4373070b59392b1756c8f
BNB(BSC) e5032c083f0da67ca141331b6005e4a3740c50218f151a5e829e9d03227e33e2
LTC 7450ad98cb8c967afb76503944ab30b4ce3560ed8f3acc3155f687641ae34135
DASH 447b088f1c2a8e08bff622ef43a477e98af22b64ea34f99278f4b550d285fbff
ZEC a609150120c0247407e6d7725f2a9701dcbb7bab5337a70b9cef801f34bc2b5c
BUSD(BSC) be02b225bcd26eeae00d3a51e554ac0adcdcc09de77ad03202904666d427a7e4
BNB b2655152784e8639fa19521a7035f331eea1f1e911b2f3200a507ebb4554387b
</code></pre>
<hr>
<h2><strong>Instruction overview</strong></h2>
<ol start="0">
<li>Download incognito-cli tool</li>
<li>Get token balances by private key</li>
<li>Get a share of a pair in pDEX v2</li>
<li>Withdraw share</li>
<li>Withdraw earned trading fee</li>
</ol>
<h2>Step by step to withdraw pDEX v2 liquidity</h2>
<p>The example below shows steps to withdraw liquidity from the PRV - USDT pair by using the incognito-cli on Windows. You can apply a similar process for other pairs on other operating systems like Linux or MacOSX too.</p>
<p><strong>Note</strong>: In the following commands, you should replace <code>PRIVATEKEY</code> with a string you find in the Incognito app (under <code>Keychain &gt; Private key</code>). Similarly, replace <code>ADDRESS</code> with a string you find under <code>Keychain &gt; Your incognito address</code>.</p>
<p><strong>0/ Download incognito-cli tool</strong><br>
Please download the pre-built executable binary of the incognito-cli  <a href="https://drive.google.com/drive/folders/12gtK73ATZ2YroqF7XUEMXVlNTa6134BC?usp=sharing">here</a> or follow this <a href="https://we.incognito.org/t/incognito-command-line/15375">instruction</a> to build your own. NOTE: if you want to withdraw pDEX v2 liquidity or rewards, in <strong>Step 4</strong> of <strong>Part 1</strong>, check out this <a href="http://2e842843297a80a345feadfbe02f314986f3c815">COMMIT</a> instead of <code>development</code>.</p>
<p>Once the CLI tool is downloaded, you can run it from your command line program as follows:</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/1/1/1147dd9f334c3b0bf35b3fb05d870e3dfc8fc50a.png" data-download-href="/uploads/short-url/2sS5uted6UAaDRM1br8nzSzBTYu.png?dl=1" title="image"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/1/1/1147dd9f334c3b0bf35b3fb05d870e3dfc8fc50a.png" alt="image" data-base62-sha1="2sS5uted6UAaDRM1br8nzSzBTYu" width="689" height="228" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/1/1/1147dd9f334c3b0bf35b3fb05d870e3dfc8fc50a_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1207×400 13.2 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><strong>1a/ Get PRV balance</strong> (note: this may take up to 20 minutes for the first run)</p>
<pre><code class="lang-auto">PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 balance -p PRIVATEKEY
</code></pre>
<p><strong>1b/ Get USDT balance</strong></p>
<pre><code class="lang-auto">PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 balance -p PRIVATEKEY --tokenID 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
</code></pre>
<p><strong>2/ Get your share of a pair</strong></p>
<pre><code class="lang-auto">PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 pdeshare --address ADDRESS --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
</code></pre>
<p><strong>3/ Withdraw your share</strong></p>
<p>In the following command, please replace <code>AMOUNT</code> with a share amount returned from previous step.</p>
<pre><code class="lang-auto">PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 pdewithdraw --privateKey PRIVATEKEY --amount AMOUNT --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
</code></pre>
<p>After <strong>5 minutes</strong>, call the get balance command again to see your balance changes or check history on the wallet app.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/3/1/310c5df24ca4b8d784eca9f94768554a88f10097.png" alt="image" data-base62-sha1="6ZTTjxK3wUwSAdudlmv8rD3wAgn" width="327" height="235"></p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/9/3/9338efb4d97044dce572fbf1c6f1a9b0e01b7294.png" alt="image" data-base62-sha1="l0oaLONZXiWCB3tUon8SS3f2qlm" width="325" height="240"></p>
<p><strong>4/ Withdraw earned trading fee</strong></p>
<ul>
<li>get the earned fees amount.</li>
</ul>
<pre><code class="lang-auto">PS C:\Users\khanhj\Documents\repo\incognito-cli&gt;  .\incognito-cli.exe -c 1 -d 1 pdelpfee --address ADDRESS --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
</code></pre>
<ul>
<li>withdraw the earned fees.</li>
</ul>
<pre><code class="lang-auto">PS C:\Users\khanhj\Documents\repo\incognito-cli&gt; .\incognito-cli.exe -c 1 -d 1 pdefeewithdraw --privateKey PRIVATEKEY --tokenID1 0000000000000000000000000000000000000000000000000000000000000004 --tokenID2 716fd1009e2a1669caacc36891e707bfdf02590f96ebd897548e8963c95ebac0
</code></pre>
<ul>
<li>check PRV balance again.</li>
</ul>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/2/7/275906a7032975698da5ee3e27874bdcf4673dba.png" alt="image" data-base62-sha1="5C5meCAf9z2jRD78njURK8WwUhs" width="330" height="345"></p>
<hr>
<p>That’s it.<br>
Thank you for your patience and happy New Year!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376/1</link>
        <pubDate>Fri, 31 Dec 2021 15:53:00 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-15376-1</guid>
        <source url="https://we.incognito.org/t/how-to-withdraw-liquidity-from-pdex-v2-after-pdex-v3-released/15376.rss">How to withdraw liquidity from pDEX V2 (after pDEX V3 released)</source>
      </item>
  </channel>
</rss>
