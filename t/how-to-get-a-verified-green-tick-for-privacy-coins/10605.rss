<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>How to get a verified (green) tick for privacy coins</title>
    <link>https://we.incognito.org/t/how-to-get-a-verified-green-tick-for-privacy-coins/10605</link>
    <description>As you navigate the list of supported coins in the Incognito app, you’ll notice that some coins have green check marks, while others don’t. The green mark is a “verified tick”, and it signifies the legitimacy of a listed coin.

![verify-1_1|1200x628,60%](upload://zn9fOnCclcedOlqe7SKa6tG82SL.gif)

## What’s a verified tick and why is it needed?

Anyone can create a custom privacy coin with Incognito. In addition to serving as the privacy form of existing cryptocurrencies, they can serve as utility tokens in a dApp, or act as personal invitations to a wedding. You can create them for a school project or to act as coupons for your online store. The uses for custom coins are limited only to your imagination.

While a custom token without a tick can be sent/received between Incognito addresses, a coin with a verified tick can be traded, shielded, unshielded, etc. For this reason, you may not need to get your coin verified, if it’s just a personal project or only intended for people you know. 

A tick also dispels any doubt about the origin of the token. For example, if someone created a fake version of a cryptocurrency like BTC, you’ll know which one to trade for because of the verified tick.

If you want to get your coin verified, follow these simple steps and you’ll have it verified in no time:

## Classify your coin

Before requesting a verified tick, you must know what type of coin you’ve created.

* **Minted coin**: You minted a privacy coin on Incognito chain with the “***Mint***” function in the app.

* **ERC20/BEP20 coin**: You added an ERC20/BEP2 coin to the network using the bridge implementation. 

## How to get a verified tick

* **Minted coin**: simply send a direct message to [Support](https://we.incognito.org/g/Support) with your coin information (coin ID, owner, email, website, total supply, etc.). We will check and verify your possession of the coin, then give it a green tick.

* **ERC20/BEP20 added coin**: after adding the coin to the network, please perform one Shield and one Unshield transaction with the coin, then send a direct message to [Support](https://we.incognito.org/g/Support) with:

    :point_right:  Coin ID
    :point_right: The transaction hashes
    :point_right: Screenshots of Shield and Unshield transactions

![12 copy 12|2048x1213,35%](upload://aJ0qTvUhH0jxrV3Z3vpmt8CoCZB.png) 

[How to shield and unshield a coin &gt;](https://we.incognito.org/t/how-to-shield-send-receive-and-unshield-cryptocurrency/1114)

If both transactions are confirmed, your coin is valid and will be granted a verified tick.

It’s as easy as that! Now let’s go turn on Incognito mode for every cryptocurrency!</description>
    
    <lastBuildDate>Thu, 04 Mar 2021 04:51:25 +0000</lastBuildDate>
    <category>Tutorials</category>
    <atom:link href="https://we.incognito.org/t/how-to-get-a-verified-green-tick-for-privacy-coins/10605.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>How to get a verified (green) tick for privacy coins</title>
        <dc:creator><![CDATA[@elvis.p]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/elvis.p">@elvis.p</a> wrote:</p>
          <blockquote>
              <p>As you navigate the list of supported coins in the Incognito app, you’ll notice that some coins have green check marks, while others don’t. The green mark is a “verified tick”, and it signifies the legitimacy of a listed coin.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/f/f7e9fd4524a9a7115c7e8a8e07ec3f65cb0a608b.gif" alt="verify-1_1" data-base62-sha1="zn9fOnCclcedOlqe7SKa6tG82SL" width="720" height="376"></p>
<h2>What’s a verified tick and why is it needed?</h2>
<p>Anyone can create a custom privacy coin with Incognito. In addition to serving as the privacy form of existing cryptocurrencies, they can serve as utility tokens in a dApp, or act as personal invitations to a wedding. You can create them for a school project or to act as coupons for your online store. The uses for custom coins are limited only to your imagination.</p>
<p>While a custom token without a tick can be sent/received between Incognito addresses, a coin with a verified tick can be traded, shielded, unshielded, etc. For this reason, you may not need to get your coin verified, if it’s just a personal project or only intended for people you know.</p>
<p>A tick also dispels any doubt about the origin of the token. For example, if someone created a fake version of a cryptocurrency like BTC, you’ll know which one to trade for because of the verified tick.</p>
<p>If you want to get your coin verified, follow these simple steps and you’ll have it verified in no time:</p>
<h2>Classify your coin</h2>
<p>Before requesting a verified tick, you must know what type of coin you’ve created.</p>
<ul>
<li>
<p><strong>Minted coin</strong>: You minted a privacy coin on Incognito chain with the “<em><strong>Mint</strong></em>” function in the app.</p>
</li>
<li>
<p><strong>ERC20/BEP20 coin</strong>: You added an ERC20/BEP2 coin to the network using the bridge implementation.</p>
</li>
</ul>
<h2>How to get a verified tick</h2>
<ul>
<li>
<p><strong>Minted coin</strong>: simply send a direct message to <a href="https://we.incognito.org/g/Support">Support</a> with your coin information (coin ID, owner, email, website, total supply, etc.). We will check and verify your possession of the coin, then give it a green tick.</p>
</li>
<li>
<p><strong>ERC20/BEP20 added coin</strong>: after adding the coin to the network, please perform one Shield and one Unshield transaction with the coin, then send a direct message to <a href="https://we.incognito.org/g/Support">Support</a> with:</p>
<p><img src="https://we.incognito.org/images/emoji/apple/point_right.png?v=9" title=":point_right:" class="emoji" alt=":point_right:">  Coin ID<br>
<img src="https://we.incognito.org/images/emoji/apple/point_right.png?v=9" title=":point_right:" class="emoji" alt=":point_right:"> The transaction hashes<br>
<img src="https://we.incognito.org/images/emoji/apple/point_right.png?v=9" title=":point_right:" class="emoji" alt=":point_right:"> Screenshots of Shield and Unshield transactions</p>
</li>
</ul>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/4/4b2c2a4ace4700942d906e456f26846799123d63.png" data-download-href="/uploads/short-url/aJ0qTvUhH0jxrV3Z3vpmt8CoCZB.png?dl=1" title="12 copy 12"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/4/4b2c2a4ace4700942d906e456f26846799123d63_2_716x424.png" alt="12 copy 12" data-base62-sha1="aJ0qTvUhH0jxrV3Z3vpmt8CoCZB" width="716" height="424" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/4/4b2c2a4ace4700942d906e456f26846799123d63_2_716x424.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/4/4b2c2a4ace4700942d906e456f26846799123d63_2_1074x636.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/4/4b2c2a4ace4700942d906e456f26846799123d63_2_1432x848.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/2X/4/4b2c2a4ace4700942d906e456f26846799123d63_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">12 copy 12</span><span class="informations">4110×2436 606 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><a href="https://we.incognito.org/t/how-to-shield-send-receive-and-unshield-cryptocurrency/1114">How to shield and unshield a coin &gt;</a></p>
<p>If both transactions are confirmed, your coin is valid and will be granted a verified tick.</p>
<p>It’s as easy as that! Now let’s go turn on Incognito mode for every cryptocurrency!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/how-to-get-a-verified-green-tick-for-privacy-coins/10605/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/how-to-get-a-verified-green-tick-for-privacy-coins/10605/1</link>
        <pubDate>Thu, 04 Mar 2021 04:51:25 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-10605-1</guid>
        <source url="https://we.incognito.org/t/how-to-get-a-verified-green-tick-for-privacy-coins/10605.rss">How to get a verified (green) tick for privacy coins</source>
      </item>
  </channel>
</rss>
