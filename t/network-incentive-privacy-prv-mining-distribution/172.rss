<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Network Incentive: Privacy (PRV) Mining &amp; Distribution</title>
    <link>https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172</link>
    <description>[Introduction: A Platform of Decentralized Privacy Coins ▸](https://incognito.org/t/incognito-whitepaper-incognito-mode-for-cryptonetworks/168)

[Shielding Cryptocurrencies: Turning Any Cryptocurrency Into a Privacy Coin ▸](https://incognito.org/t/shielding-cryptocurrencies-turning-any-cryptocurrency-into-a-privacy-coin/83)

 [Trustless Custodians: A Decentralized Approach to Cryptocurrency Custodianship ▸](https://incognito.org/t/trustless-custodians-a-decentralized-approach-to-cryptocurrency-custodianship/84)

 [Sending Cryptocurrencies Confidentially: Ring Signature, Homomorphic Commitment, and Zero-Knowledge Range Proofs ▸](https://incognito.org/t/sending-cryptocurrencies-confidentially-ring-signature-homomorphic-commitment-and-zero-knowledge-range-proofs/170)

[Privacy at Scale with Sharding ▸](https://incognito.org/t/scaling-blockchain-privacy-with-sharding/169)

 [Consensus: A Combination of PoS, pBFT, and BLS ▸](https://incognito.org/t/consensus-a-combination-of-pos-pbft-and-bls/114)

 [Incognito Software Stack: Navigating the Incognito Source Code ▸](https://incognito.org/t/incognito-software-stack-navigating-the-incognito-source-code/115)

 [Incognito Performance ▸](https://incognito.org/t/incognito-performance/270)

# Network Incentive: Privacy (PRV) Mining &amp; Distribution ▾

Privacy (PRV) is the native coin of Incognito.

## Total Supply

A strict limit of 100M Privacy (PRV) will be minted. This number will never increase.

## Self-Funded

The Core Development Team pooled together a collective $1M to fund the project. No ICO, private sale or VC funding. Potential users and investors should not be asked to pay prior to any code being written. As such, the founding team bears all risks.

The $1M was funded in May 2018 and primarily used for protocol development and user adoption.

This funding structure will reduce price speculation around the project, avoid public market pressure, and keep the team laser-focused on building and shipping a great platform for decentralized privacy tokens.

## 95M Fair-Mining

Proof-of-Stake networks often heavily pre-allocate tokens for the team, investors, and ICO. Incognito takes a different approach. Incognito is a Proof-of-Stake blockchain that designs its block rewards similarly to Proof-of-Work blockchains like Bitcoin.

95M of the 100M PRV total supply is mined through block rewards. The total block reward for the first year is 8,751,970 PRV. Block rewards are reduced by 9% for every subsequent year. PRV will be fully mined after 40 years.

![image%20(33)|587x472](upload://zdUYnzL8aG18Pe5Kzu0nDQhfFB8.png) 

*Figure 1. PRV block rewards over time*

## 5M Pre-Mining

5M PRV is pre-mined and purchased by the Core Development Team for the aforementioned $1M, and has been used to cover salaries, server costs, operational expenses, and marketing activities.

## Initial Staking

There is a chicken-and-egg problem with any Proof-of-Stake blockchain. How can validators start staking when they don’t have coins to stake?

Incognito solves this in 2 ways:

* Testnet validators are rewarded with Testnet PRV, which can be converted 1:1 to Mainnet PRV at Mainnet launch.

* Incognito has its own hardware, Node. If Node owners don’t have PRV to get started, they can borrow PRV from the Node Staking Fund, where anyone can lend out their PRV to Node owners.

## Block Reward Split

95% of PRV total supply is minted through block rewards. Block rewards are split between the validators and Incognito DAO, a decentralized autonomous organization designed to fund protocol development and network growth. Incognito DAO collects a gradually reducing percentage of the block rewards, from 10% to 3%.

With this income, Incognito DAO will fuel the growth of the network, fund interesting projects, and give the project longevity.

Incognito DAO’s funds are initially managed by the Core Development Team. Management responsibilities will be gradually distributed to the community.

|Year|Validators|Incognito DAO|
| --- | --- | --- |
|1|90%|10%|
|2|91%|9%|
|3|92%|8%|
|4|93%|7%|
|5|94%|6%|
|6|95%|5%|
|7|96%|4%|
|8+|97%|3%|

## PRV Denominations

The smallest sub-denomination of Privacy is Nano. 1 Privacy is defined as 10&lt;sup&gt;9&lt;/sup&gt; Nano. There exist other sub-denominations of Privacy.

In the future, we expect Privacy to be used for regular transactions, Milli for microtransactions, Micro for transaction fees, and Nano for technical discussions and implementations.

|Multiplier|Name|
| --- | --- |
|10&lt;sup&gt;9&lt;/sup&gt;|Privacy|
|10&lt;sup&gt;6&lt;/sup&gt;|Milli|
|10&lt;sup&gt;3&lt;/sup&gt;|Micro|
|10&lt;sup&gt;0&lt;/sup&gt;|Nano|

## Token System

There are 3 types of tokens:

* **Privacy**. Privacy (PRV) is Incognito’s native coin — a work token&lt;sup&gt;1&lt;/sup&gt;. Users stake PRV to become validators. Validators earn block rewards in PRV and transaction fees in various cryptoassets (i.e. pBTC, pETH, etc). This model avoids speculators and only attracts people interested in growing the network. If the demand for private transactions grows, validators will earn more revenue, which naturally triggers an increase in the price of PRV.

* **Bridged Privacy Coins**. Anyone can convert cryptocurrencies (or &quot;public coins&quot;) on other blockchains (i.e. BTC, ETH, USDT) to privacy coins on Incognito (i.e. pBTC, pETH, pDAI). Privacy coins maintain a 1:1 peg and are completely confidential. Because of this, anyone can store, send and receive any cryptoassets with total privacy. Private coins can also be used to pay for transaction fees.

* **Issue-Created Privacy Coins**. Anyone can issue their own privacy coin on Incognito.

## Transaction Fees

Users can pay transaction fees in Incognito native token - PRV.

### [User-Created Privacy Coins ▸](https://incognito.org/t/user-created-privacy-coins/173)

### [Use Cases: Privacy Stablecoins, Privacy DEX, Confidential Crypto Payroll, and more ▸](https://incognito.org/t/use-cases-privacy-stablecoins-privacy-dex-confidential-crypto-payroll-and-more/174)

### [Future Work: Smart Contracts, Confidential Assets, Confidential IP, and more ▸](https://incognito.org/t/future-work-smart-contracts-confidential-assets-confidential-ip-and-more/175/2)

### [Conclusions, Acknowledgments, and References ▸](https://incognito.org/t/conclusions-acknowledgments-and-references/176)

&lt;p&gt;
--
&lt;/p&gt;

&lt;sup&gt;1&lt;/sup&gt; https://multicoin.capital/2018/02/13/new-models-utility-tokens/</description>
    
    <lastBuildDate>Sat, 22 Jan 2022 12:00:06 +0000</lastBuildDate>
    <category>👾 Tech</category>
    <atom:link href="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Network Incentive: Privacy (PRV) Mining &amp; Distribution</title>
        <dc:creator><![CDATA[@fredlee 🦆]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/fredlee">@fredlee</a> wrote:</p>
          <blockquote>
              <p>Thread revival!</p>
<p>Are we still waiting for year 1? For the past couple of years, the Incognito team has collected at a minimum of 67% of all block rewards. According to the original plan and block reward halving, we’re now on year 3, where the DAO should only collect 8%. Yet, the most recent plan for 2022 is talking about getting it down to 34%?</p>
<p>What went wrong?</p>
<p>Doing some rough calculations, the Incognito team over the first two years collected a total of 5M + (8.7M + 7.9M) * 2/3 = 16M PRV. Out of transparency and for the sake of community trust, is there a public ledger where we can see exactly how much is still held and what the rest has been used for?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/6</link>
        <pubDate>Sat, 22 Jan 2022 12:00:06 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-172-6</guid>
        <source url="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172.rss">Network Incentive: Privacy (PRV) Mining &amp; Distribution</source>
      </item>
      <item>
        <title>Network Incentive: Privacy (PRV) Mining &amp; Distribution</title>
        <dc:creator><![CDATA[@andrey A ]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/andrey">@andrey</a> wrote:</p>
          <blockquote>
              <p>Hey <a class="mention" href="/u/crouton">@crouton</a>, you easily quickly calculate it <img src="https://we.incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:">  I’ll help to calculate to record this moment for history:)</p>
<p>On this moment:</p>
<ul>
<li>On average every 24 hours happens ≈3k transactions in the network.</li>
<li>An average transaction fee is 0.0000006 PRV</li>
<li>Fee generated by transaction 0.0018 PRV</li>
</ul>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/9/987cdeed4ee719ad0e1c88ef77c28e0a2fc22efa.png" alt="image" data-base62-sha1="lKY6OLInRk6mLsLGRScIhfPeVoC" width="586" height="205"></p>
<p>If you are interested to dive in the network fee-related discussion, jump <a href="https://we.incognito.org/t/kyber-and-uniswap-benefits-to-platform-supporters/3349">here</a> and <a href="https://we.incognito.org/t/the-incognito-network-ponzi-or-economically-sustainable-blockchain/2179">here</a>.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/5</link>
        <pubDate>Tue, 04 Aug 2020 03:13:23 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-172-5</guid>
        <source url="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172.rss">Network Incentive: Privacy (PRV) Mining &amp; Distribution</source>
      </item>
      <item>
        <title>Network Incentive: Privacy (PRV) Mining &amp; Distribution</title>
        <dc:creator><![CDATA[@crouton]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/crouton">@crouton</a> wrote:</p>
          <blockquote>
              <p>Just curious, is there a way to view how much is currently generated via transaction fees vs minted? E.g. if my node is earning, how much of that is actually from transaction fees</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/4</link>
        <pubDate>Mon, 03 Aug 2020 20:11:27 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-172-4</guid>
        <source url="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172.rss">Network Incentive: Privacy (PRV) Mining &amp; Distribution</source>
      </item>
      <item>
        <title>Network Incentive: Privacy (PRV) Mining &amp; Distribution</title>
        <dc:creator><![CDATA[@duy duy huynh]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duy">@duy</a> wrote:</p>
          <blockquote>
              <p>transaction fees.  hopefully by then incognito is a really helpful service, and node will earn transaction fees in PRV, pBTC, pETH, pDAI, etc.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/3</link>
        <pubDate>Wed, 22 Apr 2020 18:31:50 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-172-3</guid>
        <source url="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172.rss">Network Incentive: Privacy (PRV) Mining &amp; Distribution</source>
      </item>
      <item>
        <title>Network Incentive: Privacy (PRV) Mining &amp; Distribution</title>
        <dc:creator><![CDATA[@Kelsomatic]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/kelsomatic">@Kelsomatic</a> wrote:</p>
          <blockquote>
              <p>How will the nodes earn after 40 years?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/2</link>
        <pubDate>Wed, 22 Apr 2020 18:30:53 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-172-2</guid>
        <source url="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172.rss">Network Incentive: Privacy (PRV) Mining &amp; Distribution</source>
      </item>
      <item>
        <title>Network Incentive: Privacy (PRV) Mining &amp; Distribution</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <p><a href="https://incognito.org/t/incognito-whitepaper-incognito-mode-for-cryptonetworks/168">Introduction: A Platform of Decentralized Privacy Coins ▸</a></p>
<p><a href="https://incognito.org/t/shielding-cryptocurrencies-turning-any-cryptocurrency-into-a-privacy-coin/83">Shielding Cryptocurrencies: Turning Any Cryptocurrency Into a Privacy Coin ▸</a></p>
<p><a href="https://incognito.org/t/trustless-custodians-a-decentralized-approach-to-cryptocurrency-custodianship/84">Trustless Custodians: A Decentralized Approach to Cryptocurrency Custodianship ▸</a></p>
<p><a href="https://incognito.org/t/sending-cryptocurrencies-confidentially-ring-signature-homomorphic-commitment-and-zero-knowledge-range-proofs/170">Sending Cryptocurrencies Confidentially: Ring Signature, Homomorphic Commitment, and Zero-Knowledge Range Proofs ▸</a></p>
<p><a href="https://incognito.org/t/scaling-blockchain-privacy-with-sharding/169">Privacy at Scale with Sharding ▸</a></p>
<p><a href="https://incognito.org/t/consensus-a-combination-of-pos-pbft-and-bls/114">Consensus: A Combination of PoS, pBFT, and BLS ▸</a></p>
<p><a href="https://incognito.org/t/incognito-software-stack-navigating-the-incognito-source-code/115">Incognito Software Stack: Navigating the Incognito Source Code ▸</a></p>
<p><a href="https://incognito.org/t/incognito-performance/270">Incognito Performance ▸</a></p>
<h1>Network Incentive: Privacy (PRV) Mining &amp; Distribution ▾</h1>
<p>Privacy (PRV) is the native coin of Incognito.</p>
<h2>Total Supply</h2>
<p>A strict limit of 100M Privacy (PRV) will be minted. This number will never increase.</p>
<h2>Self-Funded</h2>
<p>The Core Development Team pooled together a collective $1M to fund the project. No ICO, private sale or VC funding. Potential users and investors should not be asked to pay prior to any code being written. As such, the founding team bears all risks.</p>
<p>The $1M was funded in May 2018 and primarily used for protocol development and user adoption.</p>
<p>This funding structure will reduce price speculation around the project, avoid public market pressure, and keep the team laser-focused on building and shipping a great platform for decentralized privacy tokens.</p>
<h2>95M Fair-Mining</h2>
<p>Proof-of-Stake networks often heavily pre-allocate tokens for the team, investors, and ICO. Incognito takes a different approach. Incognito is a Proof-of-Stake blockchain that designs its block rewards similarly to Proof-of-Work blockchains like Bitcoin.</p>
<p>95M of the 100M PRV total supply is mined through block rewards. The total block reward for the first year is 8,751,970 PRV. Block rewards are reduced by 9% for every subsequent year. PRV will be fully mined after 40 years.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="/uploads/default/original/1X/f6dee108e2da6db34cd1cba91d6a0d9a16716dfa.png" data-download-href="/uploads/short-url/zdUYnzL8aG18Pe5Kzu0nDQhfFB8.png?dl=1" title="image%20(33)"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/f6dee108e2da6db34cd1cba91d6a0d9a16716dfa_2_587x472.png" alt="image%20(33)" data-base62-sha1="zdUYnzL8aG18Pe5Kzu0nDQhfFB8" width="587" height="472" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/f6dee108e2da6db34cd1cba91d6a0d9a16716dfa_2_587x472.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/f6dee108e2da6db34cd1cba91d6a0d9a16716dfa_2_880x708.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/f6dee108e2da6db34cd1cba91d6a0d9a16716dfa_2_1174x944.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/1X/f6dee108e2da6db34cd1cba91d6a0d9a16716dfa_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image%20(33)</span><span class="informations">1762×1418 162 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><em>Figure 1. PRV block rewards over time</em></p>
<h2>5M Pre-Mining</h2>
<p>5M PRV is pre-mined and purchased by the Core Development Team for the aforementioned $1M, and has been used to cover salaries, server costs, operational expenses, and marketing activities.</p>
<h2>Initial Staking</h2>
<p>There is a chicken-and-egg problem with any Proof-of-Stake blockchain. How can validators start staking when they don’t have coins to stake?</p>
<p>Incognito solves this in 2 ways:</p>
<ul>
<li>
<p>Testnet validators are rewarded with Testnet PRV, which can be converted 1:1 to Mainnet PRV at Mainnet launch.</p>
</li>
<li>
<p>Incognito has its own hardware, Node. If Node owners don’t have PRV to get started, they can borrow PRV from the Node Staking Fund, where anyone can lend out their PRV to Node owners.</p>
</li>
</ul>
<h2>Block Reward Split</h2>
<p>95% of PRV total supply is minted through block rewards. Block rewards are split between the validators and Incognito DAO, a decentralized autonomous organization designed to fund protocol development and network growth. Incognito DAO collects a gradually reducing percentage of the block rewards, from 10% to 3%.</p>
<p>With this income, Incognito DAO will fuel the growth of the network, fund interesting projects, and give the project longevity.</p>
<p>Incognito DAO’s funds are initially managed by the Core Development Team. Management responsibilities will be gradually distributed to the community.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th>Year</th>
<th>Validators</th>
<th>Incognito DAO</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>90%</td>
<td>10%</td>
</tr>
<tr>
<td>2</td>
<td>91%</td>
<td>9%</td>
</tr>
<tr>
<td>3</td>
<td>92%</td>
<td>8%</td>
</tr>
<tr>
<td>4</td>
<td>93%</td>
<td>7%</td>
</tr>
<tr>
<td>5</td>
<td>94%</td>
<td>6%</td>
</tr>
<tr>
<td>6</td>
<td>95%</td>
<td>5%</td>
</tr>
<tr>
<td>7</td>
<td>96%</td>
<td>4%</td>
</tr>
<tr>
<td>8+</td>
<td>97%</td>
<td>3%</td>
</tr>
</tbody>
</table>
</div><h2>PRV Denominations</h2>
<p>The smallest sub-denomination of Privacy is Nano. 1 Privacy is defined as 10<sup>9</sup> Nano. There exist other sub-denominations of Privacy.</p>
<p>In the future, we expect Privacy to be used for regular transactions, Milli for microtransactions, Micro for transaction fees, and Nano for technical discussions and implementations.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th>Multiplier</th>
<th>Name</th>
</tr>
</thead>
<tbody>
<tr>
<td>10<sup>9</sup>
</td>
<td>Privacy</td>
</tr>
<tr>
<td>10<sup>6</sup>
</td>
<td>Milli</td>
</tr>
<tr>
<td>10<sup>3</sup>
</td>
<td>Micro</td>
</tr>
<tr>
<td>10<sup>0</sup>
</td>
<td>Nano</td>
</tr>
</tbody>
</table>
</div><h2>Token System</h2>
<p>There are 3 types of tokens:</p>
<ul>
<li>
<p><strong>Privacy</strong>. Privacy (PRV) is Incognito’s native coin — a work token<sup>1</sup>. Users stake PRV to become validators. Validators earn block rewards in PRV and transaction fees in various cryptoassets (i.e. pBTC, pETH, etc). This model avoids speculators and only attracts people interested in growing the network. If the demand for private transactions grows, validators will earn more revenue, which naturally triggers an increase in the price of PRV.</p>
</li>
<li>
<p><strong>Bridged Privacy Coins</strong>. Anyone can convert cryptocurrencies (or “public coins”) on other blockchains (i.e. BTC, ETH, USDT) to privacy coins on Incognito (i.e. pBTC, pETH, pDAI). Privacy coins maintain a 1:1 peg and are completely confidential. Because of this, anyone can store, send and receive any cryptoassets with total privacy. Private coins can also be used to pay for transaction fees.</p>
</li>
<li>
<p><strong>Issue-Created Privacy Coins</strong>. Anyone can issue their own privacy coin on Incognito.</p>
</li>
</ul>
<h2>Transaction Fees</h2>
<p>Users can pay transaction fees in Incognito native token - PRV.</p>
<h3><a href="https://incognito.org/t/user-created-privacy-coins/173">User-Created Privacy Coins ▸</a></h3>
<h3><a href="https://incognito.org/t/use-cases-privacy-stablecoins-privacy-dex-confidential-crypto-payroll-and-more/174">Use Cases: Privacy Stablecoins, Privacy DEX, Confidential Crypto Payroll, and more ▸</a></h3>
<h3><a href="https://incognito.org/t/future-work-smart-contracts-confidential-assets-confidential-ip-and-more/175/2">Future Work: Smart Contracts, Confidential Assets, Confidential IP, and more ▸</a></h3>
<h3><a href="https://incognito.org/t/conclusions-acknowledgments-and-references/176">Conclusions, Acknowledgments, and References ▸</a></h3>
<h2></h2><p>
</p>
<p><sup>1</sup> <a href="https://multicoin.capital/2018/02/13/new-models-utility-tokens/">https://multicoin.capital/2018/02/13/new-models-utility-tokens/</a></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172/1</link>
        <pubDate>Sun, 09 Feb 2020 03:20:49 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-172-1</guid>
        <source url="https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172.rss">Network Incentive: Privacy (PRV) Mining &amp; Distribution</source>
      </item>
  </channel>
</rss>
