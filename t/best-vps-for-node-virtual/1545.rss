<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Best VPS for Node Virtual?</title>
    <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545</link>
    <description># What VPS should you use?

If you’ve seen @annie’s tutorial on [how to host a virtual node](https://incognito.org/t/how-to-host-a-virtual-node/194), then you know you’ll need a virtual private server, or VPS.  Below, we’ll do a comparison of a few popular services.

&gt; :warning: Running a Node Virtual is not for everyone. It requires a level of **technical experience which you should make sure you possess before committing to a service.** Check the [Node Virtual Tutorial](https://incognito.org/t/how-to-host-a-virtual-node/194) and these tutorials for setting up [AWS](https://incognito.org/t/how-to-launch-and-connect-your-aws-instance/61/2?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps), [Digital Ocean](https://incognito.org/t/how-to-launch-and-connect-your-digital-ocean-instance/67/3?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps), or [Vultr](https://incognito.org/t/how-to-launch-and-connect-your-vultr-instance/358?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps) servers before deciding. 

If you already host a Node Virtual, **help everyone out by telling us about your favorite VPS in the comments below!** Tell us:
- How much does it cost?
- What are the specifications (RAM, storage, etc)?
- Why do you use it/what do you like/dislike about it?
- Anything else you want to say.

Now, before we get into comparing some of the options, another **warning:**

&gt;**We do NOT recommend using Google Cloud** for your VPS. You will eventually be banned.

Now, on to the good stuff:

![Screen Shot 2020-04-17 at 12.52.39 PM|1540x744, 45%](upload://sz3gq8LMf9IYFzCxcWyITS5o1oY.png) 

&gt; **Disclaimer:** Incognito does not endorse any of the services found in this post, or in the comments. This is just a discussion around various hosting options. Please be cautious before committing to any service.

## Amazon Web Services (AWS)

If a megacorporation is more your style, [Amazon Web Services](https://aws.amazon.com/ec2/pricing/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps) might just be the way to go.

Reliable hosting with consistent uptime and scalable storage, but convoluted pricing and hosting. Can be confusing to navigate and you may require specialist support. AWS is designed for scalable businesses, so it might not be the best solution for an individual. But if you’ve found otherwise, let me know below!

&gt;**What you get: (A1.xlarge option**) ~$47 a month with a one-year term for 8GB RAM, 4vCPUs, EBS. You pay for how much you use, rather than a fixed monthly price.

**Can you upgrade easily?:** Not with a contract. Without a contract, yes, but the service will cost more on a month-by-month basis.

**Is backup included?:** No. You can add backup options in which you pay for what you use.

## Digital Ocean

[Digital Ocean](https://www.digitalocean.com/pricing/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps) is known for their great customer service. Servers are available worldwide. Simple, straightforward pricing and setup.

&gt;**What you get:** $40/month for 8GB RAM, 4vCPUs, and 5TB bandwidth, 80GB SSD.

**Can you upgrade easily?:** Yes. You can upgrade to a better plan at any time.

**Is backup included?:** No. But you can add it to any plan you get for an additional 20% of the base price

## Vultr

[Vultr](https://www.vultr.com/products/cloud-compute/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps) offers simple and easy-to-use interface with powerful computing and block storage that rivals AWS and is designed to be scalable. Fast network and a clean control panel for ease of use.

&gt;**What you get:** $40/month for 8GB RAM, 4vCPUs, 4TB bandwidth, 160GB SSD.

**Can you upgrade easily?:** Yes. Storage can be added at any time, or you can upgrade to a better plan.

**Is backup included?:** No. But you can add it to any plan you get for an additional 20% of the base price.

## Linode

[Linode](https://www.linode.com/pricing/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps) is well known, well-reviewed, and well-priced for its services. However, it is unmanaged hosting, so you have to be prepared to do it yourself. Not the easiest to use, but not difficult if you have the right skillset to maintain a virtual node yourself anyway.

&gt;**What you get:** $40/month for 8GB RAM, 4vCPUs, 5TB bandwidth, 160GB SSD.

**Can you upgrade easily?:** Yes. Attachable block storage options can be added any time, or you can upgrade to a better plan.

**Is backup included?:** No. You must purchase it from an external service.

## Contabo

Extremely affordable servers with good customer service, unlimited traffic, and incredibly simple setup, [Contabo](https://contabo.com/?show=configurator&amp;vserver_id=221&amp;utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps) might take the cake for a basic VPS. It is an unmanaged server, so security and upkeep is up to you. Every additional feature you require comes at an added cost.

&gt;**What you get:** 4.99 EUR ($5.41)/month for 8GB RAM, 4 vCPUs, unlimited bandwidth, 200GB SSD.

**Can you upgrade easily?:** Yes, you can upgrade to a better plan at any time.

**Is backup included?:** No, it must be purchased, 500GB for 11.99 EUR

## Conclusion
Choosing the right VPS for your preferences, skillset, and location is important, and these are just a few of the many options out there. If you already have a VPS you love, we&#39;d love to know. **Comment which one you use, and why!**

**Related topics:**
[How to set up Digital Ocean](https://incognito.org/t/how-to-launch-and-connect-your-digital-ocean-instance/67/3?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps)
[How to set up Vultr](https://incognito.org/t/how-to-launch-and-connect-your-vultr-instance/358?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps)
[How to set up AWS](https://incognito.org/t/how-to-launch-and-connect-your-aws-instance/61/2?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps)
[How to host a virtual node](https://incognito.org/t/how-to-host-a-virtual-node/194)
[Node Physical](https://incognito.org/t/node/338)
[Node Pool](https://incognito.org/t/node-pool/1626#utm_source=discourse&amp;utm_medium=VPS_page)</description>
    
    <lastBuildDate>Fri, 01 May 2020 12:36:53 +0000</lastBuildDate>
    <category>Help</category>
    <atom:link href="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <p>Awesome! Thanks for making that!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/23">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/23</link>
        <pubDate>Fri, 01 May 2020 12:36:53 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-23</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@palumboe1 Eric]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/palumboe1">@palumboe1</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/aaron">@aaron</a><br>
<a class="mention" href="/u/joe_moffett">@Joe_Moffett</a><br>
<a class="mention" href="/u/j053">@J053</a></p>
<p>See tutorial here<br>
<aside class="quote quote-modified" data-post="1" data-topic="2015">
  <div class="title">
    <div class="quote-controls"></div>
    <img alt="" width="20" height="20" src="/user_avatar/incognito.org/palumboe1/40/3052_2.png" class="avatar">
    <a href="https://incognito.org/t/how-to-create-a-vnode-on-contabo/2015">How to create a Vnode on Contabo</a> <a class="badge-wrapper  box" href="/c/help/tutorials"><span class="badge-category-parent-bg" style="background-color: #1ABC9C;"></span><span class="badge-category-bg" style="background-color: #1ABC9C;"></span><span style="color: #FFFFFF" data-drop-close="true" class="badge-category clear-badge">Tutorials</span></a>
  </div>
  <blockquote>
    Sign up for Contabo and create an order for a VPS 100% SSD. I would check the box for in USA as well, and I would not get the cheapest one I would get the one for $8.99/mo. 


Confirm the order via email or the order will not process. It will take up to 24hrs to process, but mine took 3 hrs. 


Once the Server setup email is received, In this email you will have your server credentials. 


ssh into your server, open up a terminal run “ssh root@”… then enter in the password in the email from st…
  </blockquote>
</aside>
</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/22">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/22</link>
        <pubDate>Thu, 30 Apr 2020 16:53:44 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-22</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@palumboe1 Eric]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/palumboe1">@palumboe1</a> wrote:</p>
          <blockquote>
              <p>Definitely I’ll help you stake on your own, for a shirt!! Hell yeah, I’ll write some instructions.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/21">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/21</link>
        <pubDate>Thu, 30 Apr 2020 16:43:51 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-21</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Joe_Moffett Moff]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/joe_moffett">@Joe_Moffett</a> wrote:</p>
          <blockquote>
              <p>thanks <a class="mention" href="/u/palumboe1">@palumboe1</a> Might circle back with you for some help to set up on cantabo. I think you offered to stake on a node you set up on a recent post? but if you could help set one up myself, i got a shirt with your name on it haha</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/20">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/20</link>
        <pubDate>Thu, 30 Apr 2020 16:43:16 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-20</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@palumboe1 Eric]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/palumboe1">@palumboe1</a> wrote:</p>
          <blockquote>
              <p>Aws is way to expensive I would get if off there asap</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/19">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/19</link>
        <pubDate>Thu, 30 Apr 2020 16:40:59 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-19</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Joe_Moffett Moff]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/joe_moffett">@Joe_Moffett</a> wrote:</p>
          <blockquote>
              <p>Wondering if anyone can help me understand something.</p>
<p>I set up an AWS vNode last Thursday I believe and so far it’s charged for 156 hours for t3a.xlarge on demand, which was what the setup said to do. It charges a rate of .15 cents per hour, so for just one week, it’s costing $23, but looking at this it says it’s $47 for the month.</p>
<p>Does that number sound right? Should I switch and is will the benefit outweigh the cost? It looks like there was a fair amount of data transfer just over that past week, but not sure if that means anything for mining since I was just able to put funds in my vNode to start staking.</p>
<p>Any thoughts? and should I switch when I get a chance?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/18">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/18</link>
        <pubDate>Thu, 30 Apr 2020 16:36:31 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-18</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@palumboe1 Eric]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/palumboe1">@palumboe1</a> wrote:</p>
          <blockquote>
              <p>Same setup as docker!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/17">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/17</link>
        <pubDate>Thu, 30 Apr 2020 14:27:20 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-17</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/palumboe1">@palumboe1</a> just set one up, he might be able to help.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/16">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/16</link>
        <pubDate>Thu, 30 Apr 2020 14:20:40 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-16</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@J053 Jose Fabio]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/j053">@J053</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="aaron" data-post="1" data-topic="1545">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://incognito.org/user_avatar/incognito.org/aaron/40/1203_2.png" class="avatar"> aaron:</div>
<blockquote>
<p>Contabo</p>
</blockquote>
</aside>
<p>Is there any tutorial on how to setup a vNode for this server?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/15">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/15</link>
        <pubDate>Thu, 30 Apr 2020 14:18:52 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-15</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@palumboe1 Eric]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/palumboe1">@palumboe1</a> wrote:</p>
          <blockquote>
              <p>Contabo is very cheap, I just switched my digital ocean instance of $40/mo to contabo for a bigger and faster instance for $10/mo. They also take PayPal which will charge you in your countries dollar and convert it to Euros, so your card won’t decline.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/14">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/14</link>
        <pubDate>Wed, 29 Apr 2020 11:37:14 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-14</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Peter Peter]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/peter">@Peter</a> wrote:</p>
          <blockquote>
              <p>You can see it here: <a href="https://incognito.org/t/solved-change-vnode-machine/1220/3" class="inline-onebox">[SOLVED] Change vNode machine</a></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/13">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/13</link>
        <pubDate>Fri, 24 Apr 2020 05:06:59 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-13</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p>Last question I promise <img src="https://incognito.org/images/emoji/apple/grinning.png?v=9" title=":grinning:" class="emoji" alt=":grinning:"></p>
<p>Any advice or guidance on how to go about switching servers? What’s the best way to take the node I have now. Export it from my current server and then unpackage it on the new server?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/12">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/12</link>
        <pubDate>Thu, 23 Apr 2020 23:14:11 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-12</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Jamie Jamie]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jamie">@Jamie</a> wrote:</p>
          <blockquote>
              <p>6-12 months, 5 servers.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/11">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/11</link>
        <pubDate>Thu, 23 Apr 2020 21:31:11 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-11</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p>Can I how long you have used their services? &lt;6 months? 6 - 12 months? 1+ years?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/10">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/10</link>
        <pubDate>Thu, 23 Apr 2020 21:26:32 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-10</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Jamie Jamie]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jamie">@Jamie</a> wrote:</p>
          <blockquote>
              <p>I am using Contabo, for several projects. No problems yet.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/9">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/9</link>
        <pubDate>Thu, 23 Apr 2020 21:19:18 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-9</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Jared]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jared">@Jared</a> wrote:</p>
          <blockquote>
              <p>Does anyone here have experience using Contabo? Prices seems like a clear winner over price and choice. Would like to hear what others have to say about it before jumping ship from Vultr.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/8">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/8</link>
        <pubDate>Thu, 23 Apr 2020 21:11:39 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-8</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@Jamie Jamie]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jamie">@Jamie</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/mir">@mir</a> Can you share your experience with these services? That is what brings value to the suggested VPS services.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/6</link>
        <pubDate>Wed, 22 Apr 2020 00:40:26 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-6</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@mir mir]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/mir">@mir</a> wrote:</p>
          <blockquote>
              <p><aside class="onebox whitelistedgeneric">
  <header class="source">
      <img src="https://azurecomcdn.azureedge.net/cvt-c9006b7b3e6c0738a1f9731e6d7971e6e5a108d94feb533e41de4eb78e08795c/images/icon/favicon.ico" class="site-icon" width="128" height="128">
      <a href="https://azure.microsoft.com/en-us/" target="_blank" rel="nofollow noopener">azure.microsoft.com</a>
  </header>
  <article class="onebox-body">
    <img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/b/bfb5481a0bb22c6240db2fa7135fab0adcb52c89.png" class="thumbnail onebox-avatar" width="250" height="250">

<h3><a href="https://azure.microsoft.com/en-us/" target="_blank" rel="nofollow noopener">Cloud Computing Services | Microsoft Azure</a></h3>

<p>Invent with purpose, realize cost savings, and make your organization more efficient with Microsoft Azure’s open and flexible cloud computing platform.</p>


  </article>
  <div class="onebox-metadata">
    
    
  </div>
  <div style="clear: both"></div>
</aside>
<br>
<aside class="onebox whitelistedgeneric">
  <header class="source">
      <img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/0/0062b935104530993cdb6098930247b9574d87f6.png" class="site-icon" width="32" height="32">
      <a href="https://cloud.yandex.com/" target="_blank" rel="nofollow noopener">cloud.yandex.com</a>
  </header>
  <article class="onebox-body">
    

<h3><a href="https://cloud.yandex.com/" target="_blank" rel="nofollow noopener">Yandex.Cloud</a></h3>

<p>Use Yandex infrastructure, data processing services, and unique technologies to create and develop your digital services.</p>


  </article>
  <div class="onebox-metadata">
    
    
  </div>
  <div style="clear: both"></div>
</aside>
</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/5</link>
        <pubDate>Tue, 21 Apr 2020 20:39:46 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-5</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <p>Thanks! Do you have a favorite VPS? Feel free to comment about it here!</p>
<p>This is a discussion thread, so I’d love for you to suggest a VPS. That way, people who are looking for one can have many options to choose from.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/4</link>
        <pubDate>Tue, 21 Apr 2020 20:25:47 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-4</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@mir mir]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/mir">@mir</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/aaron">@aaron</a> Cool idea <img src="https://incognito.org/images/emoji/apple/v.png?v=9" title=":v:" class="emoji" alt=":v:">, what I would like to add is other VPS <img src="https://incognito.org/images/emoji/apple/wink.png?v=9" title=":wink:" class="emoji" alt=":wink:"> And your help in getting a free VPS for validators, have you thought about it?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/3</link>
        <pubDate>Tue, 21 Apr 2020 20:24:14 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-3</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/2</link>
        <pubDate>Tue, 21 Apr 2020 11:43:14 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-2</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
      <item>
        <title>Best VPS for Node Virtual?</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <h1>What VPS should you use?</h1>
<p>If you’ve seen <a class="mention" href="/u/annie">@annie</a>’s tutorial on <a href="https://incognito.org/t/how-to-host-a-virtual-node/194">how to host a virtual node</a>, then you know you’ll need a virtual private server, or VPS.  Below, we’ll do a comparison of a few popular services.</p>
<blockquote>
<p><img src="https://incognito.org/images/emoji/apple/warning.png?v=9" title=":warning:" class="emoji" alt=":warning:"> Running a Node Virtual is not for everyone. It requires a level of <strong>technical experience which you should make sure you possess before committing to a service.</strong> Check the <a href="https://incognito.org/t/how-to-host-a-virtual-node/194">Node Virtual Tutorial</a> and these tutorials for setting up <a href="https://incognito.org/t/how-to-launch-and-connect-your-aws-instance/61/2?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">AWS</a>, <a href="https://incognito.org/t/how-to-launch-and-connect-your-digital-ocean-instance/67/3?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">Digital Ocean</a>, or <a href="https://incognito.org/t/how-to-launch-and-connect-your-vultr-instance/358?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">Vultr</a> servers before deciding.</p>
</blockquote>
<p>If you already host a Node Virtual, <strong>help everyone out by telling us about your favorite VPS in the comments below!</strong> Tell us:</p>
<ul>
<li>How much does it cost?</li>
<li>What are the specifications (RAM, storage, etc)?</li>
<li>Why do you use it/what do you like/dislike about it?</li>
<li>Anything else you want to say.</li>
</ul>
<p>Now, before we get into comparing some of the options, another <strong>warning:</strong></p>
<blockquote>
<p><strong>We do NOT recommend using Google Cloud</strong> for your VPS. You will eventually be banned.</p>
</blockquote>
<p>Now, on to the good stuff:</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/c/c8333c31f4611348bed83acb2573d9d0915963a0.png" alt="Screen Shot 2020-04-17 at 12.52.39 PM" data-base62-sha1="sz3gq8LMf9IYFzCxcWyITS5o1oY" width="693" height="334"></p>
<blockquote>
<p><strong>Disclaimer:</strong> Incognito does not endorse any of the services found in this post, or in the comments. This is just a discussion around various hosting options. Please be cautious before committing to any service.</p>
</blockquote>
<h2>Amazon Web Services (AWS)</h2>
<p>If a megacorporation is more your style, <a href="https://aws.amazon.com/ec2/pricing/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">Amazon Web Services</a> might just be the way to go.</p>
<p>Reliable hosting with consistent uptime and scalable storage, but convoluted pricing and hosting. Can be confusing to navigate and you may require specialist support. AWS is designed for scalable businesses, so it might not be the best solution for an individual. But if you’ve found otherwise, let me know below!</p>
<blockquote>
<p><strong>What you get: (A1.xlarge option</strong>) ~$47 a month with a one-year term for 8GB RAM, 4vCPUs, EBS. You pay for how much you use, rather than a fixed monthly price.</p>
</blockquote>
<p><strong>Can you upgrade easily?:</strong> Not with a contract. Without a contract, yes, but the service will cost more on a month-by-month basis.</p>
<p><strong>Is backup included?:</strong> No. You can add backup options in which you pay for what you use.</p>
<h2>Digital Ocean</h2>
<p><a href="https://www.digitalocean.com/pricing/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">Digital Ocean</a> is known for their great customer service. Servers are available worldwide. Simple, straightforward pricing and setup.</p>
<blockquote>
<p><strong>What you get:</strong> $40/month for 8GB RAM, 4vCPUs, and 5TB bandwidth, 80GB SSD.</p>
</blockquote>
<p><strong>Can you upgrade easily?:</strong> Yes. You can upgrade to a better plan at any time.</p>
<p><strong>Is backup included?:</strong> No. But you can add it to any plan you get for an additional 20% of the base price</p>
<h2>Vultr</h2>
<p><a href="https://www.vultr.com/products/cloud-compute/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">Vultr</a> offers simple and easy-to-use interface with powerful computing and block storage that rivals AWS and is designed to be scalable. Fast network and a clean control panel for ease of use.</p>
<blockquote>
<p><strong>What you get:</strong> $40/month for 8GB RAM, 4vCPUs, 4TB bandwidth, 160GB SSD.</p>
</blockquote>
<p><strong>Can you upgrade easily?:</strong> Yes. Storage can be added at any time, or you can upgrade to a better plan.</p>
<p><strong>Is backup included?:</strong> No. But you can add it to any plan you get for an additional 20% of the base price.</p>
<h2>Linode</h2>
<p><a href="https://www.linode.com/pricing/?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">Linode</a> is well known, well-reviewed, and well-priced for its services. However, it is unmanaged hosting, so you have to be prepared to do it yourself. Not the easiest to use, but not difficult if you have the right skillset to maintain a virtual node yourself anyway.</p>
<blockquote>
<p><strong>What you get:</strong> $40/month for 8GB RAM, 4vCPUs, 5TB bandwidth, 160GB SSD.</p>
</blockquote>
<p><strong>Can you upgrade easily?:</strong> Yes. Attachable block storage options can be added any time, or you can upgrade to a better plan.</p>
<p><strong>Is backup included?:</strong> No. You must purchase it from an external service.</p>
<h2>Contabo</h2>
<p>Extremely affordable servers with good customer service, unlimited traffic, and incredibly simple setup, <a href="https://contabo.com/?show=configurator&amp;vserver_id=221&amp;utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">Contabo</a> might take the cake for a basic VPS. It is an unmanaged server, so security and upkeep is up to you. Every additional feature you require comes at an added cost.</p>
<blockquote>
<p><strong>What you get:</strong> 4.99 EUR ($5.41)/month for 8GB RAM, 4 vCPUs, unlimited bandwidth, 200GB SSD.</p>
</blockquote>
<p><strong>Can you upgrade easily?:</strong> Yes, you can upgrade to a better plan at any time.</p>
<p><strong>Is backup included?:</strong> No, it must be purchased, 500GB for 11.99 EUR</p>
<h2>Conclusion</h2>
<p>Choosing the right VPS for your preferences, skillset, and location is important, and these are just a few of the many options out there. If you already have a VPS you love, we’d love to know. <strong>Comment which one you use, and why!</strong></p>
<p><strong>Related topics:</strong><br>
<a href="https://incognito.org/t/how-to-launch-and-connect-your-digital-ocean-instance/67/3?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">How to set up Digital Ocean</a><br>
<a href="https://incognito.org/t/how-to-launch-and-connect-your-vultr-instance/358?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">How to set up Vultr</a><br>
<a href="https://incognito.org/t/how-to-launch-and-connect-your-aws-instance/61/2?utm_source=google%2C%20duckduckgo%2C%20firefox%2C%20safari&amp;utm_medium=organic%2C%20social&amp;utm_campaign=vps">How to set up AWS</a><br>
<a href="https://incognito.org/t/how-to-host-a-virtual-node/194">How to host a virtual node</a><br>
<a href="https://incognito.org/t/node/338">Node Physical</a><br>
<a href="https://incognito.org/t/node-pool/1626#utm_source=discourse&amp;utm_medium=VPS_page">Node Pool</a></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/best-vps-for-node-virtual/1545/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/best-vps-for-node-virtual/1545/1</link>
        <pubDate>Thu, 16 Apr 2020 20:50:33 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1545-1</guid>
        <source url="https://we.incognito.org/t/best-vps-for-node-virtual/1545.rss">Best VPS for Node Virtual?</source>
      </item>
  </channel>
</rss>
