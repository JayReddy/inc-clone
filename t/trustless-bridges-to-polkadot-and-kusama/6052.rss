<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Trustless bridges to Polkadot and Kusama</title>
    <link>https://we.incognito.org/t/trustless-bridges-to-polkadot-and-kusama/6052</link>
    <description>Hey guys :wave:

A week ago we kickstarted the development of [Portal v3](https://we.incognito.org/t/trustless-custodians-a-decentralized-approach-to-cryptocurrency-custodianship/84). This technology will be utilized to maintain a trustless bridge to the Polkadot and Kusama networks. 

Here are more details about the process and timeline. 

## Project name

**Privacy for Polkadot** and beyond through a decentralized approach to cryptocurrency custodianship.


## Project description

Incognito is a privacy project focused on delivering trustless privacy for any crypto asset, and any dApp. Our main areas of research are in scalability, interoperability, and of course, privacy.

Through Incognito, all crypto transactions are confidential and untraceable. Incognito uses several cryptographic primitives, such as linkable ring signature scheme, homomorphic commitment scheme, and zero-knowledge range proofs, to shield sending addresses, receiving addresses, and transacted amounts.

Portal is a universal bridge that connects Incognito’s privacy layer to Polkadot, and to other blockchains like Bitcoin that lack a smart contract language. Ultimately, Portal is designed to connect blockchains to each other through a common need for privacy. It uses a system of over-collateralized bonds, similar to how the DAI stablecoin is created on MakerDAO. The first implementation is a programmed smart contract, which controls bonds and provides real-time processing, as opposed to the multi-day manual process adopted by centralized custodian companies.

As for what success looks like for our project, we are focused on enabling better privacy, for more people. We also hope that the Web3 community will find our decentralized approach to custodianship helpful, and expand upon it to build more fully-decentralized systems of custodians.

## Impact

For crypto users today, having privacy is hard. They are required to either compromise privacy to stay in their favorite ecosystem, or leave their ecosystem entirely to have privacy. This divide benefits no one. We strongly believe that the ideal privacy solution is not another privacy coin. The ideal solution embraces diversity, supports individual choice – and above all, is easy and safe to use. We aim to make having privacy simpler, safer, and more inclusive – for anyone who owns crypto now or will in the future.


## Development Roadmap
![image|1280x1192, 50%](upload://nw5pFZFP7GYGXPCqlQjcgVTSbxa.png)  

### Milestone 1 - Implement shield process 

* Shielding via a bond contract that allows the user to turn DOT into privacy DOT (pDOT). With the new bond contract, custodians have one more option for collateralization, i.e. DOT alongside PRV (Incognito’s native token)

* Working module deployed on Incognito’s devnet (or testnet) environment, so anyone can test the new functionality.
* Unit-test coverage to ensure functionality and robustness.
* Documentation describing how the module can be used and tested.

### Milestone 2 - Implement auto-liquidation process

* Implement the auto-liquidation process to handle 2 cases: custodian misbehavior or drop in the value of bonded collateral.

* Deliver a working module deployed on Incognito’s devnet (or testnet) environment.

* Unit-test coverage to ensure functionality and robustness.

* Documentation describing how the module can be used and tested.

### Milestone 3 - Implement unshield process 

* Implement unshield process to turn shielded pDOT back into DOT.

* Deliver a working module deployed on Incognito’s devnet (or testnet) environment.

* Unit-test coverage to ensure functionality and robustness.

* Documentation describing how the module can be used and tested.

### Milestone 4 - Test, fix bugs and document 

* Integration test for the 3 implemented modules to ensure the protocol works well.

* Complete documentation on our exposed APIs, how the protocol works as well as test-cases.

## Future Plans

Once this initiative is complete, we will continue to contribute to the Web3 ecosystem. Here is a list of potential next steps.

1. Maintain a privacy bridge for Kusama based on the implementation described above.

2. Maintain liquidity for Kusama and Polkadot on the Incognito DEX to allow cross-chain swaps. Adding DOT and KSM to https://we.incognito.org/t/provide/4247, allowing token holders to earn rewards for contributing liquidity.
3. Research into enabling privacy for other assets on the Polkadot and Kusama networks.
4. Explore the possibility of integrating Kusama and Polkadot networks into  Incognito&#39;s [Node Tree](https://we.incognito.org/t/node-tree-multi-pnode-set/523/59) devices.</description>
    
    <lastBuildDate>Mon, 14 Sep 2020 11:45:56 +0000</lastBuildDate>
    <category>Ideas</category>
    <atom:link href="https://we.incognito.org/t/trustless-bridges-to-polkadot-and-kusama/6052.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Trustless bridges to Polkadot and Kusama</title>
        <dc:creator><![CDATA[@andrey A ]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/andrey">@andrey</a> wrote:</p>
          <blockquote>
              <p>Hey guys <img src="https://we.incognito.org/images/emoji/apple/wave.png?v=9" title=":wave:" class="emoji" alt=":wave:"></p>
<p>A week ago we kickstarted the development of <a href="https://we.incognito.org/t/trustless-custodians-a-decentralized-approach-to-cryptocurrency-custodianship/84">Portal v3</a>. This technology will be utilized to maintain a trustless bridge to the Polkadot and Kusama networks.</p>
<p>Here are more details about the process and timeline.</p>
<h2>Project name</h2>
<p><strong>Privacy for Polkadot</strong> and beyond through a decentralized approach to cryptocurrency custodianship.</p>
<h2>Project description</h2>
<p>Incognito is a privacy project focused on delivering trustless privacy for any crypto asset, and any dApp. Our main areas of research are in scalability, interoperability, and of course, privacy.</p>
<p>Through Incognito, all crypto transactions are confidential and untraceable. Incognito uses several cryptographic primitives, such as linkable ring signature scheme, homomorphic commitment scheme, and zero-knowledge range proofs, to shield sending addresses, receiving addresses, and transacted amounts.</p>
<p>Portal is a universal bridge that connects Incognito’s privacy layer to Polkadot, and to other blockchains like Bitcoin that lack a smart contract language. Ultimately, Portal is designed to connect blockchains to each other through a common need for privacy. It uses a system of over-collateralized bonds, similar to how the DAI stablecoin is created on MakerDAO. The first implementation is a programmed smart contract, which controls bonds and provides real-time processing, as opposed to the multi-day manual process adopted by centralized custodian companies.</p>
<p>As for what success looks like for our project, we are focused on enabling better privacy, for more people. We also hope that the Web3 community will find our decentralized approach to custodianship helpful, and expand upon it to build more fully-decentralized systems of custodians.</p>
<h2>Impact</h2>
<p>For crypto users today, having privacy is hard. They are required to either compromise privacy to stay in their favorite ecosystem, or leave their ecosystem entirely to have privacy. This divide benefits no one. We strongly believe that the ideal privacy solution is not another privacy coin. The ideal solution embraces diversity, supports individual choice – and above all, is easy and safe to use. We aim to make having privacy simpler, safer, and more inclusive – for anyone who owns crypto now or will in the future.</p>
<h2>Development Roadmap</h2>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/a/a4d28db867d49042851d57086e59af6fb4db72d4.png" alt="image" data-base62-sha1="nw5pFZFP7GYGXPCqlQjcgVTSbxa" width="640" height="596"></p>
<h3>Milestone 1 - Implement shield process</h3>
<ul>
<li>
<p>Shielding via a bond contract that allows the user to turn DOT into privacy DOT (pDOT). With the new bond contract, custodians have one more option for collateralization, i.e. DOT alongside PRV (Incognito’s native token)</p>
</li>
<li>
<p>Working module deployed on Incognito’s devnet (or testnet) environment, so anyone can test the new functionality.</p>
</li>
<li>
<p>Unit-test coverage to ensure functionality and robustness.</p>
</li>
<li>
<p>Documentation describing how the module can be used and tested.</p>
</li>
</ul>
<h3>Milestone 2 - Implement auto-liquidation process</h3>
<ul>
<li>
<p>Implement the auto-liquidation process to handle 2 cases: custodian misbehavior or drop in the value of bonded collateral.</p>
</li>
<li>
<p>Deliver a working module deployed on Incognito’s devnet (or testnet) environment.</p>
</li>
<li>
<p>Unit-test coverage to ensure functionality and robustness.</p>
</li>
<li>
<p>Documentation describing how the module can be used and tested.</p>
</li>
</ul>
<h3>Milestone 3 - Implement unshield process</h3>
<ul>
<li>
<p>Implement unshield process to turn shielded pDOT back into DOT.</p>
</li>
<li>
<p>Deliver a working module deployed on Incognito’s devnet (or testnet) environment.</p>
</li>
<li>
<p>Unit-test coverage to ensure functionality and robustness.</p>
</li>
<li>
<p>Documentation describing how the module can be used and tested.</p>
</li>
</ul>
<h3>Milestone 4 - Test, fix bugs and document</h3>
<ul>
<li>
<p>Integration test for the 3 implemented modules to ensure the protocol works well.</p>
</li>
<li>
<p>Complete documentation on our exposed APIs, how the protocol works as well as test-cases.</p>
</li>
</ul>
<h2>Future Plans</h2>
<p>Once this initiative is complete, we will continue to contribute to the Web3 ecosystem. Here is a list of potential next steps.</p>
<ol>
<li>
<p>Maintain a privacy bridge for Kusama based on the implementation described above.</p>
</li>
<li>
<p>Maintain liquidity for Kusama and Polkadot on the Incognito DEX to allow cross-chain swaps. Adding DOT and KSM to <a href="https://we.incognito.org/t/provide/4247" class="inline-onebox">Provide</a>, allowing token holders to earn rewards for contributing liquidity.</p>
</li>
<li>
<p>Research into enabling privacy for other assets on the Polkadot and Kusama networks.</p>
</li>
<li>
<p>Explore the possibility of integrating Kusama and Polkadot networks into  Incognito’s <a href="https://we.incognito.org/t/node-tree-multi-pnode-set/523/59">Node Tree</a> devices.</p>
</li>
</ol>
          </blockquote>
          <p><a href="https://we.incognito.org/t/trustless-bridges-to-polkadot-and-kusama/6052/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/trustless-bridges-to-polkadot-and-kusama/6052/1</link>
        <pubDate>Mon, 14 Sep 2020 11:28:33 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-6052-1</guid>
        <source url="https://we.incognito.org/t/trustless-bridges-to-polkadot-and-kusama/6052.rss">Trustless bridges to Polkadot and Kusama</source>
      </item>
  </channel>
</rss>
