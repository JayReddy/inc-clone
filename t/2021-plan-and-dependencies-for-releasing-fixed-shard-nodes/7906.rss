<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>2021 plan and dependencies for releasing fixed shard nodes</title>
    <link>https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906</link>
    <description>The Incognito network must eventually operate autonomously. One of the most important milestones is the release of fixed shard nodes, and a lot of work is being done in this department to ensure we can do so as soon as possible, as safely as possible.

Since we are all eager for this milestone to be completed, here is some contextual information on what is being done behind the scenes. We’d also like to invite you to share your opinions and expectations so we can work together towards this event.

### Challenges

Here are the 2 main challenges we need to address prior to the release:

**1. Security**
a. Malicious proposer: validators might not be able to verify that proposed blocks (and their txs) are malicious, due to a lack of validation code.
b. Validator collusion: validators and a proposer collude to agree on a malicious block containing double spending or illegal minting txs.

**2. Network stability**
a. Blocks would not be created if the majority of nodes in the network are not functional because of connection issues, power outage, etc.

### Solutions

**1. Security:**

The core dev team’s foremost priority is safety of user funds. Both internal and external code audits are submitted regularly to mitigate problem 1.a.

Problem 1.b is a bit trickier. If the validation code in 1.a is invalid because validators accept proposed blocks without any validation, another party is needed to re-verify these blocks and take action as soon as possible to prevent attackers from removing assets from Incognito.

Specifically, beacon nodes will re-verify all blocks sent by a shard, and if there are any invalid transactions detected in a block, the network will either be paused for a post mortem (the most practical thing we can do currently), or the malicious block will be rejected, and the shard’s committee swapped out.

**2. Network stability:**

Work currently being done on [Dynamic Committee Size (DCS)](https://we.incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961) is directed towards ensuring network stability in multiple eventualities. An essential feature of DCS is Slashing, which will force dysfunctional nodes that are not contributing to consensus to unstake. Slashing will be released on mainnet around Feb 2021, and the core dev team will endeavor to provide advance support in the form of notifications, dedicated validator support groups and so on to ensure the transition is as smooth as possible for honest validators. We hope that after slashing has run for a couple of months, the network will consist of only “healthy” nodes.

The clue is in the name – another integral part of DCS is making the committee size dynamic based on the number of substitutes in the shard pool. For more details regarding terminologies and mechanisms, [please have a look at this document](https://docs.google.com/document/d/19KChrg0B2LmfT_t-K8vovCG-mXUkAYG88j5FQsKPXT4/edit#heading=h.v9ucwlffgwj0) and feel free to leave your comments. This is aimed at making the network more fault tolerant. The tentative release date is June 2021.

### The plan

This is the current plan of action:

* The network will be released in stages to ensure continued stability. For example, keeping 6 proposers to propose blocks in a round-robin fashion and releasing 16 validators to the community. This will only happen if the DCS runs stably for a period of time, say 1-2 months (August 2021). By being cautious in this way, we minimize the likelihood of security issues, and will have more time to ensure the system is airtight.

* Once we feel confident in the security of the network, we will release all shard proposers to the community. The core team will keep just the beacon nodes in the meantime, for security reasons stated in the sections above. We also hope to release these at a future date.

We hope this gives you a little clarity on the work being done towards decentralization and full autonomy. Releasing the network in a stable and secure way is a shared goal – many of you contribute to the network in some way by taking on the roles of validator, provider, user, or community member.

So please share your thoughts. Do you have any questions, or ideas on how to ease this transition? We’re excited to work towards this huge milestone with you.</description>
    
    <lastBuildDate>Wed, 25 Nov 2020 12:02:55 +0000</lastBuildDate>
    <category>👾 Tech</category>
    <atom:link href="https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>2021 plan and dependencies for releasing fixed shard nodes</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <p>hey <a class="mention" href="/u/doc">@doc</a>, thanks for sharing your thoughts, from my point of view, there will be two things (or tools for more correctly) may help validators against the transition, (I guess these tools should be provided probably even prior to slashing’s launch):</p>
<ol>
<li>
<p>The way for a validator to observe how many blocks a node actually validates during the time it being in a committee. Noticed that a validator just gets slashed if its validated blocks smaller a certain threshold.</p>
</li>
<li>
<p>Yes, as you suggested, a notification may help also if there are some things wrong with the node that could affect its contribution to the network’s consensus (a.k.a validating blocks).</p>
</li>
</ol>
<p>These are just some things in my head currently, any other suggestions to help the transition happen better <a class="mention" href="/u/doc">@doc</a> ?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906/4</link>
        <pubDate>Wed, 25 Nov 2020 12:02:55 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-7906-4</guid>
        <source url="https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906.rss">2021 plan and dependencies for releasing fixed shard nodes</source>
      </item>
      <item>
        <title>2021 plan and dependencies for releasing fixed shard nodes</title>
        <dc:creator><![CDATA[@doc Corey Clark]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/doc">@doc</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/duc">@duc</a> Thank you for the post and giving some clarity of the process.  I agree that the process to release all of the current locked spots for validators in the committee should be a slow, tested and validated process.  It does no one any good to rush that and create an unstable/unsecure network.</p>
<p>With that in mind, I would like to focus in one specific comment you provided:</p>
<aside class="quote no-group" data-username="duc" data-post="1" data-topic="7906">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/duc/40/357_2.png" class="avatar"> duc:</div>
<blockquote>
<p>Slashing will be released on mainnet around Feb 2021, and the core dev team will endeavor to provide advance support in the form of notifications, dedicated validator support groups and so on to ensure the transition is as smooth as possible for honest validators.</p>
</blockquote>
</aside>
<p>I believe the current group of validators online want to support the network and are looking to provide a reliable and secure validation of blocks, but I also have concerns that some may not be operating correctly, without the users knowledge.  I would ask that prior to deploying slashing, there be some sort of notification that could be created that would inform validators if they failed while in committee.  I would personally want to know if my pNode or vNodes are missing validation of blocks prior to slashing going into effect.  I think items, such as you listed above, like network connection or even improper vNode setup could cause validators to get slashed, but community members may not have any idea that their setup has any issues at all with the current process being used with fixed spots.</p>
<p>I wanted to specifically call this out, as to make sure when you say “notifications”, its not just related to roll out announcements, but is also related to giving operational feedback to current validators on the health of their nodes.</p>
<p>I know that I have seen my pNode go offline many times (hopefully this is fixed with the latest firmware update), but I do not have any real visibility as to how well my vNodes have been doing, or if they are actually validating blocks properly.  I would just like the opportunity to identify and fix issue, prior to being penalized and removed from the selection pool with slashing.</p>
<p>overall, I think this is best for the network transition overall and will improve the community experience as it gives everyone the the ability to make sure nodes are operating properly and giving the best chance possible for a stable and secure transfer to full decentralization.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906/3</link>
        <pubDate>Wed, 25 Nov 2020 05:55:48 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-7906-3</guid>
        <source url="https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906.rss">2021 plan and dependencies for releasing fixed shard nodes</source>
      </item>
      <item>
        <title>2021 plan and dependencies for releasing fixed shard nodes</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <p>The Incognito network must eventually operate autonomously. One of the most important milestones is the release of fixed shard nodes, and a lot of work is being done in this department to ensure we can do so as soon as possible, as safely as possible.</p>
<p>Since we are all eager for this milestone to be completed, here is some contextual information on what is being done behind the scenes. We’d also like to invite you to share your opinions and expectations so we can work together towards this event.</p>
<h3>Challenges</h3>
<p>Here are the 2 main challenges we need to address prior to the release:</p>
<p><strong>1. Security</strong><br>
a. Malicious proposer: validators might not be able to verify that proposed blocks (and their txs) are malicious, due to a lack of validation code.<br>
b. Validator collusion: validators and a proposer collude to agree on a malicious block containing double spending or illegal minting txs.</p>
<p><strong>2. Network stability</strong><br>
a. Blocks would not be created if the majority of nodes in the network are not functional because of connection issues, power outage, etc.</p>
<h3>Solutions</h3>
<p><strong>1. Security:</strong></p>
<p>The core dev team’s foremost priority is safety of user funds. Both internal and external code audits are submitted regularly to mitigate problem 1.a.</p>
<p>Problem 1.b is a bit trickier. If the validation code in 1.a is invalid because validators accept proposed blocks without any validation, another party is needed to re-verify these blocks and take action as soon as possible to prevent attackers from removing assets from Incognito.</p>
<p>Specifically, beacon nodes will re-verify all blocks sent by a shard, and if there are any invalid transactions detected in a block, the network will either be paused for a post mortem (the most practical thing we can do currently), or the malicious block will be rejected, and the shard’s committee swapped out.</p>
<p><strong>2. Network stability:</strong></p>
<p>Work currently being done on <a href="https://we.incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961">Dynamic Committee Size (DCS)</a> is directed towards ensuring network stability in multiple eventualities. An essential feature of DCS is Slashing, which will force dysfunctional nodes that are not contributing to consensus to unstake. Slashing will be released on mainnet around Feb 2021, and the core dev team will endeavor to provide advance support in the form of notifications, dedicated validator support groups and so on to ensure the transition is as smooth as possible for honest validators. We hope that after slashing has run for a couple of months, the network will consist of only “healthy” nodes.</p>
<p>The clue is in the name – another integral part of DCS is making the committee size dynamic based on the number of substitutes in the shard pool. For more details regarding terminologies and mechanisms, <a href="https://docs.google.com/document/d/19KChrg0B2LmfT_t-K8vovCG-mXUkAYG88j5FQsKPXT4/edit#heading=h.v9ucwlffgwj0">please have a look at this document</a> and feel free to leave your comments. This is aimed at making the network more fault tolerant. The tentative release date is June 2021.</p>
<h3>The plan</h3>
<p>This is the current plan of action:</p>
<ul>
<li>
<p>The network will be released in stages to ensure continued stability. For example, keeping 6 proposers to propose blocks in a round-robin fashion and releasing 16 validators to the community. This will only happen if the DCS runs stably for a period of time, say 1-2 months (August 2021). By being cautious in this way, we minimize the likelihood of security issues, and will have more time to ensure the system is airtight.</p>
</li>
<li>
<p>Once we feel confident in the security of the network, we will release all shard proposers to the community. The core team will keep just the beacon nodes in the meantime, for security reasons stated in the sections above. We also hope to release these at a future date.</p>
</li>
</ul>
<p>We hope this gives you a little clarity on the work being done towards decentralization and full autonomy. Releasing the network in a stable and secure way is a shared goal – many of you contribute to the network in some way by taking on the roles of validator, provider, user, or community member.</p>
<p>So please share your thoughts. Do you have any questions, or ideas on how to ease this transition? We’re excited to work towards this huge milestone with you.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906/1</link>
        <pubDate>Tue, 24 Nov 2020 09:38:45 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-7906-1</guid>
        <source url="https://we.incognito.org/t/2021-plan-and-dependencies-for-releasing-fixed-shard-nodes/7906.rss">2021 plan and dependencies for releasing fixed shard nodes</source>
      </item>
  </channel>
</rss>
