<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Letter to PRV Holders (November 2020)</title>
    <link>https://we.incognito.org/t/letter-to-prv-holders-november-2020/8124</link>
    <description>**To PRV Holders:**

2021 isn’t here yet, but for us at Incognito, we’re one month into a new year (our first birthday was last month). And with that new year came a new drive, a fresh disposition toward this mission we’re on, to bring privacy to crypto. 

**Last year, we built a fully-functioning privacy ecosystem** that includes the first anonymous, trustless DEX, the first multi-currency privacy wallet, the first trustless bridge to Ethereum, and quite a few more “firsts”.

**This year, we set out to make it self-sustaining.** We’re building all of the extra details and safeguards that prepare the network for release. It’s an intensive checklist that, when completed, will turn Incognito into a fully autonomous privacy network, **run by you.**

We’ve been at it for a month now, so it’s time to pause, reflect on what’s been done, and plan for what’s ahead.

[View PRV Holders presentation (November 2020) &gt;](https://we.incognito.org/t/prv-holders-presentation-november-2020/8135)
[Review the 2020 Roadmap &gt;](https://we.incognito.org/t/incognito-core-dev-2020-live-roadmap/552)
[Explore the 2021 Roadmap &gt;](https://we.incognito.org/t/incognitos-2021-privacy-roadmap-for-the-world/7235)
[Get PRV &gt;](https://we.incognito.org/t/where-to-get-prv/253)

# Projects and progress

The [2021 privacy roadmap](https://we.incognito.org/t/incognitos-2021-privacy-roadmap-for-the-world/7235) lays out three essential ingredients for making Incognito a fully-decentralized and self-sustaining shield that protects you from the growing threat of surveillance: Privacy, resilience, and ownership. 

Each category has its own set of goals to reach, and many take multiple months to complete. We wasted no time getting busy in November. In the sections below, you’ll find a list of projects we started or continued this month, as well as a brief description of the main purpose of the project.

## Privacy 

We already built privacy, and every transaction you make on Incognito is anonymous. Now it’s time for Privacy v2, focused on ease of use, and extending privacy to every blockchain application. To that end, this month we’ve been continuing to build Portal, and developed and tested new privacy features like confidential assets and one-time addresses, which hide more transaction details and prevent transactions from being linked to one another. 

|Project|Purpose|
|-|-|
|[Portal v3](https://we.incognito.org/t/incognitos-trustless-bridge-v3-with-ethereum-bond-contract/2963/14)| Trustless bridge for coins w/o smart contract language|
|[Confidential assets](https://we.incognito.org/t/confidential-asset/5607)|Hide sender, receiver, and amount|
|[One-time addresses](https://we.incognito.org/t/confidential-asset/5607)|Make tx unlinkable|
|[View key](https://we.incognito.org/t/confidential-asset/5607)|Encrypted balances, user-controlled distributed access|

## Resilience

For the network to be released, it has to be tough enough to stand on its own two feet. To ensure privacy can withstand anything, we’re testing every part of Incognito, and building some long-awaited features. Chief among them are things like dynamic committee size and dynamic sharding, which boost efficiency and help the network scale. When finished, we’ll be able to release the fixed validator slots as well!  

In November, a big part of increasing resilience was testing. We’re taking the time to thoroughly inspect what we develop at each stage.

|Project|Purpose|
|-|-|
|[QA processes](https://we.incognito.org/t/quality-assurance-processes-for-incognito-blockchain-nov-2020/7666)|Testing, quality assurance, bug management| 
|[Dev framework](https://we.incognito.org/t/incognito-dev-framework/6820)|Tools for simulating blockchain for testing| 
|[Block creation optimization](https://we.incognito.org/t/optimization-of-block-creation/7088)|Improve throughput|
|[Dynamic committee size and dynamic sharding](https://we.incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961)| Network adaptability and chain robustness, release fixed validator slots
|[Implement slashing](https://we.incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961)|Remove misbehaving nodes
|HD Wallet (Dec)[(App v4)](https://we.incognito.org/t/app-v-4/7108)|Mnemonic key management|

## Ownership

We’ve said from the start that Incognito isn’t a company, and it’s not just the core team. It’s all of us. While we’re stewarding the network for now, every step we take brings us one step closer to handing it over to you. This month, we made progress toward growing your control in several areas: 

Privacy products like the wallet and pDEX (app) give you control over your personal privacy, while the Nodes like the coming Node Tree give you a piece of shared control of the privacy network. Documentation and an API layer make it easy for builders to take charge of development alongside the core team.  

|Project|Purpose|
|-|-|
|[Node Tree](https://we.incognito.org/t/node-tree-multi-pnode-set/523)|Multi-pNode stackable device
|[Reliable API layer for builders](https://we.incognito.org/t/incognito-app-service/7106)| Data sync/organization of app services
|Documentation [(App v4)](https://we.incognito.org/t/app-v-4/7108)|Complete Incognito resources for builders
|[Helpful community](https://we.incognito.org/t/build-a-helpful-community/8107)|Further build community and culture for network management
|pDEX improvements 1 [(App v4)](https://we.incognito.org/t/app-v-4/7108)|Price impact display, UX improvements
|[Grow shielded tx](https://we.incognito.org/t/grow-number-of-shielded-tx/7155)|Grow network use with Privacy Quest, media, project collaborations

# By the numbers

In November, as in every month before, growth has been accelerating project-wide. In fact, we hit [3 major milestones](https://we.incognito.org/t/incognito-meets-major-shielding-trading-and-liquidity-milestones-all-at-once/7823) this month, one in each of the categories below. Here’s a bird’s-eye-view of the month’s progress:

## Monthly shield volume: +$7M 

Shield volume was a major focus for us in November. When a large amount of people anonymize a large amount of crypto, it proves that Incognito’s mission is important, and that we’re addressing a real need in the world.

This month, $7 million worth of crypto was deposited in the Incognito network. To put this in perspective, shield volume grew to $2 million from the launch of the pDEX in Nov 2019 until mid April 2020. From then to the end of Nov 2020, it has now grown to reach $33 million.

![image|1024x768,69%](upload://rSfdfd6jzBdb1xyYU4mZI5jdiio.jpeg) 

## Monthly trading volume: +$7.4M

The rate of growth in trading volume is also increasing. On September 1st, 2020, total trading volume was at $9 million. Today, total trading volume exceeds $27 million.

We also saw some unique applications of trading pop up in the community, as liquidity pools for QUEST and LOTTO tokens were set up and people bought and sold what they earned in the Privacy Quest.

![image|1024x768,69%](upload://ui5dzOFdwlc5Z27Pt000egNDg7c.jpeg) 

## pDEX liquidity: +$5.5M 

Liquidity is vital to the utility of a crypto exchange. The more liquidity in the pDEX, the more crypto can be traded anonymously. In November, pDEX-only liquidity grew 46%, from $12 million on Nov 1st to $17.5 million on Dec 1st.

But that’s not all. We reintegrated Uniswap into the pDEX, bringing total combined liquidity to an astounding $1,175,000,000.

![image|1024x768,69%](upload://qJYT5wUYwer9zyKgHSSYqQP6DJQ.jpeg) 


# Builder development

One of the most promising signs that Incognito is flourishing can be found in the [Builders category](we.incognito.org/c/core-dev/community-dev). Multiple new projects were announced, existing products were improved, and both competition and output increased. To push the program to a new level of quality, we raised the bar and tightened eligibility criteria for the rewards program, and builders took the challenge in stride.

### New projects

- @q9656296 aims to make Incognito the default crypto privacy layer for Telegram with the [Incognito x Telegram integration](https://we.incognito.org/t/q9656296-integrate-incognito-into-telegram-poc/7903).

- @Andireuter is tackling a similar use case, working on an [Incognito x Signal integration](https://we.incognito.org/t/integrate-incognito-into-signal-poc/7412).

- @Sato’s [Convy the Conversion Helper](https://we.incognito.org/t/convy-your-personal-incognito-helper-bot/7089) converts currency amounts in your Incognito app to take the math out of understanding pDEX rates.

### Established projects

- @J053’s [ENSI Telegram Bot](https://we.incognito.org/t/ensi-telegram-bot-j053-fabi0/6831) helps validators track and manage their Node earnings more conveniently.

- @Abduraman’s [IncogWhaleBot](https://we.incognito.org/t/nitowhalebot-a-whale-alert-bot-for-incognito-ecosystem/6757) alerts you to massive trades and potential whale activity in the Incognito ecosystem.

- @Abduraman also built a [Python API for the Incognito SDK](https://we.incognito.org/t/abduraman-python-api-for-incognito-sdk/6972) to help builders do what they do best - build on Incognito.

- @Thriftinkid’s [Earnings Tracker](https://we.incognito.org/t/thriftinkid-how-to-track-your-incognito-rewards-through-google-sheets-v4/5857) enables validators to track the status and earnings of their Nodes, project future earnings, and track investments across the pDEX.

- @raz and the ZGEN team created [prv.finance - an Incognito web wallet](https://we.incognito.org/t/raz-we-made-a-web-wallet-for-you-and-your-friends/6235) to make trading from a browser possible.

- @Taind’s [Light Shadow Box](https://we.incognito.org/t/taind-light-shadow-box-anonymous-crypto-web-wallet-that-works/3233) is a web wallet extension that allows you to hold and swap crypto anonymously on Incognito from a web browser.

- @Incsmile’s [pStore.app](https://we.incognito.org/t/incsmile-pstore-app-a-platform-for-building-privacy-applications/4592) allows developers to build their own privacy applications on Incognito’s pEthereum functionality. It’s the start of what will be a vast ecosystem of privacy apps.

- @Josh_Hamon’s [Nito the Telegram bot](https://we.incognito.org/t/josh-hamon-nito-a-telegram-bot-for-push-notifications/3129) was Incognito’s first helpful friend. Nito can send info specific to your Node right to your Telegram. 

- @inccry’s [Network Explorer](https://we.incognito.org/t/network-explorer/2086) aggregates all sorts of data helpful to traders and privacy enthusiasts alike. The site is frequently upgraded with new features.

You can build for privacy, too! With Incognito, you’re free to build any privacy product you want, however you want. If you’re interested in the rewards program, take a look at the [eligibility criteria](https://we.incognito.org/t/builder-rewards-what-every-builder-must-know-eligibility-criteria/5198/).


# Goals for December

That’s enough looking back for this month, it’s time to get excited about the future! There’s no shortage of development goals for the month of December.

For the month of December, we’re strengthening the layer-1 core with intensive quality control, and the layer-2 core with further improvements to the user experience (a second round of pDEX updates, a web extension, an HD wallet feature, and more). We’re seeking out partnerships for the Privacy Quest, and delving into R&amp;D that will enable us to innovate and iterate frequently. We’ll also be moving into the first implementation stage for many of the projects we developed and tested in November. 



### Win the privacy niche

[Marketing and game management to add 6,000 Quest players](https://we.incognito.org/t/grow-number-of-shielded-tx/7155/6)

[Build a sticky and engaged community](https://we.incognito.org/t/build-a-helpful-community/8107)

[Continue the Privacy Quest](https://we.incognito.org/t/join-the-privacy-quest-earn-prizes/7204)

### Build a strong layer-2 core

[Node Tree - Finish mold and find a factory](https://we.incognito.org/t/node-tree-multi-pnode-set/523/157)

[App v4, including:](https://we.incognito.org/t/app-v-4/7108)

- Payment gateway

- HD wallet

- pDEX improvements pt 2

- Web extension 
- Documentation

### Build a strong layer-1 core

[Block creation time reduction](https://we.incognito.org/t/optimization-of-block-creation/7088/3)

[Deploy Portal v3 in testnet](https://we.incognito.org/t/incognitos-trustless-bridge-v3-with-ethereum-bond-contract/2963/16)

[Quality control and development processes for:](https://we.incognito.org/t/quality-assurance-processes-for-incognito-blockchain-dec-2020/8065)

- Slashing

- Multikey

- One-time addresses

-  New RNG for staking

- Security upgrade

- Portal v3

### R&amp;D

[Incognito Development Framework v2](https://we.incognito.org/t/incognito-dev-framework/6820/6)

[Test and implement fullnode service](https://we.incognito.org/t/incognito-app-service/7106/3)

[Privacy v2 merge and retest](https://we.incognito.org/t/confidential-asset/5607/6)

[Dynamic Committee Size](https://incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961/28)

## Get involved

Progress is best when everyone is involved. You can join the privacy effort, and even get rewarded for what you contribute. Check out the [Builder Rewards program](https://we.incognito.org/t/builder-rewards-what-every-builder-must-know-eligibility-criteria/5198) if you want to build privacy for the world!

# December PRV Holders call

You’ve read this far, and heard what we have to say, so now we’d like to give you a chance to share your thoughts. Join us for a lively chat with the core team and the rest of the Incognito community in the next PRV Holders call.

&gt; **Time**: 2PM UTC | 9PM GMT+7 | 9AM EST | 6AM PST
**Date**: Friday, 4 December 2020
**Where**: https://us02web.zoom.us/j/84119849586 
**Livestream**: [on Incognito YouTube channel](https://youtu.be/swr2sgr5UBw).

Please be sure to [RSVP here](https://we.incognito.org/t/join-us-for-the-december-prv-holders-call/8091).

Until next month, see you in the call and around we.incognito.org.

*The Incognito Core Team*</description>
    
    <lastBuildDate>Thu, 03 Dec 2020 18:56:02 +0000</lastBuildDate>
    <category>Updates</category>
    <atom:link href="https://we.incognito.org/t/letter-to-prv-holders-november-2020/8124.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Letter to PRV Holders (November 2020)</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <p><strong>To PRV Holders:</strong></p>
<p>2021 isn’t here yet, but for us at Incognito, we’re one month into a new year (our first birthday was last month). And with that new year came a new drive, a fresh disposition toward this mission we’re on, to bring privacy to crypto.</p>
<p><strong>Last year, we built a fully-functioning privacy ecosystem</strong> that includes the first anonymous, trustless DEX, the first multi-currency privacy wallet, the first trustless bridge to Ethereum, and quite a few more “firsts”.</p>
<p><strong>This year, we set out to make it self-sustaining.</strong> We’re building all of the extra details and safeguards that prepare the network for release. It’s an intensive checklist that, when completed, will turn Incognito into a fully autonomous privacy network, <strong>run by you.</strong></p>
<p>We’ve been at it for a month now, so it’s time to pause, reflect on what’s been done, and plan for what’s ahead.</p>
<p><a href="https://we.incognito.org/t/prv-holders-presentation-november-2020/8135">View PRV Holders presentation (November 2020) &gt;</a><br>
<a href="https://we.incognito.org/t/incognito-core-dev-2020-live-roadmap/552">Review the 2020 Roadmap &gt;</a><br>
<a href="https://we.incognito.org/t/incognitos-2021-privacy-roadmap-for-the-world/7235">Explore the 2021 Roadmap &gt;</a><br>
<a href="https://we.incognito.org/t/where-to-get-prv/253">Get PRV &gt;</a></p>
<h1>Projects and progress</h1>
<p>The <a href="https://we.incognito.org/t/incognitos-2021-privacy-roadmap-for-the-world/7235">2021 privacy roadmap</a> lays out three essential ingredients for making Incognito a fully-decentralized and self-sustaining shield that protects you from the growing threat of surveillance: Privacy, resilience, and ownership.</p>
<p>Each category has its own set of goals to reach, and many take multiple months to complete. We wasted no time getting busy in November. In the sections below, you’ll find a list of projects we started or continued this month, as well as a brief description of the main purpose of the project.</p>
<h2>Privacy</h2>
<p>We already built privacy, and every transaction you make on Incognito is anonymous. Now it’s time for Privacy v2, focused on ease of use, and extending privacy to every blockchain application. To that end, this month we’ve been continuing to build Portal, and developed and tested new privacy features like confidential assets and one-time addresses, which hide more transaction details and prevent transactions from being linked to one another.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th>Project</th>
<th>Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://we.incognito.org/t/incognitos-trustless-bridge-v3-with-ethereum-bond-contract/2963/14">Portal v3</a></td>
<td>Trustless bridge for coins w/o smart contract language</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/confidential-asset/5607">Confidential assets</a></td>
<td>Hide sender, receiver, and amount</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/confidential-asset/5607">One-time addresses</a></td>
<td>Make tx unlinkable</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/confidential-asset/5607">View key</a></td>
<td>Encrypted balances, user-controlled distributed access</td>
</tr>
</tbody>
</table>
</div><h2>Resilience</h2>
<p>For the network to be released, it has to be tough enough to stand on its own two feet. To ensure privacy can withstand anything, we’re testing every part of Incognito, and building some long-awaited features. Chief among them are things like dynamic committee size and dynamic sharding, which boost efficiency and help the network scale. When finished, we’ll be able to release the fixed validator slots as well!</p>
<p>In November, a big part of increasing resilience was testing. We’re taking the time to thoroughly inspect what we develop at each stage.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th>Project</th>
<th>Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://we.incognito.org/t/quality-assurance-processes-for-incognito-blockchain-nov-2020/7666">QA processes</a></td>
<td>Testing, quality assurance, bug management</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/incognito-dev-framework/6820">Dev framework</a></td>
<td>Tools for simulating blockchain for testing</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/optimization-of-block-creation/7088">Block creation optimization</a></td>
<td>Improve throughput</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961">Dynamic committee size and dynamic sharding</a></td>
<td>Network adaptability and chain robustness, release fixed validator slots</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961">Implement slashing</a></td>
<td>Remove misbehaving nodes</td>
</tr>
<tr>
<td>HD Wallet (Dec)<a href="https://we.incognito.org/t/app-v-4/7108">(App v4)</a>
</td>
<td>Mnemonic key management</td>
</tr>
</tbody>
</table>
</div><h2>Ownership</h2>
<p>We’ve said from the start that Incognito isn’t a company, and it’s not just the core team. It’s all of us. While we’re stewarding the network for now, every step we take brings us one step closer to handing it over to you. This month, we made progress toward growing your control in several areas:</p>
<p>Privacy products like the wallet and pDEX (app) give you control over your personal privacy, while the Nodes like the coming Node Tree give you a piece of shared control of the privacy network. Documentation and an API layer make it easy for builders to take charge of development alongside the core team.</p>
<div class="md-table">
<table>
<thead>
<tr>
<th>Project</th>
<th>Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://we.incognito.org/t/node-tree-multi-pnode-set/523">Node Tree</a></td>
<td>Multi-pNode stackable device</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/incognito-app-service/7106">Reliable API layer for builders</a></td>
<td>Data sync/organization of app services</td>
</tr>
<tr>
<td>Documentation <a href="https://we.incognito.org/t/app-v-4/7108">(App v4)</a>
</td>
<td>Complete Incognito resources for builders</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/build-a-helpful-community/8107">Helpful community</a></td>
<td>Further build community and culture for network management</td>
</tr>
<tr>
<td>pDEX improvements 1 <a href="https://we.incognito.org/t/app-v-4/7108">(App v4)</a>
</td>
<td>Price impact display, UX improvements</td>
</tr>
<tr>
<td><a href="https://we.incognito.org/t/grow-number-of-shielded-tx/7155">Grow shielded tx</a></td>
<td>Grow network use with Privacy Quest, media, project collaborations</td>
</tr>
</tbody>
</table>
</div><h1>By the numbers</h1>
<p>In November, as in every month before, growth has been accelerating project-wide. In fact, we hit <a href="https://we.incognito.org/t/incognito-meets-major-shielding-trading-and-liquidity-milestones-all-at-once/7823">3 major milestones</a> this month, one in each of the categories below. Here’s a bird’s-eye-view of the month’s progress:</p>
<h2>Monthly shield volume: +$7M</h2>
<p>Shield volume was a major focus for us in November. When a large amount of people anonymize a large amount of crypto, it proves that Incognito’s mission is important, and that we’re addressing a real need in the world.</p>
<p>This month, $7 million worth of crypto was deposited in the Incognito network. To put this in perspective, shield volume grew to $2 million from the launch of the pDEX in Nov 2019 until mid April 2020. From then to the end of Nov 2020, it has now grown to reach $33 million.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/c/c35c773998ac28b2ba0fb71343660ddfc639d064.jpeg" alt="image" data-base62-sha1="rSfdfd6jzBdb1xyYU4mZI5jdiio" width="706" height="529"></p>
<h2>Monthly trading volume: +$7.4M</h2>
<p>The rate of growth in trading volume is also increasing. On September 1st, 2020, total trading volume was at $9 million. Today, total trading volume exceeds $27 million.</p>
<p>We also saw some unique applications of trading pop up in the community, as liquidity pools for QUEST and LOTTO tokens were set up and people bought and sold what they earned in the Privacy Quest.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/d/d44c8b52c3686bdb8e93de9549a727496fdd2e56.jpeg" alt="image" data-base62-sha1="ui5dzOFdwlc5Z27Pt000egNDg7c" width="706" height="529"></p>
<h2>pDEX liquidity: +$5.5M</h2>
<p>Liquidity is vital to the utility of a crypto exchange. The more liquidity in the pDEX, the more crypto can be traded anonymously. In November, pDEX-only liquidity grew 46%, from $12 million on Nov 1st to $17.5 million on Dec 1st.</p>
<p>But that’s not all. We reintegrated Uniswap into the pDEX, bringing total combined liquidity to an astounding $1,175,000,000.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/b/bb6b2a6ea0dfa515b53e44964e34cc72c14cb9b6.jpeg" alt="image" data-base62-sha1="qJYT5wUYwer9zyKgHSSYqQP6DJQ" width="706" height="529"></p>
<h1>Builder development</h1>
<p>One of the most promising signs that Incognito is flourishing can be found in the <a>Builders category</a>. Multiple new projects were announced, existing products were improved, and both competition and output increased. To push the program to a new level of quality, we raised the bar and tightened eligibility criteria for the rewards program, and builders took the challenge in stride.</p>
<h3>New projects</h3>
<ul>
<li>
<p><a class="mention" href="/u/q9656296">@q9656296</a> aims to make Incognito the default crypto privacy layer for Telegram with the <a href="https://we.incognito.org/t/q9656296-integrate-incognito-into-telegram-poc/7903">Incognito x Telegram integration</a>.</p>
</li>
<li>
<p><a class="mention" href="/u/andireuter">@Andireuter</a> is tackling a similar use case, working on an <a href="https://we.incognito.org/t/integrate-incognito-into-signal-poc/7412">Incognito x Signal integration</a>.</p>
</li>
<li>
<p><a class="mention" href="/u/sato">@Sato</a>’s <a href="https://we.incognito.org/t/convy-your-personal-incognito-helper-bot/7089">Convy the Conversion Helper</a> converts currency amounts in your Incognito app to take the math out of understanding pDEX rates.</p>
</li>
</ul>
<h3>Established projects</h3>
<ul>
<li>
<p><a class="mention" href="/u/j053">@J053</a>’s <a href="https://we.incognito.org/t/ensi-telegram-bot-j053-fabi0/6831">ENSI Telegram Bot</a> helps validators track and manage their Node earnings more conveniently.</p>
</li>
<li>
<p><a class="mention" href="/u/abduraman">@Abduraman</a>’s <a href="https://we.incognito.org/t/nitowhalebot-a-whale-alert-bot-for-incognito-ecosystem/6757">IncogWhaleBot</a> alerts you to massive trades and potential whale activity in the Incognito ecosystem.</p>
</li>
<li>
<p><a class="mention" href="/u/abduraman">@Abduraman</a> also built a <a href="https://we.incognito.org/t/abduraman-python-api-for-incognito-sdk/6972">Python API for the Incognito SDK</a> to help builders do what they do best - build on Incognito.</p>
</li>
<li>
<p><a class="mention" href="/u/thriftinkid">@Thriftinkid</a>’s <a href="https://we.incognito.org/t/thriftinkid-how-to-track-your-incognito-rewards-through-google-sheets-v4/5857">Earnings Tracker</a> enables validators to track the status and earnings of their Nodes, project future earnings, and track investments across the pDEX.</p>
</li>
<li>
<p><a class="mention" href="/u/raz">@raz</a> and the ZGEN team created <a href="https://we.incognito.org/t/raz-we-made-a-web-wallet-for-you-and-your-friends/6235">prv.finance - an Incognito web wallet</a> to make trading from a browser possible.</p>
</li>
<li>
<p><a class="mention" href="/u/taind">@Taind</a>’s <a href="https://we.incognito.org/t/taind-light-shadow-box-anonymous-crypto-web-wallet-that-works/3233">Light Shadow Box</a> is a web wallet extension that allows you to hold and swap crypto anonymously on Incognito from a web browser.</p>
</li>
<li>
<p><a class="mention" href="/u/incsmile">@Incsmile</a>’s <a href="https://we.incognito.org/t/incsmile-pstore-app-a-platform-for-building-privacy-applications/4592">pStore.app</a> allows developers to build their own privacy applications on Incognito’s pEthereum functionality. It’s the start of what will be a vast ecosystem of privacy apps.</p>
</li>
<li>
<p><a class="mention" href="/u/josh_hamon">@Josh_Hamon</a>’s <a href="https://we.incognito.org/t/josh-hamon-nito-a-telegram-bot-for-push-notifications/3129">Nito the Telegram bot</a> was Incognito’s first helpful friend. Nito can send info specific to your Node right to your Telegram.</p>
</li>
<li>
<p><a class="mention" href="/u/inccry">@inccry</a>’s <a href="https://we.incognito.org/t/network-explorer/2086">Network Explorer</a> aggregates all sorts of data helpful to traders and privacy enthusiasts alike. The site is frequently upgraded with new features.</p>
</li>
</ul>
<p>You can build for privacy, too! With Incognito, you’re free to build any privacy product you want, however you want. If you’re interested in the rewards program, take a look at the <a href="https://we.incognito.org/t/builder-rewards-what-every-builder-must-know-eligibility-criteria/5198/">eligibility criteria</a>.</p>
<h1>Goals for December</h1>
<p>That’s enough looking back for this month, it’s time to get excited about the future! There’s no shortage of development goals for the month of December.</p>
<p>For the month of December, we’re strengthening the layer-1 core with intensive quality control, and the layer-2 core with further improvements to the user experience (a second round of pDEX updates, a web extension, an HD wallet feature, and more). We’re seeking out partnerships for the Privacy Quest, and delving into R&amp;D that will enable us to innovate and iterate frequently. We’ll also be moving into the first implementation stage for many of the projects we developed and tested in November.</p>
<h3>Win the privacy niche</h3>
<p><a href="https://we.incognito.org/t/grow-number-of-shielded-tx/7155/6">Marketing and game management to add 6,000 Quest players</a></p>
<p><a href="https://we.incognito.org/t/build-a-helpful-community/8107">Build a sticky and engaged community</a></p>
<p><a href="https://we.incognito.org/t/join-the-privacy-quest-earn-prizes/7204">Continue the Privacy Quest</a></p>
<h3>Build a strong layer-2 core</h3>
<p><a href="https://we.incognito.org/t/node-tree-multi-pnode-set/523/157">Node Tree - Finish mold and find a factory</a></p>
<p><a href="https://we.incognito.org/t/app-v-4/7108">App v4, including:</a></p>
<ul>
<li>
<p>Payment gateway</p>
</li>
<li>
<p>HD wallet</p>
</li>
<li>
<p>pDEX improvements pt 2</p>
</li>
<li>
<p>Web extension</p>
</li>
<li>
<p>Documentation</p>
</li>
</ul>
<h3>Build a strong layer-1 core</h3>
<p><a href="https://we.incognito.org/t/optimization-of-block-creation/7088/3">Block creation time reduction</a></p>
<p><a href="https://we.incognito.org/t/incognitos-trustless-bridge-v3-with-ethereum-bond-contract/2963/16">Deploy Portal v3 in testnet</a></p>
<p><a href="https://we.incognito.org/t/quality-assurance-processes-for-incognito-blockchain-dec-2020/8065">Quality control and development processes for:</a></p>
<ul>
<li>
<p>Slashing</p>
</li>
<li>
<p>Multikey</p>
</li>
<li>
<p>One-time addresses</p>
</li>
<li>
<p>New RNG for staking</p>
</li>
<li>
<p>Security upgrade</p>
</li>
<li>
<p>Portal v3</p>
</li>
</ul>
<h3>R&amp;D</h3>
<p><a href="https://we.incognito.org/t/incognito-dev-framework/6820/6">Incognito Development Framework v2</a></p>
<p><a href="https://we.incognito.org/t/incognito-app-service/7106/3">Test and implement fullnode service</a></p>
<p><a href="https://we.incognito.org/t/confidential-asset/5607/6">Privacy v2 merge and retest</a></p>
<p><a href="https://incognito.org/t/dynamic-committee-size-and-dynamic-sharding-implementation-phase/2961/28">Dynamic Committee Size</a></p>
<h2>Get involved</h2>
<p>Progress is best when everyone is involved. You can join the privacy effort, and even get rewarded for what you contribute. Check out the <a href="https://we.incognito.org/t/builder-rewards-what-every-builder-must-know-eligibility-criteria/5198">Builder Rewards program</a> if you want to build privacy for the world!</p>
<h1>December PRV Holders call</h1>
<p>You’ve read this far, and heard what we have to say, so now we’d like to give you a chance to share your thoughts. Join us for a lively chat with the core team and the rest of the Incognito community in the next PRV Holders call.</p>
<blockquote>
<p><strong>Time</strong>: 2PM UTC | 9PM GMT+7 | 9AM EST | 6AM PST<br>
<strong>Date</strong>: Friday, 4 December 2020<br>
<strong>Where</strong>: <a href="https://us02web.zoom.us/j/84119849586">https://us02web.zoom.us/j/84119849586</a><br>
<strong>Livestream</strong>: <a href="https://youtu.be/swr2sgr5UBw">on Incognito YouTube channel</a>.</p>
</blockquote>
<p>Please be sure to <a href="https://we.incognito.org/t/join-us-for-the-december-prv-holders-call/8091">RSVP here</a>.</p>
<p>Until next month, see you in the call and around <a href="http://we.incognito.org">we.incognito.org</a>.</p>
<p><em>The Incognito Core Team</em></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/letter-to-prv-holders-november-2020/8124/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/letter-to-prv-holders-november-2020/8124/1</link>
        <pubDate>Wed, 02 Dec 2020 01:19:40 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-8124-1</guid>
        <source url="https://we.incognito.org/t/letter-to-prv-holders-november-2020/8124.rss">Letter to PRV Holders (November 2020)</source>
      </item>
  </channel>
</rss>
