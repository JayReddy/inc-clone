<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>How to use WIC - Desktop wallet for Incognito</title>
    <link>https://we.incognito.org/t/how-to-use-wic-desktop-wallet-for-incognito/5228</link>
    <description># Welcome to Incognito Desktop Wallet - WIC

Hello, Incognito’s community! 

We are building WIC, a desktop wallet for Incognito. You can find our original post [here](https://we.incognito.org/t/wic-desktop-wallet/4893). 

Yesterday, we released the first version of WIC. We are very appreciative if you can try and give us your feedback as well as comment on the features you want to have in the next version. 

### How to install WIC
Here is the guide to run IncWallet on your PC. 

**Step 1:** Install docker 

**Step 2:** Create file `run.sh`:
```
#!/bin/sh bash

ps aux | grep &#39;[r]un.sh&#39; | awk &#39;{ print $2}&#39; | grep -v &quot;^$$\$&quot; | xargs kill -9
docker rm -f incwallet
docker network create --driver bridge incwallet_net || true
docker pull isyyyy/incwallet
docker run -ti --restart=always --net incwallet_net --name incwallet -d -p 9000:9000 isyyyy/incwallet
docker run --net incwallet_net --name mongo -d -p 27018:27017 mongo
shopt -s expand_aliases
alias wic=&#39;docker exec -it incwallet wic&#39;
```
**Step 3:** Try with `wic` command in your terminal. If you cannot see the welcome message, try with this command
```alias wic=&#39;docker exec -it incwallet wic&#39;```.

**Step 4:** `Welcome to WIC`


### Demo wallet on testnet
Before becoming a fan of ```wic```, you can try with the prepared wallet with Incognito Testnet.  
* Account 1:
```
    Private key: 112t8rnbCjhDpBQjjNJQ2bABVAbkZC2GaFvbTw7kCaN7RyqLC9Pwh7v7bNoLh5PqDcj2SYDk1HNqoKDwcFdeDRhdcB2mjGuRdnykkqka5HV1
    Payment add: 12S4CvTpc5wHbNXyvdzPktYMGCZcj3pSTCUS73DCShRz3kTmsjorQHdhjmhTMMqLfthMMFTpwVDBbY7kyHYeJcB3wMiK56mum1TaBkU
```
* Account 2:
```
    Private key: 112t8rnZvxYbAAvUmDT9PM6pivcFnexU5P8Fr83AzaAGmpLqRUJMWn87qpQDRfkguxCpiQRxWrneikQUP4j1oYXYV8h2zagME9PMuWamJKD3
    Payment add: 12RwWpgC1K9czfuwJjUhF8k4CxMaQVYmHiTfBLc6E8HnmERyn1fKMtyfrSNrk7A8hHgGU69T7UXy8JkE1bYXwpADGtgife9hSkTPDJw
```
Both accounts also are recovered using ```wallet import``` command with the recover seed words
``` 
    seed words: &quot;day clarify timber glue stable color thunder blossom author crane moon prepare&quot;
``` 
##### Note: If you want to remove your wallet, please remove docker container: ```mongo```

```
    docker rm -f mongo
```
However, remember backup the ```(seed words, password)``` or ```private key```.

### How to use WIC
Start with ```wic``` you will see 

```
    __      __       .__                                  __            __      __.__        
     /  \    /  \ ____ |  |   ____  ____   _____   ____   _/  |_  ____   /  \    /  \__| ____  
     \   \/\/   // __ \|  | _/ ___\/  _ \ /     \_/ __ \  \   __\/  _ \  \   \/\/   /  |/ ___\ 
      \        /\  ___/|  |_\  \__(  &lt;_&gt; )  Y Y  \  ___/   |  | (  &lt;_&gt; )  \        /|  \  \___ 
       \__/\  /  \___  &gt;____/\___  &gt;____/|__|_|  /\___  &gt;  |__|  \____/    \__/\  / |__|\___  &gt;
            \/       \/          \/            \/     \/                        \/          \/
     
     Usage:
       wic [command]
     
     Available Commands:
         account     account commands for add, import, switch, and ls
         balance     show current balance
         help        Help about any command
         tx          tx commands for history, send, and receive
         version     current version
         wallet      wallet commands for import and create

     
     Flags:
       -h, --help   help for wic
     
     Use &quot;wic [command] --help&quot; for more information about a command.
```
### List of commands

### ```wallet``` command

```
Usage:
     wic wallet [command]
   
   Available Commands:
     create      create a new wallet
     import      import wallet by seed phrase
   
   Flags:
     -h, --help   help for wallet
```
##### Example:

Create a new wallet: ``` wic wallet create -s 256 -n mainnet```
- Create a new wallet with 256 bits security level (24 seed words) on ```mainnet```. Note that you can create a wallet on ```testnet```  with ```-n testnet```.
- Remember to set up a password to protect your wallet in the system. 

Import a wallet from seed words: ```wic import -n mainnet -m &quot;24 seed words&quot;``` 

Note: 
* Remember to back up your **seed words** and **password** to recover the wallet in case you delete or remove ```wic```
* Multiple wallets are not supported in this version. That means you only can create a mainnet or testnet wallet.

### ```account``` command
After create or import a wallet. You can add or import accounts into the wallet. 
```
Usage:
  wic account [command]

Available Commands:
  add         add new account
  export      export private key &amp; info
  import      import account by private key
  info        show wallet info
  ls          show all imported/added accounts
  switch      switch between accounts by name

Flags:
  -h, --help   help for account
```
##### Example:
- Add account belonging wallet: ```wic account add -n account_name```

- Import account from another wallet (e.g., web, mobile wallets): ```wic account import -n account_name -k private_key```

- Switch account by name: ```wic account switch -n account_name```

- List add accounts: ```wic account ls```

- View account info: ```wic account info```

- Export private key: ```wic account export```

Note: 
- For all accounts generated by ```add``` command will be fully recovered if you know the seed words and password. That means you do not need to back up the private key from ```export``` command. 
- Imported accounts do not belong to the wallet, remember to back up private keys for them.

### ```tx``` command
```
Usage:
     wic tx [command]
   
   Available Commands:
     history     Show send/receive transactions history
     receive     generate QR code to receive PRV or pToken
     send        create PRV/pToken transaction
   
   Flags:
     -h, --help   help for tx
```
#### Example
- Show transaction history: ```wic tx history```
- Create and send PRV: ```wic tx send -r receiver_address -a amount ```
- Create and send pToken: ```wic tx send -r receiver_address -a amount -t token_id ```

Note: 
- Using flag ```-t token_id``` to filter by token ID in ```history``` command. 
- The amount is in nano. For example ```-a 1000000``` that means send ```0.001``` PRV.
- You can change the transaction fee (default ```5```) using flag ```-f```.

### ```balance``` command
```
Usage:
  wic balance [flags]
  wic balance [command]

Available Commands:
  utxo        show current unspent coins

Flags:
  -h, --help           help for balance
  -t, --token string   Add your token

```

#### Example
- Show balance: ```wic balance```
- Show balance with utxo: ```wic balance utxo```

Note: 
- Using flag ```-t token_id``` to filter by token ID in ```balance``` command. 
- The balance is in nano. For example, ```-a 1000000``` that means you have ```0.001``` PRV.</description>
    
    <lastBuildDate>Mon, 02 Nov 2020 04:18:47 +0000</lastBuildDate>
    <category>Builders</category>
    <atom:link href="https://we.incognito.org/t/how-to-use-wic-desktop-wallet-for-incognito/5228.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>How to use WIC - Desktop wallet for Incognito</title>
        <dc:creator><![CDATA[@Isyyyy Tuấn Phạm]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/isyyyy">@Isyyyy</a> wrote:</p>
          <blockquote>
              <h1>Welcome to Incognito Desktop Wallet - WIC</h1>
<p>Hello, Incognito’s community!</p>
<p>We are building WIC, a desktop wallet for Incognito. You can find our original post <a href="https://we.incognito.org/t/wic-desktop-wallet/4893">here</a>.</p>
<p>Yesterday, we released the first version of WIC. We are very appreciative if you can try and give us your feedback as well as comment on the features you want to have in the next version.</p>
<h3>How to install WIC</h3>
<p>Here is the guide to run IncWallet on your PC.</p>
<p><strong>Step 1:</strong> Install docker</p>
<p><strong>Step 2:</strong> Create file <code>run.sh</code>:</p>
<pre><code class="lang-auto">#!/bin/sh bash

ps aux | grep '[r]un.sh' | awk '{ print $2}' | grep -v "^$$\$" | xargs kill -9
docker rm -f incwallet
docker network create --driver bridge incwallet_net || true
docker pull isyyyy/incwallet
docker run -ti --restart=always --net incwallet_net --name incwallet -d -p 9000:9000 isyyyy/incwallet
docker run --net incwallet_net --name mongo -d -p 27018:27017 mongo
shopt -s expand_aliases
alias wic='docker exec -it incwallet wic'
</code></pre>
<p><strong>Step 3:</strong> Try with <code>wic</code> command in your terminal. If you cannot see the welcome message, try with this command<br>
<code>alias wic='docker exec -it incwallet wic'</code>.</p>
<p><strong>Step 4:</strong> <code>Welcome to WIC</code></p>
<h3>Demo wallet on testnet</h3>
<p>Before becoming a fan of <code>wic</code>, you can try with the prepared wallet with Incognito Testnet.</p>
<ul>
<li>Account 1:</li>
</ul>
<pre><code class="lang-auto">    Private key: 112t8rnbCjhDpBQjjNJQ2bABVAbkZC2GaFvbTw7kCaN7RyqLC9Pwh7v7bNoLh5PqDcj2SYDk1HNqoKDwcFdeDRhdcB2mjGuRdnykkqka5HV1
    Payment add: 12S4CvTpc5wHbNXyvdzPktYMGCZcj3pSTCUS73DCShRz3kTmsjorQHdhjmhTMMqLfthMMFTpwVDBbY7kyHYeJcB3wMiK56mum1TaBkU
</code></pre>
<ul>
<li>Account 2:</li>
</ul>
<pre><code class="lang-auto">    Private key: 112t8rnZvxYbAAvUmDT9PM6pivcFnexU5P8Fr83AzaAGmpLqRUJMWn87qpQDRfkguxCpiQRxWrneikQUP4j1oYXYV8h2zagME9PMuWamJKD3
    Payment add: 12RwWpgC1K9czfuwJjUhF8k4CxMaQVYmHiTfBLc6E8HnmERyn1fKMtyfrSNrk7A8hHgGU69T7UXy8JkE1bYXwpADGtgife9hSkTPDJw
</code></pre>
<p>Both accounts also are recovered using <code>wallet import</code> command with the recover seed words</p>
<pre><code class="lang-auto">    seed words: "day clarify timber glue stable color thunder blossom author crane moon prepare"
</code></pre>
<h5>Note: If you want to remove your wallet, please remove docker container: <code>mongo</code>
</h5>
<pre><code class="lang-auto">    docker rm -f mongo
</code></pre>
<p>However, remember backup the <code>(seed words, password)</code> or <code>private key</code>.</p>
<h3>How to use WIC</h3>
<p>Start with <code>wic</code> you will see</p>
<pre><code class="lang-auto">    __      __       .__                                  __            __      __.__        
     /  \    /  \ ____ |  |   ____  ____   _____   ____   _/  |_  ____   /  \    /  \__| ____  
     \   \/\/   // __ \|  | _/ ___\/  _ \ /     \_/ __ \  \   __\/  _ \  \   \/\/   /  |/ ___\ 
      \        /\  ___/|  |_\  \__(  &lt;_&gt; )  Y Y  \  ___/   |  | (  &lt;_&gt; )  \        /|  \  \___ 
       \__/\  /  \___  &gt;____/\___  &gt;____/|__|_|  /\___  &gt;  |__|  \____/    \__/\  / |__|\___  &gt;
            \/       \/          \/            \/     \/                        \/          \/
     
     Usage:
       wic [command]
     
     Available Commands:
         account     account commands for add, import, switch, and ls
         balance     show current balance
         help        Help about any command
         tx          tx commands for history, send, and receive
         version     current version
         wallet      wallet commands for import and create

     
     Flags:
       -h, --help   help for wic
     
     Use "wic [command] --help" for more information about a command.
</code></pre>
<h3>List of commands</h3>
<h3>
<code>wallet</code> command</h3>
<pre><code class="lang-auto">Usage:
     wic wallet [command]
   
   Available Commands:
     create      create a new wallet
     import      import wallet by seed phrase
   
   Flags:
     -h, --help   help for wallet
</code></pre>
<h5>Example:</h5>
<p>Create a new wallet: <code> wic wallet create -s 256 -n mainnet</code></p>
<ul>
<li>Create a new wallet with 256 bits security level (24 seed words) on <code>mainnet</code>. Note that you can create a wallet on <code>testnet</code>  with <code>-n testnet</code>.</li>
<li>Remember to set up a password to protect your wallet in the system.</li>
</ul>
<p>Import a wallet from seed words: <code>wic import -n mainnet -m "24 seed words"</code></p>
<p>Note:</p>
<ul>
<li>Remember to back up your <strong>seed words</strong> and <strong>password</strong> to recover the wallet in case you delete or remove <code>wic</code>
</li>
<li>Multiple wallets are not supported in this version. That means you only can create a mainnet or testnet wallet.</li>
</ul>
<h3>
<code>account</code> command</h3>
<p>After create or import a wallet. You can add or import accounts into the wallet.</p>
<pre><code class="lang-auto">Usage:
  wic account [command]

Available Commands:
  add         add new account
  export      export private key &amp; info
  import      import account by private key
  info        show wallet info
  ls          show all imported/added accounts
  switch      switch between accounts by name

Flags:
  -h, --help   help for account
</code></pre>
<h5>Example:</h5>
<ul>
<li>
<p>Add account belonging wallet: <code>wic account add -n account_name</code></p>
</li>
<li>
<p>Import account from another wallet (e.g., web, mobile wallets): <code>wic account import -n account_name -k private_key</code></p>
</li>
<li>
<p>Switch account by name: <code>wic account switch -n account_name</code></p>
</li>
<li>
<p>List add accounts: <code>wic account ls</code></p>
</li>
<li>
<p>View account info: <code>wic account info</code></p>
</li>
<li>
<p>Export private key: <code>wic account export</code></p>
</li>
</ul>
<p>Note:</p>
<ul>
<li>For all accounts generated by <code>add</code> command will be fully recovered if you know the seed words and password. That means you do not need to back up the private key from <code>export</code> command.</li>
<li>Imported accounts do not belong to the wallet, remember to back up private keys for them.</li>
</ul>
<h3>
<code>tx</code> command</h3>
<pre><code class="lang-auto">Usage:
     wic tx [command]
   
   Available Commands:
     history     Show send/receive transactions history
     receive     generate QR code to receive PRV or pToken
     send        create PRV/pToken transaction
   
   Flags:
     -h, --help   help for tx
</code></pre>
<h4>Example</h4>
<ul>
<li>Show transaction history: <code>wic tx history</code>
</li>
<li>Create and send PRV: <code>wic tx send -r receiver_address -a amount </code>
</li>
<li>Create and send pToken: <code>wic tx send -r receiver_address -a amount -t token_id </code>
</li>
</ul>
<p>Note:</p>
<ul>
<li>Using flag <code>-t token_id</code> to filter by token ID in <code>history</code> command.</li>
<li>The amount is in nano. For example <code>-a 1000000</code> that means send <code>0.001</code> PRV.</li>
<li>You can change the transaction fee (default <code>5</code>) using flag <code>-f</code>.</li>
</ul>
<h3>
<code>balance</code> command</h3>
<pre><code class="lang-auto">Usage:
  wic balance [flags]
  wic balance [command]

Available Commands:
  utxo        show current unspent coins

Flags:
  -h, --help           help for balance
  -t, --token string   Add your token

</code></pre>
<h4>Example</h4>
<ul>
<li>Show balance: <code>wic balance</code>
</li>
<li>Show balance with utxo: <code>wic balance utxo</code>
</li>
</ul>
<p>Note:</p>
<ul>
<li>Using flag <code>-t token_id</code> to filter by token ID in <code>balance</code> command.</li>
<li>The balance is in nano. For example, <code>-a 1000000</code> that means you have <code>0.001</code> PRV.</li>
</ul>
          </blockquote>
          <p><a href="https://we.incognito.org/t/how-to-use-wic-desktop-wallet-for-incognito/5228/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/how-to-use-wic-desktop-wallet-for-incognito/5228/1</link>
        <pubDate>Wed, 19 Aug 2020 11:09:02 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-5228-1</guid>
        <source url="https://we.incognito.org/t/how-to-use-wic-desktop-wallet-for-incognito/5228.rss">How to use WIC - Desktop wallet for Incognito</source>
      </item>
  </channel>
</rss>
