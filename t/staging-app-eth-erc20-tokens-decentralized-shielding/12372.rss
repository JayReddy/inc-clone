<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>[Staging app] ETH/ERC20 tokens decentralized shielding</title>
    <link>https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372</link>
    <description>We’re changing the way of releasing products to letting users experience new features sooner so that the core team can receive feedback and bug reports prior to rolling an official version out. In the new process, users can give new features a try beforehand it is published in the production environment. The first feature that is piloted with this release process is ETH/ERC20 decentralized shielding.

As mentioned in [the topic](https://we.incognito.org/t/improvement-of-eth-erc20-shielding-design/12140), a user will be able to shield ETH/ERC20 tokens right from his personal wallet, e.g. Metamask, Trust Wallet, etc. – and will no longer need to send funds to a temporary address managed by the Incognito team.

Android users can now experience this feature on a staging app at https://github.com/incognitochain/incognito-wallet/releases/tag/v4.3.10

The app version is pointing to Incognito testnet and Ethereum Kovan testnet so you should not worry about any expense or loss with your real money. The following is how to use the decentralized shielding feature.

### Install an Ethereum wallet app

You must have at least 1 Ethereum wallet app that supports WalletConnect on your phone. See the full supported list here: https://registry.walletconnect.org/wallets

We will use Metamask(https://metamask.io/) in this example.

### Switching To ETH Testnet (optional)

**IMPORTANT**: Backup your key (if any) before testing

Switching to testnet is required since you’re testing this new feature out on the staging environment.

Switching to testnet in Incognito Wallet. Going to Settings:

![image|380x800](upload://idjYCTovlf4VhP4lQuueSfpfUFQ.jpeg)

Under Network sections, select to view available networks

![image|380x800](upload://cRrFdWofvQw8a2YdORH1hCfRB4e.jpeg)

Click several times on “Testnet” network until app starts reloading

![image|380x800](upload://c1wUyeD9nglfo7AANFTB3NTMMCH.png)

Done, you are now using Incognito testnet along with Ethereum Kovan testnet

### Switching to the Kovan network in the Metamask app.
![image|380x800](upload://7dP19Y0vWCOLiu0oQ29rfi1S6VG.jpeg)

**Tip**: you can get 2 ETH in Kovan network at: https://faucet.kovan.network/

or contact us @support to get some tokens for testing.

### Select crypto you want to shield

The Incognito app currently supports ETH and some ERC20 tokens for this feature. We will use ETH in this example.

Go to Assets -&gt; Shield my crypto and then select Ethereum

![image|380x800](upload://ks2UqtSx1cgZhku8BYGhrAXZnwg.jpeg)

### Connect Incognito app with 3rd wallet via WalletConnect

To allow Incognito Wallet to check your balance and send transaction requests, first we need to connect the Incognito app to your wallet (e.g. Metamask). After connecting, Incognito Wallet will now can:

* Getting information about your account: remaining balances, address.
* Creating a new request. Incognito Wallet CAN’T access your keys, assets, transaction histories,… It can only send a request to Metamask in order to create a transaction. All transactions will need your approval before processing.

Select “I will shield from my own wallet”

![image|380x800](upload://h2CgPW7hPNeFVa1rZab6AVe4Tko.png)

Available Wallets will show up for connection. Click on MetaMask.

![image|380x800](upload://wqu0aPMcmqcdvrK8BOBUDBDoL7U.jpeg)

You will now be navigated to the Metamask app, wait a minute till Metamask finishes loading the request. A confirmation box will show up, waiting for your approval to connect. Click Connect now.

![image|380x800](upload://cSAPHYPctT2WnkctGR5ZaMGYsDL.jpeg)

Connected, now open back Incognito app

![image|379x800](upload://fNkMT0CIF8dYac4K00M96UYg6g0.jpeg)

Waiting for a while, the Incognito app will show your account information here when it has successfully connected to your Metamask wallet. You can now start shielding.

![image|380x800](upload://5E14Ivt8zIOzIkpeU1bQ5IbsjW9.png)

### Shield Your Money

After connecting with other wallets, you can now start shielding.

Input your amount to be shielded, then click on **Shield** button

![image|380x800](upload://s7Xj8IdfN2QVxkFIhmFzPurvlmc.jpeg)

Select Metamask to continue

![image|380x800](upload://5Y94VAOCgqbigfAParlP9WkESiH.jpeg)

Wait a few seconds, Metamask will show a transaction confirmation and wait for your approval. If everything is OK, click Confirm, Metamask will submit this transaction.

![image|380x800](upload://eIRd3hJ6p8060sNjFOmsU3Wkiha.jpeg)

Transaction sent! Now go back to Incognito Wallet to check the result.

![image|380x800](upload://rqGBSmHe2z0QVGfVtGocJLtE8vI.jpeg)

![image|380x800](upload://40TBwJaxXEN4C7ekPHyqyPEXGQu.png)

You can click on **Sure thing** to view your shielding history. Remember that your recent shielding will only appear here when the background jobs start processing it. Keep calm and check later.

![image|380x800](upload://wSXyKXPOKcmxWoWNuLK1DVL5LBP.jpeg)



That’s it, please give it a try and let us know your thoughts, thanks!</description>
    
    <lastBuildDate>Tue, 01 Jun 2021 00:58:15 +0000</lastBuildDate>
    <category>App users</category>
    <atom:link href="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>[Staging app] ETH/ERC20 tokens decentralized shielding</title>
        <dc:creator><![CDATA[@marko]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/marko">@marko</a> wrote:</p>
          <blockquote>
              <p>Thanks for the update.  I hope that you’ll offer the ability to unshield by directly interacting with the contract also.  It allows users to control the fees paid but more importantly assuring us users that we can access our funds and we are not waiting for a custodial process to return our funds.  The current unshielding process is waiting for if/when incognito decides to process funds.  We trust you, but it would be far better if we didn’t have to trust you. <img src="https://we.incognito.org/images/emoji/apple/wink.png?v=9" title=":wink:" class="emoji" alt=":wink:"> <img src="https://we.incognito.org/images/emoji/apple/grinning.png?v=9" title=":grinning:" class="emoji" alt=":grinning:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/5</link>
        <pubDate>Tue, 01 Jun 2021 00:58:15 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-12372-5</guid>
        <source url="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372.rss">[Staging app] ETH/ERC20 tokens decentralized shielding</source>
      </item>
      <item>
        <title>[Staging app] ETH/ERC20 tokens decentralized shielding</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <blockquote>
<p>This is very cool. What inputs are required from the app? Just token name (ETH, USDC, etc), amount, and destination incognito address?</p>
</blockquote>
<p>Yes, token id (ETH, USDC, …), amount, and destination incognito address are all that the shielding process needs. Behind the scene, we also used web3 for Ethereum smart contract interaction, it’s completely appliable for other environments like desktop app or web. These may come later after we complete the higher priorities.</p>
<p>This is mostly for supporting a decentralized shielding method that allows users to interact directly with Incognito vault contract from their personal crypto wallets without a need for temporary addresses as intermediary. If you look into the current unshielding process, it would not require a temporary address holding user’s assets except unshielding fees that would be used for paying smart contract interaction fees. In my opinion, the unshielding process is acceptable now as it balances out user experience and decentralization.</p>
<blockquote>
<p>inconigto is not charging any fee to do this; it’s only the ethereum network fee and no deduction from the shielded amount?</p>
</blockquote>
<p>Absolutely, Incognito is not charging any fees for both shielding and unshielding. Currently, we’re even covering 40% of shielding fees to encourage people to deposit funds to Incognito. For the unshielding, I’m working with an external platform to offer another option for unshielding ETH/ERC20 tokens with zero fees. The detail will be announced soon.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/4</link>
        <pubDate>Tue, 01 Jun 2021 00:39:20 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-12372-4</guid>
        <source url="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372.rss">[Staging app] ETH/ERC20 tokens decentralized shielding</source>
      </item>
      <item>
        <title>[Staging app] ETH/ERC20 tokens decentralized shielding</title>
        <dc:creator><![CDATA[@marko]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/marko">@marko</a> wrote:</p>
          <blockquote>
              <p>This is very cool.  What inputs are required from the app?  Just token name (ETH, USDC, etc), amount, and destination incognito address?</p>
<p>I wish there was a builder that could put together a simple website that I could web3 connect on PC and I can simply type in the incognito address.</p>
<p>I’m also assuming that unshielding can be done this way in the near future?  And just to double check, inconigto is not charging any fee to do this; it’s only the ethereum network fee and no deduction from the shielded amount?  Keep up the good work!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/3</link>
        <pubDate>Mon, 31 May 2021 02:04:49 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-12372-3</guid>
        <source url="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372.rss">[Staging app] ETH/ERC20 tokens decentralized shielding</source>
      </item>
      <item>
        <title>[Staging app] ETH/ERC20 tokens decentralized shielding</title>
        <dc:creator><![CDATA[@khanhj]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/khanhj">@khanhj</a> wrote:</p>
          <blockquote>
              
          </blockquote>
          <p><a href="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/2">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/2</link>
        <pubDate>Fri, 28 May 2021 20:32:55 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-12372-2</guid>
        <source url="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372.rss">[Staging app] ETH/ERC20 tokens decentralized shielding</source>
      </item>
      <item>
        <title>[Staging app] ETH/ERC20 tokens decentralized shielding</title>
        <dc:creator><![CDATA[@duc duc]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/duc">@duc</a> wrote:</p>
          <blockquote>
              <p>We’re changing the way of releasing products to letting users experience new features sooner so that the core team can receive feedback and bug reports prior to rolling an official version out. In the new process, users can give new features a try beforehand it is published in the production environment. The first feature that is piloted with this release process is ETH/ERC20 decentralized shielding.</p>
<p>As mentioned in <a href="https://we.incognito.org/t/improvement-of-eth-erc20-shielding-design/12140">the topic</a>, a user will be able to shield ETH/ERC20 tokens right from his personal wallet, e.g. Metamask, Trust Wallet, etc. – and will no longer need to send funds to a temporary address managed by the Incognito team.</p>
<p>Android users can now experience this feature on a staging app at <a href="https://github.com/incognitochain/incognito-wallet/releases/tag/v4.3.10">https://github.com/incognitochain/incognito-wallet/releases/tag/v4.3.10</a></p>
<p>The app version is pointing to Incognito testnet and Ethereum Kovan testnet so you should not worry about any expense or loss with your real money. The following is how to use the decentralized shielding feature.</p>
<h3>Install an Ethereum wallet app</h3>
<p>You must have at least 1 Ethereum wallet app that supports WalletConnect on your phone. See the full supported list here: <a href="https://registry.walletconnect.org/wallets">https://registry.walletconnect.org/wallets</a></p>
<p>We will use Metamask(<a href="https://metamask.io/">https://metamask.io/</a>) in this example.</p>
<h3>Switching To ETH Testnet (optional)</h3>
<p><strong>IMPORTANT</strong>: Backup your key (if any) before testing</p>
<p>Switching to testnet is required since you’re testing this new feature out on the staging environment.</p>
<p>Switching to testnet in Incognito Wallet. Going to Settings:</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/7/7fa8a75fd4771e5180c3053f306ffa841ef6f3ba.jpeg" alt="image" data-base62-sha1="idjYCTovlf4VhP4lQuueSfpfUFQ" width="380" height="800"></p>
<p>Under Network sections, select to view available networks</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/5/5a24bac0154b9a8f09c67785102b521a3b078ae2.jpeg" alt="image" data-base62-sha1="cRrFdWofvQw8a2YdORH1hCfRB4e" width="380" height="800"></p>
<p>Click several times on “Testnet” network until app starts reloading</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/5/54466335fd3cf0105d7c77642ebf6b12220c392f.png" alt="image" data-base62-sha1="c1wUyeD9nglfo7AANFTB3NTMMCH" width="380" height="800"></p>
<p>Done, you are now using Incognito testnet along with Ethereum Kovan testnet</p>
<h3>Switching to the Kovan network in the Metamask app.</h3>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/3/329f3a7628dad1dc6599cc61c2e687ec42807170.jpeg" alt="image" data-base62-sha1="7dP19Y0vWCOLiu0oQ29rfi1S6VG" width="380" height="800"></p>
<p><strong>Tip</strong>: you can get 2 ETH in Kovan network at: <a href="https://faucet.kovan.network/">https://faucet.kovan.network/</a></p>
<p>or contact us <a class="mention-group notify" href="/groups/support">@support</a> to get some tokens for testing.</p>
<h3>Select crypto you want to shield</h3>
<p>The Incognito app currently supports ETH and some ERC20 tokens for this feature. We will use ETH in this example.</p>
<p>Go to Assets -&gt; Shield my crypto and then select Ethereum</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/8/8f571c1a79d7b4c4faa15341a8419d4031b731b4.jpeg" alt="image" data-base62-sha1="ks2UqtSx1cgZhku8BYGhrAXZnwg" width="380" height="800"></p>
<h3>Connect Incognito app with 3rd wallet via WalletConnect</h3>
<p>To allow Incognito Wallet to check your balance and send transaction requests, first we need to connect the Incognito app to your wallet (e.g. Metamask). After connecting, Incognito Wallet will now can:</p>
<ul>
<li>Getting information about your account: remaining balances, address.</li>
<li>Creating a new request. Incognito Wallet CAN’T access your keys, assets, transaction histories,… It can only send a request to Metamask in order to create a transaction. All transactions will need your approval before processing.</li>
</ul>
<p>Select “I will shield from my own wallet”</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/7/7770b27a5213b0459b8ed1e945a7c83c9715d16c.png" alt="image" data-base62-sha1="h2CgPW7hPNeFVa1rZab6AVe4Tko" width="380" height="800"></p>
<p>Available Wallets will show up for connection. Click on MetaMask.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/e/e343f75c6579da2f7277b313f6d1256a0feea776.jpeg" alt="image" data-base62-sha1="wqu0aPMcmqcdvrK8BOBUDBDoL7U" width="380" height="800"></p>
<p>You will now be navigated to the Metamask app, wait a minute till Metamask finishes loading the request. A confirmation box will show up, waiting for your approval to connect. Click Connect now.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/5/5a45f28ae5cefba10d9e465a65a32ccc2c079bd1.jpeg" alt="image" data-base62-sha1="cSAPHYPctT2WnkctGR5ZaMGYsDL" width="380" height="800"></p>
<p>Connected, now open back Incognito app</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/6/6eb449e822af43f4d3af9581045a960b525ed738.jpeg" alt="image" data-base62-sha1="fNkMT0CIF8dYac4K00M96UYg6g0" width="379" height="800"></p>
<p>Waiting for a while, the Incognito app will show your account information here when it has successfully connected to your Metamask wallet. You can now start shielding.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/2/2790e758eece51c4d1080aac043c62b2c55791f1.png" alt="image" data-base62-sha1="5E14Ivt8zIOzIkpeU1bQ5IbsjW9" width="380" height="800"></p>
<h3>Shield Your Money</h3>
<p>After connecting with other wallets, you can now start shielding.</p>
<p>Input your amount to be shielded, then click on <strong>Shield</strong> button</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/c/c5231f0d90f6bc24ade052239cd52b0a08f0e6ec.jpeg" alt="image" data-base62-sha1="s7Xj8IdfN2QVxkFIhmFzPurvlmc" width="380" height="800"></p>
<p>Select Metamask to continue</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/2/29d767b1f2823069b9e92ada3180c12633959fdf.jpeg" alt="image" data-base62-sha1="5Y94VAOCgqbigfAParlP9WkESiH" width="380" height="800"></p>
<p>Wait a few seconds, Metamask will show a transaction confirmation and wait for your approval. If everything is OK, click Confirm, Metamask will submit this transaction.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/6/67308e5ffb1db9969e2ba57534ef9246ab98cb10.jpeg" alt="image" data-base62-sha1="eIRd3hJ6p8060sNjFOmsU3Wkiha" width="380" height="800"></p>
<p>Transaction sent! Now go back to Incognito Wallet to check the result.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/c/c03efb2fb0cfecc1284754793739248d1d35ebbe.jpeg" alt="image" data-base62-sha1="rqGBSmHe2z0QVGfVtGocJLtE8vI" width="380" height="800"></p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/1/1c22a642b572fdcc89d77dde056ebc7ff84bfe26.png" alt="image" data-base62-sha1="40TBwJaxXEN4C7ekPHyqyPEXGQu" width="380" height="800"></p>
<p>You can click on <strong>Sure thing</strong> to view your shielding history. Remember that your recent shielding will only appear here when the background jobs start processing it. Keep calm and check later.</p>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/2X/e/e67c08cc264618de51b5b1ee97558bb1914dbacd.jpeg" alt="image" data-base62-sha1="wSXyKXPOKcmxWoWNuLK1DVL5LBP" width="380" height="800"></p>
<p>That’s it, please give it a try and let us know your thoughts, thanks!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372/1</link>
        <pubDate>Fri, 28 May 2021 13:00:15 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-12372-1</guid>
        <source url="https://we.incognito.org/t/staging-app-eth-erc20-tokens-decentralized-shielding/12372.rss">[Staging app] ETH/ERC20 tokens decentralized shielding</source>
      </item>
  </channel>
</rss>
