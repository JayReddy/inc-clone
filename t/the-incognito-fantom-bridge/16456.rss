<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>The Incognito &lt;&gt; Fantom bridge</title>
    <link>https://we.incognito.org/t/the-incognito-fantom-bridge/16456</link>
    <description>![_Fantom X Incognito|690x388, 100%](upload://lQQ6jHNQmdolhNmBRguw3tgPAHE.jpeg) 

:wave: Hi everyone,

We are so pleased to announce that **the Incognito &lt;&gt; Fantom bridge** is now live.
As you know, Incognito is the privacy blockchain. Apart from privacy, interoperability is also a key component of Incognito. To date, Incognito bridged to 12 major blockchains (Bitcoin, Ethereum, Binance Smart Chain, Polygon, etc.), and now Fantom.

## **What problems are the bridge solving?**

Like other Incognito bridges, Incognito - Fantom bridge allows users to make cross-chain transactions between Fantom and Incognito networks. The bridge offers the community **combined advantages** of both Incognito and Fantom:

### 1. **Transfer Fantom tokens privately**


![bridge|624x105,100%](upload://nzyIrbVWRuLoRkBN1edDiiOaLz6.png) 

Fantom is a public blockchain. It means anyone can see your balance, and track your transactions. Thanks to the bridge, users can shield Fantom tokens to Incognito and transfer them privately. The sender, the receiver, the amount, and also the asset type, all stay anonymously in the Incognito network. Therefore, no one can either track your activities or search financial histories.

### 2. **Trade for tokens anonymously**

Shielding tokens to Incognito is similar to stepping into Incognito ecosystem. One of the best Incognito’s applications is the privacy exchange. It’s an AMM decentralized exchange that users can trade anonymously (don’t reveal the identity of traders). Especially, users can swap Fantom tokens to any favorite tokens on other networks (such as Bitcoin, Monero, Ethereum, Polygon, etc.).

### 3. **Interact with dApps on Fantom ecosystem anonymously**

After shielding Fantom tokens to Incognito, users can still interact with dApps built on Fantom ecosystem while enjoying anonymity feature from Incognito. E.g. swapping anonymously on SpookySwap which is the leading AMM exchange on Fantom.

### 4. **Shield and unshield faster and cheaper**

Fantom is a scalable blockchain with high throughput and low transaction cost. So the bridge also benefits from the development: shielding/unshielding transactions would be faster and cheaper.

The following table shows the comparison of the shielding and unshielding fees between bridges at the writing time:

||Gas used shielding|Gas used unshielding|Gas price (Gwei)|Native token price ($)|Shielding fee ($)|Unshielding fee ($)|
| --- | --- | --- | --- | --- | --- | --- |
|**Ethereum**|67200|262000|50|3282|11|33|
|**BSC**|58000|204000|5|442|0.13|0.45|
|**Polygon**|58000|204000|30|1.62|0.003|0.01|
|**Fantom**|58000|204000|220|1.41|0.02|0.06|

## **How it works?**

The Incognito &lt;&gt; Fantom bridge mechanism is quite similar to the [Incognito - Ethereum trustless bridge](https://we.incognito.org/t/incognito-mode-for-ethereum/53) that also applies the locking, minting, and burning mechanisms to **maintain the 1:1 ratio** between the private token minted on Incognito chain and the original token shielded from Fantom chain.

### **Fantom :arrow_right: Incognito**

* **Step 1**: To convert Fantom tokens to the equivalent privacy tokens (pToken) on Incognito blockchain, user simply needs to send their tokens to the Incognito &lt;&gt; Fantom bridge smart contract to lock funds.
* **Step 2**: After the deposit transaction is confirmed aka getting at least **5 block confirmations** on the Fantom chain, user can get the deposit proof and submit it to the Incognito chain.
* **Step 3**: Incognito chain will verify the proof and extract information from it to get the deposit amount and user’s incognito address. If the proof is valid,  Incognito chain will mint the equivalent pToken at the same deposit amount to user’s incognito address.

   The process is shown in the following diagram.

![shield|690x474, 100%](upload://k9IRxL8UMh64HTlRjHdfWF1Rq7m.png) 

### **Incognito :arrow_right: Fantom**

Conversely, to withdraw pToken from Incognito chain to original Fantom chain, the process will be contrary: pToken will be burnt on Incognito chain and the burning proof will then be submitted to the Incognito &lt;&gt; Fantom bridge smart contract. The smart contract will verify the validity of submitted proof and unlock Fantom tokens to user&#39;s receiving address at the amount ratio of 1:1. 

The process is shown in the below diagram.

![unshield|690x405](upload://7LPlVDv2HLit2kPqxR0H4V1Gyr2.png) 

## **Conclusion**

We hope that the Incognito &lt;&gt; Fantom bridge will facilitate the co-operation between two networks, and bringing more utilities to our users. Besides, the team has also been working on making shielding flow more privacy - hiding who are shielding with [one-time shielding addresses](https://we.incognito.org/t/work-in-progress-one-time-shielding-addresses/15677).

Feel free to share your thought and feedback.
Thank you! :hugs:

### Download Incognito app and start providing liquidity for FTM :iphone:

[![appstore.7bf52fb9|318x100, 50%](upload://yKjLu056dqWJBF2NNOEmmOECp2y.png)](https://incognito.onelink.me/I7zE/walletbraveios)         [![play.d7c4cecd|340x100, 50%](upload://rBipb6Plqzhi77tPaUL2d3q8xrq.png)](https://incognito.onelink.me/I7zE/walletbrave)[![apk.818b2bb7|340x100, 50%](upload://pH7Wwjzcan5ppmAVjnki2ibI2to.png)](https://github.com/incognitochain/incognito-wallet/releases/)

[Learn about the wallet &gt;](https://we.incognito.org/t/wallet/336)
[Join community conversation &gt;](https://t.me/incognitochain)
[Provide liquidity for FTM and earn rewards &gt;](https://we.incognito.org/t/how-to-contribute-liquidity-and-earn-rewards/15254)</description>
    
    <lastBuildDate>Thu, 21 Apr 2022 09:02:42 +0000</lastBuildDate>
    <category>Interoperability</category>
    <atom:link href="https://we.incognito.org/t/the-incognito-fantom-bridge/16456.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>The Incognito &lt;&gt; Fantom bridge</title>
        <dc:creator><![CDATA[@Ducky]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/ducky">@Ducky</a> wrote:</p>
          <blockquote>
              
          </blockquote>
          <p><a href="https://we.incognito.org/t/the-incognito-fantom-bridge/16456/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/the-incognito-fantom-bridge/16456/3</link>
        <pubDate>Mon, 09 May 2022 11:21:05 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16456-3</guid>
        <source url="https://we.incognito.org/t/the-incognito-fantom-bridge/16456.rss">The Incognito &lt;&gt; Fantom bridge</source>
      </item>
      <item>
        <title>The Incognito &lt;&gt; Fantom bridge</title>
        <dc:creator><![CDATA[@0xkraken 0xkraken]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/0xkraken">@0xkraken</a> wrote:</p>
          <blockquote>
              <p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/9/9/9926c3470be740f7178094a42b56ab3e14c16aaa.jpeg" data-download-href="/uploads/short-url/lQQ6jHNQmdolhNmBRguw3tgPAHE.jpeg?dl=1" title="_Fantom X Incognito"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/9/9926c3470be740f7178094a42b56ab3e14c16aaa_2_690x388.jpeg" alt="_Fantom X Incognito" data-base62-sha1="lQQ6jHNQmdolhNmBRguw3tgPAHE" width="690" height="388" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/9/9926c3470be740f7178094a42b56ab3e14c16aaa_2_690x388.jpeg, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/9/9926c3470be740f7178094a42b56ab3e14c16aaa_2_1035x582.jpeg 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/9/9/9926c3470be740f7178094a42b56ab3e14c16aaa.jpeg 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/9/9/9926c3470be740f7178094a42b56ab3e14c16aaa_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">_Fantom X Incognito</span><span class="informations">1280×720 230 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p><img src="https://we.incognito.org/images/emoji/apple/wave.png?v=9" title=":wave:" class="emoji" alt=":wave:"> Hi everyone,</p>
<p>We are so pleased to announce that <strong>the Incognito &lt;&gt; Fantom bridge</strong> is now live.<br>
As you know, Incognito is the privacy blockchain. Apart from privacy, interoperability is also a key component of Incognito. To date, Incognito bridged to 12 major blockchains (Bitcoin, Ethereum, Binance Smart Chain, Polygon, etc.), and now Fantom.</p>
<h2><strong>What problems are the bridge solving?</strong></h2>
<p>Like other Incognito bridges, Incognito - Fantom bridge allows users to make cross-chain transactions between Fantom and Incognito networks. The bridge offers the community <strong>combined advantages</strong> of both Incognito and Fantom:</p>
<h3>1. <strong>Transfer Fantom tokens privately</strong>
</h3>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/a/5/a5370b9c542e362acbeaeb6084e5d886fde49ecc.png" data-download-href="/uploads/short-url/nzyIrbVWRuLoRkBN1edDiiOaLz6.png?dl=1" title="bridge"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/5/a5370b9c542e362acbeaeb6084e5d886fde49ecc_2_624x105.png" alt="bridge" data-base62-sha1="nzyIrbVWRuLoRkBN1edDiiOaLz6" width="624" height="105" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/5/a5370b9c542e362acbeaeb6084e5d886fde49ecc_2_624x105.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/5/a5370b9c542e362acbeaeb6084e5d886fde49ecc_2_936x157.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/5/a5370b9c542e362acbeaeb6084e5d886fde49ecc_2_1248x210.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/a/5/a5370b9c542e362acbeaeb6084e5d886fde49ecc_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">bridge</span><span class="informations">2730×462 49.7 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p>Fantom is a public blockchain. It means anyone can see your balance, and track your transactions. Thanks to the bridge, users can shield Fantom tokens to Incognito and transfer them privately. The sender, the receiver, the amount, and also the asset type, all stay anonymously in the Incognito network. Therefore, no one can either track your activities or search financial histories.</p>
<h3>2. <strong>Trade for tokens anonymously</strong>
</h3>
<p>Shielding tokens to Incognito is similar to stepping into Incognito ecosystem. One of the best Incognito’s applications is the privacy exchange. It’s an AMM decentralized exchange that users can trade anonymously (don’t reveal the identity of traders). Especially, users can swap Fantom tokens to any favorite tokens on other networks (such as Bitcoin, Monero, Ethereum, Polygon, etc.).</p>
<h3>3. <strong>Interact with dApps on Fantom ecosystem anonymously</strong>
</h3>
<p>After shielding Fantom tokens to Incognito, users can still interact with dApps built on Fantom ecosystem while enjoying anonymity feature from Incognito. E.g. swapping anonymously on SpookySwap which is the leading AMM exchange on Fantom.</p>
<h3>4. <strong>Shield and unshield faster and cheaper</strong>
</h3>
<p>Fantom is a scalable blockchain with high throughput and low transaction cost. So the bridge also benefits from the development: shielding/unshielding transactions would be faster and cheaper.</p>
<p>The following table shows the comparison of the shielding and unshielding fees between bridges at the writing time:</p>
<div class="md-table">
<table>
<thead>
<tr>
<th></th>
<th>Gas used shielding</th>
<th>Gas used unshielding</th>
<th>Gas price (Gwei)</th>
<th>Native token price ($)</th>
<th>Shielding fee ($)</th>
<th>Unshielding fee ($)</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>Ethereum</strong></td>
<td>67200</td>
<td>262000</td>
<td>50</td>
<td>3282</td>
<td>11</td>
<td>33</td>
</tr>
<tr>
<td><strong>BSC</strong></td>
<td>58000</td>
<td>204000</td>
<td>5</td>
<td>442</td>
<td>0.13</td>
<td>0.45</td>
</tr>
<tr>
<td><strong>Polygon</strong></td>
<td>58000</td>
<td>204000</td>
<td>30</td>
<td>1.62</td>
<td>0.003</td>
<td>0.01</td>
</tr>
<tr>
<td><strong>Fantom</strong></td>
<td>58000</td>
<td>204000</td>
<td>220</td>
<td>1.41</td>
<td>0.02</td>
<td>0.06</td>
</tr>
</tbody>
</table>
</div><h2><strong>How it works?</strong></h2>
<p>The Incognito &lt;&gt; Fantom bridge mechanism is quite similar to the <a href="https://we.incognito.org/t/incognito-mode-for-ethereum/53">Incognito - Ethereum trustless bridge</a> that also applies the locking, minting, and burning mechanisms to <strong>maintain the 1:1 ratio</strong> between the private token minted on Incognito chain and the original token shielded from Fantom chain.</p>
<h3><strong>Fantom <img src="https://we.incognito.org/images/emoji/apple/arrow_right.png?v=9" title=":arrow_right:" class="emoji" alt=":arrow_right:"> Incognito</strong></h3>
<ul>
<li>
<p><strong>Step 1</strong>: To convert Fantom tokens to the equivalent privacy tokens (pToken) on Incognito blockchain, user simply needs to send their tokens to the Incognito &lt;&gt; Fantom bridge smart contract to lock funds.</p>
</li>
<li>
<p><strong>Step 2</strong>: After the deposit transaction is confirmed aka getting at least <strong>5 block confirmations</strong> on the Fantom chain, user can get the deposit proof and submit it to the Incognito chain.</p>
</li>
<li>
<p><strong>Step 3</strong>: Incognito chain will verify the proof and extract information from it to get the deposit amount and user’s incognito address. If the proof is valid,  Incognito chain will mint the equivalent pToken at the same deposit amount to user’s incognito address.</p>
<p>The process is shown in the following diagram.</p>
</li>
</ul>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/8/d/8d44dd2a77f661c7cc68efa22f6555e24e6aee78.png" data-download-href="/uploads/short-url/k9IRxL8UMh64HTlRjHdfWF1Rq7m.png?dl=1" title="shield"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/8/d/8d44dd2a77f661c7cc68efa22f6555e24e6aee78_2_690x474.png" alt="shield" data-base62-sha1="k9IRxL8UMh64HTlRjHdfWF1Rq7m" width="690" height="474" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/8/d/8d44dd2a77f661c7cc68efa22f6555e24e6aee78_2_690x474.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/8/d/8d44dd2a77f661c7cc68efa22f6555e24e6aee78_2_1035x711.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/8/d/8d44dd2a77f661c7cc68efa22f6555e24e6aee78_2_1380x948.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/8/d/8d44dd2a77f661c7cc68efa22f6555e24e6aee78_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">shield</span><span class="informations">1746×1200 86.5 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<h3><strong>Incognito <img src="https://we.incognito.org/images/emoji/apple/arrow_right.png?v=9" title=":arrow_right:" class="emoji" alt=":arrow_right:"> Fantom</strong></h3>
<p>Conversely, to withdraw pToken from Incognito chain to original Fantom chain, the process will be contrary: pToken will be burnt on Incognito chain and the burning proof will then be submitted to the Incognito &lt;&gt; Fantom bridge smart contract. The smart contract will verify the validity of submitted proof and unlock Fantom tokens to user’s receiving address at the amount ratio of 1:1.</p>
<p>The process is shown in the below diagram.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/3/6/3677495cb5decdf572abab87137c0bfce4cddc34.png" data-download-href="/uploads/short-url/7LPlVDv2HLit2kPqxR0H4V1Gyr2.png?dl=1" title="unshield"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3677495cb5decdf572abab87137c0bfce4cddc34_2_690x405.png" alt="unshield" data-base62-sha1="7LPlVDv2HLit2kPqxR0H4V1Gyr2" width="690" height="405" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3677495cb5decdf572abab87137c0bfce4cddc34_2_690x405.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3677495cb5decdf572abab87137c0bfce4cddc34_2_1035x607.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3677495cb5decdf572abab87137c0bfce4cddc34_2_1380x810.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/6/3677495cb5decdf572abab87137c0bfce4cddc34_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">unshield</span><span class="informations">1811×1063 52.1 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<h2><strong>Conclusion</strong></h2>
<p>We hope that the Incognito &lt;&gt; Fantom bridge will facilitate the co-operation between two networks, and bringing more utilities to our users. Besides, the team has also been working on making shielding flow more privacy - hiding who are shielding with <a href="https://we.incognito.org/t/work-in-progress-one-time-shielding-addresses/15677">one-time shielding addresses</a>.</p>
<p>Feel free to share your thought and feedback.<br>
Thank you! <img src="https://we.incognito.org/images/emoji/apple/hugs.png?v=9" title=":hugs:" class="emoji" alt=":hugs:"></p>
<h3>Download Incognito app and start providing liquidity for FTM <img src="https://we.incognito.org/images/emoji/apple/iphone.png?v=9" title=":iphone:" class="emoji" alt=":iphone:">
</h3>
<p><a href="https://incognito.onelink.me/I7zE/walletbraveios"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/f/3/f3864d4b0b3738c1b3b7c40d5bf2e7da2ccd0792.png" alt="appstore.7bf52fb9" data-base62-sha1="yKjLu056dqWJBF2NNOEmmOECp2y" width="159" height="50"></a>         <a href="https://incognito.onelink.me/I7zE/walletbrave"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/c/1/c172013f8645c388fd5876fc07e9a8d8ac98ec68.png" alt="play.d7c4cecd" data-base62-sha1="rBipb6Plqzhi77tPaUL2d3q8xrq" width="170" height="50"></a><a href="https://github.com/incognitochain/incognito-wallet/releases/"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/b/4/b4166749b5840e6b5122d232648b99c3894b97b6.png" alt="apk.818b2bb7" data-base62-sha1="pH7Wwjzcan5ppmAVjnki2ibI2to" width="170" height="50"></a></p>
<p><a href="https://we.incognito.org/t/wallet/336">Learn about the wallet &gt;</a><br>
<a href="https://t.me/incognitochain">Join community conversation &gt;</a><br>
<a href="https://we.incognito.org/t/how-to-contribute-liquidity-and-earn-rewards/15254">Provide liquidity for FTM and earn rewards &gt;</a></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/the-incognito-fantom-bridge/16456/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/the-incognito-fantom-bridge/16456/1</link>
        <pubDate>Tue, 19 Apr 2022 17:00:00 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16456-1</guid>
        <source url="https://we.incognito.org/t/the-incognito-fantom-bridge/16456.rss">The Incognito &lt;&gt; Fantom bridge</source>
      </item>
  </channel>
</rss>
