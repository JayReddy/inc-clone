<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Incognito reduces blockchain size by 90%</title>
    <link>https://we.incognito.org/t/incognito-reduces-blockchain-size-by-90/1410</link>
    <description>Incognito developers took a look at the blockchain and noticed some room for improvement. According to @hungngo, one of the devs on this project, there was “a lot of redundant data” in the blockchain, causing nodes to become “enormous” from syncing so much data.

Well now, validators will be happy to hear that **the blockchain has been reduced an average of 90%, and that there is much less consumption of data in the network process.** 

This reduction took place in two main spots: **The beacon, and shards.** If you’re familiar with these terms, read ahead, but if not, I recommend pausing to [read up on validators](https://incognito.org/t/lifecycle-of-a-validator-explanation/957/3).

## Beacon Reduction

Because it tracks all the important data for keeping the shards operating correctly, the beacon node makes up about 99% of the blockchain’s size. It includes a history of blocks to verify that the chain is consistent. Initially, a “store and retrieve” method was in place for this, which is rather self explanatory: The nodes stored the information directly to recall it when necessary. **But this took up too much space.**

At first, development of Incognito was intentionally unstructured, to allow for fast and easy coding. This made Incognito a flexible project that rapidly developed, and was great for the project’s inception. Now, however, the chain has gotten much larger and the redundancies caused by lack of structure became more of a problem. 

To solve this, the developers created an efficient way of organizing past data and structuring new data, which has been nicknamed the “time travel schema”. This new method allows nodes to easily trace past data states very quickly, as if they travelled in time and back. The schema has allowed for the size of the beacon data on each node to **shrink from 140 GB to only 2 GB.** This was a “huge victory” according to @hungngo, and will **extend the life expectancy of nodes significantly** without needing to increase memory space.

## Shard Reduction

The shard part of the data was enhanced, but not reduced. The same recall method that was implemented for the beacon was also implemented for the shard, allowing for greater efficiency. The size minimally increased from about 1.5 GB to 2GB, but the effects of the upgrade will increase node performance, rather than decrease it, since the size has already been reduced so drastically.

Altogether, when coupled with a few other changes, **the blockchain size is now down to just 12GB.** In addition, thanks to the latest update, **block insertion speeds increased from 10-20%.** 

## What does this mean for Node owners?

Whether you run a physical or virtual node, this is great news. The amount of storage space the blockchain will take up in your node has been reduced by around 90%, and the rate at which it fills up has been slowed. **This means a longer, happier life for your nodes** before you even have to think about adding storage.


**There is one step you need to take, however.** See this tutorial for the quick and easy process of [updating your node](https://incognito.org/t/important-update-your-node-to-extend-its-life-cycle/1094).




Thanks for reading! Don’t forget to share your thoughts and comment with your feedback. If I&#39;ve missed anything, let me know. 

If you’ve got ideas on how to improve Incognito, [write a proposal!](https://incognito.org/c/DAO/proposals)</description>
    
    <lastBuildDate>Tue, 14 Apr 2020 13:29:09 +0000</lastBuildDate>
    <category>Newsroom</category>
    <atom:link href="https://we.incognito.org/t/incognito-reduces-blockchain-size-by-90/1410.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Incognito reduces blockchain size by 90%</title>
        <dc:creator><![CDATA[@aaron Aaron Ahmadi]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/aaron">@aaron</a> wrote:</p>
          <blockquote>
              <p>Incognito developers took a look at the blockchain and noticed some room for improvement. According to <a class="mention" href="/u/hungngo">@hungngo</a>, one of the devs on this project, there was “a lot of redundant data” in the blockchain, causing nodes to become “enormous” from syncing so much data.</p>
<p>Well now, validators will be happy to hear that <strong>the blockchain has been reduced an average of 90%, and that there is much less consumption of data in the network process.</strong></p>
<p>This reduction took place in two main spots: <strong>The beacon, and shards.</strong> If you’re familiar with these terms, read ahead, but if not, I recommend pausing to <a href="https://incognito.org/t/lifecycle-of-a-validator-explanation/957/3">read up on validators</a>.</p>
<h2>Beacon Reduction</h2>
<p>Because it tracks all the important data for keeping the shards operating correctly, the beacon node makes up about 99% of the blockchain’s size. It includes a history of blocks to verify that the chain is consistent. Initially, a “store and retrieve” method was in place for this, which is rather self explanatory: The nodes stored the information directly to recall it when necessary. <strong>But this took up too much space.</strong></p>
<p>At first, development of Incognito was intentionally unstructured, to allow for fast and easy coding. This made Incognito a flexible project that rapidly developed, and was great for the project’s inception. Now, however, the chain has gotten much larger and the redundancies caused by lack of structure became more of a problem.</p>
<p>To solve this, the developers created an efficient way of organizing past data and structuring new data, which has been nicknamed the “time travel schema”. This new method allows nodes to easily trace past data states very quickly, as if they travelled in time and back. The schema has allowed for the size of the beacon data on each node to <strong>shrink from 140 GB to only 2 GB.</strong> This was a “huge victory” according to <a class="mention" href="/u/hungngo">@hungngo</a>, and will <strong>extend the life expectancy of nodes significantly</strong> without needing to increase memory space.</p>
<h2>Shard Reduction</h2>
<p>The shard part of the data was enhanced, but not reduced. The same recall method that was implemented for the beacon was also implemented for the shard, allowing for greater efficiency. The size minimally increased from about 1.5 GB to 2GB, but the effects of the upgrade will increase node performance, rather than decrease it, since the size has already been reduced so drastically.</p>
<p>Altogether, when coupled with a few other changes, <strong>the blockchain size is now down to just 12GB.</strong> In addition, thanks to the latest update, <strong>block insertion speeds increased from 10-20%.</strong></p>
<h2>What does this mean for Node owners?</h2>
<p>Whether you run a physical or virtual node, this is great news. The amount of storage space the blockchain will take up in your node has been reduced by around 90%, and the rate at which it fills up has been slowed. <strong>This means a longer, happier life for your nodes</strong> before you even have to think about adding storage.</p>
<p><strong>There is one step you need to take, however.</strong> See this tutorial for the quick and easy process of <a href="https://incognito.org/t/important-update-your-node-to-extend-its-life-cycle/1094">updating your node</a>.</p>
<p>Thanks for reading! Don’t forget to share your thoughts and comment with your feedback. If I’ve missed anything, let me know.</p>
<p>If you’ve got ideas on how to improve Incognito, <a href="https://incognito.org/c/DAO/proposals">write a proposal!</a></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-reduces-blockchain-size-by-90/1410/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-reduces-blockchain-size-by-90/1410/1</link>
        <pubDate>Thu, 09 Apr 2020 10:42:01 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1410-1</guid>
        <source url="https://we.incognito.org/t/incognito-reduces-blockchain-size-by-90/1410.rss">Incognito reduces blockchain size by 90%</source>
      </item>
  </channel>
</rss>
