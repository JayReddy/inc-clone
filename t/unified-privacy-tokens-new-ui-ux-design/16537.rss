<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Unified privacy tokens - New UI/UX Design</title>
    <link>https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537</link>
    <description>Hello Incognito users :raised_hand_with_fingers_splayed:

Here are some new updates on the progress of Incognito’s interoperability with unified privacy tokens.

In [the previous post](https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914), we shared the main idea of Unified pTokens feature, as well as the benefits that it brings to the community.

Now, let’s go through the new UI/UX design for the initiative.

## First, converting privacy tokens to unified pTokens.

After the feature is released, users can convert their existing privacy tokens to unified privacy tokens, then takes action on them more conveniently and with more benefits.

To convert tokens in the app, select `Settings` in More tab, and then tap `Convert`.

![group 1|690x478](upload://a8ubvpUJmoI5KJTK70sQ0MbdmJB.png) 

* The app will check balances and group existing tokens **by currency**. Users can choose one or multiple coins to convert.

* The conversion process will be shown on app screen, and it may take a few minutes to complete, so please don’t navigate away until the process finishes.

* Users can press the `Go back to wallet` button to check balances when the process completes.

![group 2 (1)|690x478](upload://f9iZPfA25JcYmuulq4RPAYps4HS.png) 

When the conversion finishes, users will receive back unified ptoken with **a new balance equals to the sum of the grouped privacy tokens&#39; balances**.  Users can both transfer privately and swap anonymously with the unified pToken (of course with all balances).

:point_right: **Example**:
The account has **5 pETH (Ethereum)**, **3 pETH (BSC)**, **2 pETH (Polygon)**.
After converting, the account now has **5 + 3 + 2 = 10 pETH (unified pToken)**.

:point_right: **Note**: 
In the first release, the protocol will only support unifying popular tokens (e.g ETH, DAI, USDT, USDC, MATIC, ...). The list of supported tokens will be published when the feature is officialy launched and updated usually in the future. 

## Second, shielding with unified pTokens.

One of the biggest advantages that users can benefit from this new feature is that it enables users to shield unified pTokens to Incognito **from any networks** that the protocol supports.

Let&#39;s make a quick comparison:

- Current approach: if users shield the **same currency from different networks**,  they will receive **different privacy tokens** correspondingly in their wallet.

- New approach: if users shield the **same currency from different networks**, they will receive **the same unified pToken** in their wallet.

Basically, the UI/UX for unified pTokens shielding flow is quite similar to the current one and adds in some updates to make it clearer and better in terms of users&#39; experience. The new UI will be generally applied to all tokens (including not unified pTokens).

![group 3|690x478](upload://s7ggGTfUVZFbbihXNAa7MmSVcgK.png) 

* Select a coin to shield.

* The app will show a list of networks that support the selected coin. Users can pick one as expected and send funds from the picked network to Incognito.

* There are two ways for sending funds to Incognito: generating shielding address or connecting to users’ Ethereum wallet. If users choose `Generate a shielding address`, all needed information like the shielding address, minimum shielding amount, and estimated shielding fee will be shown on the screen (refer to the screenshots above).

* Also on this screen, users can switch to other networks by tapping on the icon in the `Network type` field and choose another one.

:point_right: **Example**:
- If user shields 1 ETH on Ethereum,  he receives 1 pETH (unified pToken) on Incognito.

- If user shields 1 ETH on Polygon, he also receives 1 pETH (unified pToken) on Incognito.

 As a result, now that he has 2 pETH (unified pToken) in his wallet and can send, receive, trade, provide, etc. privately in Incognito network.

## Third, unshielding with unified pTokens.

Conversely, users can unshield unified pTokens to get their public tokens back. Different from the current approach, thanks to the unified pTokens feature,  **users now can choose any networks** (which are supported by the protocol) to receive tokens withdrawn from Incognito.

![group 4|690x478](upload://7kdzFtv4MWToYFKptKwoS3Xq9mi.png) 

* Select a coin to unshield. 

* Press `Send` button.

* Enter the amount, the external address (aka receiving address), and the network that users want to receive their assets back.

* The app will estimate the unshielding fees in unified pToken. If the actually charged fee is less than the estimated fee, the redundant fee will be summed up to the receiving amount. Therefore, the receiving amount may be a little more than the unshielding amount.


That’s all about UI-UX for unified pTokens! :raised_hands:

We are looking forward to having your instructive thoughts and feedback. Let’s share with us via comment below. Thank you. :heart:</description>
    
    <lastBuildDate>Thu, 18 Aug 2022 17:13:32 +0000</lastBuildDate>
    <category>App users</category>
    <atom:link href="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Unified privacy tokens - New UI/UX Design</title>
        <dc:creator><![CDATA[@Incognita]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/incognita">@Incognita</a> wrote:</p>
          <blockquote>
              <p>Just trying out this feature for the first time. This is a really awesome feature! Hoping to learn more about it as time goes on, well done guys!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/6</link>
        <pubDate>Thu, 18 Aug 2022 17:13:32 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16537-6</guid>
        <source url="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537.rss">Unified privacy tokens - New UI/UX Design</source>
      </item>
      <item>
        <title>Unified privacy tokens - New UI/UX Design</title>
        <dc:creator><![CDATA[@0xkraken 0xkraken]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/0xkraken">@0xkraken</a> wrote:</p>
          <blockquote>
              <p>Hi <a class="mention" href="/u/abduraman">@abduraman</a>, thanks for your great questions!</p>
<p><strong>1. Unshielding but vaults are not enough?</strong><br>
We have built the <strong>re-balancing vault mechanism</strong>. The main idea for the mechanism is derived from the AMM protocol (but not for trading). The mechanism’s technical details will be updated in the other post.</p>
<p>It makes an <strong>incentive for anyone</strong> to rebalance vaults. <strong>Rewards for shielding and fees for unshielding</strong>. The higher the vault demand is, the higher the shielding reward or unshielding fee will be earned or paid. In other words, users can shield to a “high demand” vault with a high reward or unshield from a “low demand” vault with a low fee.</p>
<p>But it doesn’t actually resolve your case. In this case, you can’t unshield 15 ETH once time with the current vaults: 10 ETH (native) and 10 ETH (BSC). You should unshield 2 times. First, unshield 10 ETH to BSC. Then, the ETH vault on BSC has high demand, so the great rewards will be paid for anyone shielding to this vault. You can wait until the vault is grow up and unshield 5 ETH left to BSC. If you don’t want to wait, you can unshield the remaining to the Ethereum network.</p>
<p><strong>2. Unified pTokens and pDEX?</strong><br>
First, the unified pTokens feature is independent of the pDEX. It means that after releasing this feature, the pDEX’s pairs with un-unified pTokens (old pTokens) won’t be affected.</p>
<p>Second, any liquidity provider can <strong>withdraw liquidity from pairs with old pTokens</strong>, then convert to unified pTokens and <strong>re-add to the liquidity pool with new pTokens</strong>. That means liquidity providers can group these pools have the same currency to increase liquidity pool size and reduce swap slippage on pDEX.</p>
<p><strong>3. The old un-unified tokens?</strong><br>
No problem with the old un-unified tokens. Users <strong>still can take actions</strong> with the old pTokens as before such as transferring, unshielding, swapping with pDEX (on the old pairs) or pApps, <strong>except shielding</strong>. As you said, shielding with the old pTokens will be disabled to prevent confusion.</p>
<p><strong>4. Merging “Receive” and “Shield”?</strong></p>
<p>There is a little different UX between “Receive” and “Shield”:</p>
<ul>
<li>“Receive”: get the incognito address to receive PRV or pTokens from others.</li>
<li>“Shield”: get the external address (shielding address) and have to send funds to this external address to receive pTokens back.</li>
</ul>
<p>So I’m not sure merging “Receive” and “Shield” will be better. But anyway, thanks for your idea. The team will consider this for the next update.</p>
<p>If there is any feedback, feel free to share with us. Thank you.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/5</link>
        <pubDate>Wed, 20 Apr 2022 14:36:11 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16537-5</guid>
        <source url="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537.rss">Unified privacy tokens - New UI/UX Design</source>
      </item>
      <item>
        <title>Unified privacy tokens - New UI/UX Design</title>
        <dc:creator><![CDATA[@abduraman Abduraman Blackram]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/abduraman">@abduraman</a> wrote:</p>
          <blockquote>
              <p>Not directly related to this awesome feature and I know you want to keep “Shield” word. However, like “Send” interface, how about merging “Receive” (Incognito, Ethereum, Polygon …) interface instead of another screen just for Shielding? So just Send and Receive buttons will exist.</p>
<aside class="quote no-group quote-modified" data-username="0xkraken" data-post="1" data-topic="16537">
<div class="title">
<div class="quote-controls"></div>
<img alt="" width="20" height="20" src="/user_avatar/we.incognito.org/0xkraken/40/9124_2.png" class="avatar"> 0xkraken:</div>
<blockquote>
<p><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_690x478.png" alt="group 3" data-base62-sha1="s7ggGTfUVZFbbihXNAa7MmSVcgK" width="690" height="478" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_690x478.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_1035x717.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_1380x956.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_10x10.png"></p>
</blockquote>
</aside>
          </blockquote>
          <p><a href="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/4</link>
        <pubDate>Tue, 19 Apr 2022 11:19:13 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16537-4</guid>
        <source url="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537.rss">Unified privacy tokens - New UI/UX Design</source>
      </item>
      <item>
        <title>Unified privacy tokens - New UI/UX Design</title>
        <dc:creator><![CDATA[@abduraman Abduraman Blackram]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/abduraman">@abduraman</a> wrote:</p>
          <blockquote>
              <p>Thanks again for this awesome feature. Now, questions <img src="https://we.incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
<p>1- How have you solved this problem?</p>
<aside class="quote no-group" data-username="abduraman" data-post="3" data-topic="15914">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/abduraman/40/832_2.png" class="avatar"><a href="https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914/3">Work in progress: unified privacy tokens and interoperable apps</a>
</div>
<blockquote>
<p>How will you manage unshielding of multichain coins? Let’s assume that Incognito has 10 ETH (native) and 10 ETH (BSC) in its vaults and somebody wants to unshield 15 ETH (BSC). In such cases, what will happen?</p>
</blockquote>
</aside>
<p>2- Will you re-organize pDEX pairs to use the new unified tokens? Or will the whole process be managed by pDEX module?</p>
<p>3- What will the old un-unified tokens be? Their association with the real assets will be broken later. I think shielding to those tokens should be disabled to prevent confusion. Right?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/3</link>
        <pubDate>Tue, 19 Apr 2022 11:13:10 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16537-3</guid>
        <source url="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537.rss">Unified privacy tokens - New UI/UX Design</source>
      </item>
      <item>
        <title>Unified privacy tokens - New UI/UX Design</title>
        <dc:creator><![CDATA[@0xkraken 0xkraken]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/0xkraken">@0xkraken</a> wrote:</p>
          <blockquote>
              <p>Hello Incognito users <img src="https://we.incognito.org/images/emoji/apple/raised_hand_with_fingers_splayed.png?v=9" title=":raised_hand_with_fingers_splayed:" class="emoji" alt=":raised_hand_with_fingers_splayed:"></p>
<p>Here are some new updates on the progress of Incognito’s interoperability with unified privacy tokens.</p>
<p>In <a href="https://we.incognito.org/t/work-in-progress-unified-privacy-tokens-and-interoperable-apps/15914">the previous post</a>, we shared the main idea of Unified pTokens feature, as well as the benefits that it brings to the community.</p>
<p>Now, let’s go through the new UI/UX design for the initiative.</p>
<h2>First, converting privacy tokens to unified pTokens.</h2>
<p>After the feature is released, users can convert their existing privacy tokens to unified privacy tokens, then takes action on them more conveniently and with more benefits.</p>
<p>To convert tokens in the app, select <code>Settings</code> in More tab, and then tap <code>Convert</code>.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/4/7/470b55c48e21da1f79571005874f6b92b28bbcab.png" data-download-href="/uploads/short-url/a8ubvpUJmoI5KJTK70sQ0MbdmJB.png?dl=1" title="group 1"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/4/7/470b55c48e21da1f79571005874f6b92b28bbcab_2_690x478.png" alt="group 1" data-base62-sha1="a8ubvpUJmoI5KJTK70sQ0MbdmJB" width="690" height="478" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/4/7/470b55c48e21da1f79571005874f6b92b28bbcab_2_690x478.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/4/7/470b55c48e21da1f79571005874f6b92b28bbcab_2_1035x717.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/4/7/470b55c48e21da1f79571005874f6b92b28bbcab_2_1380x956.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/4/7/470b55c48e21da1f79571005874f6b92b28bbcab_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">group 1</span><span class="informations">2570×1784 120 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<ul>
<li>
<p>The app will check balances and group existing tokens <strong>by currency</strong>. Users can choose one or multiple coins to convert.</p>
</li>
<li>
<p>The conversion process will be shown on app screen, and it may take a few minutes to complete, so please don’t navigate away until the process finishes.</p>
</li>
<li>
<p>Users can press the <code>Go back to wallet</code> button to check balances when the process completes.</p>
</li>
</ul>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/6/a/6a2debc81b4ace083bc0bd4404cc754e4bdd8860.png" data-download-href="/uploads/short-url/f9iZPfA25JcYmuulq4RPAYps4HS.png?dl=1" title="group 2 (1)"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/6/a/6a2debc81b4ace083bc0bd4404cc754e4bdd8860_2_690x478.png" alt="group 2 (1)" data-base62-sha1="f9iZPfA25JcYmuulq4RPAYps4HS" width="690" height="478" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/6/a/6a2debc81b4ace083bc0bd4404cc754e4bdd8860_2_690x478.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/6/a/6a2debc81b4ace083bc0bd4404cc754e4bdd8860_2_1035x717.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/6/a/6a2debc81b4ace083bc0bd4404cc754e4bdd8860_2_1380x956.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/6/a/6a2debc81b4ace083bc0bd4404cc754e4bdd8860_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">group 2 (1)</span><span class="informations">2570×1784 121 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p>When the conversion finishes, users will receive back unified ptoken with <strong>a new balance equals to the sum of the grouped privacy tokens’ balances</strong>.  Users can both transfer privately and swap anonymously with the unified pToken (of course with all balances).</p>
<p><img src="https://we.incognito.org/images/emoji/apple/point_right.png?v=9" title=":point_right:" class="emoji" alt=":point_right:"> <strong>Example</strong>:<br>
The account has <strong>5 pETH (Ethereum)</strong>, <strong>3 pETH (BSC)</strong>, <strong>2 pETH (Polygon)</strong>.<br>
After converting, the account now has <strong>5 + 3 + 2 = 10 pETH (unified pToken)</strong>.</p>
<p><img src="https://we.incognito.org/images/emoji/apple/point_right.png?v=9" title=":point_right:" class="emoji" alt=":point_right:"> <strong>Note</strong>:<br>
In the first release, the protocol will only support unifying popular tokens (e.g ETH, DAI, USDT, USDC, MATIC, …). The list of supported tokens will be published when the feature is officialy launched and updated usually in the future.</p>
<h2>Second, shielding with unified pTokens.</h2>
<p>One of the biggest advantages that users can benefit from this new feature is that it enables users to shield unified pTokens to Incognito <strong>from any networks</strong> that the protocol supports.</p>
<p>Let’s make a quick comparison:</p>
<ul>
<li>
<p>Current approach: if users shield the <strong>same currency from different networks</strong>,  they will receive <strong>different privacy tokens</strong> correspondingly in their wallet.</p>
</li>
<li>
<p>New approach: if users shield the <strong>same currency from different networks</strong>, they will receive <strong>the same unified pToken</strong> in their wallet.</p>
</li>
</ul>
<p>Basically, the UI/UX for unified pTokens shielding flow is quite similar to the current one and adds in some updates to make it clearer and better in terms of users’ experience. The new UI will be generally applied to all tokens (including not unified pTokens).</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096.png" data-download-href="/uploads/short-url/s7ggGTfUVZFbbihXNAa7MmSVcgK.png?dl=1" title="group 3"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_690x478.png" alt="group 3" data-base62-sha1="s7ggGTfUVZFbbihXNAa7MmSVcgK" width="690" height="478" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_690x478.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_1035x717.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_1380x956.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/c/5/c50f086316f9b007a771adb9c734c35f252fe096_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">group 3</span><span class="informations">2570×1784 208 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<ul>
<li>
<p>Select a coin to shield.</p>
</li>
<li>
<p>The app will show a list of networks that support the selected coin. Users can pick one as expected and send funds from the picked network to Incognito.</p>
</li>
<li>
<p>There are two ways for sending funds to Incognito: generating shielding address or connecting to users’ Ethereum wallet. If users choose <code>Generate a shielding address</code>, all needed information like the shielding address, minimum shielding amount, and estimated shielding fee will be shown on the screen (refer to the screenshots above).</p>
</li>
<li>
<p>Also on this screen, users can switch to other networks by tapping on the icon in the <code>Network type</code> field and choose another one.</p>
</li>
</ul>
<p><img src="https://we.incognito.org/images/emoji/apple/point_right.png?v=9" title=":point_right:" class="emoji" alt=":point_right:"> <strong>Example</strong>:</p>
<ul>
<li>
<p>If user shields 1 ETH on Ethereum,  he receives 1 pETH (unified pToken) on Incognito.</p>
</li>
<li>
<p>If user shields 1 ETH on Polygon, he also receives 1 pETH (unified pToken) on Incognito.</p>
</li>
</ul>
<p>As a result, now that he has 2 pETH (unified pToken) in his wallet and can send, receive, trade, provide, etc. privately in Incognito network.</p>
<h2>Third, unshielding with unified pTokens.</h2>
<p>Conversely, users can unshield unified pTokens to get their public tokens back. Different from the current approach, thanks to the unified pTokens feature,  <strong>users now can choose any networks</strong> (which are supported by the protocol) to receive tokens withdrawn from Incognito.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://incognito-discourse.s3-us-west-2.amazonaws.com/original/3X/3/3/335851ded8b28e3425d7b8b2ad456f916c52128a.png" data-download-href="/uploads/short-url/7kdzFtv4MWToYFKptKwoS3Xq9mi.png?dl=1" title="group 4"><img src="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/3/335851ded8b28e3425d7b8b2ad456f916c52128a_2_690x478.png" alt="group 4" data-base62-sha1="7kdzFtv4MWToYFKptKwoS3Xq9mi" width="690" height="478" srcset="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/3/335851ded8b28e3425d7b8b2ad456f916c52128a_2_690x478.png, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/3/335851ded8b28e3425d7b8b2ad456f916c52128a_2_1035x717.png 1.5x, https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/3/335851ded8b28e3425d7b8b2ad456f916c52128a_2_1380x956.png 2x" data-small-upload="https://incognito-discourse.s3-us-west-2.amazonaws.com/optimized/3X/3/3/335851ded8b28e3425d7b8b2ad456f916c52128a_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">group 4</span><span class="informations">2570×1784 140 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<ul>
<li>
<p>Select a coin to unshield.</p>
</li>
<li>
<p>Press <code>Send</code> button.</p>
</li>
<li>
<p>Enter the amount, the external address (aka receiving address), and the network that users want to receive their assets back.</p>
</li>
<li>
<p>The app will estimate the unshielding fees in unified pToken. If the actually charged fee is less than the estimated fee, the redundant fee will be summed up to the receiving amount. Therefore, the receiving amount may be a little more than the unshielding amount.</p>
</li>
</ul>
<p>That’s all about UI-UX for unified pTokens! <img src="https://we.incognito.org/images/emoji/apple/raised_hands.png?v=9" title=":raised_hands:" class="emoji" alt=":raised_hands:"></p>
<p>We are looking forward to having your instructive thoughts and feedback. Let’s share with us via comment below. Thank you. <img src="https://we.incognito.org/images/emoji/apple/heart.png?v=9" title=":heart:" class="emoji" alt=":heart:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537/1</link>
        <pubDate>Tue, 19 Apr 2022 09:15:48 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-16537-1</guid>
        <source url="https://we.incognito.org/t/unified-privacy-tokens-new-ui-ux-design/16537.rss">Unified privacy tokens - New UI/UX Design</source>
      </item>
  </channel>
</rss>
