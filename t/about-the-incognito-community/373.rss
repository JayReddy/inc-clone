<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>About the Incognito Community</title>
    <link>https://we.incognito.org/t/about-the-incognito-community/373</link>
    <description># Purpose

&gt; Incognito is a global community of privacy-minded builders seeking to protect the world from surveillance and control.

We build privacy-protecting, blockchain-based financial products for the future of money. All of our development is open-source. Progress, idea generation and decision making are all publicly viewable at we.incognito.org.

*See also:* [Roadmap](https://we.incognito.org/t/incognito-2022-technical-roadmap/15002), [Whitepaper](https://we.incognito.org/t/incognito-whitepaper-incognito-mode-for-cryptonetworks/168), [Network](https://explorer.incognito.org/), and [Code](https://github.com/incognitochain).

# Problem

Blockchains have introduced an entirely new asset class: cryptoassets. These cryptoassets use peer-to-peer technology to operate without central authority or banks. The management of transactions and the issuance of cryptoassets are carried out collectively by the users of these networks.

Bitcoin was the first cryptoasset; today there are over 1,600. People have started buying Bitcoin, instead of gold, as their long-term store of value. Stored under the mattresses of volatile economies, the world’s most desirable fiat currencies are being replaced by stablecoins, that can be sent and received with borderless freedom. Waves of startups now sell their native cryptoassets to investors, not equity.

For those who value privacy, cryptoassets come with a significant tradeoff. Transactions are recorded on public ledgers, displaying amounts involved, inscribing virtual identities of their senders and receivers. Given the choice, we firmly believe that very few people will willingly disclose their crypto financials to the entire world.

# Solution

Existing solutions like Monero, Zcash, Grin, and Beam introduced their own version of cryptocurrencies that focus on privacy.

Incognito takes a different approach, based on the premise that people don’t want a new cryptocurrency with privacy. What they really want is privacy for their existing cryptocurrencies: incognito mode for any cryptocurrency.

&gt; Privacy needs to be ubiquitous, inclusive, and accessible.

Incognito is designed so users don’t have to choose between their favorite cryptocurrencies and privacy coins. They can have both. They can hold any cryptocurrency and still be able to use it confidentially whenever they want.

# Why now

If this development trend continues, as we believe it will, cryptoassets are likely to play a crucial role in the near future. It is not far fetched to estimate that cryptoassets will increasingly compose an individual’s net worth or a company’s balance sheet.

Incognito hopes to give these new assets and their owners - both now and in the future - the option to claim their right to privacy.

![b6xpar9o8o111|750x520](upload://agUcpNjAf3ZKi7W1xPDtGvuwzKm.png) 
*Figure 1. The internet in 1994 looked nothing like what we have today. Blockchain’s 1994 is happening right now.*

# Our story

## 2018 - 2019: From a diverse founding team

The founding team consists of a diverse group of 40+ builders. Over the past 2 years, this team assembled itself to tackle a real and pressing danger: the lack of privacy.

Our story is a patchwork of many. Thuc, a professor of applied cryptography at the [National University of Vietnam,](https://en.wikipedia.org/wiki/Vietnam_National_University,_Ho_Chi_Minh_City) had been researching post-quantum cryptography, privacy, and randomness for the past 30 years before finally finding the perfect application for their research. Andrey, a physicist from Ukraine, founded [Vincoin Cash,](https://github.com/cryptofriend/vincoin) a privacy-protecting cryptocurrency, and decided to join forces with the team. Bao was the former head of engineering at [Viettel,](http://viettel.com.vn/en) the largest mobile network operator in Vietnam with 110M subscribers spread across Asia, Africa and the Americas, and grew frustrated with the monopolistic practices of a huge state-owned telco. Annie was the project lead at [Ninja,](https://www.producthunt.com/posts/ninja-400b71c5-3702-4069-8715-6a0569765ea1) a crypto prediction market, and experienced firsthand from her users the need for privacy.

In the first two years, the founding team self-funded the project, wrote over [1 million lines of code](https://github.com/incognitochain), and shipped a number of foundational building blocks for the community.

* A [universal privacy blockchain](https://explorer.incognito.org/) powered by 2,000 nodes and growing
* A friendly, [plug &amp; play crypto mining gadget](https://we.incognito.org/t/incognito-physical-node/338) for every home
* A [privacy-protecting crypto wallet](https://we.incognito.org/t/incognito-wallet/336) for any cryptocurrency
* A [privacy bridge to Ethereum](https://we.incognito.org/t/incognito-mode-for-ethereum/53)
* And a [privacy-protecting decentralized exchange](https://we.incognito.org/t/pdex-the-first-privacy-protecting-decentralized-exchange/66)

## 2020 - Onward: To a diverse community of builders

2020 marks a shift. From a founding team of builders to an entire community of builders; each working to secure our right to privacy in this new world. From a few foundational building blocks to many privacy-first applications.

&gt; When we say builders, we don’t mean just programmers and researchers.

Are you… a graphic designer, a UI/UX designer, a copywriter, a hardware engineer, a cryptographer, a distributed systems researcher, a business developer, an operator, a financier, a programmer, a community manager, or just someone who understands other people and wants to give feedback on the latest app release? Welcome to Incognito.

Have a product idea? Start a topic in the [Ideas](https://we.incognito.org/c/community/ideas) category. Want to solve challenging research problems? Immerse yourself in the [Research](https://we.incognito.org/c/tech) category. Want to build your own privacy project? Connect with your users and earn rewards in the [Building](https://we.incognito.org/c/core-dev/community-dev) category. Want to help work through the community to-do list? Check out the [Wanted](https://we.incognito.org/c/community/wanted) category, cross off a task and earn PRV.

# An autonomous community

## Cathedral vs. Bazaar

Inspired by the structures and principles pioneered by Linux and Wikipedia, Incognito is adopting the [Bazaar development model](https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar) and building we.incognito.org as a community-driven privacy playground.

&gt; Development is not restricted to just the founding team. Anyone can have a good idea, anyone can build it. Anyone can fund it, and everyone now owns a share of it.

In this open model, idea generation, fundraising, development, and decision making are public, dynamic, and ever-evolving.

## Incentive: PRV as a build token

PRV was originally designed as a [work token](https://multicoin.capital/2018/02/13/new-models-utility-tokens/), but can now be better understood as a build token. It makes sense that contributors to the network should own a piece of it, so PRV will be distributed to everyone who plays a part. These are the ways you can claim your share of Incognito:

* Block mining. Mine PRV by verifying transactions and [producing new blocks](https://we.incognito.org/t/network-incentive-privacy-prv/172).

* Product mining. Mine PRV by [building new products and services](https://we.incognito.org/t/community-builder-rewards-program/3057/) on top of Incognito or building features for Incognito.

* Bridge mining. Mine PRV by [securing privacy bridges](https://we.incognito.org/t/trustless-custodians-a-decentralized-approach-to-cryptocurrency-custodianship/84) to other blockchains like Ethereum and Bitcoin.

* Liquidity mining. Mine PRV by [providing liquidity](https://we.incognito.org/t/provide/4247/) to the pDEX.

* Community mining. Mine PRV by [contributing to the community](https://we.incognito.org/badges) on incognito.org with useful content and helpful feedback.

## What does an autonomous community look like?

We hope to grow the Incognito community to be an autonomous community.

![image%20(36)|750x455](upload://t3AMfhBEa0aqBZulVDQgiGSd8XM.png) 
*Figure 2. An autonomous community with three features: self-funding, self-incentivizing, and self-organizing.*
 
**Self-funding.** From a macro perspective, an autonomous community must ensure that it is sustainable both in the short and long term.

The founding team self-funded Incognito’s development for 2 years prior to the mainnet launch. A percentage of mainnet block rewards now goes to the [Incognito DAO](https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172) to fund the platform’s on-going development and operations. This autonomous funding model is designed to ensure the platform can independently sustain itself.

**Self-incentivizing.** From a micro perspective, an autonomous community must have an in-built incentive mechanism to facilitate long-lasting, large-scale collaboration among community members.

Incognito already provides [block mining](https://we.incognito.org/t/network-incentive-privacy-prv-mining-distribution/172). Additional mining mechanisms such as bridge mining, liquidity mining, and community mining are under development to better deploy Incognito DAO funds and further incentivize community contribution.

**Self-organizing.** An autonomous community must implement a bottom-up approach. Its members self-organize based on common goals and take necessary actions without requiring permission.

Here at Incognito, anyone can build their idea for a new product (for example, [node cluster](https://we.incognito.org/t/node-cluster-for-multi-node-owners/259)), a new feature (for example, [incognito mode for smart contracts and dapps](https://we.incognito.org/t/incognito-mode-for-smart-contracts-and-dapps/330)), a new service (for example, [staking pools](https://we.incognito.org/t/grow-prv-staking-pool-options/254)), or a new marketing activity (for example, [stickers for Bitcoin ATMs](https://we.incognito.org/t/sticker-design-for-bitcoin-atms/279)). Anyone who builds anything the community deems useful will be [rewarded by that same community](https://we.incognito.org/t/builder-rewards/3057).

# Welcome to Incognito

Incognito is not just a wallet, a privacy coin, a salesperson, a company, or even a blockchain. Incognito is a conversation held among a global community of privacy-minded builders. All we’ve done is lay the groundwork needed to support this conversation. Now, all we need is for you to drop in and [say hello](https://we.incognito.org/).</description>
    
    <lastBuildDate>Mon, 04 Apr 2022 00:05:46 +0000</lastBuildDate>
    <category>General</category>
    <atom:link href="https://we.incognito.org/t/about-the-incognito-community/373.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@khanhj]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/khanhj">@khanhj</a> wrote:</p>
          <blockquote>
              <p>Thanks for your kind words, and welcome to the community <a class="mention" href="/u/witzo1000">@witzo1000</a> <img src="https://we.incognito.org/images/emoji/apple/handshake.png?v=9" title=":handshake:" class="emoji" alt=":handshake:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/34">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/34</link>
        <pubDate>Mon, 04 Apr 2022 00:05:46 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-34</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@witzo1000 witzo1000]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/witzo1000">@witzo1000</a> wrote:</p>
          <blockquote>
              <p>I have been with you for 2 days, you are amazing <img src="/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/33">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/33</link>
        <pubDate>Fri, 01 Apr 2022 21:28:41 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-33</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@ESAQUIEL_NASCIMENTO ESAQUIEL NASCIMENTO]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/esaquiel_nascimento">@ESAQUIEL_NASCIMENTO</a> wrote:</p>
          <blockquote>
              <p>Legal já faço parte do incógnito</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/32">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/32</link>
        <pubDate>Sun, 27 Mar 2022 20:06:29 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-32</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@HJScanlan H.J. Scanlan]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/hjscanlan">@HJScanlan</a> wrote:</p>
          <blockquote>
              <p>The builders and community’s cooperation is great!!!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/31">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/31</link>
        <pubDate>Sat, 03 Jul 2021 01:30:11 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-31</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Estem]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/estem">@Estem</a> wrote:</p>
          <blockquote>
              <p>The Telegram chat used to be very friendly and respectful but it is true that it doesn’t feel like that anymore, I left the group and decided to just stay in the forum <img src="https://we.incognito.org/images/emoji/apple/cry.png?v=9" title=":cry:" class="emoji" alt=":cry:">.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/30">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/30</link>
        <pubDate>Fri, 19 Feb 2021 18:17:07 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-30</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@key]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/key">@key</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="Mike_Wagner" data-post="28" data-topic="373">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/mike_wagner/40/356_2.png" class="avatar"> Mike_Wagner:</div>
<blockquote>
<p>downgraded as unofficial “official” channels</p>
</blockquote>
</aside>
<p><img src="https://we.incognito.org/images/emoji/apple/face_with_raised_eyebrow.png?v=9" title=":face_with_raised_eyebrow:" class="emoji only-emoji" alt=":face_with_raised_eyebrow:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/29">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/29</link>
        <pubDate>Wed, 17 Feb 2021 02:40:08 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-29</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Mike_Wagner Mike Wagner]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/mike_wagner">@Mike_Wagner</a> wrote:</p>
          <blockquote>
              <p>The Telegram channels (<span class="mention">@incognitochain</span>, <span class="mention">@incognitonode</span>, <span class="mention">@incognitobuild</span>, <span class="mention">@incognitochannel</span>) are <strong>UN</strong>official chat channels.  The project moved community communications away from Telegram a year ago this month.</p>
<p>The Telegram channels have since been downgraded as unofficial “official” channels for new user discovery and community talk around.  Recently the Incognito team has been tapping community members to moderate the Telegram channels as they continue to step further back from Telegram.  Project leaders may be involved in Telegram discussion and moderation.  However official support, project discussion and team communication are encouraged to be taken up on this website.  It simply is not tenable to maintain consistent thread topics on Telegram; hence the move to a proper forum format.</p>
<p>On a related note – there is NO official Discord server.  And never has been.  Any Discord servers are run by community members.  The quality and veracity of the discussion on those servers will only be as good as the members involved.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/28">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/28</link>
        <pubDate>Tue, 16 Feb 2021 05:30:45 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-28</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@key]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/key">@key</a> wrote:</p>
          <blockquote>
              <p>There are admins in the Telegram group who are Absent from the list you shared. They are contributing to the controversial noise.</p>
<p>Thank you</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/27">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/27</link>
        <pubDate>Tue, 16 Feb 2021 05:16:33 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-27</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Mike_Wagner Mike Wagner]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/mike_wagner">@Mike_Wagner</a> wrote:</p>
          <blockquote>
              <p><a href="https://we.incognito.org/about">https://we.incognito.org/about</a></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/26">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/26</link>
        <pubDate>Tue, 16 Feb 2021 04:52:37 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-26</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@key]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/key">@key</a> wrote:</p>
          <blockquote>
              <p>Where can I find a roster of community-facing staff members? I would like to understand roles when anonymous community members speak authoritatively or take actions against Telegram users. Thank you.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/25">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/25</link>
        <pubDate>Tue, 16 Feb 2021 04:48:48 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-25</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@MAurelius4579 RCastillo]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/maurelius4579">@MAurelius4579</a> wrote:</p>
          <blockquote>
              <p>Thank you. Really good to be here…</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/24">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/24</link>
        <pubDate>Mon, 15 Feb 2021 04:57:49 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-24</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@MAurelius4579 RCastillo]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/maurelius4579">@MAurelius4579</a> wrote:</p>
          <blockquote>
              <p>My better half found incognito. I really can’t take the credit for that. Still, sounds like just what we need.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/23">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/23</link>
        <pubDate>Mon, 15 Feb 2021 04:55:57 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-23</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@MAurelius4579 RCastillo]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/maurelius4579">@MAurelius4579</a> wrote:</p>
          <blockquote>
              <p>Hello… <img src="https://we.incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/22">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/22</link>
        <pubDate>Mon, 15 Feb 2021 04:52:09 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-22</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Chucky]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/chucky">@Chucky</a> wrote:</p>
          <blockquote>
              <p>Welcome <a class="mention" href="/u/maurelius4579">@MAurelius4579</a> glad to have you! If i may ask, how did you find us?</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/21">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/21</link>
        <pubDate>Wed, 10 Feb 2021 14:52:10 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-21</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Tempestblack Vincent Valencia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tempestblack">@Tempestblack</a> wrote:</p>
          <blockquote>
              <p>Welcome to the community <a class="mention" href="/u/maurelius4579">@MAurelius4579</a>… <img src="https://we.incognito.org/images/emoji/apple/sunglasses.png?v=9" title=":sunglasses:" class="emoji" alt=":sunglasses:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/20">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/20</link>
        <pubDate>Wed, 10 Feb 2021 05:55:26 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-20</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Estem]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/estem">@Estem</a> wrote:</p>
          <blockquote>
              <p>Hello there!</p>
<p>Welcome to the Incognito community <img src="https://we.incognito.org/images/emoji/apple/grin.png?v=9" title=":grin:" class="emoji" alt=":grin:">.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/19">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/19</link>
        <pubDate>Wed, 10 Feb 2021 03:47:20 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-19</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@MAurelius4579 RCastillo]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/maurelius4579">@MAurelius4579</a> wrote:</p>
          <blockquote>
              <p>Hello, great to meet everyone</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/18">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/18</link>
        <pubDate>Wed, 10 Feb 2021 03:43:06 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-18</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Chucky]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/chucky">@Chucky</a> wrote:</p>
          <blockquote>
              <p>Welcome to incognito! We are glad to have you as well!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/16">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/16</link>
        <pubDate>Mon, 28 Dec 2020 15:11:10 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-16</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Thomas_Edible Thomas Edible]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/thomas_edible">@Thomas_Edible</a> wrote:</p>
          <blockquote>
              <p>Amazing!<br>
I’m so excited to find this project and join the community! I’m a long time privacy advocate and entrepreneur and I’m excited for the future of this!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/15">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/15</link>
        <pubDate>Sat, 26 Dec 2020 14:06:39 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-15</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@jtamign]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jtamign">@jtamign</a> wrote:</p>
          <blockquote>
              <aside class="quote no-group" data-username="ning" data-post="1" data-topic="373">
<div class="title">
<div class="quote-controls"></div>
<img alt width="20" height="20" src="https://we.incognito.org/user_avatar/we.incognito.org/ning/40/1778_2.png" class="avatar"> ning:</div>
<blockquote>
<p>Privacy needs to be ubiquitous, inclusive, and accessible.</p>
</blockquote>
</aside>
<p>I  align completely with this premise, and let’s build on it to brighten our world.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/14">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/14</link>
        <pubDate>Sat, 19 Dec 2020 06:45:22 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-14</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Chucky]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/chucky">@Chucky</a> wrote:</p>
          <blockquote>
              <p>Welcome to incognito! Glad to have you <img src="https://we.incognito.org/images/emoji/apple/slight_smile.png?v=9" title=":slight_smile:" class="emoji" alt=":slight_smile:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/13">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/13</link>
        <pubDate>Mon, 30 Nov 2020 14:38:35 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-13</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Tempestblack Vincent Valencia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tempestblack">@Tempestblack</a> wrote:</p>
          <blockquote>
              <p>I know…I know…I should have read up this particular string/topic long ago…but I hadn’t…<img src="https://we.incognito.org/images/emoji/apple/blush.png?v=9" title=":blush:" class="emoji" alt=":blush:">…my bad…but I am delighted to have done so now and getting to know about the founders a bit more…impressive credentials they all have…and once again I am delighted to be part of this community and seeing its growth… <img src="https://we.incognito.org/images/emoji/apple/100.png?v=9" title=":100:" class="emoji" alt=":100:"> <img src="https://we.incognito.org/images/emoji/apple/partying_face.png?v=9" title=":partying_face:" class="emoji" alt=":partying_face:">…Incognito!!!..woohoo!!.. <img src="https://we.incognito.org/images/emoji/apple/sunglasses.png?v=9" title=":sunglasses:" class="emoji" alt=":sunglasses:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/12">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/12</link>
        <pubDate>Sun, 29 Nov 2020 01:36:14 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-12</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Nils.Srvd Nils Srvd]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/nils.srvd">@Nils.Srvd</a> wrote:</p>
          <blockquote>
              <p>Incredible to be here! This looks amazing, it is exactly what I was looking for.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/11">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/11</link>
        <pubDate>Sat, 28 Nov 2020 08:25:22 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-11</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Flow Flow]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/flow">@Flow</a> wrote:</p>
          <blockquote>
              <p>Wow. Well done <img src="https://we.incognito.org/images/emoji/apple/heart_eyes.png?v=9" title=":heart_eyes:" class="emoji" alt=":heart_eyes:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/10">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/10</link>
        <pubDate>Thu, 26 Nov 2020 20:49:39 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-10</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
      <item>
        <title>About the Incognito Community</title>
        <dc:creator><![CDATA[@Marina Marina  Shim]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/marina">@Marina</a> wrote:</p>
          <blockquote>
              <p><a class="mention" href="/u/raz">@raz</a> <a class="mention" href="/u/mira2020">@mira2020</a> <a class="mention" href="/u/ri_royal">@RI_Royal</a> <a class="mention" href="/u/umut_sevinc">@Umut_Sevinc</a> happy to have you here!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/about-the-incognito-community/373/9">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/about-the-incognito-community/373/9</link>
        <pubDate>Sat, 21 Nov 2020 11:20:32 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-373-9</guid>
        <source url="https://we.incognito.org/t/about-the-incognito-community/373.rss">About the Incognito Community</source>
      </item>
  </channel>
</rss>
