<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Incognito Wallet App&#39;s tracking and permissions</title>
    <link>https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915</link>
    <description>Recently, a user ran a check for our app on [εxodus](https://reports.exodus-privacy.eu.org/en/reports/com.incognito.wallet/latest/) to to see how private the Incognito Wallet app is.

To some, it was quite surprising to see that the app requires 19 permissions and has 3 trackers. At face value, that seems anti-privacy.

To that user, thank you. We now realize we need to be more clear on what we do and why, so to that end I am going to break down what information we gather and why.

## 1. Permissions:

Below are all of the permissions the Incognito Wallet app requires of your phone and what they do. We do not require any permissions to be able to control your device in ways that you aren’t specifically trying to initiate. Without these permissions, the app cannot function properly.

### **A: Node set up**

**ACCESS_COARSE_LOCATION -** This lets the app access your approximate location for WiFi connectivity. You can learn more about WiFi scanning [here](https://developer.android.com/guide/topics/connectivity/wifi-scan).

**ACCESS_NETWORK_STATE -** This lets the app view Incognito network connections, so it knows if you are connected or not, That way, if you’re not, it can give error messages and suggest the solution.

**CHANGE_NETWORK_STATE -** This lets the app change your Incognito network connection, which is essential for app features like transactions.

**CHANGE_WIFI_STATE -** This allows the app to connect and disconnect from the WiFi network your phone is connected to. This is necessary to use WiFi-based functions.

**ACCESS_WIFI_STATE -** With this permission, the app can view your WiFi connectivity, to know whether you can use the functions in the app that require WiFi.

**CHANGE_WIFI_MULTICAST_STATE -** Similar to network state, this allows the app to enable WiFi Multicast reception, so your device can send data to nodes across the network, which is necessary for app features like transactions.

**CAMERA -** This lets the app use the camera on your phone to scan QR codes, or to be unlocked with your FaceID (if you have that enabled). We can’t control it remotely, and if you’d like, you can disable this permission in your phone’s app permission settings. If you do, you just won’t be able to scan QR wallet addresses, but you can still manually input them of copy/paste them.

**INTERNET -** This allows the app to have access the worldwide web from within the app, which is how you can access the “Community” tab. We cannot use your other web browser apps or check your history

Overall, these permissions are required because the Node setup process is complicated. It needs to check your internet information, scan the QR code of the device, network status, and help to connect to Node&#39;s hotspot.

### **B: Security access**

**USE_FINGERPRINT -** This allows the app to use your phone’s fingerprint software to enter your passcode that way, if you’ve enabled it. You can disable it at any time.

### **C: Private key access**

**READ_EXTERNAL_STORAGE -** This enables the app to read your SD card, not for personal content, but for storage capability. Do you have room for the app’s data, and space to backup your private keys?

**WRITE_EXTERNAL_STORAGE -** These two permissions go together. This allows the app to create/delete a file in your storage to back up your private keys.

### **D: Miscellaneous**

**BLUETOOTH -** The app can pair with Bluetooth devices, helps for Node connectivity.

**BLUETOOTH_ADMIN -** The app can initiate a Bluetooth connection.

These are for connecting to your Node via Bluetooth. However, we’ve made it mostly unnecessary. We’re going to remove this permission as soon as we’re sure it won’t disrupt users who have connected their device this way.

**READ_PHONE_STATE -** This is to verify that you’re a real person using a real phone. It’s built into backend and not something we can use for any proactive purpose. 

**RECEIVE_BOOT_COMPLETED -** This allows the app to send you notifications on your phone once you turn it on.

**WAKE_LOCK -** This prevents your phone from entering sleep mode while a transaction is ongoing, so that it does not make the transaction fail. Eventually we can move this process to the background so you can navigate away during a transaction and this permission won’t be needed.

**RECEIVE -** This is not something we initiated. It’s a default for apps on the app stores, for communication between the host and the app for things like app updates. 


**BIND_GET_INSTALL_REFERRER_SERVICE -** This is for AppsFlyer. It gathers the general info I described earlier.

**VIBRATE -** This is an old feature we are removing soon. It allows the app to vibrate your phone with notifications.

So, some permissions are necessary for the app to function, others make the app experience more pleasant and easy-to-use. We’re constantly looking for ways to lessen the permissions we use without degrading the app. As with any product, we keep track of certain metrics described above in order to know what type of people use Incognito, and how, so we can help the community grow effectively. 

## 2. Trackers

* AppsFlyer

* Google CrashLytics

* Google Firebase Analytics

These tools are common for developers. We use them to see the different ways people can download our app (whether they clicked the link from Facebook, Twitter, the Apple App Store, Google Play, etc), and what device it&#39;s on. We also use it to understand the way the app is being used, how long people generally spend inside the app so we can optimize UX for users over time. We also collect data on when and why the app encounters a problem, like crashing.

**This information is not tied to your personal identity.** We use it to know where to focus our growth activities and how to make the app better. I would not be able to pick you out of that data to know any more about you than, say, one of your Twitter followers.

Also, we have decided to stop using Google Firebase Analytics because it’s become unnecessary.

Privacy is our main priority and we will never track you specifically or access your phone, identity, or data in a way that you didn’t specifically intend to initiate as an in-app function.

**If you have any questions, comments, or concerns, please share them here.** If there’s a way we can make privacy better, we’re all in. Thanks!</description>
    
    <lastBuildDate>Fri, 05 Mar 2021 18:39:20 +0000</lastBuildDate>
    <category>App users</category>
    <atom:link href="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915.rss" rel="self" type="application/rss+xml" />
      <item>
        <title>Incognito Wallet App&#39;s tracking and permissions</title>
        <dc:creator><![CDATA[@Tempestblack Vincent Valencia]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/tempestblack">@Tempestblack</a> wrote:</p>
          <blockquote>
              <p>Excellent post <a class="mention" href="/u/peter">@Peter</a>…I was not aware it existed so I guess I am a lil late to reading it…lol…but indeed a very good post and the information is much appreciated… <img src="https://we.incognito.org/images/emoji/apple/sunglasses.png?v=9" title=":sunglasses:" class="emoji" alt=":sunglasses:"></p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/7">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/7</link>
        <pubDate>Fri, 05 Mar 2021 18:39:20 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1915-7</guid>
        <source url="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915.rss">Incognito Wallet App&#39;s tracking and permissions</source>
      </item>
      <item>
        <title>Incognito Wallet App&#39;s tracking and permissions</title>
        <dc:creator><![CDATA[@Jamie Jamie]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/jamie">@Jamie</a> wrote:</p>
          <blockquote>
              <p>Since Peter is a team member and in Vietnam where the devs are, my guess would be the information was provided to him by the devs when he asked them about it. There is no need for him to browse the code himself.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/6">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/6</link>
        <pubDate>Wed, 10 Feb 2021 00:58:49 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1915-6</guid>
        <source url="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915.rss">Incognito Wallet App&#39;s tracking and permissions</source>
      </item>
      <item>
        <title>Incognito Wallet App&#39;s tracking and permissions</title>
        <dc:creator><![CDATA[@RobynFitzooth Robyn Fitzooth]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/robynfitzooth">@RobynFitzooth</a> wrote:</p>
          <blockquote>
              <p>Interesting topic.  I appreciate that there is some technical detail here!  How can it be that the app does not track your IP?  How can you know this for sure?  Have you reviewed all of the code?  How about the latest updates, have you reviewed these?  Seems like a tough task to review all the updates that get</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/5">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/5</link>
        <pubDate>Tue, 09 Feb 2021 21:34:32 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1915-5</guid>
        <source url="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915.rss">Incognito Wallet App&#39;s tracking and permissions</source>
      </item>
      <item>
        <title>Incognito Wallet App&#39;s tracking and permissions</title>
        <dc:creator><![CDATA[@Peter Peter]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/peter">@Peter</a> wrote:</p>
          <blockquote>
              <p>Hi <a class="mention" href="/u/z32552f">@Z32552F</a>, some concerns can be explained in the topic above. The app doesn’t track your IP/ geographic location at all.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/4">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/4</link>
        <pubDate>Thu, 19 Nov 2020 08:48:56 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1915-4</guid>
        <source url="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915.rss">Incognito Wallet App&#39;s tracking and permissions</source>
      </item>
      <item>
        <title>Incognito Wallet App&#39;s tracking and permissions</title>
        <dc:creator><![CDATA[@Z32552F]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/z32552f">@Z32552F</a> wrote:</p>
          <blockquote>
              <p>How private and anonymous is using Incognito via mobile app?<br>
What does the mobile app know about me?<br>
What information does the mobile app have access to on my mobile device?<br>
Does the mobile app track/log my IP address?<br>
Does the mobile app track/log my geographic location?</p>
<p>Thank you.</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/3">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/3</link>
        <pubDate>Wed, 04 Nov 2020 17:56:53 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1915-3</guid>
        <source url="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915.rss">Incognito Wallet App&#39;s tracking and permissions</source>
      </item>
      <item>
        <title>Incognito Wallet App&#39;s tracking and permissions</title>
        <dc:creator><![CDATA[@Peter Peter]]></dc:creator>
        <description><![CDATA[
          <p><a href="https://we.incognito.org/u/peter">@Peter</a> wrote:</p>
          <blockquote>
              <p>Recently, a user ran a check for our app on <a href="https://reports.exodus-privacy.eu.org/en/reports/com.incognito.wallet/latest/">εxodus</a> to to see how private the Incognito Wallet app is.</p>
<p>To some, it was quite surprising to see that the app requires 19 permissions and has 3 trackers. At face value, that seems anti-privacy.</p>
<p>To that user, thank you. We now realize we need to be more clear on what we do and why, so to that end I am going to break down what information we gather and why.</p>
<h2>1. Permissions:</h2>
<p>Below are all of the permissions the Incognito Wallet app requires of your phone and what they do. We do not require any permissions to be able to control your device in ways that you aren’t specifically trying to initiate. Without these permissions, the app cannot function properly.</p>
<h3><strong>A: Node set up</strong></h3>
<p><strong>ACCESS_COARSE_LOCATION -</strong> This lets the app access your approximate location for WiFi connectivity. You can learn more about WiFi scanning <a href="https://developer.android.com/guide/topics/connectivity/wifi-scan">here</a>.</p>
<p><strong>ACCESS_NETWORK_STATE -</strong> This lets the app view Incognito network connections, so it knows if you are connected or not, That way, if you’re not, it can give error messages and suggest the solution.</p>
<p><strong>CHANGE_NETWORK_STATE -</strong> This lets the app change your Incognito network connection, which is essential for app features like transactions.</p>
<p><strong>CHANGE_WIFI_STATE -</strong> This allows the app to connect and disconnect from the WiFi network your phone is connected to. This is necessary to use WiFi-based functions.</p>
<p><strong>ACCESS_WIFI_STATE -</strong> With this permission, the app can view your WiFi connectivity, to know whether you can use the functions in the app that require WiFi.</p>
<p><strong>CHANGE_WIFI_MULTICAST_STATE -</strong> Similar to network state, this allows the app to enable WiFi Multicast reception, so your device can send data to nodes across the network, which is necessary for app features like transactions.</p>
<p><strong>CAMERA -</strong> This lets the app use the camera on your phone to scan QR codes, or to be unlocked with your FaceID (if you have that enabled). We can’t control it remotely, and if you’d like, you can disable this permission in your phone’s app permission settings. If you do, you just won’t be able to scan QR wallet addresses, but you can still manually input them of copy/paste them.</p>
<p><strong>INTERNET -</strong> This allows the app to have access the worldwide web from within the app, which is how you can access the “Community” tab. We cannot use your other web browser apps or check your history</p>
<p>Overall, these permissions are required because the Node setup process is complicated. It needs to check your internet information, scan the QR code of the device, network status, and help to connect to Node’s hotspot.</p>
<h3><strong>B: Security access</strong></h3>
<p><strong>USE_FINGERPRINT -</strong> This allows the app to use your phone’s fingerprint software to enter your passcode that way, if you’ve enabled it. You can disable it at any time.</p>
<h3><strong>C: Private key access</strong></h3>
<p><strong>READ_EXTERNAL_STORAGE -</strong> This enables the app to read your SD card, not for personal content, but for storage capability. Do you have room for the app’s data, and space to backup your private keys?</p>
<p><strong>WRITE_EXTERNAL_STORAGE -</strong> These two permissions go together. This allows the app to create/delete a file in your storage to back up your private keys.</p>
<h3><strong>D: Miscellaneous</strong></h3>
<p><strong>BLUETOOTH -</strong> The app can pair with Bluetooth devices, helps for Node connectivity.</p>
<p><strong>BLUETOOTH_ADMIN -</strong> The app can initiate a Bluetooth connection.</p>
<p>These are for connecting to your Node via Bluetooth. However, we’ve made it mostly unnecessary. We’re going to remove this permission as soon as we’re sure it won’t disrupt users who have connected their device this way.</p>
<p><strong>READ_PHONE_STATE -</strong> This is to verify that you’re a real person using a real phone. It’s built into backend and not something we can use for any proactive purpose.</p>
<p><strong>RECEIVE_BOOT_COMPLETED -</strong> This allows the app to send you notifications on your phone once you turn it on.</p>
<p><strong>WAKE_LOCK -</strong> This prevents your phone from entering sleep mode while a transaction is ongoing, so that it does not make the transaction fail. Eventually we can move this process to the background so you can navigate away during a transaction and this permission won’t be needed.</p>
<p><strong>RECEIVE -</strong> This is not something we initiated. It’s a default for apps on the app stores, for communication between the host and the app for things like app updates.</p>
<p><strong>BIND_GET_INSTALL_REFERRER_SERVICE -</strong> This is for AppsFlyer. It gathers the general info I described earlier.</p>
<p><strong>VIBRATE -</strong> This is an old feature we are removing soon. It allows the app to vibrate your phone with notifications.</p>
<p>So, some permissions are necessary for the app to function, others make the app experience more pleasant and easy-to-use. We’re constantly looking for ways to lessen the permissions we use without degrading the app. As with any product, we keep track of certain metrics described above in order to know what type of people use Incognito, and how, so we can help the community grow effectively.</p>
<h2>2. Trackers</h2>
<ul>
<li>
<p>AppsFlyer</p>
</li>
<li>
<p>Google CrashLytics</p>
</li>
<li>
<p>Google Firebase Analytics</p>
</li>
</ul>
<p>These tools are common for developers. We use them to see the different ways people can download our app (whether they clicked the link from Facebook, Twitter, the Apple App Store, Google Play, etc), and what device it’s on. We also use it to understand the way the app is being used, how long people generally spend inside the app so we can optimize UX for users over time. We also collect data on when and why the app encounters a problem, like crashing.</p>
<p><strong>This information is not tied to your personal identity.</strong> We use it to know where to focus our growth activities and how to make the app better. I would not be able to pick you out of that data to know any more about you than, say, one of your Twitter followers.</p>
<p>Also, we have decided to stop using Google Firebase Analytics because it’s become unnecessary.</p>
<p>Privacy is our main priority and we will never track you specifically or access your phone, identity, or data in a way that you didn’t specifically intend to initiate as an in-app function.</p>
<p><strong>If you have any questions, comments, or concerns, please share them here.</strong> If there’s a way we can make privacy better, we’re all in. Thanks!</p>
          </blockquote>
          <p><a href="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/1">Read full topic</a></p>
        ]]></description>
        <link>https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915/1</link>
        <pubDate>Tue, 28 Apr 2020 07:33:29 +0000</pubDate>
        <guid isPermaLink="false">we.incognito.org-post-1915-1</guid>
        <source url="https://we.incognito.org/t/incognito-wallet-apps-tracking-and-permissions/1915.rss">Incognito Wallet App&#39;s tracking and permissions</source>
      </item>
  </channel>
</rss>
